USE [dbDogwood]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblAttributeMetadata_tblDataType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata] DROP CONSTRAINT [FK_tblAttributeMetadata_tblDataType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblClassMetadataString_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata] DROP CONSTRAINT [FK_tblClassMetadataString_tblClass]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblClassMetadataString_tblStringAttribute]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata] DROP CONSTRAINT [FK_tblClassMetadataString_tblStringAttribute]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblStringAttributeValue_tblClassMetadataString]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue] DROP CONSTRAINT [FK_tblStringAttributeValue_tblClassMetadataString]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblStringAttributeValues_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue] DROP CONSTRAINT [FK_tblStringAttributeValues_tblObjectRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] DROP CONSTRAINT [FK_tblBranchHeadObject_tblClass]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] DROP CONSTRAINT [FK_tblBranchHeadObject_tblObject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] DROP CONSTRAINT [FK_tblBranchHeadObject_tblObjectRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] DROP CONSTRAINT [FK_tblBranchHeadObject_tblRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblRevision1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] DROP CONSTRAINT [FK_tblBranchHeadObject_tblRevision1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObject]'))
ALTER TABLE [dbo].[tblObject] DROP CONSTRAINT [FK_tblObject_tblClass]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblData_tblEntry]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObjectRevision]'))
ALTER TABLE [dbo].[tblObjectRevision] DROP CONSTRAINT [FK_tblData_tblEntry]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblData_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObjectRevision]'))
ALTER TABLE [dbo].[tblObjectRevision] DROP CONSTRAINT [FK_tblData_tblRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] DROP CONSTRAINT [FK_tblRelationshipAttributeValue_tblObject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] DROP CONSTRAINT [FK_tblRelationshipAttributeValue_tblObjectRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblRelationshipMetadata]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] DROP CONSTRAINT [FK_tblRelationshipAttributeValue_tblRelationshipMetadata]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FKClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] DROP CONSTRAINT [FK_FKClass]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PKClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] DROP CONSTRAINT [FK_PKClass]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipMetadata_tblForeignKeyAttribute]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] DROP CONSTRAINT [FK_tblRelationshipMetadata_tblForeignKeyAttribute]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipMetadata_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] DROP CONSTRAINT [FK_tblRelationshipMetadata_tblObject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevision_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevision]'))
ALTER TABLE [dbo].[tblRevision] DROP CONSTRAINT [FK_tblRevision_tblRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] DROP CONSTRAINT [FK_tblRevisionHeadObject_tblClass]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] DROP CONSTRAINT [FK_tblRevisionHeadObject_tblObject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] DROP CONSTRAINT [FK_tblRevisionHeadObject_tblObjectRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] DROP CONSTRAINT [FK_tblRevisionHeadObject_tblRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblRevision1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] DROP CONSTRAINT [FK_tblRevisionHeadObject_tblRevision1]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblAttributeValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblAttributeValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue] DROP CONSTRAINT [CK_tblAttributeValue]

END
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblRelationshipAttributeValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblRelationshipAttributeValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] DROP CONSTRAINT [CK_tblRelationshipAttributeValue]

END
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblRelationshipAttributeValue_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
BEGIN
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblRelationshipAttributeValue_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] DROP CONSTRAINT [CK_tblRelationshipAttributeValue_1]

END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTurnBackClock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spTurnBackClock]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllRelatedToObject]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetAllRelatedToObject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] DROP CONSTRAINT [FK_tblRelationshipAttributeValue_tblObject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] DROP CONSTRAINT [FK_tblRelationshipAttributeValue_tblObjectRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblRelationshipMetadata]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] DROP CONSTRAINT [FK_tblRelationshipAttributeValue_tblRelationshipMetadata]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblRelationshipAttributeValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] DROP CONSTRAINT [CK_tblRelationshipAttributeValue]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblRelationshipAttributeValue_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] DROP CONSTRAINT [CK_tblRelationshipAttributeValue_1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]') AND type in (N'U'))
DROP TABLE [dbo].[tblRelationAttributeValue]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwIndexedRevisions]'))
DROP VIEW [dbo].[vwIndexedRevisions]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddRevisionToHeadObjects]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAddRevisionToHeadObjects]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFillHeadObjects]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFillHeadObjects]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblStringAttributeValue_tblClassMetadataString]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue] DROP CONSTRAINT [FK_tblStringAttributeValue_tblClassMetadataString]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblStringAttributeValues_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue] DROP CONSTRAINT [FK_tblStringAttributeValues_tblObjectRevision]
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblAttributeValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue] DROP CONSTRAINT [CK_tblAttributeValue]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]') AND type in (N'U'))
DROP TABLE [dbo].[tblAttributeValue]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] DROP CONSTRAINT [FK_tblBranchHeadObject_tblClass]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] DROP CONSTRAINT [FK_tblBranchHeadObject_tblObject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] DROP CONSTRAINT [FK_tblBranchHeadObject_tblObjectRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] DROP CONSTRAINT [FK_tblBranchHeadObject_tblRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblRevision1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] DROP CONSTRAINT [FK_tblBranchHeadObject_tblRevision1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]') AND type in (N'U'))
DROP TABLE [dbo].[tblBranchHeadObject]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnFkAttributeIsValid]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnFkAttributeIsValid]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnPkAttributeIsValid]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnPkAttributeIsValid]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnStringAttributeIsValid]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnStringAttributeIsValid]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwRevisionTimeline]'))
DROP VIEW [dbo].[vwRevisionTimeline]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetObjectHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetObjectHistory]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] DROP CONSTRAINT [FK_tblRevisionHeadObject_tblClass]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] DROP CONSTRAINT [FK_tblRevisionHeadObject_tblObject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] DROP CONSTRAINT [FK_tblRevisionHeadObject_tblObjectRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] DROP CONSTRAINT [FK_tblRevisionHeadObject_tblRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblRevision1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] DROP CONSTRAINT [FK_tblRevisionHeadObject_tblRevision1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]') AND type in (N'U'))
DROP TABLE [dbo].[tblRevisionHeadObject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblData_tblEntry]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObjectRevision]'))
ALTER TABLE [dbo].[tblObjectRevision] DROP CONSTRAINT [FK_tblData_tblEntry]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblData_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObjectRevision]'))
ALTER TABLE [dbo].[tblObjectRevision] DROP CONSTRAINT [FK_tblData_tblRevision]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblObjectRevision]') AND type in (N'U'))
DROP TABLE [dbo].[tblObjectRevision]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FKClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] DROP CONSTRAINT [FK_FKClass]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PKClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] DROP CONSTRAINT [FK_PKClass]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipMetadata_tblForeignKeyAttribute]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] DROP CONSTRAINT [FK_tblRelationshipMetadata_tblForeignKeyAttribute]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipMetadata_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] DROP CONSTRAINT [FK_tblRelationshipMetadata_tblObject]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tblRelationshipMetadata_IsImmutable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRelationshipMetadata] DROP CONSTRAINT [DF_tblRelationshipMetadata_IsImmutable]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]') AND type in (N'U'))
DROP TABLE [dbo].[tblRelationshipMetadata]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwBranches]'))
DROP VIEW [dbo].[vwBranches]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblAttributeMetadata_tblDataType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata] DROP CONSTRAINT [FK_tblAttributeMetadata_tblDataType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblClassMetadataString_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata] DROP CONSTRAINT [FK_tblClassMetadataString_tblClass]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblClassMetadataString_tblStringAttribute]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata] DROP CONSTRAINT [FK_tblClassMetadataString_tblStringAttribute]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tblClassMetadataString_IsImmutable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttributeMetadata] DROP CONSTRAINT [DF_tblClassMetadataString_IsImmutable]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]') AND type in (N'U'))
DROP TABLE [dbo].[tblAttributeMetadata]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[branch_table]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[branch_table]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObject]'))
ALTER TABLE [dbo].[tblObject] DROP CONSTRAINT [FK_tblObject_tblClass]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblObject]') AND type in (N'U'))
DROP TABLE [dbo].[tblObject]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblClass]') AND type in (N'U'))
DROP TABLE [dbo].[tblClass]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDataType]') AND type in (N'U'))
DROP TABLE [dbo].[tblDataType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnListToTableInt]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnListToTableInt]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAttribute]') AND type in (N'U'))
DROP TABLE [dbo].[tblAttribute]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevision_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevision]'))
ALTER TABLE [dbo].[tblRevision] DROP CONSTRAINT [FK_tblRevision_tblRevision]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tblRevision_DateCreated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRevision] DROP CONSTRAINT [DF_tblRevision_DateCreated]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRevision]') AND type in (N'U'))
DROP TABLE [dbo].[tblRevision]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRevision]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRevision](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NOT NULL,
	[Tag] [varchar](50) NULL,
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_tblRevision_DateCreated]  DEFAULT (getdate()),
	[CreatedBy] [int] NULL,
	[Comments] [varchar](255) NULL,
 CONSTRAINT [PK_tblRevision] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAttribute]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAttribute](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Value] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblStringAttribute] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnListToTableInt]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fnListToTableInt](@list as varchar(8000))
RETURNS @listTable table(
  Value INT
  )
AS
BEGIN
    --Declare helper to identify the position of the delim
    DECLARE @DelimPosition INT
    
    --Prime the loop, with an initial check for the delim
    SET @DelimPosition = CHARINDEX('','', @list)

    --Loop through, until we no longer find the delimiter
    WHILE @DelimPosition > 0
    BEGIN
        --Add the item to the table
        INSERT INTO @listTable(Value)
            VALUES(CAST(RTRIM(LEFT(@list, @DelimPosition - 1)) AS INT))
    
        --Remove the entry from the List
        SET @list = right(@list, len(@list) - @DelimPosition)

        --Perform position comparison
        SET @DelimPosition = CHARINDEX('','', @list)
    END

    --If we still have an entry, add it to the list
    IF len(@list) > 0
        insert into @listTable(Value)
        values(CAST(RTRIM(@list) AS INT))

  RETURN
END

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDataType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDataType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Value] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblDataType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblClass]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblClass](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblClass] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_tblClass] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblObject]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblObject](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClassId] [int] NOT NULL,
 CONSTRAINT [PK_tblEntry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[branch_table]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[branch_table]
(
	@branchId int 
)
RETURNS 
@branch_range TABLE 
(
	branchid int,
	lo int,
	hi int
)
AS
BEGIN

	
	DECLARE @reshi int
	SET @reshi = 2147483647
	
	DECLARE @reslo int
	SET @reslo = @branchId

	INSERT INTO @branch_range VALUES (@branchId, @reslo, @reshi)

	WHILE @branchId > 0
	
	BEGIN	
		
		SELECT @branchId = (SELECT BranchId from tblRevision where Id = @branchid)
		SET @reshi = @reslo
		SET @reslo = @branchId
		
		INSERT INTO @branch_range VALUES (@branchId, @reslo, @reshi)
	END

	RETURN 
END

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAttributeMetadata](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClassId] [int] NOT NULL,
	[DataTypeId] [int] NOT NULL,
	[AttributeId] [int] NOT NULL,
	[IsNullable] [bit] NOT NULL,
	[IsImmutable] [bit] NOT NULL CONSTRAINT [DF_tblClassMetadataString_IsImmutable]  DEFAULT ((0)),
 CONSTRAINT [PK_tblClassMetadataString] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_tblClassMetadataString] UNIQUE NONCLUSTERED 
(
	[ClassId] ASC,
	[AttributeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwBranches]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[vwBranches]
AS
SELECT     TOP (100) PERCENT BranchId
FROM         dbo.tblRevision
GROUP BY BranchId
ORDER BY BranchId

'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRelationshipMetadata](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PkClassId] [int] NOT NULL,
	[FkClassId] [int] NOT NULL,
	[AttributeId] [int] NOT NULL,
	[IsNullable] [bit] NOT NULL,
	[OneToManyValue] [varchar](50) NULL,
	[CascadeBehavior] [int] NOT NULL,
	[CascadeDefaultValue] [int] NULL,
	[IsImmutable] [bit] NOT NULL CONSTRAINT [DF_tblRelationshipMetadata_IsImmutable]  DEFAULT ((0)),
 CONSTRAINT [PK_tblRelationshipMetadata] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_tblRelationshipMetadata] UNIQUE NONCLUSTERED 
(
	[AttributeId] ASC,
	[PkClassId] ASC,
	[FkClassId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblObjectRevision]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblObjectRevision](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ObjectId] [int] NOT NULL,
	[RevisionId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_tblData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Object_Revision] UNIQUE NONCLUSTERED 
(
	[ObjectId] ASC,
	[RevisionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRevisionHeadObject](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[ObjectId] [int] NOT NULL,
	[ObjectRevisionId] [int] NOT NULL,
	[RevisionId] [int] NOT NULL,
 CONSTRAINT [PK_tblRevisionHeadObject] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_tblRevisionHeadObject] UNIQUE NONCLUSTERED 
(
	[RevisionId] ASC,
	[ObjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetObjectHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spGetObjectHistory]
	@objectId int,
	@branchId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT     tblObjectRevision_1.Id, tblObjectRevision_1.ObjectId
	FROM    (SELECT     branchid, lo, hi
			FROM          dbo.branch_table(@branchId) AS b) AS branch_table_1 INNER JOIN
	   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
	   branch_table_1.hi >= tblRevision.Id INNER JOIN
	   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId INNER JOIN
	   tblObject AS tblObject_1 ON tblObjectRevision_1.ObjectId = tblObject_1.Id
	where tblObjectRevision_1.ObjectId = @objectId
END
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwRevisionTimeline]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[vwRevisionTimeline]
AS
SELECT     TOP (100) PERCENT dbo.tblRevision.Id, COUNT(dbo.tblObjectRevision.Id) AS CountOfObjectsRevised, dbo.tblRevision.TimeStamp, dbo.tblRevision.CreatedBy
FROM         dbo.tblRevision INNER JOIN
                      dbo.tblObjectRevision ON dbo.tblRevision.Id = dbo.tblObjectRevision.RevisionId
GROUP BY dbo.tblRevision.Id, dbo.tblRevision.TimeStamp, dbo.tblRevision.CreatedBy
ORDER BY dbo.tblRevision.TimeStamp

'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnStringAttributeIsValid]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fnStringAttributeIsValid] 
(
	-- Add the parameters for the function here
	@objRevId int,
	@metadataId int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @class1 int
	DECLARE @class2 int
	DECLARE @retVal int

	-- Add the T-SQL statements to compute the return value here
	SELECT   @class1=  tblObject.ClassId
	FROM         tblObjectRevision INNER JOIN
                      tblObject ON tblObjectRevision.ObjectId = tblObject.Id
	WHERE     (tblObjectRevision.Id = @objRevId) 

	SELECT  @class2=   ClassId
	FROM         tblAttributeMetadata
	WHERE     (Id = @metadataId)

	-- Return the result of the function
	if (@class1 = @class2)
		begin
			set @retVal = 1
		end
	else
		begin 
			set @retVal = 0
		end

	return @retVal
END

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnPkAttributeIsValid]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fnPkAttributeIsValid] 
(
	-- Add the parameters for the function here
	@value int,
	@metadataId int
)
RETURNS int
AS
BEGIN

	DECLARE @class1 int
	DECLARE @class2 int
	DECLARE @retVal int

	-- Add the T-SQL statements to compute the return value here
SELECT   @class1 =  PkClassId
FROM         tblRelationshipMetadata
WHERE     (Id = @metadataId)

	SELECT   @class2 =  tblObject.ClassId
FROM         tblRelationshipAttributeValue INNER JOIN
                      tblObject ON tblRelationshipAttributeValue.Value = tblObject.Id
WHERE     (tblRelationshipAttributeValue.Value = @value)

	-- Return the result of the function
	if (@class1 = @class2)
		begin
			set @retVal = 1
		end
	else
		begin 
			set @retVal = 0
		end

	return @retVal
END

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnFkAttributeIsValid]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fnFkAttributeIsValid] 
(
	-- Add the parameters for the function here
	@objRevId int,
	@metadataId int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @class1 int
	DECLARE @class2 int
	DECLARE @retVal int

	-- Add the T-SQL statements to compute the return value here
	SELECT   @class1=  tblObject.ClassId
	FROM         tblObjectRevision INNER JOIN
                      tblObject ON tblObjectRevision.ObjectId = tblObject.Id
	WHERE     (tblObjectRevision.Id = @objRevId) 

	SELECT  @class2=  FkClassId
	FROM         tblRelationshipMetadata
	WHERE     (Id = @metadataId)

	-- Return the result of the function
	if (@class1 = @class2)
		begin
			set @retVal = 1
		end
	else
		begin 
			set @retVal = 0
		end

	return @retVal
END

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblBranchHeadObject](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[ObjectId] [int] NOT NULL,
	[ObjectRevisionId] [int] NOT NULL,
	[RevisionId] [int] NOT NULL,
 CONSTRAINT [PK_tblBranchHeadObject] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_tblBranchHeadObject] UNIQUE NONCLUSTERED 
(
	[BranchId] ASC,
	[ObjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAttributeValue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ObjectRevisionId] [int] NOT NULL,
	[AttributeMetadataId] [int] NOT NULL,
	[Value] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_tblStringAttributeValues] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_StringAttributeValueId_ObjectRevisionId] UNIQUE NONCLUSTERED 
(
	[ObjectRevisionId] ASC,
	[AttributeMetadataId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFillHeadObjects]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spFillHeadObjects]
	
AS
BEGIN

	SET NOCOUNT ON;

	declare @mycur cursor
	declare @instanceId int

	-- clear out existing head objects table
	delete from tblBranchHeadObject

	SET @mycur = cursor

	FOR
		SELECT BranchId
		FROM tblRevision
		GROUP BY BranchId
	OPEN @mycur

	FETCH NEXT FROM @mycur INTO @instanceId

	WHILE @@FETCH_STATUS = 0
	BEGIN
	insert into tblBranchHeadObject(BranchId, ObjectRevisionId, ObjectId, RevisionId, ClassId)
		SELECT @instanceId as BranchId, tblObjectRevision.Id AS ObjectRevisionId, tblObjectRevision.ObjectId, tblObjectRevision.RevisionId, tblObject.ClassId
		FROM         tblObjectRevision INNER JOIN
							  tblObject ON tblObjectRevision.ObjectId = tblObject.Id
		WHERE     (tblObjectRevision.Id IN
		  (SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
			FROM          (SELECT     branchid, lo, hi
									FROM          dbo.branch_table(@instanceId) AS branch_table_2) AS branch_table_1 INNER JOIN
								   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
								   branch_table_1.hi >= tblRevision.Id INNER JOIN
								   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId INNER JOIN
								   tblObject AS tblObject_1 ON tblObjectRevision_1.ObjectId = tblObject_1.Id
			GROUP BY tblObjectRevision_1.ObjectId, tblObject_1.ClassId)) AND (tblObjectRevision.IsDeleted = 0)
	    
	    
		FETCH NEXT FROM @mycur INTO @instanceId
	END

	DEALLOCATE @mycur

END

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAddRevisionToHeadObjects]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spAddRevisionToHeadObjects]
	-- Add the parameters for the stored procedure here
	@branchId int,
	@revisionId int
AS
BEGIN

	SET NOCOUNT ON;

    insert into tblRevisionHeadObject(BranchId, ObjectRevisionId, ObjectId, RevisionId, ClassId)
	SELECT @branchId as BranchId, tblObjectRevision.Id AS ObjectRevisionId, tblObjectRevision.ObjectId, @revisionId as RevisionId, tblObject.ClassId
	FROM         tblObjectRevision INNER JOIN
						  tblObject ON tblObjectRevision.ObjectId = tblObject.Id
	WHERE     (tblObjectRevision.Id IN
	(SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
	FROM          (SELECT     branchid, lo, hi
							FROM          dbo.branch_table(@branchId) AS branch_table_2) AS branch_table_1 INNER JOIN
						   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
						   branch_table_1.hi >= tblRevision.Id AND tblRevision.Id <= @revisionId INNER JOIN
						   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId INNER JOIN
						   tblObject AS tblObject_1 ON tblObjectRevision_1.ObjectId = tblObject_1.Id
	GROUP BY tblObjectRevision_1.ObjectId, tblObject_1.ClassId)) AND (tblObjectRevision.IsDeleted = 0)
END


' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwIndexedRevisions]'))
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[vwIndexedRevisions]
AS
SELECT     RevisionId, BranchId
FROM         dbo.tblRevisionHeadObject
GROUP BY RevisionId, BranchId


'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRelationAttributeValue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ObjectRevisionId] [int] NOT NULL,
	[RelationshipMetadataId] [int] NOT NULL,
	[Value] [int] NOT NULL,
 CONSTRAINT [PK_tblForeignKey] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_tblForeignKey] UNIQUE NONCLUSTERED 
(
	[RelationshipMetadataId] ASC,
	[ObjectRevisionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetAllRelatedToObject]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spGetAllRelatedToObject]
	@branchId int,
	@objectId int
	
AS
BEGIN
	SET NOCOUNT ON;
	
SELECT     tblBranchHeadObject.ObjectRevisionId, tblBranchHeadObject.ObjectId, tblBranchHeadObject.RevisionId, tblRelationAttributeValue.RelationshipMetadataId
FROM         tblRelationAttributeValue INNER JOIN
                      tblBranchHeadObject ON tblRelationAttributeValue.ObjectRevisionId = tblBranchHeadObject.ObjectRevisionId
WHERE     (tblBranchHeadObject.BranchId = @branchId) AND (tblRelationAttributeValue.Value = @objectId)
END
      
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTurnBackClock]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spTurnBackClock]
	-- Add the parameters for the stored procedure here
	@dateTarget datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	delete from tblBranchHeadObject
	
    -- Insert statements for procedure here
	-- define the last revision ID handled
	DECLARE @LastRevisionID INT
	SET @LastRevisionID = 0

	-- define the revision ID to be handled now
	DECLARE @RevisionIDToHandle INT

	-- select the next revision to handle    
	SELECT TOP 1 @RevisionIDToHandle = Id
	FROM tblRevision
	WHERE [TimeStamp] > @dateTarget
	ORDER BY Id DESC

	-- as long as we revisions exist......    
	WHILE @RevisionIDToHandle IS NOT NULL
	BEGIN
		-- delete the revision
		-- delete all object revisions, will cascade to attribute value tables
		delete from tblObjectRevision where RevisionId = @RevisionIDToHandle
		-- delete revision
		delete from tblRevision where Id = @RevisionIDToHandle
		
		-- set the last revision handled to the one we just handled
		SET @LastRevisionID = @RevisionIDToHandle
		SET @RevisionIDToHandle = NULL

		-- select the next revision to handle    
		SELECT TOP 1 @RevisionIDToHandle = Id
		FROM tblRevision
		WHERE [TimeStamp] > @dateTarget
		ORDER BY Id DESC
	END

	exec spFillHeadObjects

END

' 
END
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblAttributeValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue]  WITH CHECK ADD  CONSTRAINT [CK_tblAttributeValue] CHECK  (([dbo].[fnStringAttributeIsValid]([ObjectRevisionId],[AttributeMetadataId])=(1)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblAttributeValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue] CHECK CONSTRAINT [CK_tblAttributeValue]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblRelationshipAttributeValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue]  WITH NOCHECK ADD  CONSTRAINT [CK_tblRelationshipAttributeValue] CHECK  (([dbo].[fnFkAttributeIsValid]([ObjectRevisionId],[RelationshipMetadataId])=(1)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblRelationshipAttributeValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] CHECK CONSTRAINT [CK_tblRelationshipAttributeValue]
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblRelationshipAttributeValue_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue]  WITH NOCHECK ADD  CONSTRAINT [CK_tblRelationshipAttributeValue_1] CHECK  (([Value]=NULL OR [dbo].[fnPkAttributeIsValid]([Value],[RelationshipMetadataId])=(1)))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_tblRelationshipAttributeValue_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] CHECK CONSTRAINT [CK_tblRelationshipAttributeValue_1]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblAttributeMetadata_tblDataType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata]  WITH CHECK ADD  CONSTRAINT [FK_tblAttributeMetadata_tblDataType] FOREIGN KEY([DataTypeId])
REFERENCES [dbo].[tblDataType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblAttributeMetadata_tblDataType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata] CHECK CONSTRAINT [FK_tblAttributeMetadata_tblDataType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblClassMetadataString_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata]  WITH CHECK ADD  CONSTRAINT [FK_tblClassMetadataString_tblClass] FOREIGN KEY([ClassId])
REFERENCES [dbo].[tblClass] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblClassMetadataString_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata] CHECK CONSTRAINT [FK_tblClassMetadataString_tblClass]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblClassMetadataString_tblStringAttribute]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata]  WITH CHECK ADD  CONSTRAINT [FK_tblClassMetadataString_tblStringAttribute] FOREIGN KEY([AttributeId])
REFERENCES [dbo].[tblAttribute] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblClassMetadataString_tblStringAttribute]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeMetadata]'))
ALTER TABLE [dbo].[tblAttributeMetadata] CHECK CONSTRAINT [FK_tblClassMetadataString_tblStringAttribute]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblStringAttributeValue_tblClassMetadataString]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue]  WITH CHECK ADD  CONSTRAINT [FK_tblStringAttributeValue_tblClassMetadataString] FOREIGN KEY([AttributeMetadataId])
REFERENCES [dbo].[tblAttributeMetadata] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblStringAttributeValue_tblClassMetadataString]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue] CHECK CONSTRAINT [FK_tblStringAttributeValue_tblClassMetadataString]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblStringAttributeValues_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue]  WITH CHECK ADD  CONSTRAINT [FK_tblStringAttributeValues_tblObjectRevision] FOREIGN KEY([ObjectRevisionId])
REFERENCES [dbo].[tblObjectRevision] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblStringAttributeValues_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblAttributeValue]'))
ALTER TABLE [dbo].[tblAttributeValue] CHECK CONSTRAINT [FK_tblStringAttributeValues_tblObjectRevision]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject]  WITH NOCHECK ADD  CONSTRAINT [FK_tblBranchHeadObject_tblClass] FOREIGN KEY([ClassId])
REFERENCES [dbo].[tblClass] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] CHECK CONSTRAINT [FK_tblBranchHeadObject_tblClass]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject]  WITH NOCHECK ADD  CONSTRAINT [FK_tblBranchHeadObject_tblObject] FOREIGN KEY([ObjectId])
REFERENCES [dbo].[tblObject] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] CHECK CONSTRAINT [FK_tblBranchHeadObject_tblObject]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject]  WITH NOCHECK ADD  CONSTRAINT [FK_tblBranchHeadObject_tblObjectRevision] FOREIGN KEY([ObjectRevisionId])
REFERENCES [dbo].[tblObjectRevision] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] CHECK CONSTRAINT [FK_tblBranchHeadObject_tblObjectRevision]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject]  WITH NOCHECK ADD  CONSTRAINT [FK_tblBranchHeadObject_tblRevision] FOREIGN KEY([BranchId])
REFERENCES [dbo].[tblRevision] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] CHECK CONSTRAINT [FK_tblBranchHeadObject_tblRevision]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblRevision1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject]  WITH NOCHECK ADD  CONSTRAINT [FK_tblBranchHeadObject_tblRevision1] FOREIGN KEY([RevisionId])
REFERENCES [dbo].[tblRevision] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblBranchHeadObject_tblRevision1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblBranchHeadObject]'))
ALTER TABLE [dbo].[tblBranchHeadObject] CHECK CONSTRAINT [FK_tblBranchHeadObject_tblRevision1]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObject]'))
ALTER TABLE [dbo].[tblObject]  WITH NOCHECK ADD  CONSTRAINT [FK_tblObject_tblClass] FOREIGN KEY([ClassId])
REFERENCES [dbo].[tblClass] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObject]'))
ALTER TABLE [dbo].[tblObject] CHECK CONSTRAINT [FK_tblObject_tblClass]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblData_tblEntry]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObjectRevision]'))
ALTER TABLE [dbo].[tblObjectRevision]  WITH NOCHECK ADD  CONSTRAINT [FK_tblData_tblEntry] FOREIGN KEY([ObjectId])
REFERENCES [dbo].[tblObject] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblData_tblEntry]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObjectRevision]'))
ALTER TABLE [dbo].[tblObjectRevision] CHECK CONSTRAINT [FK_tblData_tblEntry]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblData_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObjectRevision]'))
ALTER TABLE [dbo].[tblObjectRevision]  WITH NOCHECK ADD  CONSTRAINT [FK_tblData_tblRevision] FOREIGN KEY([RevisionId])
REFERENCES [dbo].[tblRevision] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblData_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblObjectRevision]'))
ALTER TABLE [dbo].[tblObjectRevision] CHECK CONSTRAINT [FK_tblData_tblRevision]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue]  WITH NOCHECK ADD  CONSTRAINT [FK_tblRelationshipAttributeValue_tblObject] FOREIGN KEY([Value])
REFERENCES [dbo].[tblObject] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] CHECK CONSTRAINT [FK_tblRelationshipAttributeValue_tblObject]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue]  WITH NOCHECK ADD  CONSTRAINT [FK_tblRelationshipAttributeValue_tblObjectRevision] FOREIGN KEY([ObjectRevisionId])
REFERENCES [dbo].[tblObjectRevision] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] CHECK CONSTRAINT [FK_tblRelationshipAttributeValue_tblObjectRevision]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblRelationshipMetadata]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue]  WITH NOCHECK ADD  CONSTRAINT [FK_tblRelationshipAttributeValue_tblRelationshipMetadata] FOREIGN KEY([RelationshipMetadataId])
REFERENCES [dbo].[tblRelationshipMetadata] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipAttributeValue_tblRelationshipMetadata]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationAttributeValue]'))
ALTER TABLE [dbo].[tblRelationAttributeValue] CHECK CONSTRAINT [FK_tblRelationshipAttributeValue_tblRelationshipMetadata]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FKClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata]  WITH NOCHECK ADD  CONSTRAINT [FK_FKClass] FOREIGN KEY([FkClassId])
REFERENCES [dbo].[tblClass] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FKClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] CHECK CONSTRAINT [FK_FKClass]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PKClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata]  WITH NOCHECK ADD  CONSTRAINT [FK_PKClass] FOREIGN KEY([PkClassId])
REFERENCES [dbo].[tblClass] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PKClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] CHECK CONSTRAINT [FK_PKClass]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipMetadata_tblForeignKeyAttribute]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata]  WITH NOCHECK ADD  CONSTRAINT [FK_tblRelationshipMetadata_tblForeignKeyAttribute] FOREIGN KEY([AttributeId])
REFERENCES [dbo].[tblAttribute] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipMetadata_tblForeignKeyAttribute]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] CHECK CONSTRAINT [FK_tblRelationshipMetadata_tblForeignKeyAttribute]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipMetadata_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata]  WITH NOCHECK ADD  CONSTRAINT [FK_tblRelationshipMetadata_tblObject] FOREIGN KEY([CascadeDefaultValue])
REFERENCES [dbo].[tblObject] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRelationshipMetadata_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRelationshipMetadata]'))
ALTER TABLE [dbo].[tblRelationshipMetadata] CHECK CONSTRAINT [FK_tblRelationshipMetadata_tblObject]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevision_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevision]'))
ALTER TABLE [dbo].[tblRevision]  WITH NOCHECK ADD  CONSTRAINT [FK_tblRevision_tblRevision] FOREIGN KEY([BranchId])
REFERENCES [dbo].[tblRevision] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevision_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevision]'))
ALTER TABLE [dbo].[tblRevision] CHECK CONSTRAINT [FK_tblRevision_tblRevision]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblRevisionHeadObject_tblClass] FOREIGN KEY([ClassId])
REFERENCES [dbo].[tblClass] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] CHECK CONSTRAINT [FK_tblRevisionHeadObject_tblClass]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblRevisionHeadObject_tblObject] FOREIGN KEY([ObjectId])
REFERENCES [dbo].[tblObject] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] CHECK CONSTRAINT [FK_tblRevisionHeadObject_tblObject]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblRevisionHeadObject_tblObjectRevision] FOREIGN KEY([ObjectRevisionId])
REFERENCES [dbo].[tblObjectRevision] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblObjectRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] CHECK CONSTRAINT [FK_tblRevisionHeadObject_tblObjectRevision]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblRevisionHeadObject_tblRevision] FOREIGN KEY([RevisionId])
REFERENCES [dbo].[tblRevision] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblRevision]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] CHECK CONSTRAINT [FK_tblRevisionHeadObject_tblRevision]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblRevision1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblRevisionHeadObject_tblRevision1] FOREIGN KEY([BranchId])
REFERENCES [dbo].[tblRevision] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblRevisionHeadObject_tblRevision1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblRevisionHeadObject]'))
ALTER TABLE [dbo].[tblRevisionHeadObject] CHECK CONSTRAINT [FK_tblRevisionHeadObject_tblRevision1]
GO
