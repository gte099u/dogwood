SELECT     tblClass.Name, tblObjectRevision.ObjectId, tblObjectRevision.RevisionId, tblObjectRevision.Id AS ObjectRevisionId, tblObjectRevision.IsDeleted, tblRevision.BranchId, tblRevision.TimeStamp, 
                      tblAttribute_1.Value AS RelationAttribute, tblRelationAttributeValue.Value AS RelationValue, tblAttribute.Value AS Attribute, tblClass.Id AS ClassId, 
                      CASE tblAttributeMetadata.DataTypeId WHEN 1 THEN CAST(tblAttributeValue.Value AS varchar(MAX)) WHEN 2 THEN CAST(CAST(tblAttributeValue.Value AS int) 
                      AS varchar(MAX)) WHEN 3 THEN CAST(CAST(tblAttributeValue.Value AS bigint) AS varchar(MAX)) WHEN 4 THEN CAST(CAST(tblAttributeValue.Value AS bit) 
                      AS varchar(MAX)) WHEN 5 THEN CAST(CAST(tblAttributeValue.Value AS datetime) AS varchar(MAX)) ELSE CAST(tblAttributeValue.Value AS varchar(MAX)) 
                      END AS AttributeValue
FROM         tblObject INNER JOIN
                      tblObjectRevision ON tblObject.Id = tblObjectRevision.ObjectId INNER JOIN
                      tblRevision ON tblObjectRevision.RevisionId = tblRevision.Id INNER JOIN
                      tblRelationAttributeValue ON tblObjectRevision.Id = tblRelationAttributeValue.ObjectRevisionId INNER JOIN
                      tblRelationshipMetadata ON tblRelationAttributeValue.RelationshipMetadataId = tblRelationshipMetadata.Id INNER JOIN
                      tblClass ON tblObject.ClassId = tblClass.Id INNER JOIN
                      tblAttributeValue ON tblObjectRevision.Id = tblAttributeValue.ObjectRevisionId INNER JOIN
                      tblAttributeMetadata ON tblAttributeValue.AttributeMetadataId = tblAttributeMetadata.Id INNER JOIN
                      tblAttribute ON tblAttributeMetadata.AttributeId = tblAttribute.Id INNER JOIN
                      tblAttribute AS tblAttribute_1 ON tblRelationshipMetadata.AttributeId = tblAttribute_1.Id
WHERE     (tblClass.Id = 22)