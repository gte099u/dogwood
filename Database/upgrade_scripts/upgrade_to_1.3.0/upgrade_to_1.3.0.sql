use dbCBRA1

-- drop spFillBranchAncestry
drop procedure spFillBranchAncestry

-- drop spCreateNewDataRequest
drop procedure spCreateNewDataRequest

-- drop spGetObjectChangesInBranchSinceRevision
drop procedure spGetObjectChangesInBranchSinceRevision

-- drop spGetAllByTypeWithFkRelation
drop procedure spGetAllByTypeWithFkRelation

-- drop spGetAllByTypeWithId
drop procedure spGetAllByTypeWithId

-- drop spGetAllDataForBranchByType
drop procedure spGetAllDataForBranchByType

-- drop spGetAllRelatedToObject
drop procedure spGetAllRelatedToObject

-- drop spGetValueForObjectInBranch
drop procedure spGetValueForObjectInBranch



-- drop tblBranchAncestry
drop table tblBranchAncestry

-- drop tblDataRequest
drop table tblDataRequest

-- update revision sprocs

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spGetAllAtRevisionWithId]
	@branchId int,
	@revisionId int,
	@idValues varchar(8000)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @IdTable TABLE ( pk int )

	INSERT INTO @IdTable SELECT Value FROM dbo.fnListToTableInt(@idValues)


SELECT     Id AS ObjectRevisionId, ObjectId, RevisionId
FROM         tblObjectRevision        
WHERE     (Id IN
                          (SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
                            FROM          (SELECT * from branch_table(@branchId)) AS branch_table_1 
											INNER JOIN
                                                tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
                                                   branch_table_1.hi >= tblRevision.Id AND tblRevision.Id <= @revisionId
                                            INNER JOIN
                                                tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId 
                                            INNER JOIN
                                                   tblObject AS tblObject_1 ON tblObjectRevision_1.ObjectId = tblObject_1.Id
                                            INNER JOIN  @IdTable as tblA ON tblObject_1.Id = tblA.pk       
                            GROUP BY tblObjectRevision_1.ObjectId, tblObject_1.ClassId
                            )) AND (IsDeleted = 0)
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spGetAllDataForRevisionByType]
	@branchId int,
	@revisionId int,
	@classId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT     Id AS ObjectRevisionId, ObjectId, RevisionId
FROM         tblObjectRevision
WHERE     (Id IN
                          (SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
                            FROM          (SELECT * FROM branch_table( @branchId)) AS branch_table_1 INNER JOIN
                                                   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
                                                   branch_table_1.hi >= tblRevision.Id AND tblRevision.Id <= @revisionId INNER JOIN
                                                   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId INNER JOIN
                                                   tblObject ON tblObjectRevision_1.ObjectId = tblObject.Id
                            GROUP BY tblObjectRevision_1.ObjectId, tblObject.ClassId
                            HAVING      (tblObject.ClassId = @classId))) AND (IsDeleted = 0)
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spGetAllForRevisionWithFkRelation]

	@branchId int,
	@revisionId int,
	@classId int,
	@foreignKeyAttributeId int,
	@foreignKeyValuesList varchar(8000)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FkTable TABLE ( FkId int )

	INSERT INTO @FkTable SELECT Value FROM dbo.fnListToTableInt(@foreignKeyValuesList)

	SELECT     tblObjectRevision.Id AS ObjectRevisionId, ObjectId, RevisionId
FROM         tblObjectRevision INNER JOIN
                      tblForeignKeyAttributeValue ON tblObjectRevision.Id = tblForeignKeyAttributeValue.ObjectRevisionId INNER JOIN
                      @FkTable as tblA ON tblForeignKeyAttributeValue.Value = tblA.FkId  
WHERE     (tblObjectRevision.Id IN
                          (SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
                            FROM          (SELECT * from branch_table(@branchId)) AS branch_table_1 INNER JOIN
                                                   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
                                                   branch_table_1.hi >= tblRevision.Id AND tblRevision.Id <= @revisionId INNER JOIN
                                                   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId INNER JOIN
                                                   tblObject ON tblObjectRevision_1.ObjectId = tblObject.Id
                            GROUP BY tblObjectRevision_1.ObjectId, tblObject.ClassId
                            HAVING      (tblObject.ClassId = @classId))) AND (IsDeleted = 0)
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spGetValueForObjectAtRevision]
	@branchId int,
	@objectId int,
	@revisionId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT     Id, ObjectId, RevisionId
FROM         tblObjectRevision
WHERE     (Id =
                          (SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
                            FROM          (SELECT * FROM branch_table(@branchId)) AS branch_table_1 INNER JOIN
                                                   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
                                                   branch_table_1.hi >= tblRevision.Id AND tblRevision.Id <= @revisionId INNER JOIN
                                                   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId
                            WHERE      (tblObjectRevision_1.ObjectId = @objectId)
                            GROUP BY tblObjectRevision_1.ObjectId)) AND (IsDeleted = 0)
END



-- recreate tblStringAttributeValue

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tblStringAttributeValue
	DROP CONSTRAINT FK_tblStringAttributeValues_tblObjectRevision
GO
ALTER TABLE dbo.tblObjectRevision SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.tblObjectRevision', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.tblObjectRevision', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.tblObjectRevision', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.tblStringAttributeValue
	DROP CONSTRAINT FK_tblStringAttributeValue_tblClassMetadataString
GO
ALTER TABLE dbo.tblClassMetadataString SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.tblClassMetadataString', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.tblClassMetadataString', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.tblClassMetadataString', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_tblStringAttributeValue
	(
	Id int NOT NULL IDENTITY (1, 1),
	ObjectRevisionId int NOT NULL,
	StringMetadataId int NOT NULL,
	Value varchar(MAX) NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblStringAttributeValue SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_tblStringAttributeValue ON
GO
IF EXISTS(SELECT * FROM dbo.tblStringAttributeValue)
	 EXEC('INSERT INTO dbo.Tmp_tblStringAttributeValue (Id, ObjectRevisionId, StringMetadataId, Value)
		SELECT Id, ObjectRevisionId, StringMetadataId, CONVERT(varchar(MAX), Value) FROM dbo.tblStringAttributeValue WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblStringAttributeValue OFF
GO
DROP TABLE dbo.tblStringAttributeValue
GO
EXECUTE sp_rename N'dbo.Tmp_tblStringAttributeValue', N'tblStringAttributeValue', 'OBJECT' 
GO
ALTER TABLE dbo.tblStringAttributeValue ADD CONSTRAINT
	PK_tblStringAttributeValues PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblStringAttributeValue ADD CONSTRAINT
	UK_StringAttributeValueId_ObjectRevisionId UNIQUE NONCLUSTERED 
	(
	ObjectRevisionId,
	StringMetadataId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblStringAttributeValue ADD CONSTRAINT
	CK_tblStringAttributeValue CHECK (([dbo].[fnStringAttributeIsValid]([ObjectRevisionId],[StringMetadataId])=(1)))
GO
ALTER TABLE dbo.tblStringAttributeValue ADD CONSTRAINT
	FK_tblStringAttributeValue_tblClassMetadataString FOREIGN KEY
	(
	StringMetadataId
	) REFERENCES dbo.tblClassMetadataString
	(
	Id
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblStringAttributeValue ADD CONSTRAINT
	FK_tblStringAttributeValues_tblObjectRevision FOREIGN KEY
	(
	ObjectRevisionId
	) REFERENCES dbo.tblObjectRevision
	(
	Id
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.tblStringAttributeValue', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.tblStringAttributeValue', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.tblStringAttributeValue', 'Object', 'CONTROL') as Contr_Per 

-- create tblBranchHeadObject

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblBranchHeadObject](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[ObjectId] [int] NOT NULL,
	[ObjectRevisionId] [int] NOT NULL,
	[RevisionId] [int] NOT NULL,
 CONSTRAINT [PK_tblBranchHeadObject] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblBranchHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblBranchHeadObject_tblClass] FOREIGN KEY([ClassId])
REFERENCES [dbo].[tblClass] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblBranchHeadObject] CHECK CONSTRAINT [FK_tblBranchHeadObject_tblClass]
GO

ALTER TABLE [dbo].[tblBranchHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblBranchHeadObject_tblObject] FOREIGN KEY([ObjectId])
REFERENCES [dbo].[tblObject] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblBranchHeadObject] CHECK CONSTRAINT [FK_tblBranchHeadObject_tblObject]
GO

ALTER TABLE [dbo].[tblBranchHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblBranchHeadObject_tblObjectRevision] FOREIGN KEY([ObjectRevisionId])
REFERENCES [dbo].[tblObjectRevision] ([Id])
GO

ALTER TABLE [dbo].[tblBranchHeadObject] CHECK CONSTRAINT [FK_tblBranchHeadObject_tblObjectRevision]
GO

ALTER TABLE [dbo].[tblBranchHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblBranchHeadObject_tblRevision] FOREIGN KEY([BranchId])
REFERENCES [dbo].[tblRevision] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[tblBranchHeadObject] CHECK CONSTRAINT [FK_tblBranchHeadObject_tblRevision]
GO

ALTER TABLE [dbo].[tblBranchHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblBranchHeadObject_tblRevision1] FOREIGN KEY([RevisionId])
REFERENCES [dbo].[tblRevision] ([Id])
GO

ALTER TABLE [dbo].[tblBranchHeadObject] CHECK CONSTRAINT [FK_tblBranchHeadObject_tblRevision1]
GO



-- fill tblBranchHeadObject

insert into tblBranchHeadObject (BranchId, ClassId, ObjectId, ObjectRevisionId, RevisionId) 
SELECT  17520,  tblObject.ClassId, tblObjectRevision.ObjectId,  tblObjectRevision.Id AS ObjectRevisionId, tblObjectRevision.RevisionId
FROM         tblObjectRevision INNER JOIN
                      tblObject ON tblObjectRevision.ObjectId = tblObject.Id
WHERE     (tblObjectRevision.Id IN
                          (SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
                            FROM          (SELECT     branchid, lo, hi
                                                    FROM          dbo.branch_table(17520) AS branch_table_2) AS branch_table_1 INNER JOIN
                                                   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
                                                   branch_table_1.hi >= tblRevision.Id INNER JOIN
                                                   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId
                            GROUP BY tblObjectRevision_1.ObjectId)) AND (tblObjectRevision.IsDeleted = 0)
union 

SELECT  8555,  tblObject.ClassId, tblObjectRevision.ObjectId,  tblObjectRevision.Id AS ObjectRevisionId, tblObjectRevision.RevisionId
FROM         tblObjectRevision INNER JOIN
                      tblObject ON tblObjectRevision.ObjectId = tblObject.Id
WHERE     (tblObjectRevision.Id IN
                          (SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
                            FROM          (SELECT     branchid, lo, hi
                                                    FROM          dbo.branch_table(8555) AS branch_table_2) AS branch_table_1 INNER JOIN
                                                   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
                                                   branch_table_1.hi >= tblRevision.Id INNER JOIN
                                                   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId
                            GROUP BY tblObjectRevision_1.ObjectId)) AND (tblObjectRevision.IsDeleted = 0)
union
SELECT  12933,  tblObject.ClassId, tblObjectRevision.ObjectId,  tblObjectRevision.Id AS ObjectRevisionId, tblObjectRevision.RevisionId
FROM         tblObjectRevision INNER JOIN
                      tblObject ON tblObjectRevision.ObjectId = tblObject.Id
WHERE     (tblObjectRevision.Id IN
                          (SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
                            FROM          (SELECT     branchid, lo, hi
                                                    FROM          dbo.branch_table(12933) AS branch_table_2) AS branch_table_1 INNER JOIN
                                                   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
                                                   branch_table_1.hi >= tblRevision.Id INNER JOIN
                                                   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId
                            GROUP BY tblObjectRevision_1.ObjectId)) AND (tblObjectRevision.IsDeleted = 0)
union
SELECT  5645,  tblObject.ClassId, tblObjectRevision.ObjectId,  tblObjectRevision.Id AS ObjectRevisionId, tblObjectRevision.RevisionId
FROM         tblObjectRevision INNER JOIN
                      tblObject ON tblObjectRevision.ObjectId = tblObject.Id
WHERE     (tblObjectRevision.Id IN
                          (SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
                            FROM          (SELECT     branchid, lo, hi
                                                    FROM          dbo.branch_table(5645) AS branch_table_2) AS branch_table_1 INNER JOIN
                                                   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
                                                   branch_table_1.hi >= tblRevision.Id INNER JOIN
                                                   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId
                            GROUP BY tblObjectRevision_1.ObjectId)) AND (tblObjectRevision.IsDeleted = 0)
  union                          
SELECT  0,  tblObject.ClassId, tblObjectRevision.ObjectId,  tblObjectRevision.Id AS ObjectRevisionId, tblObjectRevision.RevisionId
FROM         tblObjectRevision INNER JOIN
                      tblObject ON tblObjectRevision.ObjectId = tblObject.Id
WHERE     (tblObjectRevision.Id IN
                          (SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
                            FROM          (SELECT     branchid, lo, hi
                                                    FROM          dbo.branch_table(0) AS branch_table_2) AS branch_table_1 INNER JOIN
                                                   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
                                                   branch_table_1.hi >= tblRevision.Id INNER JOIN
                                                   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId
                            GROUP BY tblObjectRevision_1.ObjectId)) AND (tblObjectRevision.IsDeleted = 0)                         
                            
                            
-- create new getAllRelatedToObject


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAllRelatedToObject]
	@branchId int,
	@objectId int
	
AS
BEGIN
	SET NOCOUNT ON;
	
SELECT     tblBranchHeadObject.ObjectId, tblBranchHeadObject.ObjectRevisionId, tblBranchHeadObject.RevisionId, tblClassMetadataForeignKey.Id
FROM         tblBranchHeadObject INNER JOIN
                      tblClassMetadataForeignKey ON tblBranchHeadObject.Id = tblClassMetadataForeignKey.Id
WHERE     (tblBranchHeadObject.BranchId = @branchId) AND (tblBranchHeadObject.ObjectId = @objectId)
END
                               