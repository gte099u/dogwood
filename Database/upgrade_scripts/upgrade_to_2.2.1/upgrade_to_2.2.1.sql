USE [dbDogwood]
GO

/****** Object:  Table [dbo].[tblRevisionHeadObject]    Script Date: 04/20/2012 12:48:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblRevisionHeadObject](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchId] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[ObjectId] [int] NOT NULL,
	[ObjectRevisionId] [int] NOT NULL,
	[RevisionId] [int] NOT NULL,
 CONSTRAINT [PK_tblRevisionHeadObject] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblRevisionHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblRevisionHeadObject_tblClass] FOREIGN KEY([ClassId])
REFERENCES [dbo].[tblClass] ([Id])
GO

ALTER TABLE [dbo].[tblRevisionHeadObject] CHECK CONSTRAINT [FK_tblRevisionHeadObject_tblClass]
GO

ALTER TABLE [dbo].[tblRevisionHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblRevisionHeadObject_tblObject] FOREIGN KEY([ObjectId])
REFERENCES [dbo].[tblObject] ([Id])
GO

ALTER TABLE [dbo].[tblRevisionHeadObject] CHECK CONSTRAINT [FK_tblRevisionHeadObject_tblObject]
GO

ALTER TABLE [dbo].[tblRevisionHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblRevisionHeadObject_tblObjectRevision] FOREIGN KEY([ObjectRevisionId])
REFERENCES [dbo].[tblObjectRevision] ([Id])
GO

ALTER TABLE [dbo].[tblRevisionHeadObject] CHECK CONSTRAINT [FK_tblRevisionHeadObject_tblObjectRevision]
GO

ALTER TABLE [dbo].[tblRevisionHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblRevisionHeadObject_tblRevision] FOREIGN KEY([RevisionId])
REFERENCES [dbo].[tblRevision] ([Id])
GO

ALTER TABLE [dbo].[tblRevisionHeadObject] CHECK CONSTRAINT [FK_tblRevisionHeadObject_tblRevision]
GO

ALTER TABLE [dbo].[tblRevisionHeadObject]  WITH CHECK ADD  CONSTRAINT [FK_tblRevisionHeadObject_tblRevision1] FOREIGN KEY([BranchId])
REFERENCES [dbo].[tblRevision] ([Id])
GO

ALTER TABLE [dbo].[tblRevisionHeadObject] CHECK CONSTRAINT [FK_tblRevisionHeadObject_tblRevision1]
GO

/****** Object:  StoredProcedure [dbo].[spAddRevisionToHeadObjects]    Script Date: 04/20/2012 12:48:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spAddRevisionToHeadObjects]
	-- Add the parameters for the stored procedure here
	@branchId int,
	@revisionId int
AS
BEGIN

	SET NOCOUNT ON;

    insert into tblRevisionHeadObject(BranchId, ObjectRevisionId, ObjectId, RevisionId, ClassId)
	SELECT @branchId as BranchId, tblObjectRevision.Id AS ObjectRevisionId, tblObjectRevision.ObjectId, @revisionId as RevisionId, tblObject.ClassId
	FROM         tblObjectRevision INNER JOIN
						  tblObject ON tblObjectRevision.ObjectId = tblObject.Id
	WHERE     (tblObjectRevision.Id IN
	(SELECT     MAX(tblObjectRevision_1.Id) AS MaxId
	FROM          (SELECT     branchid, lo, hi
							FROM          dbo.branch_table(@branchId) AS branch_table_2) AS branch_table_1 INNER JOIN
						   tblRevision ON branch_table_1.branchid = tblRevision.BranchId AND branch_table_1.lo <= tblRevision.Id AND 
						   branch_table_1.hi >= tblRevision.Id AND tblRevision.Id <= @revisionId INNER JOIN
						   tblObjectRevision AS tblObjectRevision_1 ON tblRevision.Id = tblObjectRevision_1.RevisionId INNER JOIN
						   tblObject AS tblObject_1 ON tblObjectRevision_1.ObjectId = tblObject_1.Id
	GROUP BY tblObjectRevision_1.ObjectId, tblObject_1.ClassId)) AND (tblObjectRevision.IsDeleted = 0)
END

GO


drop procedure spGetAllAtRevisionWithId
drop procedure spGetAllDataForRevisionByType
drop procedure spGetAllForRevisionWithFkRelation
drop procedure spGetValueForObjectAtRevision

CREATE NONCLUSTERED INDEX [_dta_index_tblAttributeValue_6_896722247__K2_4] ON [dbo].[tblAttributeValue] 
(
	[ObjectRevisionId] ASC
)
INCLUDE ( [Value]) WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_tblRelationAttributeValue_6_832722019__K2] ON [dbo].[tblRelationAttributeValue] 
(
	[ObjectRevisionId] ASC
)WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [_dta_index_tblRevisionHeadObject_6_1376723957__K6_K3_K5] ON [dbo].[tblRevisionHeadObject] 
(
	[RevisionId] ASC,
	[ClassId] ASC,
	[ObjectRevisionId] ASC
)WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

/****** Object:  View [dbo].[vwIndexedRevisions]    Script Date: 04/25/2012 12:07:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwIndexedRevisions]
AS
SELECT     RevisionId, BranchId
FROM         dbo.tblRevisionHeadObject
GROUP BY RevisionId, BranchId

GO

/****** Object:  Index [IX_tblBranchHeadObject]    Script Date: 04/25/2012 12:23:47 ******/
ALTER TABLE [dbo].[tblBranchHeadObject] ADD  CONSTRAINT [IX_tblBranchHeadObject] UNIQUE NONCLUSTERED 
(
	[BranchId] ASC,
	[ObjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Index [IX_tblRevisionHeadObject]    Script Date: 04/25/2012 12:24:59 ******/
ALTER TABLE [dbo].[tblRevisionHeadObject] ADD  CONSTRAINT [IX_tblRevisionHeadObject] UNIQUE NONCLUSTERED 
(
	[RevisionId] ASC,
	[ObjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

