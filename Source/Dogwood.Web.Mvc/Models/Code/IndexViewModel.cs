﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Web.Mvc.Models.Code
{
    public class IndexViewModel
    {
        public IEnumerable<AttributeMetadataEntity> AttributeMetadataEntities { get; set; }
        public IEnumerable<RelationshipMetadataEntity> RelationMetadataEntities { get; set; }

        public IndexViewModel()
        {
            using (var adapter = new DataAccessAdapter(true))
            {
                var attributeMetadata = new EntityCollection<AttributeMetadataEntity>();
                var amPath = new PrefetchPath2(EntityType.AttributeMetadataEntity);
                amPath.Add(AttributeMetadataEntity.PrefetchPathClass);
                amPath.Add(AttributeMetadataEntity.PrefetchPathAttribute);
                adapter.FetchEntityCollection(attributeMetadata, null, amPath);
                AttributeMetadataEntities = attributeMetadata;

                var relationMetadata = new EntityCollection<RelationshipMetadataEntity>();
                var rmPath = new PrefetchPath2(EntityType.RelationshipMetadataEntity);
                rmPath.Add(RelationshipMetadataEntity.PrefetchPathFkClass);
                rmPath.Add(RelationshipMetadataEntity.PrefetchPathAttribute);
                adapter.FetchEntityCollection(relationMetadata, null, rmPath);
                RelationMetadataEntities = relationMetadata;
            }
        }
    }
}