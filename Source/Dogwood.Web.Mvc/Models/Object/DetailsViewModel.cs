﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.HelperClasses;
using Dogwood.Data;

namespace Dogwood.Web.Mvc.Models.Object
{
    public class DetailsViewModel
    {
        public ObjectEntity Obj { get; set; }

        public DetailsViewModel(int id)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                IPrefetchPath2 path = new PrefetchPath2(EntityType.ObjectEntity);
                
                IPrefetchPathElement2 objRevElement = path.Add(ObjectEntity.PrefetchPathObjectRevision);
                    objRevElement.SubPath.Add(ObjectRevisionEntity.PrefetchPathRevision);
                    objRevElement.SubPath.Add(ObjectRevisionEntity.PrefetchPathAttributeValue);
                    objRevElement.SubPath.Add(ObjectRevisionEntity.PrefetchPathRelationAttributeValue);

                IPrefetchPathElement2 classElement = path.Add(ObjectEntity.PrefetchPathClass);
                    classElement.SubPath.Add(ClassEntity.PrefetchPathRelationshipMetadataAsFk).SubPath.Add(RelationshipMetadataEntity.PrefetchPathAttribute);                    
                    classElement.SubPath.Add(ClassEntity.PrefetchPathAttributeMetadata).SubPath.Add(AttributeMetadataEntity.PrefetchPathAttribute);

                this.Obj = new ObjectEntity(id);
                adapter.FetchEntity(this.Obj, path);
            }
        }
    }
}