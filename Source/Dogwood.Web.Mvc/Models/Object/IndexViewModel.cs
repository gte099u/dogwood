﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.HelperClasses;
using Dogwood.Data;

namespace Dogwood.Web.Mvc.Models.Object
{
    public class IndexViewModel
    {
        public IList<ClassEntity> Classes { get; set; }

        public IndexViewModel()
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                IPrefetchPath2 path = new PrefetchPath2(EntityType.ClassEntity);
                path.Add(ClassEntity.PrefetchPathObject);
                EntityCollection<ClassEntity> classes = new EntityCollection<ClassEntity>();
                adapter.FetchEntityCollection(classes, null, path);
                this.Classes = classes.OrderBy(m => m.Name).ToList();
            }
        }
    }
}