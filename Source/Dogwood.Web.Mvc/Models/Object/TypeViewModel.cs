﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.HelperClasses;
using Dogwood.Data;

namespace Dogwood.Web.Mvc.Models.Object
{
    public class TypeViewModel
    {
        public IList<ObjectEntity> Objects { get; set; }
        public string ClassType { get; set; }

        public TypeViewModel(int id)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                IPrefetchPath2 path = new PrefetchPath2(EntityType.ClassEntity);
                IPrefetchPathElement2 element = path.Add(ClassEntity.PrefetchPathObject);
                element.SubPath.Add(ObjectEntity.PrefetchPathObjectRevision);

                ClassEntity cla = new ClassEntity(id);
                adapter.FetchEntity(cla, path);
                this.Objects = cla.Object;
            }
        }
    }
}