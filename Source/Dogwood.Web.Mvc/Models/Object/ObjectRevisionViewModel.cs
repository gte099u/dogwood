﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.HelperClasses;
using Dogwood.Data;

namespace Dogwood.Web.Mvc.Models.Object
{
    public class ObjectRevisionViewModel
    {
        public ObjectRevisionEntity ObjRev { get; set; }

        public ObjectRevisionViewModel(int id)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                IPrefetchPath2 path = new PrefetchPath2(EntityType.ObjectRevisionEntity);
                path.Add(ObjectRevisionEntity.PrefetchPathRevision);
                path.Add(ObjectRevisionEntity.PrefetchPathAttributeValue);               
                path.Add(ObjectRevisionEntity.PrefetchPathRelationAttributeValue);

                IPrefetchPathElement2 classElement = path.Add(ObjectRevisionEntity.PrefetchPathObject)
                    .SubPath.Add(ObjectEntity.PrefetchPathClass);               
                classElement.SubPath.Add(ClassEntity.PrefetchPathRelationshipMetadataAsFk).SubPath.Add(RelationshipMetadataEntity.PrefetchPathAttribute);
                classElement.SubPath.Add(ClassEntity.PrefetchPathAttributeMetadata).SubPath.Add(AttributeMetadataEntity.PrefetchPathAttribute);

                this.ObjRev = new ObjectRevisionEntity(id);
                adapter.FetchEntity(this.ObjRev, path);
            }
        }
    }
}