﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data;
using Dogwood.Data.EntityClasses;

namespace Dogwood.Web.Mvc.Models.ForeignKeyMeta
{
    public class DeleteViewModel
    {
        public int Id { get; set; }
        public string Attribute { get; set; }

        public DeleteViewModel()
        {

        }

        public DeleteViewModel(int id)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter())
            {
                IPrefetchPath2 path = new PrefetchPath2(EntityType.RelationshipMetadataEntity);
                path.Add(RelationshipMetadataEntity.PrefetchPathAttribute);
                RelationshipMetadataEntity meta = new RelationshipMetadataEntity(id);
                adapter.FetchEntity(meta, path);
                Id = id;
                Attribute = meta.Attribute.Value;
            }
        }
    }
}