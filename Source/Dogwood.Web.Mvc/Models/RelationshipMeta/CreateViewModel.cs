﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.HelperClasses;
using Dogwood.Data.EntityClasses;

namespace Dogwood.Web.Mvc.Models.ForeignKeyMeta
{
    public class CreateViewModel
    {
        public int Id = 0;
        public int ForeignKeyAttributeId { get; set; }
        public SelectList ForeignKeyAttributeList { get; set; }
        public int ClassId { get; set; }
        public int PkClassId { get; set; }
        public SelectList ForeignKeyClassList { get; set; }
        public bool IsNullable { get; set; }
        public int DefaultValue { get; set; }
        [Remote("OneToManyValueIsValid", "RelationshipMeta", HttpMethod ="Post", AdditionalFields = "PkClassId, Id")]
        public string OneToManyValue { get; set; }
        public int SelectedBehaviorType { get; set; }
        public SelectList CascadeBehaviors { get; set; }
        public int? CascadeDefaultValue { get; set; }

        public CreateViewModel()
        {

        }

        public CreateViewModel(int classId)
        {
            this.ClassId = classId;

            using (IDataAccessAdapter adapter = new DataAccessAdapter())
            {
                // used foreignKey attributes via metadata
                EntityCollection<RelationshipMetadataEntity> fkMetas = new EntityCollection<RelationshipMetadataEntity>();
                IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
                filterBucket.PredicateExpression.Add(RelationshipMetadataFields.FkClassId == classId);
                adapter.FetchEntityCollection(fkMetas, filterBucket);

                // all foreignKey attributes
                EntityCollection<AttributeEntity> fkAttributes = new EntityCollection<AttributeEntity>();
                adapter.FetchEntityCollection(fkAttributes, null);

                // select list of unused attributes
                this.ForeignKeyAttributeList = new SelectList(
                    fkAttributes.Where(m => !fkMetas.Select(n => n.AttributeId).Contains(m.Id))
                    .ToList(), "Id", "Value");


                EntityCollection<ClassEntity> fkClasses = new EntityCollection<ClassEntity>();
                adapter.FetchEntityCollection(fkClasses, null);

                this.ForeignKeyClassList = new SelectList(
                    fkClasses.OrderBy(m => m.Name).ToList(), "Id", "Name");                
            }

            Dictionary<int, string> behaviors = new Dictionary<int,string>();
            behaviors.Add(1, "Delete");
            behaviors.Add(2, "Set to NULL");
            behaviors.Add(3, "Set to Default");
            CascadeBehaviors = new SelectList(behaviors, "Key", "Value");
        }
    }
}