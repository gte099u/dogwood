﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data;
using Dogwood.Data.EntityClasses;
using System.Web.Mvc;

namespace Dogwood.Web.Mvc.Models.ForeignKeyMeta
{
    public class EditViewModel
    {
        public int Id { get; set; }
        public bool IsNullable { get; set; }
        public string FkAttribute { get; set; }
        public int ClassId { get; set; }
        public int PkClassId { get; set; }
        public int DefaultValue { get; set; }
        [Remote("OneToManyValueIsValid", "RelationshipMeta", HttpMethod = "Post", AdditionalFields = "PkClassId, Id")]
        public string OneToManyValue { get; set; }
        public int SelectedBehaviorType { get; set; }
        public SelectList CascadeBehaviors { get; set; }
        public int? CascadeDefaultValue { get; set; }

        public EditViewModel() { }

        public EditViewModel(int id)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter())
            {
                IPrefetchPath2 path = new PrefetchPath2(EntityType.RelationshipMetadataEntity);
                path.Add(RelationshipMetadataEntity.PrefetchPathAttribute);
                RelationshipMetadataEntity meta = new RelationshipMetadataEntity(id);
                adapter.FetchEntity(meta, path);
                Id = id;
                ClassId = meta.FkClassId;
                PkClassId = meta.PkClassId;
                FkAttribute = meta.Attribute.Value;
                IsNullable = meta.IsNullable;
                OneToManyValue = meta.OneToManyValue;
                SelectedBehaviorType = meta.CascadeBehavior;
                CascadeDefaultValue = meta.CascadeDefaultValue;
            }

            Dictionary<int, string> behaviors = new Dictionary<int, string>();
            behaviors.Add(1, "Delete");
            behaviors.Add(2, "Set to NULL");
            behaviors.Add(3, "Set to Default");
            CascadeBehaviors = new SelectList(behaviors, "Key", "Value");
        }
    }
}