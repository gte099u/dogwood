﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data;
using Dogwood.Data.EntityClasses;
using System.Web.Mvc;
using Dogwood.Data.HelperClasses;

namespace Dogwood.Web.Mvc.Models.AttributeMeta
{
    public class EditViewModel
    {
        public int Id { get; set; }
        public bool IsNullable { get; set; }
        public bool IsImmutable { get; set; }
        public int SelectedAttributeId { get; set; }
        public SelectList AttributeSelectList { get; set; }
        public byte[] DefaultValue { get; set; }
        public int SelectedDataTypeId { get; set; }
        public SelectList DataTypeSelectList { get; set; }

        public EditViewModel() { }

        public EditViewModel(int id)
        {
            Dictionary<int, string> dataTypes = new Dictionary<int, string>();
            dataTypes.Add(1, "String");
            dataTypes.Add(2, "Int");
            dataTypes.Add(3, "Long");
            dataTypes.Add(4, "Bool");
            dataTypes.Add(6, "DateTime");
            DataTypeSelectList = new SelectList(dataTypes, "Key", "Value");

            using (IDataAccessAdapter adapter = new DataAccessAdapter())
            {
                EntityCollection<AttributeEntity> attributes = new EntityCollection<AttributeEntity>();
                adapter.FetchEntityCollection(attributes, null);
                AttributeSelectList = new SelectList(attributes.OrderBy(m => m.Value), "Id", "Value");
                AttributeMetadataEntity meta = new AttributeMetadataEntity(id);
                adapter.FetchEntity(meta);
                Id = id;
                SelectedAttributeId = meta.AttributeId;
                SelectedDataTypeId = meta.DataTypeId;
                IsNullable = meta.IsNullable;
            }
        }
    }
}