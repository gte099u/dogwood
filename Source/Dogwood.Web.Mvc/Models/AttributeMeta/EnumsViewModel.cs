﻿using System.Collections.Generic;
using System.Linq;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.HelperClasses;
using Dogwood.Data;

namespace Dogwood.Web.Mvc.Models.AttributeMeta
{
    public class EnumsViewModel
    {
        public IEnumerable<RelationshipMetadataEntity> RelationshipMetadata { get; set; }
        public IEnumerable<AttributeMetadataEntity> AttributeMetadata { get; set; }

        public EnumsViewModel()
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var path = new PrefetchPath2(EntityType.RelationshipMetadataEntity);
                path.Add(RelationshipMetadataEntity.PrefetchPathFkClass);
                path.Add(RelationshipMetadataEntity.PrefetchPathAttribute);

                var metadata = new EntityCollection<RelationshipMetadataEntity>();
                adapter.FetchEntityCollection(metadata, null, path);
                RelationshipMetadata = metadata.OrderBy(m => m.Id).ToList();

                var attributePath = new PrefetchPath2(EntityType.AttributeMetadataEntity);
                attributePath.Add(AttributeMetadataEntity.PrefetchPathClass);
                attributePath.Add(AttributeMetadataEntity.PrefetchPathAttribute);

                var attributeMeta = new EntityCollection<AttributeMetadataEntity>();
                adapter.FetchEntityCollection(attributeMeta, null, attributePath);
                AttributeMetadata = attributeMeta.OrderBy(m => m.Id).ToList();
            }            
        }
    }
}