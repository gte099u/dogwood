﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.HelperClasses;
using Dogwood.Data.EntityClasses;

namespace Dogwood.Web.Mvc.Models.AttributeMeta
{
    public class CreateViewModel
    {
        public int SelectedAttributeId { get; set; }
        public SelectList AttributeSelectList { get; set; }
        public short SelectedDataTypeId { get; set; }
        public SelectList DataTypeSelectList { get; set; }
        public int ClassId { get; set; }
        public bool IsNullable { get; set; }
        public bool IsImmutable { get; set; }
        public byte[] DefaultValue { get; set; }

        public CreateViewModel()
        {

        }

        public CreateViewModel(int classId)
        {
            this.ClassId = classId;

            Dictionary<int, string> dataTypes = new Dictionary<int, string>();
            dataTypes.Add(1, "String");
            dataTypes.Add(2, "Int");
            dataTypes.Add(3, "Long");
            dataTypes.Add(4, "Bool");
            dataTypes.Add(6, "DateTime");
            DataTypeSelectList = new SelectList(dataTypes, "Key", "Value");

            using (IDataAccessAdapter adapter = new DataAccessAdapter())
            {
                // used string attributes via metadata
                EntityCollection<AttributeMetadataEntity> stringMetas = new EntityCollection<AttributeMetadataEntity>();
                IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
                filterBucket.PredicateExpression.Add(AttributeMetadataFields.ClassId == classId);
                adapter.FetchEntityCollection(stringMetas, filterBucket);

                // all string attributes
                EntityCollection<AttributeEntity> stringAttributes = new EntityCollection<AttributeEntity>();
                adapter.FetchEntityCollection(stringAttributes, null);

                // select list of unused attributes
                this.AttributeSelectList = new SelectList(
                    stringAttributes.Where(m => !stringMetas.Select(n => n.AttributeId).Contains(m.Id))
                    .ToList(), "Id", "Value");
            }
        }
    }
}