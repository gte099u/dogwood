﻿using Dogwood.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data;

namespace Dogwood.Web.Mvc.Models.Class
{
    public class DetailsViewModel
    {
        public int Id { get; set; }
        public ClassEntity Class { get; set; }

        public DetailsViewModel(int id)
        {
            Id = id;

            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                Class = new ClassEntity(id);

                IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.ClassEntity);
                
                prefetchPath.Add(ClassEntity.PrefetchPathAttributeMetadata).SubPath.Add(AttributeMetadataEntity.PrefetchPathAttribute);

                var fkelement = prefetchPath.Add(ClassEntity.PrefetchPathRelationshipMetadataAsFk);
                fkelement.SubPath.Add(RelationshipMetadataEntity.PrefetchPathAttribute);
                fkelement.SubPath.Add(RelationshipMetadataEntity.PrefetchPathPkClass);

                var pkelement = prefetchPath.Add(ClassEntity.PrefetchPathRelationshipMetadataAsPk);
                pkelement.SubPath.Add(RelationshipMetadataEntity.PrefetchPathAttribute);
                pkelement.SubPath.Add(RelationshipMetadataEntity.PrefetchPathFkClass);
                
                prefetchPath.Add(ClassEntity.PrefetchPathObject);

                adapter.FetchEntity(Class, prefetchPath);
            }
        }
    }
}