﻿using System.Collections.Generic;
using Dogwood.Data.EntityClasses;

namespace Dogwood.Web.Mvc.Models.Class
{
    public class CreateObjectSqlViewModel
    {
        public string ClassName { get; set; }
        public int ClassId { get; set; }
        public IEnumerable<AttributeMetadataEntity> AttributeMeta { get; set; }
        public IEnumerable<RelationshipMetadataEntity> RelationshipMeta { get; set; }
    }
}