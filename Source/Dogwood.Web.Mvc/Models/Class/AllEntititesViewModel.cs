﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.HelperClasses;
using Dogwood.Data;
using Dogwood.Service;

namespace Dogwood.Web.Mvc.Models.Class
{
    public class AllEntitiesViewModel
    {
        public IList<EntityViewModel> AllClassTypes { get; set; }

        public AllEntitiesViewModel()
        {
            IClassService service = new ClassService();
            var allClasses = service.GetAll();
            AllClassTypes = new List<EntityViewModel>();
            ((List<ClassEntity>)allClasses.ToList()).ForEach(m => AllClassTypes.Add(new EntityViewModel()
            {
                Id = m.Id,
                Class = m
            }));
        }
    }
}