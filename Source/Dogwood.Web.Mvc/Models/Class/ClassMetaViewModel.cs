﻿using System.Collections.Generic;
using Dogwood.Data.EntityClasses;
using Dogwood.Service;

namespace Dogwood.Web.Mvc.Models.Class
{
    public class ClassMetaViewModel
    {
        public IEnumerable<ClassEntity> AllClasses { get; set; }

        public ClassMetaViewModel(IClassService service)
        {
            AllClasses = service.GetAll();
        }
    }
}