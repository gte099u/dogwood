﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.HelperClasses;

namespace Dogwood.Web.Mvc.Models.Class
{
    public class IndexViewModel
    {
        public IList<ClassEntity> Classes { get; set; }

        public IndexViewModel()
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                EntityCollection<ClassEntity> classes = new EntityCollection<ClassEntity>();
                adapter.FetchEntityCollection(classes, null);
                this.Classes = classes.OrderBy(m => m.Name).ToList();
            }
        }
    }
}