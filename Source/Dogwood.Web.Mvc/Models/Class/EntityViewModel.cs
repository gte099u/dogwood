﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.HelperClasses;
using Dogwood.Data;
using Dogwood.Core;
using Dogwood.Service;

namespace Dogwood.Web.Mvc.Models.Class
{
    public class EntityViewModel
    {
        public int Id { get; set; }
        public ClassEntity Class { get; set; }

        public EntityViewModel()
        {

        }

        public EntityViewModel(int id)
        {
            this.Id = id;

            IClassService service = new ClassService();
            Class = service.Get(id);
        }
    }
}