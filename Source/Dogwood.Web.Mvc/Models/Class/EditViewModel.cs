﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.HelperClasses;
using Dogwood.Data;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Dogwood.Web.Mvc.Models.Class
{
    public class EditViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public EditViewModel()
        {

        }

        public EditViewModel(int id)
        {
            this.Id = id;            

            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                ClassEntity cla = new ClassEntity(id);               
                adapter.FetchEntity(cla);
                Name = cla.Name;

                EntityCollection<ClassEntity> allClasses = new EntityCollection<ClassEntity>();
                ISortExpression sorter = new SortExpression(ClassFields.Name | SortOperator.Ascending);
                adapter.FetchEntityCollection(allClasses, null, 0, sorter);
            }
        }
    }
}