﻿using Dogwood.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.HelperClasses;
using System.ComponentModel.DataAnnotations;

namespace Dogwood.Web.Mvc.Models.Class
{
    public class CreateViewModel
    {
        [Required]
        public string Name { get; set; }

        public CreateViewModel()
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var allClasses = new EntityCollection<ClassEntity>();
                ISortExpression sorter = new SortExpression(ClassFields.Name | SortOperator.Ascending);
                adapter.FetchEntityCollection(allClasses, null, 0, sorter);
            }
        }
    }
}