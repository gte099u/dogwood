﻿using System.Collections.Generic;
using System.Linq;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.HelperClasses;

namespace Dogwood.Web.Mvc.Models.Class
{
    public class EnumsViewModel
    {
        public IEnumerable<ClassEntity> Classes { get; set; }

        public EnumsViewModel()
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var classes = new EntityCollection<ClassEntity>();
                adapter.FetchEntityCollection(classes, null);
                Classes = classes.OrderBy(m => m.Id).ToList();
            }            
        }
    }
}