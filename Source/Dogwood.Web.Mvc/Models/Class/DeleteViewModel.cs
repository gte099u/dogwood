﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.HelperClasses;
using Dogwood.Data;
using System.ComponentModel.DataAnnotations;

namespace Dogwood.Web.Mvc.Models.Class
{
    public class DeleteViewModel
    {
        public int Id { get; set; }        
        public string Name { get; set; }

        public DeleteViewModel()
        {

        }

        public DeleteViewModel(int id)
        {
            this.Id = id;

            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                ClassEntity cla = new ClassEntity(id);               
                adapter.FetchEntity(cla);
                Name = cla.Name;
            }
        }
    }
}