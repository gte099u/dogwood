﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.HelperClasses;

namespace Dogwood.Web.Mvc.Models.Home
{
    public class EnumViewModel
    {
        public IList<AttributeEntity> Attributes { get; set; }

        public EnumViewModel()
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                EntityCollection<AttributeEntity> stringAtts = new EntityCollection<AttributeEntity>();
                adapter.FetchEntityCollection(stringAtts, null);
                Attributes = stringAtts.ToList();
            }
        }
    }
}