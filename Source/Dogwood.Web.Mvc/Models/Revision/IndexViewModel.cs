﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.HelperClasses;

namespace Dogwood.Web.Mvc.Models.Revision
{
    public class IndexViewModel
    {
        private IList<RevisionEntity> AllRevisions { get; set; }
        public IList<Revision> RevisionTree { get; set; }

        public IndexViewModel()
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                EntityCollection<RevisionEntity> allRevisions = new EntityCollection<RevisionEntity>();
                adapter.FetchEntityCollection(allRevisions, null);
                this.AllRevisions = allRevisions.OrderBy(m=>m.Id).ToList();

                RevisionTree = new List<Revision>();

                foreach (var item in this.AllRevisions.Where(m=>m.Id == 0))
                {
                    Revision toAdd = new Revision();
                    toAdd.Item = item;
                    AddChildren(toAdd);
                    RevisionTree.Add(toAdd);
                }
            }
        }

        private void AddChildren(Revision item)
        {
            foreach (var child in AllRevisions.Where(m=>m.BranchId == item.Item.Id && m.Id != 0))
            {
                Revision toAdd = new Revision();
                toAdd.Item = child;
                
                AddChildren(toAdd);

                if (item.Children == null)
                {
                    item.Children = new List<Revision>();
                }
                
                item.Children.Add(toAdd);
            }
        }
    }

    public class Revision
    {
        public RevisionEntity Item { get; set; }
        public IList<Revision> Children { get; set; }
    }
}