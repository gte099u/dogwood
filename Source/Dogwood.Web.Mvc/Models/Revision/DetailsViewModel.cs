﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data;
using Dogwood.Data.HelperClasses;

namespace Dogwood.Web.Mvc.Models.Revision
{
    public class DetailsViewModel
    {
        public int RevisionId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? CreatedById { get; set; }
        public string Tag { get; set; }
        public IList<ObjectRevisionEntity> RevisedObjects { get; set; }
        public int? PreviousRevisionInBranch { get; set; }
        public int? NextRevisionInBranch { get; set; }

        // branches into...

        public DetailsViewModel(int id)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.RevisionEntity);
                prefetchPath.Add(RevisionEntity.PrefetchPathObjectRevision);                    
                RevisionEntity revision = new RevisionEntity(id);
                adapter.FetchEntity(revision, prefetchPath);

                EntityCollection<RevisionEntity> branchRevisions = new EntityCollection<RevisionEntity>();
                IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
                filterBucket.PredicateExpression.Add(RevisionFields.BranchId == revision.BranchId);
                adapter.FetchEntityCollection(branchRevisions, filterBucket);

                this.RevisionId = id;
                this.CreatedDate = revision.TimeStamp;
                this.CreatedById = revision.CreatedBy;
                this.Tag = revision.Tag;
                this.RevisedObjects = revision.ObjectRevision.ToList();

                for (int i = 0; i < branchRevisions.Count; i++)
                {
                    if (branchRevisions[i].Id == id)
                    {
                        if (i > 0)
                        {
                            this.PreviousRevisionInBranch = branchRevisions[i - 1].Id;
                        }

                        if (i < branchRevisions.Count - 1)
                        {
                            this.NextRevisionInBranch = branchRevisions[i+1].Id;
                        }
                    }
                }
            }
        }
    }
}