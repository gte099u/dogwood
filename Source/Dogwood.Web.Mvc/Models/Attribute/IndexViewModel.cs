﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data;
using Dogwood.Data.EntityClasses;

namespace Dogwood.Web.Mvc.Models.Attribute
{
    public class IndexViewModel
    {
        public IEnumerable<AttributeEntity> Attributes { get; set; }
    }
}