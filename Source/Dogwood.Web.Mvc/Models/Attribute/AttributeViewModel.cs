﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.EntityClasses;
using System.ComponentModel.DataAnnotations;

namespace Dogwood.Web.Mvc.Models.Attribute
{
    public class AttributeViewModel
    {
        [Required]
        public string Value { get; set; }
        public int Id { get; set; }        
    }
}