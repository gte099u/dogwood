﻿using System.Collections.Generic;
using System.Linq;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.DatabaseSpecific;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.HelperClasses;

namespace Dogwood.Web.Mvc.Models.Attribute
{
    public class EnumsViewModel
    {
        public IEnumerable<AttributeEntity> Attributes { get; set; }

        public EnumsViewModel()
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var attributes = new EntityCollection<AttributeEntity>();
                adapter.FetchEntityCollection(attributes, null);
                Attributes = attributes.OrderBy(m => m.Id).ToList();
            }            
        }
    }
}