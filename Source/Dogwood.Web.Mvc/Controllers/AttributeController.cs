﻿using System;
using System.Web.Mvc;
using Dogwood.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data;
using Dogwood.Web.Mvc.Models.Attribute;
using Dogwood.Data.HelperClasses;

namespace Dogwood.Web.Mvc.Controllers
{
    public class AttributeController : Controller
    {
        public ActionResult Index()
        {
            var vm = new IndexViewModel();
           
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var attributes = new EntityCollection<AttributeEntity>();
                adapter.FetchEntityCollection(attributes, null);
                vm.Attributes = attributes;
            }

            return View(vm);
        }

        public PartialViewResult IndexTable()
        {
            var vm = new IndexViewModel();

            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var attributes = new EntityCollection<AttributeEntity>();
                adapter.FetchEntityCollection(attributes, null);
                vm.Attributes = attributes;
            }

            return PartialView(vm);
        }

        public ActionResult Edit(int id)
        {
            var vm = new AttributeViewModel {Id = id};

            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var entity = new AttributeEntity(id);
                adapter.FetchEntity(entity);
                vm.Value = entity.Value;
            }

            return View(vm);
        }

        public ActionResult Create()
        {
            var vm = new AttributeViewModel();
            return View(vm);
        }

        public ActionResult Delete(int id)
        {
            var vm = new AttributeViewModel {Id = id};

            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var entity = new AttributeEntity(id);
                adapter.FetchEntity(entity);
                vm.Value = entity.Value;
            }

            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(AttributeViewModel vm)
        {
            try
            {
                using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
                {
                    var entity = new AttributeEntity(vm.Id);
                    adapter.FetchEntity(entity);
                    entity.Value = vm.Value;
                    adapter.SaveEntity(entity);
                    return RedirectToAction("CloseWindow", "Home");
                }
            }
            catch (Exception)
            {
                using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
                {
                    var entity = new AttributeEntity(vm.Id);
                    adapter.FetchEntity(entity);
                    vm.Value = entity.Value;
                }
                return View(vm);
            }
        }

        [HttpPost]
        public ActionResult Create(AttributeViewModel vm)
        {
            try
            {
                using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
                {
                    var entity = new AttributeEntity {Value = vm.Value};
                    adapter.SaveEntity(entity);
                    return RedirectToAction("CloseWindow", "Home");
                }
            }
            catch (Exception)
            {
                return View(vm);
            }
        }

        [HttpPost]
        public ActionResult Delete(AttributeViewModel vm)
        {
            try
            {
                using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
                {
                    var attribute = new AttributeEntity(vm.Id);

                    IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.AttributeEntity);
                    prefetchPath.Add(AttributeEntity.PrefetchPathAttributeMetadata);

                    adapter.FetchEntity(attribute, prefetchPath);

                    if (attribute.AttributeMetadata.Count == 0)
                    {
                        adapter.DeleteEntity(attribute);
                        return RedirectToAction("CloseWindow", "Home");
                    }

                    return View(vm);
                }
            }
            catch (Exception)
            {
                using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
                {
                    var entity = new AttributeEntity(vm.Id);
                    adapter.FetchEntity(entity);
                    vm.Value = entity.Value;
                }
                return View(vm);
            }
        }

        public ActionResult Enums()
        {
            var vm = new EnumsViewModel();
            return View(vm);
        }
    }
}
