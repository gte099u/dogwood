﻿using System.Web.Mvc;

namespace Dogwood.Web.Mvc.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CloseWindow()
        {
            return View();
        }     
    }
}
