﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dogwood.Web.Mvc.Models.ForeignKeyMeta;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.EntityClasses;
using System.Data;
using Dogwood.Data.HelperClasses;
using Dogwood.Data;
using Dogwood.Service;

namespace Dogwood.Web.Mvc.Controllers
{
    public class RelationshipMetaController : Controller
    {
        public ActionResult Create(int id)
        {
            CreateViewModel vm = new CreateViewModel(id);
            return View(vm);
        }

        public PartialViewResult CreateSql(int id)
        {
            IRelationshipMetadataService service = new RelationshipMetadataService();
            var entity = service.Get(id);
            return PartialView(entity);
        }

        [HttpPost]
        public ActionResult Create(CreateViewModel vm)
        {
            IRelationshipMetadataService service = new RelationshipMetadataService();

            try
            {
                RelationshipMetadataDto dto = new RelationshipMetadataDto()
                {
                    AttributeId = vm.ForeignKeyAttributeId,
                    ClassId = vm.ClassId,
                    PkClassId = vm.PkClassId,
                    IsNullable = vm.IsNullable,
                    DefaultValue = vm.DefaultValue,
                    OneToManyValue = vm.OneToManyValue,
                    BehaviorType = vm.SelectedBehaviorType,
                    CascadeDefaultValue = vm.CascadeDefaultValue
                };

                service.Create(dto);
                return RedirectToAction("CloseWindow", "Home");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View();
            }           
        }
     
        public ActionResult Edit(int id)
        {
            EditViewModel vm = new EditViewModel(id);
            return View(vm);
        }

        [HttpPost]
        public JsonResult OneToManyValueIsValid(string OneToManyValue, int PkClassId, int Id)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                EntityCollection<RelationshipMetadataEntity> classesWithOtmValue = new EntityCollection<RelationshipMetadataEntity>();
                IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
                filterBucket.PredicateExpression.Add(RelationshipMetadataFields.OneToManyValue == OneToManyValue);
                filterBucket.PredicateExpression.AddWithAnd(RelationshipMetadataFields.PkClassId == PkClassId);
                filterBucket.PredicateExpression.AddWithAnd(RelationshipMetadataFields.Id != Id);
                adapter.FetchEntityCollection(classesWithOtmValue, filterBucket);

                if (classesWithOtmValue.Count == 0)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("A relationship with that property name has already been created for this class", JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Edit(EditViewModel vm)
        {
            IRelationshipMetadataService service = new RelationshipMetadataService();

            try
            {
                RelationshipMetadataDto dto = new RelationshipMetadataDto()
                {
                    Id = vm.Id,
                    IsNullable = vm.IsNullable,
                    ClassId = vm.ClassId,
                    PkClassId = vm.PkClassId,
                    DefaultValue = vm.DefaultValue,
                    OneToManyValue = vm.OneToManyValue,
                    BehaviorType = vm.SelectedBehaviorType,
                    CascadeDefaultValue = vm.CascadeDefaultValue
                };

                service.Update(dto);
                return RedirectToAction("CloseWindow", "Home");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View();
            }  
        }

        public ActionResult Delete(int id)
        {
            DeleteViewModel vm = new DeleteViewModel(id);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Delete(DeleteViewModel vm)
        {
            try
            {
                using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
                {
                    RelationshipMetadataEntity meta = new RelationshipMetadataEntity(vm.Id);

                    // attribute-value pairs should be cascade deleted by the database
                    adapter.DeleteEntity(meta);

                    return RedirectToAction("CloseWindow", "Home");
                }
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}
