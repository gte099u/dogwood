﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dogwood.Web.Mvc.Models.Revision;

namespace Dogwood.Web.Mvc.Controllers
{
    public class RevisionController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            IndexViewModel vm = new IndexViewModel();
            return View(vm);
        }
        public ActionResult Details(int id)
        {
            DetailsViewModel vm = new DetailsViewModel(id);
            return View(vm);
        }

    }
}
