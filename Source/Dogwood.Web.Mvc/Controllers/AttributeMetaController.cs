﻿using System;
using System.Web.Mvc;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.EntityClasses;
using Dogwood.Service;
using Dogwood.Web.Mvc.Models.AttributeMeta;

namespace Dogwood.Web.Mvc.Controllers
{
    public class AttributeMetaController : Controller
    {
        public ActionResult Create(int id)
        {
            CreateViewModel vm = new CreateViewModel(id);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(CreateViewModel vm)
        {
            IMetadataService service = new AttributeMetadataService();

            try
            {
                service.Create(vm.ClassId, vm.SelectedDataTypeId, vm.SelectedAttributeId, vm.IsNullable, vm.IsImmutable, vm.DefaultValue);
                return RedirectToAction("CloseWindow", "Home");
            }
            catch (Exception)
            {
                return Create(vm.ClassId);
            }
        }

        public ActionResult Edit(int id)
        {
            EditViewModel vm = new EditViewModel(id);
            return View(vm);
        }

        public PartialViewResult CreateSql(int id)
        {
            IMetadataService service = new AttributeMetadataService();
            var entity = service.Get(id);
            return PartialView(entity);
        }

        [HttpPost]
        public ActionResult Edit(EditViewModel vm)
        {
            IMetadataService service = new AttributeMetadataService();

            try
            {
                service.Update(vm.Id, vm.SelectedDataTypeId, vm.IsNullable, vm.IsImmutable, vm.DefaultValue);
                return RedirectToAction("CloseWindow", "Home");
            }
            catch (Exception)
            {
                return Edit(vm.Id);
                throw;
            }
        }

        public ActionResult Delete(int id)
        {
            DeleteViewModel vm = new DeleteViewModel(id);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Delete(DeleteViewModel vm)
        {
            try
            {
                using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
                {
                    AttributeMetadataEntity meta = new AttributeMetadataEntity(vm.Id);

                    // attribute-value pairs should be cascade deleted by the database
                    adapter.DeleteEntity(meta);

                    return RedirectToAction("CloseWindow", "Home");
                }
            }
            catch (Exception)
            {
                return View();
            }
        }

        public ActionResult Enums()
        {
            var vm = new EnumsViewModel();
            return View(vm);
        }
    }
}
