﻿using System;
using System.Web.Mvc;
using Dogwood.Web.Mvc.Models.Class;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.EntityClasses;
using System.Data;
using Dogwood.Data.HelperClasses;
using Dogwood.Service;

namespace Dogwood.Web.Mvc.Controllers
{
    public class ClassController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            var vm = new IndexViewModel();
            return View(vm);
        }

        public PartialViewResult IndexTable()
        {
            var vm = new IndexViewModel();
            return PartialView(vm);
        }

        public ActionResult Details(int id)
        {
            var vm = new DetailsViewModel(id);
            return View(vm);
        }

        public ActionResult Entity(int id)
        {
            var vm = new EntityViewModel(id);
            return View(vm);
        }

        public ActionResult AllEntities()
        {
            var vm = new AllEntitiesViewModel();
            return View(vm);
        }

        public ActionResult AllPrefetches()
        {
            var vm = new AllEntitiesViewModel();
            return View(vm);
        }

        public PartialViewResult DetailsTable(int id)
        {
            var vm = new DetailsViewModel(id);
            return PartialView(vm);
        }

        public ActionResult Edit(int id)
        {
            var vm = new EditViewModel(id);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(EditViewModel vm)
        {
            IClassService service = new ClassService();

            try
            {
                service.Update(vm.Id, vm.Name);

                return RedirectToAction("CloseWindow", "Home");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return Edit(vm.Id);
            }
        }

        public ActionResult Create()
        {
            var vm = new CreateViewModel();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(CreateViewModel vm)
        {
            IClassService service = new ClassService();

            try
            {
                service.Create(vm.Name);

                return RedirectToAction("CloseWindow", "Home");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return Create();
            }
        }

        public PartialViewResult CreateObjectSql(int id)
        {
            var vm = new CreateObjectSqlViewModel();

            var classService = new ClassService();
            var cla = classService.Get(id);

            vm.AttributeMeta = cla.AttributeMetadata;
            vm.RelationshipMeta = cla.RelationshipMetadataAsFk;
            vm.ClassName = cla.Name;
            vm.ClassId = id;

            return PartialView(vm);
        }

        public ActionResult Delete(int id)
        {
            var vm = new DeleteViewModel(id);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Delete(DeleteViewModel vm)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                try
                {
                    adapter.StartTransaction(IsolationLevel.ReadCommitted, "CreateMetadata");

                    //~ check that no classes have fk metadata relationships established on the class to delete                    
                    var fkMeta = new EntityCollection<RelationshipMetadataEntity>();
                    IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
                    filterBucket.PredicateExpression.Add(RelationshipMetadataFields.PkClassId == vm.Id);
                    adapter.FetchEntityCollection(fkMeta, filterBucket);

                    if (fkMeta.Count > 0)
                    {
                        throw new Exception("Unable to delete the class because other classes have a foreign key relationship with it");
                    }

                    //~ delete all related object revisions - should cascade delete all attribute values in database
                    IRelationPredicateBucket objRevFilter = new RelationPredicateBucket();
                    objRevFilter.PredicateExpression.Add(ObjectFields.ClassId == vm.Id);
                    objRevFilter.Relations.Add(ObjectRevisionEntity.Relations.ObjectEntityUsingObjectId);
                    adapter.DeleteEntitiesDirectly(typeof(ObjectRevisionEntity), objRevFilter);

                    //~ delete all related objects
                    IRelationPredicateBucket objFilter = new RelationPredicateBucket();
                    objFilter.PredicateExpression.Add(ObjectFields.ClassId == vm.Id);
                    adapter.DeleteEntitiesDirectly(typeof(ObjectEntity), objFilter);

                    IRelationPredicateBucket fkBucket = new RelationPredicateBucket();
                    fkBucket.PredicateExpression.Add(RelationshipMetadataFields.FkClassId == vm.Id);
                    adapter.DeleteEntitiesDirectly(typeof(RelationshipMetadataEntity), fkBucket);

                    //~ delete the class
                    var toDelete = new ClassEntity(vm.Id);
                    adapter.DeleteEntity(toDelete);

                    adapter.Commit();

                    return RedirectToAction("Index", "Class", null);
                }
                catch (Exception)
                {
                    adapter.Rollback();
                    vm = new DeleteViewModel(vm.Id);
                    return View(vm);
                }
            }
        }

        public ActionResult ClassMeta()
        {
            var classService = new ClassService();
            var vm = new ClassMetaViewModel(classService);
            return View(vm);
        }

        public ActionResult Enums()
        {
            var vm = new EnumsViewModel();
            return View(vm);
        }
    }
}
