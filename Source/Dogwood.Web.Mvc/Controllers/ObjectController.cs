﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dogwood.Web.Mvc.Models.Object;

namespace Dogwood.Web.Mvc.Controllers
{
    public class ObjectController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            IndexViewModel vm = new IndexViewModel();
            return View(vm);
        }

        public ActionResult Type(int id)
        {
            TypeViewModel vm = new TypeViewModel(id);
            return View(vm);
        }

        public ActionResult Details(int id)
        {
            DetailsViewModel vm = new DetailsViewModel(id);
            return View(vm);
        }

        public ActionResult ObjectRevision(int id)
        {
            ObjectRevisionViewModel vm = new ObjectRevisionViewModel(id);
            return View(vm);
        }
    }
}
