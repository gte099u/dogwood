﻿using System;
using System.Web.Mvc;

namespace Dogwood.Web.Mvc
{
    public static class HtmlHelpers
    {
        public static IDisposable Fieldset(this HtmlHelper helper, string legendValue)
        {
            TagBuilder legendBuilder = new TagBuilder("Legend");
            legendBuilder.InnerHtml = legendValue;
            
            TagBuilder fieldSetBuilder = new TagBuilder("Fieldset");            

            Action beginAction = () =>
            {
                helper.ViewContext.Writer.Write(fieldSetBuilder.ToString(TagRenderMode.StartTag));
                helper.ViewContext.Writer.Write(legendBuilder.ToString());
            };

            Action endAction = () =>
            {
                helper.ViewContext.Writer.Write(fieldSetBuilder.ToString(TagRenderMode.EndTag));
            };

            return new DisposableHelper(
                () => beginAction(),
                () => endAction()
            );
        }
    }

    public class DisposableHelper : IDisposable
    {
        private Action end;

        // When the object is create, write "begin" function
        public DisposableHelper(Action begin, Action end)
        {
            this.end = end;
            begin();
        }

        // When the object is disposed (end of using block), write "end" function
        public void Dispose()
        {
            end();
        }
    }
}