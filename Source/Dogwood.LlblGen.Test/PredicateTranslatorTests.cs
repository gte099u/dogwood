﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core;
using Dogwood.Core.Test;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.LlblGen.Test
{
    [TestClass]
    public class PredicateTranslatorTests
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        [TestMethod]
        public void Convert_Uses_N_Left_Joins_For_N_RelationPredicates()
        {
            var adapter = new EcomPersistenceAdapter();
            Core.Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var filterBucket = new PredicateBucket<Order>(9);
            filterBucket.AddRelationPredicate(m => m.CustomerId, 4);
            filterBucket.AddRelationPredicate(m => m.CarrierId, 4);

            var llblPredicateBucket = PredicateTranslator.Convert(filterBucket, EavContextType.Branch);

            //~ three relations (joins) should exist in the LLBL predicate
            //~ tblBranchHeadObject -> tblObjectRevision
            //~ tblObjectRevision -> tblReltionAttributeValue[1] : CustomerAlias
            //~ tblObjectRevision -> tblReltionAttributeValue[1] : CarrierAlias

            Assert.AreEqual(3, llblPredicateBucket.Relations.Count);

            var relationAttributeValueJoin1 = llblPredicateBucket.Relations[1];
            var relationAttributeValueJoin2 = llblPredicateBucket.Relations[2];

            //~ joins on the attribute value tables should be of type LEFT JOIN

            Assert.AreEqual(JoinHint.Left, relationAttributeValueJoin1.JoinType);
            Assert.AreEqual(JoinHint.Left, relationAttributeValueJoin2.JoinType);
        }

        [TestMethod]
        public void Convert_Uses_N_Left_Joins_For_N_RelationInPredicates()
        {
            var adapter = new EcomPersistenceAdapter();
            Core.Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var filterBucket = new PredicateBucket<Order>(9);
            filterBucket.AddRelationInPredicate(m => m.CustomerId, new[] { 4, 5, 6 });
            filterBucket.AddRelationInPredicate(m => m.CarrierId, new[] { 7, 8, 9 });

            var llblPredicateBucket = PredicateTranslator.Convert(filterBucket, EavContextType.Branch);

            //~ three relations (joins) should exist in the LLBL predicate
            //~ tblBranchHeadObject -> tblObjectRevision
            //~ tblObjectRevision -> tblReltionAttributeValue[1] : CustomerAlias
            //~ tblObjectRevision -> tblReltionAttributeValue[1] : CarrierAlias

            Assert.AreEqual(3, llblPredicateBucket.Relations.Count);

            var relationAttributeValueJoin1 = llblPredicateBucket.Relations[1];
            var relationAttributeValueJoin2 = llblPredicateBucket.Relations[2];

            //~ joins on the attribute value tables should be of type LEFT JOIN

            Assert.AreEqual(JoinHint.Left, relationAttributeValueJoin1.JoinType);
            Assert.AreEqual(JoinHint.Left, relationAttributeValueJoin2.JoinType);
        }

        [TestMethod]
        public void Convert_Uses_N_Left_Joins_For_N_AttributeComparePredicates()
        {
            var adapter = new EcomPersistenceAdapter();
            Core.Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var filterBucket = new PredicateBucket<Customer>(9);
            filterBucket.AddAttributeComparePredicate(m => m.Birthday, new DateTime(2000,1,1), Core.ComparisonOperator.GreaterThan);
            filterBucket.AddAttributeComparePredicate(m => m.Birthday, new DateTime(2010, 1, 1), Core.ComparisonOperator.LessThan);

            var llblPredicateBucket = PredicateTranslator.Convert(filterBucket, EavContextType.Branch);

            //~ three relations (joins) should exist in the LLBL predicate
            //~ tblBranchHeadObject -> tblObjectRevision
            //~ tblObjectRevision -> tblAttributeValue[1] : Alias1
            //~ tblObjectRevision -> tblAttributeValue[1] : Alias2

            Assert.AreEqual(3, llblPredicateBucket.Relations.Count);

            var attributeValueJoin1 = llblPredicateBucket.Relations[1];
            var attributeValueJoin2 = llblPredicateBucket.Relations[2];

            //~ joins on the attribute value tables should be of type LEFT JOIN

            Assert.AreEqual(JoinHint.Left, attributeValueJoin1.JoinType);
            Assert.AreEqual(JoinHint.Left, attributeValueJoin2.JoinType);
        }

        [TestMethod]
        public void Convert_Uses_Formatted_Object_Alias_For_RelationPredicates()
        {
            var adapter = new EcomPersistenceAdapter();
            Core.Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var filterBucket = new PredicateBucket<Order>(9);
            filterBucket.AddRelationPredicate(m => m.CustomerId, 4);
            filterBucket.AddRelationPredicate(m => m.CarrierId, 4);

            var llblPredicateBucket = PredicateTranslator.Convert(filterBucket, EavContextType.Branch);

            var relationAttributeValueJoin1 = llblPredicateBucket.Relations[1];
            var relationAttributeValueJoin2 = llblPredicateBucket.Relations[2];

            Assert.AreEqual(string.Format("Relation_{0}", (int)RelationshipMetadata.Order_CustomerId), relationAttributeValueJoin1.AliasRightOperand);
            Assert.AreEqual(string.Format("Relation_{0}", (int)RelationshipMetadata.Order_CarrierId), relationAttributeValueJoin2.AliasRightOperand);
        }

        [TestMethod]
        public void Convert_Uses_Formatted_Object_Alias_For_RelationInPredicates()
        {
            var adapter = new EcomPersistenceAdapter();
            Core.Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var filterBucket = new PredicateBucket<Order>(9);
            filterBucket.AddRelationInPredicate(m => m.CustomerId, new[] { 4, 5, 6 });
            filterBucket.AddRelationInPredicate(m => m.CarrierId, new[] { 7, 8, 9 });

            var llblPredicateBucket = PredicateTranslator.Convert(filterBucket, EavContextType.Branch);

            var relationAttributeValueJoin1 = llblPredicateBucket.Relations[1];
            var relationAttributeValueJoin2 = llblPredicateBucket.Relations[2];

            Assert.AreEqual(string.Format("RelationIn_{0}", (int)RelationshipMetadata.Order_CustomerId), relationAttributeValueJoin1.AliasRightOperand);
            Assert.AreEqual(string.Format("RelationIn_{0}", (int)RelationshipMetadata.Order_CarrierId), relationAttributeValueJoin2.AliasRightOperand);
        }

        [TestMethod]
        public void Convert_Uses_Formatted_Object_Alias_For_AttributeComparePredicates()
        {
            var adapter = new EcomPersistenceAdapter();
            Core.Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var filterBucket = new PredicateBucket<Customer>(9);
            filterBucket.AddAttributeComparePredicate(m => m.Birthday, new DateTime(2000, 1, 1), Core.ComparisonOperator.GreaterThan);
            filterBucket.AddAttributeComparePredicate(m => m.Birthday, new DateTime(2010, 1, 1), Core.ComparisonOperator.LessThan);

            var llblPredicateBucket = PredicateTranslator.Convert(filterBucket, EavContextType.Branch);

            var attributeValueJoin1 = llblPredicateBucket.Relations[1];
            var attributeValueJoin2 = llblPredicateBucket.Relations[2];

            Assert.AreEqual(string.Format("AttributeCompare_{0}", (int)AttributeMetadata.Customer_Birthday), attributeValueJoin1.AliasRightOperand);
            Assert.AreEqual(string.Format("AttributeCompare_{0}", (int)AttributeMetadata.Customer_Birthday), attributeValueJoin2.AliasRightOperand);
        }

        [TestMethod]
        public void Convert_Uses_N_Left_Joins_For_N_AttributeLikePredicates()
        {
            var adapter = new EcomPersistenceAdapter();
            Core.Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var filterBucket = new PredicateBucket<Customer>(9);
            filterBucket.AddAttributeLikePredicate(m => m.Name, "B", LikeComparisonOperator.BeginsWith);
            filterBucket.AddAttributeLikePredicate(m => m.Name, "en", LikeComparisonOperator.EndsWith);

            var llblPredicateBucket = PredicateTranslator.Convert(filterBucket, EavContextType.Branch);

            //~ three relations (joins) should exist in the LLBL predicate
            //~ tblBranchHeadObject -> tblObjectRevision
            //~ tblObjectRevision -> tblAttributeValue[1] : Alias1
            //~ tblObjectRevision -> tblAttributeValue[1] : Alias2

            Assert.AreEqual(3, llblPredicateBucket.Relations.Count);

            var attributeValueJoin1 = llblPredicateBucket.Relations[1];
            var attributeValueJoin2 = llblPredicateBucket.Relations[2];

            //~ joins on the attribute value tables should be of type LEFT JOIN

            Assert.AreEqual(JoinHint.Left, attributeValueJoin1.JoinType);
            Assert.AreEqual(JoinHint.Left, attributeValueJoin2.JoinType);
        }

        [TestMethod]
        public void Convert_Uses_Formatted_Object_Alias_For_AttributeLikePredicates()
        {
            var adapter = new EcomPersistenceAdapter();
            Core.Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var filterBucket = new PredicateBucket<Customer>(9);
            filterBucket.AddAttributeLikePredicate(m => m.Name, "B", LikeComparisonOperator.BeginsWith);
            filterBucket.AddAttributeLikePredicate(m => m.Name, "en", LikeComparisonOperator.EndsWith);

            var llblPredicateBucket = PredicateTranslator.Convert(filterBucket, EavContextType.Branch);

            var attributeValueJoin1 = llblPredicateBucket.Relations[1];
            var attributeValueJoin2 = llblPredicateBucket.Relations[2];

            Assert.AreEqual(string.Format("AttributeLike_{0}", (int)AttributeMetadata.Customer_Name), attributeValueJoin1.AliasRightOperand);
            Assert.AreEqual(string.Format("AttributeLike_{0}", (int)AttributeMetadata.Customer_Name), attributeValueJoin2.AliasRightOperand);
        }
    }
}
