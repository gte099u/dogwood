﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dogwood.Core;

namespace Dogwood.Test
{
    [EavObject]
    [UniqueConstraint("Customer first names must be unique", "FirstName", "LastName")]
    public partial class CustomerEntity : IDogwoodEntity
    {
        public int Id { get; set; }
        public bool IsNew { get { return Id == 0; } }

        [Mappable]
        public string FirstName { get; set; }

        [Mappable]
        public string LastName { get; set; }

        [Mappable]
        public string Street { get; set; }

        [Mappable]
        public string State { get; set; }

        [Mappable]
        public string Zip { get; set; }

        [Mappable]
        public bool IsMale { get; set; }

        [Mappable]
        public DateTime BirthDate { get; set; }

        [OneToManyRelation]
        public IList<OrderEntity> Orders { get; set; }

        public static PrefetchElement PrefetchOrders
        {
            get
            {
                IEntityMetaData meta = MetadataRegistry.Instance[typeof(CustomerEntity)];
                return new PrefetchElement(meta.OneToManyRelationships[Bootstrapper.RelationshipTag(typeof(CustomerEntity), "Orders")]);
            }
        }

    }


    [EavObject]
    public partial class OrderEntity : IDogwoodEntity
    {
        public int Id { get; set; }
        public bool IsNew { get { return Id == 0; } }

        [Mappable]
        public DateTime OrderedDate { get; set; }

        [Mappable]
        public DateTime ShippedDate { get; set; }

        [Mappable]
        public DateTime DeliveryDate { get; set; }

        [RevisionRelation]
        public int FinalizedRevisionId { get; set; }

        [ManyToOneRelation]
        public int CustomerId { get; set; }
        public CustomerEntity Customer { get; set; }

        public static PrefetchElement PrefetchCustomer
        {
            get
            {
                IEntityMetaData meta = MetadataRegistry.Instance[typeof(OrderEntity)];
                return new PrefetchElement(meta.ManyToOneRelationships[Bootstrapper.RelationshipTag(typeof(OrderEntity), "Customer")]);
            }
        }


        [OneToManyRelation]
        public IList<OrderItemEntity> OrderItems { get; set; }

        public static PrefetchElement PrefetchOrderItems
        {
            get
            {
                IEntityMetaData meta = MetadataRegistry.Instance[typeof(OrderEntity)];
                return new PrefetchElement(meta.OneToManyRelationships[Bootstrapper.RelationshipTag(typeof(OrderEntity), "OrderItems")]);
            }
        }

    }


    [EavObject]
    public partial class OrderItemEntity : IDogwoodEntity
    {
        public int Id { get; set; }
        public bool IsNew { get { return Id == 0; } }

        [Mappable]
        public int Count { get; set; }

        [ManyToOneRelation]
        public int OrderId { get; set; }
        public OrderEntity Order { get; set; }

        public static PrefetchElement PrefetchOrder
        {
            get
            {
                IEntityMetaData meta = MetadataRegistry.Instance[typeof(OrderItemEntity)];
                return new PrefetchElement(meta.ManyToOneRelationships[Bootstrapper.RelationshipTag(typeof(OrderItemEntity), "Order")]);
            }
        }


        [ManyToOneRelation]
        public int ProductId { get; set; }
        public ProductEntity Product { get; set; }

        public static PrefetchElement PrefetchProduct
        {
            get
            {
                IEntityMetaData meta = MetadataRegistry.Instance[typeof(OrderItemEntity)];
                return new PrefetchElement(meta.ManyToOneRelationships[Bootstrapper.RelationshipTag(typeof(OrderItemEntity), "Product")]);
            }
        }

    }


    [EavObject]
    public partial class ProductEntity : IDogwoodEntity
    {
        public int Id { get; set; }
        public bool IsNew { get { return Id == 0; } }

        [Mappable]
        public string Name { get; set; }

        [Mappable]
        public bool IsAvailable { get; set; }

        [Mappable]
        public int Inventory { get; set; }

        [Mappable]
        public long UnitPrice { get; set; }

        [OneToManyRelation]
        public IList<OrderItemEntity> OrderItems { get; set; }

        public static PrefetchElement PrefetchOrderItems
        {
            get
            {
                IEntityMetaData meta = MetadataRegistry.Instance[typeof(ProductEntity)];
                return new PrefetchElement(meta.OneToManyRelationships[Bootstrapper.RelationshipTag(typeof(ProductEntity), "OrderItems")]);
            }
        }

    } 


}
