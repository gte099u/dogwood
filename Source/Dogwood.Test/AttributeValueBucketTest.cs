﻿using Dogwood.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Dogwood.Core.Entities;
using System.Collections.Generic;

namespace Dogwood.Test
{
    
    
    /// <summary>
    ///This is a test class for AttributeValueBucketTest and is intended
    ///to contain all AttributeValueBucketTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AttributeValueBucketTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AttributeValueBucket Constructor
        ///</summary>
        [TestMethod()]
        public void AttributeValueBucketConstructorTest()
        {
            ObjectRevisionEntity objRevision = new ObjectRevisionEntity();
            objRevision.BoolAttributeValues = new List<BoolAttributeValueEntity>();
            objRevision.BoolAttributeValues.Add(new BoolAttributeValueEntity()
            {
                Id = 1,
                BoolMetadataId = 1,
                ObjectRevisionId = 1,
                Value = true
            });
            objRevision.DateTimeAttributeValues = new List<DateTimeAttributeValueEntity>();
            objRevision.ForeignKeyAttributeValues = new List<ForeignKeyAttributeValueEntity>();
            objRevision.IntegerAttributeValues = new List<IntegerAttributeValueEntity>();
            objRevision.LongAttributeValues = new List<LongAttributeValueEntity>();
            objRevision.RevisionAttributeValues = new List<RevisionAttributeValueEntity>();
            objRevision.StringAttributeValues = new List<StringAttributeValueEntity>();
            AttributeValueBucket target = new AttributeValueBucket(objRevision);
            Assert.AreEqual(target.BoolAttributes[1], objRevision.BoolAttributeValues[0].Value); 
        }

        /// <summary>
        ///A test for AttributeValueBucket Constructor
        ///</summary>
        [TestMethod()]
        public void AttributeValueBucketConstructorTest1()
        {
            Dictionary<int, string> stringAttributes = null; // TODO: Initialize to an appropriate value
            Dictionary<int, int> foreignKeys = null; // TODO: Initialize to an appropriate value
            Dictionary<int, bool> boolAttributes = null; // TODO: Initialize to an appropriate value
            Dictionary<int, long> longAttributes = null; // TODO: Initialize to an appropriate value
            Dictionary<int, int> intAttributes = null; // TODO: Initialize to an appropriate value
            Dictionary<int, int> revAttributes = null; // TODO: Initialize to an appropriate value
            Dictionary<int, DateTime> dtAttributes = null; // TODO: Initialize to an appropriate value
            AttributeValueBucket target = new AttributeValueBucket(stringAttributes, foreignKeys, boolAttributes, longAttributes, intAttributes, revAttributes, dtAttributes);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BoolAttributes
        ///</summary>
        [TestMethod()]
        public void BoolAttributesTest()
        {
            ObjectRevisionEntity objRevision = null; // TODO: Initialize to an appropriate value
            AttributeValueBucket target = new AttributeValueBucket(objRevision); // TODO: Initialize to an appropriate value
            Dictionary<int, bool> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<int, bool> actual;
            target.BoolAttributes = expected;
            actual = target.BoolAttributes;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DateTimeAttributes
        ///</summary>
        [TestMethod()]
        public void DateTimeAttributesTest()
        {
            ObjectRevisionEntity objRevision = null; // TODO: Initialize to an appropriate value
            AttributeValueBucket target = new AttributeValueBucket(objRevision); // TODO: Initialize to an appropriate value
            Dictionary<int, DateTime> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<int, DateTime> actual;
            target.DateTimeAttributes = expected;
            actual = target.DateTimeAttributes;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for FkAttributes
        ///</summary>
        [TestMethod()]
        public void FkAttributesTest()
        {
            ObjectRevisionEntity objRevision = null; // TODO: Initialize to an appropriate value
            AttributeValueBucket target = new AttributeValueBucket(objRevision); // TODO: Initialize to an appropriate value
            Dictionary<int, int> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<int, int> actual;
            target.FkAttributes = expected;
            actual = target.FkAttributes;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for IntegerAttributes
        ///</summary>
        [TestMethod()]
        public void IntegerAttributesTest()
        {
            ObjectRevisionEntity objRevision = null; // TODO: Initialize to an appropriate value
            AttributeValueBucket target = new AttributeValueBucket(objRevision); // TODO: Initialize to an appropriate value
            Dictionary<int, int> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<int, int> actual;
            target.IntegerAttributes = expected;
            actual = target.IntegerAttributes;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for LongAttributes
        ///</summary>
        [TestMethod()]
        public void LongAttributesTest()
        {
            ObjectRevisionEntity objRevision = null; // TODO: Initialize to an appropriate value
            AttributeValueBucket target = new AttributeValueBucket(objRevision); // TODO: Initialize to an appropriate value
            Dictionary<int, long> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<int, long> actual;
            target.LongAttributes = expected;
            actual = target.LongAttributes;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for RevisionAttributes
        ///</summary>
        [TestMethod()]
        public void RevisionAttributesTest()
        {
            ObjectRevisionEntity objRevision = null; // TODO: Initialize to an appropriate value
            AttributeValueBucket target = new AttributeValueBucket(objRevision); // TODO: Initialize to an appropriate value
            Dictionary<int, int> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<int, int> actual;
            target.RevisionAttributes = expected;
            actual = target.RevisionAttributes;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for StringAttributes
        ///</summary>
        [TestMethod()]
        public void StringAttributesTest()
        {
            ObjectRevisionEntity objRevision = null; // TODO: Initialize to an appropriate value
            AttributeValueBucket target = new AttributeValueBucket(objRevision); // TODO: Initialize to an appropriate value
            Dictionary<int, string> expected = null; // TODO: Initialize to an appropriate value
            Dictionary<int, string> actual;
            target.StringAttributes = expected;
            actual = target.StringAttributes;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
