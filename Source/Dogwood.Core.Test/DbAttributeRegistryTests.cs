﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class DbAttributeRegistryTests
    {
        public DbAttributeRegistryTests()
        {
            DbAttributeRegistry.Instance.Clear();

            if (!DbAttributeRegistry.Instance.ContainsKey(99))
                DbAttributeRegistry.Instance.Add(99, "Name");
            if (!DbAttributeRegistry.Instance.ContainsKey(44))
                DbAttributeRegistry.Instance.Add(44, "Height");
            if (!DbAttributeRegistry.Instance.ContainsKey(22))
                DbAttributeRegistry.Instance.Add(22, "Weight");
        }

        [TestMethod]
        public void GetKeyReturnsIdOfRegistryEntry()
        {
            int id = DbAttributeRegistry.GetKey("Name");
            Assert.AreEqual(99, id);
        }

        [TestMethod]
        public void GetKeyReturnsThrowsInformativeException()
        {
            bool exceptionThrown = false;
            string message = null;
            try
            {
                DbAttributeRegistry.GetKey("Address");
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                message = ex.Message;
            }

            Assert.AreEqual(true, exceptionThrown);
            Assert.AreEqual(
                "A property named Address was expected in the database attribute table but was not found. Please reconcile any entity metadata specifications and reboot the application",
                message);
        }
    }
}
