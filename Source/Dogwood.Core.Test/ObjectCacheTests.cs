﻿using System;
using System.Collections.Generic;
using Dogwood.Core.Entities;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class ObjectCacheTests
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        [TestMethod]
        public void SetObject_Adds_New_Object()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            var customer1 = new Customer { Id = 1 };

            cache.SetObject(customer1);

            Assert.AreEqual(1, cache.GetObject(1).Id);
        }

        [TestMethod]
        public void SetObject_Does_Not_Store_Null_Object()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            BranchHeadObjectEntity customer1 = null;

            cache.SetHeadObject(customer1);            
        }

        [TestMethod]
        public void SetObject_Updates_Existing_Object()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());

            var cache = new ObjectCache();

            var customer1 = new Customer { Id = 1, Name = "Bob" };

            cache.SetObject(customer1);

            Assert.AreEqual("Bob", ((Customer)cache.GetObject(1)).Name);

            customer1.Name = "Ben";

            cache.SetObject(customer1);

            Assert.AreEqual("Ben", ((Customer)cache.GetObject(1)).Name);
        }

        [TestMethod]
        public void Cache_Is_Not_Saved_By_Reference_On_Add()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());

            var cache = new ObjectCache();

            var customer1 = new Customer { Id = 1, Name = "Bob" };

            cache.SetObject(customer1);

            Assert.AreEqual("Bob", ((Customer)cache.GetObject(1)).Name);

            customer1.Name = "Ben";

            Assert.AreEqual("Bob", ((Customer)cache.GetObject(1)).Name);
        }

        [TestMethod]
        public void Cache_Is_Not_Saved_By_Reference_On_Update()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());

            var cache = new ObjectCache();

            var customer1 = new Customer { Id = 1, Name = "Bob" };

            cache.SetObject(customer1);

            Assert.AreEqual("Bob", ((Customer)cache.GetObject(1)).Name);

            customer1.Name = "Ben";

            cache.SetObject(customer1);

            Assert.AreEqual("Ben", ((Customer)cache.GetObject(1)).Name);

            customer1.Name = "Joe";

            Assert.AreEqual("Ben", ((Customer)cache.GetObject(1)).Name);
        }

        [TestMethod]
        public void SetObject_Updates_All_Classes_On_Update()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            var allCustomers = new Dictionary<int, IDogwoodEntity>
                                   { 
                                      {1, new Customer {Id = 1, Name = "Ben"}} ,
                                      {2, new Customer {Id = 1, Name = "Joe"}},
                                       {3, new Customer {Id = 1, Name = "Jack"}},
                                       {4, new Customer {Id = 1, Name = "Bob"}},
                                   };

            cache.AddClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId, allCustomers);

            var customer1 = new Customer { Id = 1, Name = "Bryan" };

            cache.SetObject(customer1);

            var cust1 = (Customer)cache.GetClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId)[1];

            Assert.AreEqual("Bryan", cust1.Name);
        }

        [TestMethod]
        public void SetObject_Updates_All_Classes_On_Add()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            var allCustomers = new Dictionary<int, IDogwoodEntity>
                                   { 
                                      {1, new Customer {Id = 1, Name = "Ben"}} ,
                                      {2, new Customer {Id = 1, Name = "Joe"}},
                                       {3, new Customer {Id = 1, Name = "Jack"}},
                                       {4, new Customer {Id = 1, Name = "Bob"}},
                                   };

            cache.AddClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId, allCustomers);

            Assert.AreEqual(4, cache.GetClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId).Count);

            var customer5 = new Customer { Id = 5, Name = "Bryan" };

            cache.SetObject(customer5);

            Assert.AreEqual(5, cache.GetClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId).Count);
        }

        [TestMethod]
        public void SetObject_Updates_All_Classes_On_Update_By_Value()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            var allCustomers = new Dictionary<int, IDogwoodEntity>
                                   { 
                                      {1, new Customer {Id = 1, Name = "Ben"}} ,
                                      {2, new Customer {Id = 1, Name = "Joe"}},
                                       {3, new Customer {Id = 1, Name = "Jack"}},
                                       {4, new Customer {Id = 1, Name = "Bob"}},
                                   };

            cache.AddClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId, allCustomers);

            var customer1 = new Customer { Id = 1, Name = "Bryan" };

            cache.SetObject(customer1);

            customer1.Name = "Billy";

            var cust1 = (Customer)cache.GetClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId)[1];

            Assert.AreEqual("Bryan", cust1.Name);
        }

        [TestMethod]
        public void SetObject_Updates_All_Classes_On_Add_By_Value()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            var allCustomers = new Dictionary<int, IDogwoodEntity>
                                { 
                                    {1, new Customer {Id = 1, Name = "Ben"}},
                                    {2, new Customer {Id = 1, Name = "Joe"}},
                                    {3, new Customer {Id = 1, Name = "Jack"}},
                                    {4, new Customer {Id = 1, Name = "Bob"}},
                                };

            cache.AddClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId, allCustomers);

            var customer5 = new Customer { Id = 5, Name = "Bryan" };

            cache.SetObject(customer5);

            customer5.Name = "Billy";

            var cust5 = (Customer)cache.GetClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId)[5];

            Assert.AreEqual("Bryan", cust5.Name);
        }

        [TestMethod]
        public void Remove_Removes_From_Objects()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            cache.SetObject(new Customer { Id = 1, Name = "Ben" });
            cache.SetObject(new Customer { Id = 2, Name = "Joe" });
            cache.SetObject(new Customer { Id = 3, Name = "Jack" });
            cache.SetObject(new Customer { Id = 4, Name = "Bob" });

            Assert.IsTrue(cache.ContainsObject(3));

            cache.Remove(3, MetadataRegistry.Instance[typeof(Customer)].ClassId);

            Assert.IsFalse(cache.ContainsObject(3));
        }

        [TestMethod]
        public void Remove_Removes_From_ClassSets()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            var allCustomers = new Dictionary<int, IDogwoodEntity>
                                { 
                                    {1, new Customer {Id = 1, Name = "Ben"}},
                                    {2, new Customer {Id = 2, Name = "Joe"}},
                                    {3, new Customer {Id = 3, Name = "Jack"}},
                                    {4, new Customer {Id = 4, Name = "Bob"}},
                                };

            cache.AddClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId, allCustomers);

            Assert.AreEqual(4, cache.GetClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId).Count);

            cache.Remove(3, MetadataRegistry.Instance[typeof(Customer)].ClassId);

            Assert.AreEqual(3, cache.GetClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId).Count);
        }

        [TestMethod]
        public void Remove_Clears_HeadObjects()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            cache.SetObject(new Customer { Id = 1, Name = "Ben" });
            cache.SetObject(new Customer { Id = 2, Name = "Joe" });
            cache.SetObject(new Customer { Id = 3, Name = "Jack" });
            cache.SetObject(new Customer { Id = 4, Name = "Bob" });

            cache.SetHeadObject(new Entities.BranchHeadObjectEntity { Id = 1 });
            cache.SetHeadObject(new Entities.BranchHeadObjectEntity { Id = 2 });
            cache.SetHeadObject(new Entities.BranchHeadObjectEntity { Id = 3 });


            Assert.IsTrue(cache.ContainsObject(3));

            Assert.IsTrue(cache.ContainsHeadObject(3));
            Assert.IsTrue(cache.ContainsHeadObject(2));

            cache.Remove(3, MetadataRegistry.Instance[typeof(Customer)].ClassId);

            Assert.IsFalse(cache.ContainsObject(3));

            Assert.IsFalse(cache.ContainsHeadObject(3));
            Assert.IsFalse(cache.ContainsHeadObject(2));
        }

        [TestMethod]
        public void AddClassSet_Adds_Class_Set()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            var allCustomers = new Dictionary<int, IDogwoodEntity>
                                   { 
                                      {1, new Customer {Id = 1, Name = "Ben"}} ,
                                      {2, new Customer {Id = 2, Name = "Joe"}},
                                       {3, new Customer {Id = 3, Name = "Jack"}},
                                       {4, new Customer {Id = 4, Name = "Bob"}},
                                   };

            Assert.IsFalse(cache.ContainsClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId));

            cache.AddClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId, allCustomers);

            Assert.IsTrue(cache.ContainsClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId));
            Assert.AreEqual(4, cache.GetClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId).Count);
        }

        [TestMethod]
        public void AddClassSet_Adds_Class_Set_By_Value()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            var allCustomers = new Dictionary<int, IDogwoodEntity>
                                   { 
                                      {1, new Customer {Id = 1, Name = "Ben"}} ,
                                      {2, new Customer {Id = 2, Name = "Joe"}},
                                       {3, new Customer {Id = 3, Name = "Jack"}},
                                       {4, new Customer {Id = 4, Name = "Bob"}},
                                   };

            cache.AddClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId, allCustomers);

            allCustomers[2].Id = 44;

            var cachedItem = cache.GetClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId)[2];

            Assert.AreEqual(2, cachedItem.Id);
        }

        [TestMethod]
        public void AddClassSet_Adds_Individual_Objects()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            var allCustomers = new Dictionary<int, IDogwoodEntity>
                                   { 
                                      {1, new Customer {Id = 1, Name = "Ben"}} ,
                                      {2, new Customer {Id = 2, Name = "Joe"}},
                                       {3, new Customer {Id = 3, Name = "Jack"}},
                                       {4, new Customer {Id = 4, Name = "Bob"}},
                                   };

            Assert.IsFalse(cache.ContainsObject(1));
            Assert.IsFalse(cache.ContainsObject(2));
            Assert.IsFalse(cache.ContainsObject(3));
            Assert.IsFalse(cache.ContainsObject(4));

            cache.AddClassSet(MetadataRegistry.Instance[typeof(Customer)].ClassId, allCustomers);

            Assert.IsTrue(cache.ContainsObject(1));
            Assert.IsTrue(cache.ContainsObject(2));
            Assert.IsTrue(cache.ContainsObject(3));
            Assert.IsTrue(cache.ContainsObject(4));
        }

        [TestMethod]
        public void Get_Object_Returns_Object_By_Key()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            cache.SetObject(new Customer { Id = 1, Name = "Ben" });
            cache.SetObject(new Customer { Id = 2, Name = "Joe" });
            cache.SetObject(new Customer { Id = 3, Name = "Jack" });
            cache.SetObject(new Customer { Id = 4, Name = "Bob" });

            var customer3 = cache.GetObject(3);

            Assert.AreEqual("Jack", ((Customer)customer3).Name);
        }

        [TestMethod]
        public void Get_Object_Returns_Object_By_Value()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            cache.SetObject(new Customer { Id = 1, Name = "Ben" });
            cache.SetObject(new Customer { Id = 2, Name = "Joe" });
            cache.SetObject(new Customer { Id = 3, Name = "Jack" });
            cache.SetObject(new Customer { Id = 4, Name = "Bob" });

            var customer3 = (Customer)cache.GetObject(3);

            customer3.Name = "Will";

            Assert.AreEqual("Jack", ((Customer)cache.GetObject(3)).Name);
        }

        [TestMethod]
        public void GetRevision_Returns_Revision_By_Key()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            cache.AddRevision(new RevisionEntity { Id = 1 });
            cache.AddRevision(new RevisionEntity { Id = 2 });
            cache.AddRevision(new RevisionEntity { Id = 3 });
            cache.AddRevision(new RevisionEntity { Id = 4 });

            var rev3 = cache.GetRevision(3);

            Assert.AreEqual(3, rev3.Id);
        }

        [TestMethod]
        public void GetRevision_Returns_Revision_By_Value()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            cache.AddRevision(new RevisionEntity { Id = 1 });
            cache.AddRevision(new RevisionEntity { Id = 2 });
            cache.AddRevision(new RevisionEntity { Id = 3 });
            cache.AddRevision(new RevisionEntity { Id = 4 });

            var rev3 = cache.GetRevision(3);

            rev3.Tag = "Will";

            Assert.IsNull(cache.GetRevision(3).Tag);
        }

        [TestMethod]
        public void AddRevision_Adds_Revision()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            cache.AddRevision(new RevisionEntity { Id = 1 });
            cache.AddRevision(new RevisionEntity { Id = 2 });
            cache.AddRevision(new RevisionEntity { Id = 3 });
            cache.AddRevision(new RevisionEntity { Id = 4 });

            var rev3 = cache.GetRevision(3);          

            Assert.AreEqual(3, cache.GetRevision(3).Id);
        }

        [TestMethod]
        public void AddRevision_Adds_Revision_By_Value()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, new EcomPersistenceAdapter());
            var cache = new ObjectCache();

            cache.AddRevision(new RevisionEntity { Id = 1 });
            cache.AddRevision(new RevisionEntity { Id = 2 });
            cache.AddRevision(new RevisionEntity { Id = 3 });

            var rev4 = new RevisionEntity {Id = 4, Tag = "Yes"};

            cache.AddRevision(rev4);

            rev4.Tag = "No";

            Assert.AreEqual("Yes", cache.GetRevision(4).Tag);
        }
    }
}
