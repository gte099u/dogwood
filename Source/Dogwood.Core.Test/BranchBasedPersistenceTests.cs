﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class BranchBasedPersistenceTests
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        static void ThrowException(object sender, EventArgs e)
        {
            throw new Exception("KAAABOOOOM!!");
        }

        [TestMethod]
        public void CreateRevisions_Returns_Id_Of_New_Revision()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                               adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            int revId = per.CreateRevision();

            Assert.AreEqual(EcomPersistenceAdapter.INITIAL_REVISION_COUNT, revId);
        }

        [TestMethod]
        public void CreateRevisions_Sets_Branch_Id_On_New_Revision()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                               adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            per.CreateRevision();

            Assert.AreEqual(1, adapter.GetRevision(2).BranchId);
        }

        [TestMethod]
        public void CreateRevisions_Sets_CreatedBy_On_New_Revision()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                               adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            per.CreateRevision();

            Assert.AreEqual(99, adapter.GetRevision(EcomPersistenceAdapter.INITIAL_REVISION_COUNT).CreatedBy);
        }

        [TestMethod]
        public void CreateRevisions_Sets_Null_Comments_and_Tag_By_Default()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                               adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            per.CreateRevision();

            Assert.IsNull(adapter.GetRevision(2).Comments);
            Assert.IsNull(adapter.GetRevision(2).Tag);
        }

        [TestMethod]
        public void CreateRevisions_Sets_Timestamp_On_New_Revision()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                               adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            per.CreateRevision();
            var ticks = DateTime.Now.Ticks -adapter.GetRevision(EcomPersistenceAdapter.INITIAL_REVISION_COUNT).TimeStamp.Ticks;
            Assert.IsTrue(ticks <= 1000000);
        }

        [TestMethod]
        public void CreateRevisions_Sets_Tag_On_New_Revision()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                               adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            per.CreateRevision("tag1", "comment1");

            Assert.AreEqual("tag1", adapter.GetRevision(EcomPersistenceAdapter.INITIAL_REVISION_COUNT).Tag);
        }

        [TestMethod]
        public void CreateRevisions_Sets_Comments_On_New_Revision()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                               adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            per.CreateRevision("tag1", "comment1");

            Assert.AreEqual("comment1", adapter.GetRevision(EcomPersistenceAdapter.INITIAL_REVISION_COUNT).Comments);
        }

        [TestMethod]
        public void Save_Returns_Change_Data_With_Expected_Values()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            var toSave = new Customer
                             {
                                 Birthday = DateTime.Now,
                                 HeightInInches = 60,
                                 IsMale = true,
                                 Name = "Bob"
                             };

            var changeData = per.Save(toSave, "saving a Customer", true);

            Assert.AreEqual(1, changeData.ObjectId);
            Assert.AreEqual(EcomPersistenceAdapter.INITIAL_REVISION_COUNT, changeData.RevisionId);
            Assert.AreEqual(1, changeData.ObjectRevisionId);
        }

        [TestMethod]
        public void Save_Throws_Exception_If_Transaction_Opens_Twice()
        {
            var adapter = new EcomPersistenceAdapter();
            adapter.StartTransaction(IsolationLevel.ReadCommitted, "Opening a transaction"); //~ opening a transaction

            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            var toSave = new Customer
            {
                Birthday = DateTime.Now,
                HeightInInches = 60,
                IsMale = true,
                Name = "Bob"
            };

            bool exceptionThrown = false;
            string exceptionMessage = string.Empty;

            try
            {
                per.Save(toSave, "saving a user", true); //~ new transaction will be opened again here
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                exceptionMessage = ex.Message;
            }
            
            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual("Unable to execute this action because a transaction is already open", exceptionMessage);
        }

        [TestMethod]
        public void Save_Rolls_Back_When_Exception_Thrown()
        {
            var adapter = new EcomPersistenceAdapter();

            adapter._branchHeadObjects.AfterAdd += ThrowException;

            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            var toSave = new Customer
            {
                Birthday = DateTime.Now,
                HeightInInches = 60,
                IsMale = true,
                Name = "Bob"
            };

            int revisionCount = adapter._revisions.Count(); //~ count of revisions before save
            int objectCount = adapter._objects.Count(); //~ count of objects before save

            bool exceptionThrown = false;

            try
            {               
                per.Save(toSave, "saving a user", true);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual(revisionCount, adapter._revisions.Count());
            Assert.AreEqual(objectCount, adapter._objects.Count());

            adapter._branchHeadObjects.AfterAdd -= ThrowException;
        }

        [TestMethod]
        public void Delete_Throws_Exception_If_Transaction_Opens_Twice()
        {
            var adapter = new EcomPersistenceAdapter();
            adapter.StartTransaction(IsolationLevel.ReadCommitted, "Opening a transaction"); //~ opening a transaction

            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));           

            bool exceptionThrown = false;
            string exceptionMessage = string.Empty;

            try
            {
                per.Delete(1); //~ new transaction will be opened again here
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                exceptionMessage = ex.Message;
            }

            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual("Unable to execute this action because a transaction is already open", exceptionMessage);
        }

        [TestMethod]
        public void Delete_Records_New_Revision()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            var toSave = new Customer
            {
                Birthday = DateTime.Now,
                HeightInInches = 60,
                IsMale = true,
                Name = "Bob"
            };

            Assert.AreEqual(EcomPersistenceAdapter.INITIAL_REVISION_COUNT, adapter._revisions.Count);

            var changeData = per.Save(toSave, "saving a Customer", true);

            Assert.AreEqual(EcomPersistenceAdapter.INITIAL_REVISION_COUNT+1, adapter._revisions.Count);

            per.Delete(changeData.ObjectId);

            Assert.AreEqual(EcomPersistenceAdapter.INITIAL_REVISION_COUNT+2, adapter._revisions.Count);
        }

        [TestMethod]
        public void Delete_Saves_An_Object_Revision_Flagged_As_Deleted()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            var toSave = new Customer
            {
                Birthday = DateTime.Now,
                HeightInInches = 60,
                IsMale = true,
                Name = "Bob"
            };

            var changeData = per.Save(toSave, "saving a Customer", true);

            Assert.AreEqual(1, adapter._objectRevisions.Count);

            per.Delete(changeData.ObjectId);

            Assert.AreEqual(2, adapter._objectRevisions.Count);

            Assert.IsTrue(adapter._objectRevisions[2].IsDeleted);
        }

        [TestMethod]
        public void Delete_Rolls_Back_When_Exception_Thrown()
        {
            var adapter = new EcomPersistenceAdapter();

            adapter._branchHeadObjects.BeforeRemove += ThrowException;

            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            var toSave = new Customer
            {
                Birthday = DateTime.Now,
                HeightInInches = 60,
                IsMale = true,
                Name = "Bob"
            };

            per.Save(toSave, "saving a user", true);

            int revisionCount = adapter._revisions.Count(); //~ count of revisions before delete
            int objectCount = adapter._objects.Count(); //~ count of objects before delete

            bool exceptionThrown = false;

            try
            {
                per.Delete(1);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual(revisionCount, adapter._revisions.Count());
            Assert.AreEqual(objectCount, adapter._objects.Count());

            adapter._branchHeadObjects.AfterAdd -= ThrowException;
        }

        [TestMethod]
        public void DestroyObject_Deletes_The_Object()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            var toSave = new Customer
            {
                Birthday = DateTime.Now,
                HeightInInches = 60,
                IsMale = true,
                Name = "Bob"
            };

            var changeData = per.Save(toSave, "saving a Customer", true);

            Assert.AreEqual(1, adapter._objects.Count);

            per.DestroyObject(changeData.ObjectId);

            Assert.AreEqual(0, adapter._objects.Count);
        }

        [TestMethod]
        public void DestroyObject_Purges_Orphan_Revision()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var per = new BranchBasedPersistence<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            var toSave = new Customer
            {
                Birthday = DateTime.Now,
                HeightInInches = 60,
                IsMale = true,
                Name = "Bob"
            };

            var changeData = per.Save(toSave, "saving a Customer", true);

            Assert.AreEqual(4, adapter._revisions.Count);

            per.DestroyObject(changeData.ObjectId);

            Assert.AreEqual(2, adapter._revisions.Count);
        }
    }
}
