﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class ExpressionHelperTests
    {
        [TestMethod]
        public void GetPropertyInfo_Throws_Exception_If_Not_MemberExpression()
        {
            bool exceptionThrown = false;

            try
            {
                var result = ExpressionHelper.GetPropertyInfo<Customer, bool>(m => false);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown);
        }

        [TestMethod]
        public void GetPropertyInfo_Throws_Exception_If_Not_A_Property()
        {
            bool exceptionThrown = false;

            try
            {
                var result = ExpressionHelper.GetPropertyInfo<Customer, bool>(m => m.TestField);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown);
        }

        [TestMethod]
        public void GetPropertyInfo_Returns_PropertyInfo()
        {
            var result = ExpressionHelper.GetPropertyInfo<Customer, string>(m => m.Name);

            Assert.AreEqual(result, typeof(Customer).GetProperty("Name"));
        }
    }
}
