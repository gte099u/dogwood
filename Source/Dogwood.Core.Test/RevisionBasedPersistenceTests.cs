﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class RevisionBasedPersistenceTests
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        [TestMethod]
        public void Delete_Removes_Object_From_RevisionHeadObjects()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                               adapter);

            var toSave = new Customer
                            {
                                Birthday = new DateTime(2000, 12, 1),
                                HeightInInches = 9,
                                IsMale = true,
                                Name = "Bob",
                                Nickname = "Jim Bob"
                            };

            var rootContext = new RootDataContext(adapter, 99);

            var custRepo = new ObjectRepository<Customer>(rootContext);

            custRepo.Save(toSave);

            custRepo = new ObjectRepository<Customer>(rootContext);
            custRepo.Get(toSave.Id);

            Assert.AreEqual(EcomPersistenceAdapter.INITIAL_BRANCH_COUNT, adapter._branchHeadObjects.Count);
            Assert.AreEqual(1, adapter._revisionHeadObjects.Count);

            var per = new RevisionBasedPersistence<Customer>(rootContext);

            per.Delete(toSave.Id);

            Assert.AreEqual(0, adapter._branchHeadObjects.Count);
            Assert.AreEqual(0, adapter._revisionHeadObjects.Count);
        }

        [TestMethod]
        public void Save_Sets_Id_On_New_Object()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                               adapter);

            var toSave = new Customer
            {
                Birthday = new DateTime(2000, 12, 1),
                HeightInInches = 9,
                IsMale = true,
                Name = "Bob",
                Nickname = "Jim Bob"
            };

            var rootContext = new RootDataContext(adapter, 99);

            var custRepo = new ObjectRepository<Customer>(rootContext);

            Assert.AreEqual(0, toSave.Id);

            custRepo.Save(toSave);

            Assert.AreEqual(1, toSave.Id);
        }

        [TestMethod]
        public void Create_Root_Object_Adds_To_RevisionHeadObjects_For_Each_Revision()
        {
            #region Initialization
            //~ initializing the database to have 3 branches
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                               adapter);

            var toSave = new Customer
            {
                Birthday = new DateTime(2000, 12, 1),
                HeightInInches = 9,
                IsMale = true,
                Name = "Bob",
                Nickname = "Jim Bob"
            };

            var rootContext = new RootDataContext(adapter, 99);
            var custRepo = new ObjectRepository<Customer>(rootContext);
            custRepo.Save(toSave); //~ object saved at root

            adapter.AddRevisionToHeadObjects(0, 0);
            Assert.AreEqual(1, adapter._revisionHeadObjects.Count);

            adapter.FillHeadObjects(); //~ index existing branches (0 and 1)
            Assert.AreEqual(2, adapter._branchHeadObjects.Count);

            var branch1Context = new DogwoodBranchContext(adapter, 1, 99);
            var branch1Repo = new ObjectRepository<Customer>(branch1Context);

            toSave.Name = "Jack";
            branch1Repo.Save(toSave); //~ +1 revision
            Assert.AreEqual(EcomPersistenceAdapter.INITIAL_REVISION_COUNT + 1, adapter._revisions.Count);

            var branchService = new BranchingService(adapter);
            branchService.CreateBranch(2, 99, "Branching to Branch 2"); //~ +1 revision
            Assert.AreEqual(EcomPersistenceAdapter.INITIAL_REVISION_COUNT + 2, adapter._revisions.Count);

            var branch2Context = new DogwoodBranchContext(adapter, 2, 99);
            var branch2Repo = new ObjectRepository<Customer>(branch2Context);

            toSave.Name = "Steve";
            branch2Repo.Save(toSave); //~ +1 revision
            Assert.AreEqual(EcomPersistenceAdapter.INITIAL_REVISION_COUNT + 3, adapter._revisions.Count);

            var revision3Context = new DogwoodDataContext(adapter, 3, 99, EavContextType.Revision);
            var revision3Repo = new ObjectRepository<Customer>(revision3Context); //~ indexes revision 3
            Assert.AreEqual(2, adapter._revisionHeadObjects.Count);

            #endregion

            //~ at this point, there are branches 0, 1, and 2
            var rootContext2 = new RootDataContext(adapter, 99);
            var custRepo2 = new ObjectRepository<Customer>(rootContext2);

            var newRootCustomer = new Customer
            {
                Birthday = new DateTime(2000, 12, 2),
                HeightInInches = 9,
                IsMale = true,
                Name = "Bill"
            };

            custRepo2.Save(newRootCustomer);
            Assert.AreEqual(4, adapter._revisionHeadObjects.Count); //~ 2 records were added
        }
    }
}
