﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class DataConverterTests
    {
        [TestMethod]
        public void BytesToBool()
        {
            byte[] binary = BitConverter.GetBytes(false);
            bool result = DataConverter.BytesToBool(binary);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void BytesToChar()
        {
            byte[] binary = BitConverter.GetBytes('P');
            char result = DataConverter.BytesToChar(binary);
            Assert.AreEqual('P', result);
        }

        [TestMethod]
        public void BytesToDateTime()
        {
            byte[] binary = BitConverter.GetBytes(new DateTime(2011, 10, 11).Ticks).Reverse().ToArray();
            DateTime result = DataConverter.BytesToDateTime(binary);
            Assert.AreEqual(new DateTime(2011, 10, 11), result);
        }

        [TestMethod]
        public void BytesToInt16()
        {
            byte[] binary = BitConverter.GetBytes(30000).Reverse().ToArray();
            short result = DataConverter.BytesToInt16(binary);
            Assert.AreEqual(30000, result);
        }

        [TestMethod]
        public void BytesToInt32()
        {
            byte[] binary = BitConverter.GetBytes(165498151).Reverse().ToArray();
            int result = DataConverter.BytesToInt32(binary);
            Assert.AreEqual(165498151, result);
        }

        [TestMethod]
        public void BytesToInt64()
        {
            byte[] binary = BitConverter.GetBytes(165491128151).Reverse().ToArray();
            long result = DataConverter.BytesToInt64(binary);
            Assert.AreEqual(165491128151, result);
        }

        [TestMethod]
        public void BytesToString()
        {
            byte[] binary = System.Text.Encoding.UTF8.GetBytes("Ben Elder");
            string result = DataConverter.BytesToString(binary);
            Assert.AreEqual("Ben Elder", result);
        }

        [TestMethod]
        public void CharToBytes()
        {
            byte[] binary = DataConverter.CharToBytes('R');
            char result = BitConverter.ToChar(binary, 0);
            Assert.AreEqual('R', result);
        }

        [TestMethod]
        public void ConvertToBytes_FromBool()
        {
            byte[] binary = DataConverter.ConvertToBytes(false, DogwoodDataType.Bool);
            bool result = BitConverter.ToBoolean(binary, 0);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void ConvertToBytes_FromString()
        {
            byte[] binary = DataConverter.ConvertToBytes("Hello World", DogwoodDataType.String);
            string result = System.Text.Encoding.UTF8.GetString(binary);
            Assert.AreEqual("Hello World", result);
        }
    }
}
