﻿using System;

namespace Dogwood.Core.Test.Data
{
    /// <summary>
    /// This class is set up with relation to Department, but Dept does not have list of Employees for testing purposes
    /// </summary>
    [Serializable]
    public class Employee : DogwoodEntityBase
    {
        [ManyToOneRelation]
        public int DepartmentId { get; set; }
        public Department Department { get; set; }        
    }
}
