﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dogwood.Core.Test.Data
{
    [Serializable]
    [UniqueConstraint("Name must be unique", "Name")]
    [UniqueConstraint("Unit Price must be unique", "UnitPrice")]
    [UniqueConstraint("Quantity must be unique", "Quantity")]
    [UniqueConstraint("Gender must be unique", "IsMale")]
    [UniqueConstraint("Birthday must be unique", "Birthday")]
    [UniqueConstraint("Phone, Ship Date, and Units In Stock must be unique", "Phone", "UnitsInStock", "ShippedDate")]
    public class HyperUniqueClass : DogwoodEntityBase
    {
        [Mappable]
        public string Name { get; set; }

        [ManyToOneRelation]
        public int ProductId { get; set; }
        public Product Product { get; set; }

        [Mappable]
        public bool IsMale { get; set; }

        [Mappable]
        public DateTime Birthday { get; set; }

        [Mappable]
        public long Quantity { get; set; }

        [Mappable]
        public int? UnitPrice { get; set; }

        [Mappable]
        public string Phone { get; set; }

        [Mappable]
        public int UnitsInStock { get; set; }

        [Mappable]
        public DateTime ShippedDate { get; set; }
    }
}
