﻿using System;
using System.Collections.Generic;

namespace Dogwood.Core.Test.Data
{
    [Serializable]
    public class Product : DogwoodEntityBase
    {
        [Mappable]
        public string Name { get; set; }

        [Mappable]
        public long UnitPrice { get; set; }

        [Mappable]
        public int UnitsInStock { get; set; }

        [Mappable]
        public bool IsDiscontinued { get; set; }
    }
}
