﻿namespace Dogwood.Core.Test.Data
{
    public enum Attributes
    {
        Name = 1,
        UnitPrice = 2,
        UnitsInStock = 3,
        IsDiscontinued = 4,
        OrderId = 5,
        ProductId = 6,
        Quantity = 7,
        CustomerId = 8,
        OrderDate = 9,
        ShippedDate = 10,
        CarrierId = 11,
        Phone = 12,
        IsPreferred = 13,
        IsMale = 14,
        Birthday = 15,
        Nickname = 16,
        DepartmentId = 17
    }

    public enum Classes
    {
        Customer = 1,
        Order = 2,
        OrderLine = 3,
        Product = 4,
        Shipper = 5,
        HyperUniqueClass = 6,
        Employee = 7,
        Department = 8
    }

    public enum AttributeMetadata
    {
        Product_Name = 1,
        Product_UnitPrice = 2,
        Product_UnitsInStock = 3,
        Product_IsDiscontinued = 4,
        OrderLine_Quantity = 5,
        Order_OrderDate = 6,
        Order_ShippedDate = 7,
        Shipper_Name = 8,
        Shipper_Phone = 9,
        Shipper_IsPrefered = 10,
        Customer_Name = 11,
        Customer_IsMale = 12,
        Customer_Birthday = 13,
        Customer_NickName = 14,
        HyperUniqueClass_Name = 15,
        HyperUniqueClass_IsMale = 16,
        HyperUniqueClass_Birthday = 17,
        HyperUniqueClass_Quantity = 18,
        HyperUniqueClass_UnitPrice = 19,
        HyperUniqueClass_Phone = 20,
        HyperUniqueClass_UnitsInStock = 21,
        HyperUniqueClass_ShippedDate = 22
    }

    public enum RelationshipMetadata
    {
        OrderLine_OrderId = 1,
        OrderLine_ProductId = 2,
        Order_CustomerId = 3,
        Order_CarrierId = 4,
        HyperUniqueClass_ProductId = 5,
        Employee_DepartmentId = 6
    }
}
