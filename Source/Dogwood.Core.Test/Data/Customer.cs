﻿using System;
using System.Collections.Generic;

namespace Dogwood.Core.Test.Data
{
    [Serializable]
    [UniqueConstraint("Name must be unique", "Name")]
    [UniqueConstraint("Birthday must be unique", "Birthday")] //~ to test nullable uniqueness
    public class Customer : DogwoodEntityBase
    {
        public bool TestField; //~ dummy field for testing.  has no meaning

        [Mappable]
        public string Name { get; set; }

        [Mappable]
        public string Nickname { get; set; }

        [Mappable]
        public bool IsMale { get; set; }
   
        // unmapped attribute
        public int HeightInInches { get; set; }

        [Mappable]
        public DateTime? Birthday { get; set; }

        [OneToManyRelation]
        public IList<Order> Orders { get; set; }

        // unmapped attribute of type IList
        public IList<string> Addresses { get; set; }
    }
}
