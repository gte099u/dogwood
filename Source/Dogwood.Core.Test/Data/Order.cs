﻿using System;

namespace Dogwood.Core.Test.Data
{
    [Serializable]
    public class Order : DogwoodEntityBase
    {
        [ManyToOneRelation]
        public int? CustomerId { get; set; }  //~ nullable only for testing purposes, Dogwood normally would generate this as "int"

        public Customer Customer { get; set; }

        [Mappable]
        public DateTime OrderDate { get; set; }

        [Mappable]
        public DateTime ShippedDate { get; set; }

        [ManyToOneRelation((int)Classes.Shipper)]
        public int? CarrierId { get; set; } //~ anti-convention property name

        public Shipper Carrier { get; set; } //~ anti-convention property name

    }
}
