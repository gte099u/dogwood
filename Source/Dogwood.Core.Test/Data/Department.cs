﻿using System;

namespace Dogwood.Core.Test.Data
{
    /// <summary>
    /// This class is set up with no relation to Employee for testing purposes
    /// </summary>
    [Serializable]
    public class Department : DogwoodEntityBase { }
}
