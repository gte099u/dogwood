﻿using System;

namespace Dogwood.Core.Test.Data
{
    [Serializable]
    [UniqueConstraint("Product and Order combination must be unique per order line", "OrderId", "ProductId")]
    public class OrderLine : DogwoodEntityBase
    {
        [ManyToOneRelation]
        public int OrderId { get; set; }

        public Order Order { get; set; }

        [ManyToOneRelation]
        public int ProductId { get; set; }

        public Product Product { get; set; }

        [Mappable]
        public int Quantity { get; set; }
    }
}
