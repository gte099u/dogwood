﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dogwood.Core.Test.Data
{
    /// <summary>
    /// This class is meant to be set up with bad metadata to test exceptions
    /// </summary>
    [Serializable]
    public class Category : DogwoodEntityBase
    {
        [OneToManyRelation]
        public IList<Product> RelatedProducts { get; set; }

        public int LabelId { get; set; }
    }
}
