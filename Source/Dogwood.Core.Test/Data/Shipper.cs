﻿using System;
using System.Collections.Generic;

namespace Dogwood.Core.Test.Data
{
    [Serializable]
    public class Shipper : DogwoodEntityBase
    {
        [Mappable]
        public string Name { get; set; }

        [Mappable]
        public string Phone { get; set; }

        [Mappable]
        public bool IsPreferred { get; set; }

        [OneToManyRelation((int)Classes.Order, (int)Attributes.CarrierId)]
        public IList<Order> OrdersShipped { get; set; }
    }
}
