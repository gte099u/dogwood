﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class OneToManyRelationAttributeTests
    {
        [TestMethod]
        public void Default_Ctor_Inits_RelatedClassId_To_Zero()
        {
            var x = new OneToManyRelationAttribute();
            Assert.AreEqual(0, x.RelatedClassId);
        }

        [TestMethod]
        public void Default_Ctor_Inits_FkAttributeId_To_Zero()
        {
            var x = new OneToManyRelationAttribute();
            Assert.AreEqual(0, x.FkAttributeId);
        }

        [TestMethod]
        public void Ctor_Inits_RelatedClassId_To_Value_Passed()
        {
            var x = new OneToManyRelationAttribute(55, 77);
            Assert.AreEqual(55, x.RelatedClassId);
        }

        [TestMethod]
        public void Ctor_Inits_FkAttributeId_To_Value_Passed()
        {
            var x = new OneToManyRelationAttribute(55, 77);
            Assert.AreEqual(77, x.FkAttributeId);
        }
    }
}
