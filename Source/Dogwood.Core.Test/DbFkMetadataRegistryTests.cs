﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dogwood.Core.Entities;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class DbFkMetadataRegistryTests
    {
        public DbFkMetadataRegistryTests()
        {
            if (!DbFkMetadataRegistry.Instance.ContainsKey(99))
                DbFkMetadataRegistry.Instance.Add(99, new RelationshipMetadataEntity
                {
                    AttributeId = 5,
                    FkClassId = 40,
                    IsNullable = false,
                    IsImmutable = true
                });

            if (!DbFkMetadataRegistry.Instance.ContainsKey(55))
                DbFkMetadataRegistry.Instance.Add(55, new RelationshipMetadataEntity
                {
                    AttributeId = 6,
                    FkClassId = 44,
                    IsNullable = true,
                    IsImmutable = false
                });

            if (!DbFkMetadataRegistry.Instance.ContainsKey(2))
                DbFkMetadataRegistry.Instance.Add(2, new RelationshipMetadataEntity
                {
                    AttributeId = 6,
                    FkClassId = 33,
                    IsNullable = true,
                    IsImmutable = false
                });
        }

        [TestMethod]
        public void GetKeyReturnsIdOfRegistryEntry()
        {
            int id = DbFkMetadataRegistry.GetKey(44, 6);
            Assert.AreEqual(55, id);
        }

        [TestMethod]
        public void GetKeyReturnsThrowsInformativeException()
        {
            bool exceptionThrown = false;
            string message = null;
            try
            {
                DbFkMetadataRegistry.GetKey(44, 7);
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                message = ex.Message;
            }

            Assert.AreEqual(true, exceptionThrown);
            Assert.AreEqual(
                "An attempt by the application to bind the 7 relation property on class type 44 failed because it has not been registered in the database.",
                message);
        }
    }
}
