﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dogwood.Core.Test.Data;
using Moq;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class ObjectRepositoryTests
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        /// <summary>
        /// The delete methods should return false if the id passed in does not correspond to an object of the given type
        /// </summary>
        [TestMethod]
        public void DeleteReturnsFalseIfObjectDoesntExist()
        {
            var mockContext = new Mock<IDogwoodDataContext>();
            var mockGetter = new Mock<IVersionedRetrieval<Customer>>();
            var mockPersist = new Mock<IVersionedPersistence<Customer>>();
            var service = new ObjectRepository<Customer>(mockContext.Object, mockGetter.Object, mockPersist.Object);

            Assert.IsFalse(service.Delete(99));
        }

        [TestMethod]
        public void DeleteReturnsTrueIfExistsAndSuccessful()
        {
            var mockContext = new Mock<IDogwoodDataContext>();
            var mockGetter = new Mock<IVersionedRetrieval<Customer>>();
            var mockPersist = new Mock<IVersionedPersistence<Customer>>();

            mockPersist.Setup(m => m.Delete(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<bool>())).Returns(true);
            mockGetter.Setup(m => m.Get(It.IsAny<int>())).Returns(new Customer());

            var service = new ObjectRepository<Customer>(mockContext.Object, mockGetter.Object, mockPersist.Object);

            Assert.IsTrue(service.Delete(99));
        }

        [TestMethod]
        public void CreatePredicateBucket_Returns_Bucket_With_Context_And_Class()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var repo = new ObjectRepository<Customer>(new DogwoodBranchContext(adapter, 11, 99));

            var bucket = repo.CreateNewPredicateBucket();

            Assert.AreEqual(11, bucket.ContextId);
            Assert.AreEqual((int)Classes.Customer, bucket.ClassId);
        }

        [TestMethod]
        public void CreatePredicateBucket_Returns_Bucket_With_Empty_Predicate_Lists()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var repo = new ObjectRepository<Customer>(new DogwoodBranchContext(adapter, 11, 99));

            var bucket = repo.CreateNewPredicateBucket();

            Assert.AreEqual(0, bucket.GetAttributeComparePredicates().Count());
            Assert.AreEqual(0, bucket.GetAttributeLikePredicates().Count());
            Assert.AreEqual(0, bucket.GetRelationPredicates().Count());
        }

        /// <summary>
        /// Setting (changing) the Context object on an ObjectRepository instance should allow the consumer to switch data request
        /// and retrieval contexts on the fly, without the need to new up a new repo object.
        /// </summary>
        [TestMethod]
        public void Switching_Contexts()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);
            var mockContext1 = new Mock<IDogwoodDataContext>();
            mockContext1.Setup(m => m.ContextType).Returns(EavContextType.Branch);
            mockContext1.Setup(m => m.ContextId).Returns(1);
            var objectCache1 = new ObjectCache();
            objectCache1.SetObject(new Customer { Id = 99, Name = "Bill" });
            mockContext1.Setup(m => m.ObjectCache).Returns(objectCache1);

            var mockContext2 = new Mock<IDogwoodDataContext>();
            mockContext2.Setup(m => m.ContextType).Returns(EavContextType.Branch);
            mockContext2.Setup(m => m.ContextId).Returns(2);
            var objectCache2 = new ObjectCache();
            objectCache2.SetObject(new Customer { Id = 99, Name = "Jack" });
            mockContext2.Setup(m => m.ObjectCache).Returns(objectCache2);

            //~ initial instantiation of the repo has context with "Bill" as customer 99's name
            var repo = new ObjectRepository<Customer>(mockContext1.Object);
            Assert.AreEqual(repo.Context, mockContext1.Object);
            var cust99 = repo.Get(99);
            Assert.AreEqual("Bill", cust99.Name);


            //~ switch the context
            repo.Context = mockContext2.Object;
            Assert.AreEqual(repo.Context, mockContext2.Object);
            cust99 = repo.Get(99);
            Assert.AreEqual("Jack", cust99.Name); //~ name should now be jack
        }

        [TestMethod]
        public void Switching_Contexts_To_Different_EavContextType()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);
            var mockContext1 = new Mock<IDogwoodDataContext>();
            mockContext1.Setup(m => m.ContextType).Returns(EavContextType.Branch);
            mockContext1.Setup(m => m.ContextId).Returns(1);
            mockContext1.Setup(m => m.PersistenceAdapter).Returns(adapter);
            var objectCache1 = new ObjectCache();
            objectCache1.SetObject(new Customer { Id = 99, Name = "Bill" });
            mockContext1.Setup(m => m.ObjectCache).Returns(objectCache1);

            var mockContext2 = new Mock<IDogwoodDataContext>();
            mockContext2.Setup(m => m.ContextType).Returns(EavContextType.Revision);
            mockContext2.Setup(m => m.ContextId).Returns(2);
            mockContext2.Setup(m => m.PersistenceAdapter).Returns(adapter);
            var objectCache2 = new ObjectCache();
            objectCache2.SetObject(new Customer { Id = 99, Name = "Jack" });
            mockContext2.Setup(m => m.ObjectCache).Returns(objectCache2);

            //~ initial instantiation of the repo has context with "Bill" as customer 99's name
            var repo = new ObjectRepository<Customer>(mockContext1.Object);
            Assert.AreEqual(repo.Context, mockContext1.Object);
            var cust99 = repo.Get(99);
            Assert.AreEqual("Bill", cust99.Name);


            //~ switch the context
            repo.Context = mockContext2.Object;
            Assert.AreEqual(repo.Context, mockContext2.Object);
            cust99 = repo.Get(99);
            Assert.AreEqual("Jack", cust99.Name); //~ name should now be jack
        }

        [TestMethod]
        public void GetAll_With_Null_FilterBucket_Returns_GetAll()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var repo = new ObjectRepository<Customer>(new DogwoodBranchContext(adapter, 11, 99));

            var result = repo.GetAll((IPredicateBucket)null);
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void Delete_Maintains_Integrity_When_No_OneToMany_Relation_Exists()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Department), typeof(Employee) }, adapter);
            var context = new DogwoodBranchContext(adapter, 2, 99);

            var deptRepo = new ObjectRepository<Department>(context);
            var cd = deptRepo.Save(new Department());
            
            Assert.AreEqual(1, deptRepo.GetAll().Count);

            var empRepo = new ObjectRepository<Employee>(context);
            empRepo.Save(new Employee{DepartmentId = cd.ObjectId});
            empRepo.Save(new Employee { DepartmentId = cd.ObjectId });
            empRepo.Save(new Employee { DepartmentId = cd.ObjectId });

            Assert.AreEqual(3, empRepo.GetAll().Count);

            //~ at this point, one department is related to three employees
            //~ if we delete the department, the employees should be deleted as well (metadata has cascade delete)

            deptRepo.Delete(cd.ObjectId);

            Assert.AreEqual(0, deptRepo.GetAll().Count);
            Assert.AreEqual(0, empRepo.GetAll().Count);
        }
    }
}
