﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Entities;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class EntityMetadataTests
    {
        [TestMethod]
        public void AddAttributeMapsIsNullable()
        {
            DbAttributeRegistry.Instance.Clear();
            DbAttributeRegistry.Instance.Add(88, "Name");

            DbMetadataRegistry.Instance.Clear();
            DbMetadataRegistry.Instance.Add(77, new AttributeMetadataEntity { ClassId = 99, IsNullable = false, AttributeId = 88 });

            var meta = new EntityMetadata
                           {
                               ClassId = 99
                           };

            var toAdd = typeof(Customer).GetProperties().Single(m => m.Name == "Name");
            meta.AddAttribute(toAdd);

            Assert.IsFalse(meta.Attributes[0].IsNullable);
        }

        [TestMethod]
        public void AddAttributeAddsSingleItemToAttributeList()
        {
            DbAttributeRegistry.Instance.Clear();
            DbAttributeRegistry.Instance.Add(88, "Name");

            DbMetadataRegistry.Instance.Clear();
            DbMetadataRegistry.Instance.Add(77, new AttributeMetadataEntity { ClassId = 99, IsNullable = false, AttributeId = 88 });
            var meta = new EntityMetadata
            {
                ClassId = 99
            };

            var toAdd = typeof(Customer).GetProperties().Single(m => m.Name == "Name");
            meta.AddAttribute(toAdd);

            Assert.AreEqual(1, meta.Attributes.Count);
        }

        [TestMethod]
        public void AddAttributeMapsProperty()
        {
            DbAttributeRegistry.Instance.Clear();
            DbAttributeRegistry.Instance.Add(88, "Name");

            DbMetadataRegistry.Instance.Clear();
            DbMetadataRegistry.Instance.Add(77, new AttributeMetadataEntity { ClassId = 99, IsNullable = false, AttributeId = 88 });
            var meta = new EntityMetadata
            {
                ClassId = 99
            };

            var toAdd = typeof(Customer).GetProperties().Single(m => m.Name == "Name");
            meta.AddAttribute(toAdd);

            Assert.AreEqual(toAdd, meta.Attributes[0].Property);
        }

        [TestMethod]
        public void AddAttributeMapsDataType()
        {
            DbAttributeRegistry.Instance.Clear();
            DbAttributeRegistry.Instance.Add(88, "Name");

            DbMetadataRegistry.Instance.Clear();
            DbMetadataRegistry.Instance.Add(77, new AttributeMetadataEntity { ClassId = 99, IsNullable = false, AttributeId = 88 });
            var meta = new EntityMetadata
            {
                ClassId = 99
            };

            var toAdd = typeof(Customer).GetProperties().Single(m => m.Name == "Name");
            meta.AddAttribute(toAdd);

            Assert.AreEqual(1, meta.Attributes.Count);
            Assert.IsFalse(meta.Attributes[0].IsNullable);
            Assert.AreEqual(toAdd, meta.Attributes[0].Property);
            Assert.AreEqual(DogwoodDataType.String, meta.Attributes[0].DataType);
        }
    }
}
