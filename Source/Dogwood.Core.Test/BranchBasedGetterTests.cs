﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class BranchBasedGetterTests
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        [TestMethod]
        public void GetAll_With_Filter_Throws_If_Null()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var getter = new BranchBasedGetter<Customer>(new DogwoodBranchContext(adapter, 11, 99));

            AssertException.IsThrown(() => getter.GetAll((IPredicateBucket)null), "FilterBucket may not be null");
        }
    }
}
