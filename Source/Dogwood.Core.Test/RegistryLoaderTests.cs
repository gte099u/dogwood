﻿using System;
using System.Linq;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class RegistryLoaderTests
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        [TestMethod]
        public void LoadClasses_Loads_All_Classes()
        {
            RegistryLoader.LoadClasses(new EcomPersistenceAdapter());
            Assert.AreEqual(Enum.GetValues(typeof(Classes)).Cast<Classes>().Count(), DbTypeRegistry.Instance.Count);
        }

        [TestMethod]
        public void LoadAttributes_Loads_All_Attributes()
        {
            RegistryLoader.LoadAttributes(new EcomPersistenceAdapter());
            Assert.AreEqual(Enum.GetValues(typeof(Attributes)).Cast<Attributes>().Count(), DbAttributeRegistry.Instance.Count);
        }

        [TestMethod]
        public void LoadAttributeMetadata_Loads_All_Attribute_Meta()
        {
            RegistryLoader.LoadAttributeMetadata(new EcomPersistenceAdapter());
            Assert.AreEqual(Enum.GetValues(typeof(AttributeMetadata)).Cast<AttributeMetadata>().Count(), DbMetadataRegistry.Instance.Count);
        }

        [TestMethod]
        public void LoadRelationshipMetadata_Loads_All_Relationship_Meta()
        {
            RegistryLoader.LoadRelationshipMetadata(new EcomPersistenceAdapter());
            Assert.AreEqual(Enum.GetValues(typeof(RelationshipMetadata)).Cast<RelationshipMetadata>().Count(), DbFkMetadataRegistry.Instance.Count);
        }

        [TestMethod]
        public void LoadRelationshipMetadata_Mapps_IsImmutable()
        {
            var adapter = new EcomPersistenceAdapter();
            RegistryLoader.LoadRelationshipMetadata(adapter);
            Assert.AreEqual(adapter.GetAllRelationshipMetadata().Count(m => m.IsImmutable), DbFkMetadataRegistry.Instance.Count(m => m.Value.IsImmutable));
        }

        [TestMethod]
        public void LoadAll_Loads_Classes()
        {
            RegistryLoader.LoadAll(new EcomPersistenceAdapter());
            Assert.AreEqual(Enum.GetValues(typeof(Classes)).Cast<Classes>().Count(), DbTypeRegistry.Instance.Count);
        }

        [TestMethod]
        public void LoadAll_Loads_All_Attributes()
        {
            RegistryLoader.LoadAll(new EcomPersistenceAdapter());
            Assert.AreEqual(Enum.GetValues(typeof(Attributes)).Cast<Attributes>().Count(), DbAttributeRegistry.Instance.Count);
        }

        [TestMethod]
        public void LoadAll_Loads_All_Attribute_Meta()
        {
            RegistryLoader.LoadAll(new EcomPersistenceAdapter());
            Assert.AreEqual(Enum.GetValues(typeof(AttributeMetadata)).Cast<AttributeMetadata>().Count(), DbMetadataRegistry.Instance.Count);
        }

        [TestMethod]
        public void LoadAll_Loads_All_Relationship_Meta()
        {
            RegistryLoader.LoadAll(new EcomPersistenceAdapter());
            Assert.AreEqual(Enum.GetValues(typeof(RelationshipMetadata)).Cast<RelationshipMetadata>().Count(), DbFkMetadataRegistry.Instance.Count);
        }
    }
}
