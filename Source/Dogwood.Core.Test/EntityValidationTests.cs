﻿using System;
using System.Collections.Generic;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class EntityValidationTests
    {
        private Product GetDefaultProduct()
        {
            return new Product { IsDiscontinued = false, Name = "ABC", UnitPrice = 44, UnitsInStock = 9 };
        }

        private void CreateProductAndHyper(IPersistenceAdapter adapter)
        {
            var context = new DogwoodBranchContext(adapter, 1, 99);
            var product = GetDefaultProduct();

            var productRepo = new ObjectRepository<Product>(context);
            productRepo.Save(product);

            var existing = new HyperUniqueClass
            {
                Birthday = new DateTime(1981, 12, 1),
                IsMale = true,
                Name = "Ben",
                ProductId = product.Id,
                Quantity = 100,
                UnitPrice = 99,
                Phone = "111.222.3334",
                ShippedDate = new DateTime(2012, 1, 2),
                UnitsInStock = 3
            };

            var objectRepo = new ObjectRepository<HyperUniqueClass>(context);
            objectRepo.Save(existing);
        }

        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        [TestMethod]
        public void ValidateImmutable_Throws_When_Property_Is_Changed()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var validator = new EntityValidation<Customer>(adapter, MetadataRegistry.Instance[typeof(Customer)], 1, new ObjectCache());

            var existing = new Customer
                             {
                                 Id = 1,
                                 IsMale = true,
                                 Name = "Ben",
                                 HeightInInches = 70,
                                 Birthday = new DateTime(1981, 12, 1),
                             };

            var toSave = new Customer
            {
                Id = 1,
                IsMale = false, //~ should be immutable
                Name = "Ben",
                HeightInInches = 70,
                Birthday = new DateTime(1981, 12, 1),
            };

            AssertException.IsThrown(() => validator.ValidateImmutability(toSave, existing), "The IsMale value is read-only and may not be changed");
        }

        [TestMethod]
        public void ValidateImmutable_Passes_When_Value_Is_Unchanged()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var validator = new EntityValidation<Customer>(adapter, MetadataRegistry.Instance[typeof(Customer)], 1, new ObjectCache());

            var existing = new Customer
            {
                Id = 1,
                IsMale = true,
                Name = "Ben",
                HeightInInches = 70,
                Birthday = new DateTime(1981, 12, 1),
            };

            var toSave = new Customer
            {
                Id = 1,
                IsMale = true,
                Name = "Ben",
                HeightInInches = 70,
                Birthday = new DateTime(1981, 12, 1),
            };

            var exceptionThrown = false;

            try
            {
                validator.ValidateImmutability(toSave, existing);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            Assert.IsFalse(exceptionThrown);
        }

        [TestMethod]
        public void ValidateBeforeUpdate_Fails_On_Immutable_Error()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var validator = new EntityValidation<Customer>(adapter, MetadataRegistry.Instance[typeof(Customer)], 1, new ObjectCache());

            var existing = new Customer
            {
                Id = 1,
                IsMale = true,
                Name = "Ben",
                HeightInInches = 70,
                Birthday = new DateTime(1981, 12, 1),
            };

            var toSave = new Customer
            {
                Id = 1,
                IsMale = false, //~ immutability exception
                Name = "Ben",
                HeightInInches = 70,
                Birthday = new DateTime(1981, 12, 1),
            };

            AssertException.IsThrown(() => validator.ValidateBeforeUpdate(toSave, existing));
        }

        [TestMethod]
        public void ValidateImmutable_Throws_When_Relation_Is_Changed()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var validator = new EntityValidation<Order>(adapter, MetadataRegistry.Instance[typeof(Order)], 1, new ObjectCache());

            var existing = new Order
            {
                Id = 1,
                CarrierId = 5,
                CustomerId = 11
            };

            var toSave = new Order
            {
                Id = 1,
                CarrierId = 5,
                CustomerId = 7  //~ immutable relation changed
            };

            AssertException.IsThrown(() => validator.ValidateImmutability(toSave, existing), "CustomerId is readonly and may not be changed");
        }

        [TestMethod]
        public void ValidateImmutable_Passes_When_Relation_Is_Changed()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var validator = new EntityValidation<Order>(adapter, MetadataRegistry.Instance[typeof(Order)], 1, new ObjectCache());

            var existing = new Order
            {
                Id = 1,
                CarrierId = 5,
                CustomerId = 11
            };

            var toSave = new Order
            {
                Id = 1,
                CarrierId = 4,//~ mutable relation changed
                CustomerId = 11
            };

            AssertException.IsThrown(() => validator.ValidateImmutability(toSave, existing));
        }

        [TestMethod]
        public void ValidateNullable_Throws_When_Property_Is_Null()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var validator = new EntityValidation<Customer>(adapter, MetadataRegistry.Instance[typeof(Customer)], 1, new ObjectCache());

            var toSave = new Customer
            {
                Id = 1,
                IsMale = false,
                Name = null, //~ not nullable
                HeightInInches = 70,
                Birthday = new DateTime(1981, 12, 1),
            };

            AssertException.IsThrown(() => validator.ValidateNullableValue(toSave), "Name is required");
        }

        [TestMethod]
        public void ValidateNullable_Passes_When_Property_Is_Null()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var validator = new EntityValidation<Customer>(adapter, MetadataRegistry.Instance[typeof(Customer)], 1, new ObjectCache());

            var toSave = new Customer
            {
                Id = 1,
                IsMale = false,
                Name = "Ben",
                Nickname = null,
                HeightInInches = 70,
                Birthday = new DateTime(1981, 12, 1), //~ nullable ok
            };

            var exceptionThrown = false;

            try
            {
                validator.ValidateNullableValue(toSave);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            Assert.IsFalse(exceptionThrown);
        }

        [TestMethod]
        public void ValidateNullable_Passes_When_Relation_Is_Null()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var validator = new EntityValidation<Order>(adapter, MetadataRegistry.Instance[typeof(Order)], 1, new ObjectCache());

            var toSave = new Order
            {
                Id = 1,
                CarrierId = null,
                CustomerId = 3,
                OrderDate = DateTime.Now
            };

            var exceptionThrown = false;

            try
            {
                validator.ValidateNullableValue(toSave);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            Assert.IsFalse(exceptionThrown);
        }

        [TestMethod]
        public void ValidateNullable_Throws_When_Relation_Is_Null()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var validator = new EntityValidation<Order>(adapter, MetadataRegistry.Instance[typeof(Order)], 1, new ObjectCache());

            var toSave = new Order
            {
                Id = 1,
                CarrierId = 3,
                CustomerId = null,
                OrderDate = DateTime.Now
            };

            var exceptionThrown = false;
            var exceptionMessage = string.Empty;

            try
            {
                validator.ValidateNullableValue(toSave);
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                exceptionMessage = ex.Message;
            }

            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual("CustomerId is required", exceptionMessage);
        }

        [TestMethod]
        public void ValidateUniqueConstraints_Passes_When_All_Is_Unique()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(HyperUniqueClass), typeof(Product) }, adapter);
            var validator = new EntityValidation<HyperUniqueClass>(adapter, MetadataRegistry.Instance[typeof(HyperUniqueClass)], 1, new ObjectCache());            

            CreateProductAndHyper(adapter);

            var toSave = new HyperUniqueClass
            {
                Birthday = new DateTime(1981, 12, 2),
                IsMale = false,
                Name = "Bill",
                ProductId = 6,
                Quantity = 99,
                UnitPrice = 66,
                Phone = "111.222.3333",
                ShippedDate = new DateTime(2012, 1, 1),
                UnitsInStock = 2
            };

            var exceptionThrown = false;

            try
            {
                validator.ValidateUniqueConstraints(toSave);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            Assert.IsFalse(exceptionThrown);
        }

        [TestMethod]
        public void ValidateUniqueConstraints_Throws_String_Is_Violated()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(HyperUniqueClass), typeof(Product) }, adapter);
            var validator = new EntityValidation<HyperUniqueClass>(adapter, MetadataRegistry.Instance[typeof(HyperUniqueClass)], 1, new ObjectCache());
            
            CreateProductAndHyper(adapter);

            var toSave = new HyperUniqueClass
            {
                Birthday = new DateTime(1981, 12, 2),
                IsMale = false,
                Name = "Ben", //~ same name as existing
                ProductId = 6,
                Quantity = 99,
                UnitPrice = 66,
                Phone = "111.222.3334",
                ShippedDate = new DateTime(2012, 1, 1),
                UnitsInStock = 3
            };

            AssertException.IsThrown(() => validator.ValidateUniqueConstraints(toSave), "Name must be unique");
        }

        [TestMethod]
        public void ValidateUniqueConstraints_Throws_Int_Is_Violated()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(HyperUniqueClass), typeof(Product) }, adapter);
            var validator = new EntityValidation<HyperUniqueClass>(adapter, MetadataRegistry.Instance[typeof(HyperUniqueClass)], 1, new ObjectCache());
            
            CreateProductAndHyper(adapter);

            var toSave = new HyperUniqueClass
            {
                Birthday = new DateTime(1981, 12, 2),
                IsMale = false,
                Name = "Bill",
                ProductId = 6,
                Quantity = 8,
                UnitPrice = 99, //~ same as existing
                Phone = "111.222.3331",
                ShippedDate = new DateTime(2012, 1, 1),
                UnitsInStock = 2
            };

            AssertException.IsThrown(() => validator.ValidateUniqueConstraints(toSave), "Unit Price must be unique");
        }

        [TestMethod]
        public void ValidateUniqueConstraints_Throws_Long_Is_Violated()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(HyperUniqueClass), typeof(Product) }, adapter);
            var validator = new EntityValidation<HyperUniqueClass>(adapter, MetadataRegistry.Instance[typeof(HyperUniqueClass)], 1, new ObjectCache());
            
            CreateProductAndHyper(adapter);

            var toSave = new HyperUniqueClass
            {
                Birthday = new DateTime(1981, 12, 2),
                IsMale = false,
                Name = "Bill",
                ProductId = 6,
                Quantity = 100, //~ same as existing
                UnitPrice = 88,
                Phone = "111.222.3334",
                ShippedDate = new DateTime(2012, 1, 1),
                UnitsInStock = 3
            };

            AssertException.IsThrown(() => validator.ValidateUniqueConstraints(toSave), "Quantity must be unique");
        }

        [TestMethod]
        public void ValidateUniqueConstraints_Throws_Bool_Is_Violated()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(HyperUniqueClass), typeof(Product) }, adapter);
            var validator = new EntityValidation<HyperUniqueClass>(adapter, MetadataRegistry.Instance[typeof(HyperUniqueClass)], 1, new ObjectCache());
            
            CreateProductAndHyper(adapter);

            var toSave = new HyperUniqueClass
            {
                Birthday = new DateTime(1981, 12, 2),
                IsMale = true,//~ same as existing
                Name = "Bill",
                ProductId = 6,
                Quantity = 323,
                UnitPrice = 88,
                Phone = "111.222.3333",
                ShippedDate = new DateTime(2012, 1, 1),
                UnitsInStock = 2
            };

            AssertException.IsThrown(() => validator.ValidateUniqueConstraints(toSave), "Gender must be unique");
        }

        [TestMethod]
        public void ValidateUniqueConstraints_Throws_Date_Is_Violated()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(HyperUniqueClass), typeof(Product) }, adapter);
            var validator = new EntityValidation<HyperUniqueClass>(adapter, MetadataRegistry.Instance[typeof(HyperUniqueClass)], 1, new ObjectCache());

            CreateProductAndHyper(adapter);

            var toSave = new HyperUniqueClass
            {
                Birthday = new DateTime(1981, 12, 1),//~ same as existing
                IsMale = false,
                Name = "Bill",
                ProductId = 6,
                Quantity = 323,
                UnitPrice = 88,
                Phone = "111.222.3332",
                ShippedDate = new DateTime(2012, 1, 1),
                UnitsInStock = 32
            };

            AssertException.IsThrown(() => validator.ValidateUniqueConstraints(toSave), "Birthday must be unique");
        }

        [TestMethod]
        public void ValidateUniqueConstraints_Throws_Combo_Is_Violated()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(HyperUniqueClass), typeof(Product) }, adapter);
            var validator = new EntityValidation<HyperUniqueClass>(adapter, MetadataRegistry.Instance[typeof(HyperUniqueClass)], 1, new ObjectCache());

            CreateProductAndHyper(adapter);

            var toSave = new HyperUniqueClass
            {
                Birthday = new DateTime(1981, 12, 2),
                IsMale = false,
                Name = "Bill",
                ProductId = 6,
                Quantity = 323,
                UnitPrice = 88,
                Phone = "111.222.3334",//~ same as existing
                ShippedDate = new DateTime(2012, 1, 2),//~ same as existing
                UnitsInStock = 3//~ same as existing
            };

            AssertException.IsThrown(() => validator.ValidateUniqueConstraints(toSave), "Phone, Ship Date, and Units In Stock must be unique");
        }

        [TestMethod]
        public void ValidateUniqueConstraints_Passes_Combo_Partially_Unique()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(HyperUniqueClass), typeof(Product) }, adapter);
            var validator = new EntityValidation<HyperUniqueClass>(adapter, MetadataRegistry.Instance[typeof(HyperUniqueClass)], 1, new ObjectCache());

            CreateProductAndHyper(adapter);

            var toSave = new HyperUniqueClass
            {
                Birthday = new DateTime(1981, 12, 2),
                IsMale = false,
                Name = "Bill",
                ProductId = 6,
                Quantity = 323,
                UnitPrice = 88,
                Phone = "111.222.3334", //~ same as existing
                ShippedDate = new DateTime(2012, 1, 2),//~ same as existing
                UnitsInStock = 32
            };

            var exceptionThrown = false;

            try
            {
                validator.ValidateUniqueConstraints(toSave);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            Assert.IsFalse(exceptionThrown);
        }

        [TestMethod]
        public void ValidateUniqueConstraints_Passes_For_Null_Value()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);


            var validator = new EntityValidation<Customer>(adapter, MetadataRegistry.Instance[typeof(Customer)], 1, new ObjectCache());

            //~ save a customer with a birthday value
            var repo = new ObjectRepository<Customer>(new DogwoodBranchContext(adapter, 1, 99));
           
            var toSave = new Customer
            {
                Birthday = new DateTime(1981, 12, 2),
                IsMale = false,
                Name = "Bill",
                HeightInInches = 122
            };

            repo.Save(toSave);

            //~ create a customer with a null birthday
            var toSave2 = new Customer
            {
                Birthday = null,
                IsMale = true,
                Name = "Dan",
                HeightInInches = 125
            };

            var exceptionThrown = false;

            try
            {
                validator.ValidateUniqueConstraints(toSave2);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            //~ exception should not be thrown
            Assert.IsFalse(exceptionThrown);
        }

        [TestMethod]
        public void ValidateUniqueConstraints_Passes_For_Null_Value_On_Two_Entities()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);


            var validator = new EntityValidation<Customer>(adapter, MetadataRegistry.Instance[typeof(Customer)], 1, new ObjectCache());

            //~ save a customer with null birthday
            var repo = new ObjectRepository<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            var toSave = new Customer
            {
                Birthday = null,
                IsMale = false,
                Name = "Bill",
                HeightInInches = 122
            };

            repo.Save(toSave);

            //~ create another customer with a null birthday
            var toSave2 = new Customer
            {
                Birthday = null,
                IsMale = true,
                Name = "Dan",
                HeightInInches = 125
            };

            var exceptionThrown = false;

            try
            {
                validator.ValidateUniqueConstraints(toSave2);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            //~ exception should not be thrown
            Assert.IsFalse(exceptionThrown);
        }

        [TestMethod]
        public void ValidateUniqueConstraints_Works_For_Nullable_Values()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var validator = new EntityValidation<Customer>(adapter, MetadataRegistry.Instance[typeof(Customer)], 1, new ObjectCache());

            //~ save a customer with birthday value
            var repo = new ObjectRepository<Customer>(new DogwoodBranchContext(adapter, 1, 99));

            var toSave = new Customer
            {
                Birthday = new DateTime(2011,1,2),
                IsMale = false,
                Name = "Bill",
                HeightInInches = 122
            };

            repo.Save(toSave);

            //~ create another customer with same birthday
            var toSave2 = new Customer
            {
                Birthday = new DateTime(2011, 1, 2),
                IsMale = true,
                Name = "Dan",
                HeightInInches = 125
            };

            var exceptionThrown = false;

            try
            {
                validator.ValidateUniqueConstraints(toSave2);
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            //~ exception should be thrown
            Assert.IsTrue(exceptionThrown);
        }
    }
}
