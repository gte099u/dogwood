﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class BranchingServiceTests
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        [TestMethod]
        public void Create_Branch_Throws_Exception_If_Revision_Is_Root()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);
            var branchingService = new BranchingService(adapter);

            var exceptionThrown = false;
            string message = string.Empty;

            try
            {
                branchingService.CreateBranch(0, 55, "Branching");
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                message = ex.Message;
            }

            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual("Can not branch off ROOT", message);
        }

        [TestMethod]
        public void Create_Branch_Throws_Exception_If_Revision_Is_Not_Found()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);
            var branchingService = new BranchingService(adapter);

            var exceptionThrown = false;
            string message = string.Empty;

            try
            {
                branchingService.CreateBranch(45, 55, "Branching"); //~ 45 will not be found
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                message = ex.Message;
            }

            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual("The revisionId provided does not represent an existing revision", message);
        }

        [TestMethod]
        public void Create_Branch_Throws_Exception_If_Revision_Is_Already_Serves_As_A_Branch()
        {
            var adapter = new EcomPersistenceAdapter();

            adapter._revisions.Save(new Entities.RevisionEntity { Id = 2, BranchId = 1 });
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 3, BranchId = 1 });
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 4, BranchId = 1 });
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 5, BranchId = 4 }); //~ 5 should be suggested
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 6, BranchId = 4 });

            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);
            var branchingService = new BranchingService(adapter);

            var exceptionThrown = false;
            string message = string.Empty;

            try
            {
                branchingService.CreateBranch(4, 55, "Branching"); //~ 4 already serves as a branch
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                message = ex.Message;
            }

            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual("The revision provided as a point of branching already is a branch point.  Possibly consider using revision 5 as a branch point instead", message);
        }

        [TestMethod]
        public void Create_Branch_Creates_New_Revision()
        {
            var adapter = new EcomPersistenceAdapter();

            //~ 2 revisions already exist (0 and 1)
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 2, BranchId = 1 });
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 3, BranchId = 1 });
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 4, BranchId = 1 });
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 5, BranchId = 4 });
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 6, BranchId = 4 });

            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);
            var branchingService = new BranchingService(adapter);


            branchingService.CreateBranch(5, 55, "Branching");


            Assert.AreEqual(8, adapter._revisions.Count);
            Assert.AreEqual(5, adapter._revisions[7].BranchId);
            Assert.AreEqual("Branching", adapter._revisions[7].Tag);
        }

        [TestMethod]
        public void Create_Branch_Returns_Branch_Point_Revision()
        {
            var adapter = new EcomPersistenceAdapter();

            //~ 2 revisions already exist (0 and 1)
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 2, BranchId = 1 });
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 3, BranchId = 1 });
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 4, BranchId = 1 });
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 5, BranchId = 4 });
            adapter._revisions.Save(new Entities.RevisionEntity { Id = 6, BranchId = 4 });

            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);
            var branchingService = new BranchingService(adapter);

            var toRet = branchingService.CreateBranch(5, 55, "Branching");

            Assert.AreEqual(5, toRet);
        }
    }
}
