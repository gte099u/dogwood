﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class PredicateBucketTests
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        [TestMethod]
        public void PredicateBucket_Inits_With_Class_And_Context()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3); //~ context Id = 3
            Assert.AreEqual((int)Classes.Customer, bucket.ClassId); 
            Assert.AreEqual(3, bucket.ContextId);
        }

        [TestMethod]
        public void PredicateBucket_AddAttributeComparePredicate_Maps_Predicate_Values()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3); //~ context Id = 3

            bucket.AddAttributeComparePredicate(m => m.Name, "Ben");

            var stored = bucket.GetAttributeComparePredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual("Ben", stored[0].Value);
            Assert.AreEqual((int)AttributeMetadata.Customer_Name, stored[0].AttributeMetadatatId);
        }

        [TestMethod]
        public void PredicateBucket_AddAttributeComparePredicate_Uses_Equal_Operator_By_Default()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3); //~ context Id = 3

            bucket.AddAttributeComparePredicate(m => m.Name, "Ben");

            var stored = bucket.GetAttributeComparePredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(ComparisonOperator.Equal, stored[0].CompOperator);


        }

        [TestMethod]
        public void PredicateBucket_AddAttributeComparePredicate_Maps_Operator_If_Specified()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3); //~ context Id = 3

            bucket.AddAttributeComparePredicate(m => m.Name, "Ben", ComparisonOperator.LessThan);

            var stored = bucket.GetAttributeComparePredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(ComparisonOperator.LessThan, stored[0].CompOperator);
        }

        [TestMethod]
        public void PredicateBucket_AddRelationPredicate_Maps_Negate_If_Specified()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(OrderLine) }, adapter);

            var bucket = new PredicateBucket<OrderLine>(3); //~ context Id = 3

            bucket.AddRelationPredicate(m => m.OrderId, 88, true);

            var stored = bucket.GetRelationPredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(true, stored[0].Negate);
        }

        [TestMethod]
        public void PredicateBucket_AddRelationPredicate_Sets_Negate_False_By_Default()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(OrderLine) }, adapter);

            var bucket = new PredicateBucket<OrderLine>(3); //~ context Id = 3

            bucket.AddRelationPredicate(m => m.OrderId, 88);

            var stored = bucket.GetRelationPredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(false, stored[0].Negate);
        }

        [TestMethod]
        public void PredicateBucket_AddRelationPredicate_Maps_Values()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(OrderLine) }, adapter);

            var bucket = new PredicateBucket<OrderLine>(3);

            bucket.AddRelationPredicate(m => m.OrderId, 88);

            var stored = bucket.GetRelationPredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(88, stored[0].Value);
            Assert.AreEqual((int)RelationshipMetadata.OrderLine_OrderId, stored[0].RelationMetadatatId);
        }

        [TestMethod]
        public void PredicateBucket_AddLikePredicate_Sets_Attribute_MetadataId_To_Null()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3);

            bucket.AddAttributeLikePredicate("B"); //~ no member expression specified

            var stored = bucket.GetAttributeLikePredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.IsNull(stored[0].AttributeMetadatatId );
        }

        [TestMethod]
        public void PredicateBucket_AddLikePredicate_Sets_Value()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3);

            bucket.AddAttributeLikePredicate("B"); //~ no member expression specified

            var stored = bucket.GetAttributeLikePredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual("B", stored[0].Value);
        }

        [TestMethod]
        public void PredicateBucket_AddLikePredicate_Sets_AttributeMetadataId()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3);

            bucket.AddAttributeLikePredicate(m => m.Name, "B"); //~ specifying member expression

            var stored = bucket.GetAttributeLikePredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual((int)AttributeMetadata.Customer_Name, stored[0].AttributeMetadatatId);
        }

        [TestMethod]
        public void PredicateBucket_AddLikePredicate_2_Sets_Value()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3);

            bucket.AddAttributeLikePredicate(m => m.Name, "B"); //~ specifying member expression

            var stored = bucket.GetAttributeLikePredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual("B", stored[0].Value);
        }

        [TestMethod]
        public void PredicateBucket_AddLikePredicate_Uses_Contains_By_Default()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3);

            bucket.AddAttributeLikePredicate("B"); //~ no member expression specified

            var stored = bucket.GetAttributeLikePredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(LikeComparisonOperator.Contains, stored[0].CompOperator);
        }

        [TestMethod]
        public void PredicateBucket_AddLikePredicate_Uses_Contains_By_Default_2()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3);

            bucket.AddAttributeLikePredicate(m => m.Name, "B"); //~ specifying member expression

            var stored = bucket.GetAttributeLikePredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(LikeComparisonOperator.Contains, stored[0].CompOperator);
        }

        [TestMethod]
        public void PredicateBucket_AddLikePredicate_Sets_Operator_If_Provided()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3);

            bucket.AddAttributeLikePredicate("B", LikeComparisonOperator.BeginsWith); //~ no member expression specified

            var stored = bucket.GetAttributeLikePredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(LikeComparisonOperator.BeginsWith, stored[0].CompOperator);
        }

        [TestMethod]
        public void PredicateBucket_AddLikePredicate_Sets_Operator_If_Provided_2()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3);

            bucket.AddAttributeLikePredicate(m => m.Name, "B", LikeComparisonOperator.BeginsWith); //~ specifying member expression

            var stored = bucket.GetAttributeLikePredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(LikeComparisonOperator.BeginsWith, stored[0].CompOperator);
        }

        [TestMethod]
        public void PredicateBucket_AddRelationPredicate_Nullable_Maps_Negate_If_Specified()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var bucket = new PredicateBucket<Order>(3); //~ context Id = 3

            bucket.AddRelationPredicate(m => m.CustomerId, 88, true);

            var stored = bucket.GetRelationPredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(true, stored[0].Negate);
        }

        [TestMethod]
        public void PredicateBucket_AddRelationPredicate_Nullable_Sets_Negate_False_By_Default()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var bucket = new PredicateBucket<Order>(3); //~ context Id = 3

            bucket.AddRelationPredicate(m => m.CustomerId, 88);

            var stored = bucket.GetRelationPredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(false, stored[0].Negate);
        }

        [TestMethod]
        public void PredicateBucket_AddRelationPredicate_Nullable_Maps_Values()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var bucket = new PredicateBucket<Order>(3);

            bucket.AddRelationPredicate(m => m.CustomerId, 88);

            var stored = bucket.GetRelationPredicates().ToList();

            Assert.AreEqual(1, stored.Count());
            Assert.AreEqual(88, stored[0].Value);
            Assert.AreEqual((int)RelationshipMetadata.Order_CustomerId, stored[0].RelationMetadatatId);
        }

        [TestMethod]
        public void PredicateBucket_IsEmpty_Returns_True_When_No_Predicates_Exist()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var bucket = new PredicateBucket<Order>(3);

            Assert.IsTrue(bucket.IsEmpty);
        }

        [TestMethod]
        public void PredicateBucket_IsEmpty_Returns_False_With_One_CompareValuePredicate()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3);
            bucket.AddAttributeComparePredicate(m => m.Name, "Ben");
            Assert.IsFalse(bucket.IsEmpty);
        }

        [TestMethod]
        public void PredicateBucket_IsEmpty_Returns_False_With_One_RelationPredicate()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var bucket = new PredicateBucket<Order>(3);
            bucket.AddRelationPredicate(m => m.CustomerId, 4);
            Assert.IsFalse(bucket.IsEmpty);
        }

        [TestMethod]
        public void PredicateBucket_IsEmpty_Returns_False_With_One_RelationInPredicate()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) }, adapter);

            var bucket = new PredicateBucket<Order>(3);
            bucket.AddRelationInPredicate(m => m.CustomerId, new List<int>{1,2,3});
            Assert.IsFalse(bucket.IsEmpty);
        }

        [TestMethod]
        public void PredicateBucket_IsEmpty_Returns_False_With_One_ValueLikePredicate()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var bucket = new PredicateBucket<Customer>(3);
            bucket.AddAttributeLikePredicate(m => m.Name, "Ben");
            Assert.IsFalse(bucket.IsEmpty);
        }
    }
}
