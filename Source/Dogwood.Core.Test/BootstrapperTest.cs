﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class BootstrapperTest
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        [TestMethod]
        public void Init_Loads_All_Classes()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            Assert.AreEqual(Enum.GetValues(typeof(Classes)).Cast<Classes>().Count(), DbTypeRegistry.Instance.Count);
        }

        [TestMethod]
        public void Init_Loads_All_AttributeMetadata()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            Assert.AreEqual(Enum.GetValues(typeof(AttributeMetadata)).Cast<AttributeMetadata>().Count(), DbMetadataRegistry.Instance.Count);
        }

        [TestMethod]
        public void Init_Loads_All_Attributes()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            Assert.AreEqual(Enum.GetValues(typeof(Attributes)).Cast<Attributes>().Count(), DbAttributeRegistry.Instance.Count);
        }

        [TestMethod]
        public void Init_Loads_All_Relationships()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            Assert.AreEqual(Enum.GetValues(typeof(RelationshipMetadata)).Cast<RelationshipMetadata>().Count(), DbFkMetadataRegistry.Instance.Count);
        }
    }
}
