﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class EntityMetadataFactoryTests
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        [TestMethod]
        public void Create_Sets_ClassId_On_Returned_Object()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.OrderLine, typeof(OrderLine));

            Assert.AreEqual((int)Classes.OrderLine, meta.ClassId);
        }

        [TestMethod]
        public void Create_Adds_Attributes_That_Are_Flagged_As_Mappable()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Customer, typeof(Customer));

            Assert.AreEqual(4, meta.Attributes.Count);
            Assert.IsFalse(meta.Attributes.Any(m => m.Property == typeof(Customer).GetProperty("HeightInInches")));
        }

        [TestMethod]
        public void Create_Sets_Attributes_Immutable_Flag()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Customer, typeof(Customer));
            
            //~ IsMale property on Customer should be IsImmutable = true
            Assert.IsTrue(meta.Attributes.Single(m=>m.Property == typeof(Customer).GetProperty("IsMale")).IsImmutable);
        }

        [TestMethod]
        public void Create_Sets_Relations_Immutable_Flag()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Order) },
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Order, typeof(Order));

            //~ CustomerId property on Order should be IsImmutable = true
            Assert.IsTrue(meta.ManyToOneRelationships.Single(m => m.Value.Property == typeof(Order).GetProperty("CustomerId")).Value.IsImmutable);
        }

        [TestMethod]
        public void Create_Adds_Many_To_One_Relationships()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.OrderLine, typeof(OrderLine));

            Assert.AreEqual(2, meta.ManyToOneRelationships.Count);
        }

        [TestMethod]
        public void Create_Adds_One_To_Many_Relationships_For_ILists_That_Are_Tagged()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Customer, typeof(Customer));

            Assert.AreEqual(1, meta.OneToManyRelationships.Count);
        }

        [TestMethod]
        public void Create_Throws_Helpful_Exception_If_Type_Is_Not_Found()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            bool exceptionThrown = false;
            string message = string.Empty;

            try
            {
                // RegistryHelper is not an entity and will not be found during the creation process
                var meta = EntityMetadataFactory.Create((int)Classes.Customer, typeof(RegistryHelper));
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                message = ex.Message;
            }

            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual("The entity of type RegistryHelper was not defined in the database.", message);
        }

        [TestMethod]
        public void Many_To_One_Relationship_Works_For_Custom_Named_Properties()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Order, typeof(Order));

            Assert.AreEqual(2, meta.ManyToOneRelationships.Count);
        }

        [TestMethod]
        public void Create_Sets_ManyToOne_IsNullable()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) },
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Order, typeof(Order));

            var metadata = meta.ManyToOneRelationships.Single(m => m.Value.FkAttributeMetadataId == (int)RelationshipMetadata.Order_CarrierId).Value;

            Assert.AreEqual(true, metadata.IsNullable);
        }

        [TestMethod]
        public void Create_Sets_ClassId_For_Conventional_ManyToOne_Name()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> {  },
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.OrderLine, typeof(OrderLine));

            var metadata = meta.ManyToOneRelationships.Single(m => m.Value.FkAttributeMetadataId == (int)RelationshipMetadata.OrderLine_ProductId).Value;

            Assert.AreEqual((int)Classes.Product, metadata.ClassId);
        }

        [TestMethod]
        public void Create_Sets_ClassId_For_Unconventional_ManyToOne_Name()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Order, typeof(Order));

            var metadata = meta.ManyToOneRelationships.Single(m => m.Value.FkAttributeMetadataId == (int)RelationshipMetadata.Order_CarrierId).Value;

            Assert.AreEqual((int)Classes.Shipper, metadata.ClassId);
        }

        [TestMethod]
        public void Create_Sets_RelationProperty_For_Conventional_ManyToOne_Name()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.OrderLine, typeof(OrderLine));

            var metadata = meta.ManyToOneRelationships.Single(m => m.Value.FkAttributeMetadataId == (int)RelationshipMetadata.OrderLine_ProductId).Value;

            Assert.AreEqual(typeof(OrderLine).GetProperty("Product"), metadata.RelationProperty);
        }

        [TestMethod]
        public void Create_Sets_RelationProperty_For_Unconventional_ManyToOne_Name()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Order, typeof(Order));

            var metadata = meta.ManyToOneRelationships.Single(m => m.Value.FkAttributeMetadataId == (int)RelationshipMetadata.Order_CarrierId).Value;

            Assert.AreEqual(typeof(Order).GetProperty("Carrier"), metadata.RelationProperty);
        }

        [TestMethod]
        public void Create_Sets_Property_For_Conventional_ManyToOne_Name()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.OrderLine, typeof(OrderLine));

            var metadata = meta.ManyToOneRelationships.Single(m => m.Value.FkAttributeMetadataId == (int)RelationshipMetadata.OrderLine_ProductId).Value;

            Assert.AreEqual(typeof(OrderLine).GetProperty("ProductId"), metadata.Property);
        }

        [TestMethod]
        public void Create_Sets_Property_For_Unconventional_ManyToOne_Name()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Order, typeof(Order));

            var metadata = meta.ManyToOneRelationships.Single(m => m.Value.FkAttributeMetadataId == (int)RelationshipMetadata.Order_CarrierId).Value;

            Assert.AreEqual(typeof(Order).GetProperty("CarrierId"), metadata.Property);
        }

        [TestMethod]
        public void Create_Sets_PrimaryKey_For_OneToMany_Relationship()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Shipper, typeof(Shipper));

            var metadata = meta.OneToManyRelationships["Dogwood.Core.Test.Data.Shipper_OrdersShipped"];

            Assert.AreEqual(typeof(Shipper).GetProperty("Id"), metadata.PrimaryKey);
        }

        [TestMethod]
        public void Create_Sets_RelationProperty_For_OneToMany_Relationship()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Shipper, typeof(Shipper));

            var metadata = meta.OneToManyRelationships["Dogwood.Core.Test.Data.Shipper_OrdersShipped"];

            Assert.AreEqual(typeof(Shipper).GetProperty("OrdersShipped"), metadata.RelationProperty);
        }

        [TestMethod]
        public void Create_Sets_ClassId_For_Conventional_OneToMany_Relationship()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Customer, typeof(Customer));

            var metadata = meta.OneToManyRelationships["Dogwood.Core.Test.Data.Customer_Orders"];

            Assert.AreEqual((int)Classes.Order, metadata.ClassId);
        }

        [TestMethod]
        public void Create_Sets_ForeignKey_For_Conventional_OneToMany_Relationship()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Customer, typeof(Customer));

            var metadata = meta.OneToManyRelationships["Dogwood.Core.Test.Data.Customer_Orders"];

            Assert.AreEqual(typeof(Order).GetProperty("CustomerId"), metadata.ForeignKey);
        }

        [TestMethod]
        public void Create_Throws_Helpful_Exception_If_Conventional_OneToMany_Is_Missing_Values()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            DbTypeRegistry.Instance.Add(-1, new Entities.ClassEntity {Name = "Category"});

            bool exceptionThrown = false;
            string message = string.Empty;

            try
            {
                // Category is set up with bad metadata on purpose
                EntityMetadataFactory.Create(-1, typeof(Category));
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                message = ex.Message;
            }

            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual("A relationship between Category and Product was defined unidirectionally in the application metadata.  Please check these entities and correct the missing relationship", message);
        }

        [TestMethod]
        public void Create_Sets_ForeignKeyProperty_For_Conventional_OneToMany_Relationship()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Customer, typeof(Customer));

            var metadata = meta.OneToManyRelationships["Dogwood.Core.Test.Data.Customer_Orders"];

            Assert.AreEqual(typeof(Order).GetProperty("Customer"), metadata.ForeignKeyProperty);
        }

        [TestMethod]
        public void Create_Throws_Helpful_Exception_If_Conventional_OneToMany_Relationship_Is_Undefined()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            DbTypeRegistry.Instance.Add(-1, new Entities.ClassEntity { Name = "Category" });
            DbTypeRegistry.Instance.Add(-2, new Entities.ClassEntity { Name = "Label" });
            DbAttributeRegistry.Instance.Add(-4, "LabelId");

            bool exceptionThrown = false;
            string message = string.Empty;

            try
            {
                // Category is set up with bad metadata on purpose
                EntityMetadataFactory.Create(-1, typeof(Label));
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                message = ex.Message;
            }

            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual("A relationship between Label and Category was defined unidirectionally in the application metadata.  Please check these entities and correct the missing relationship", message);
        }

        [TestMethod]
        public void Create_Sets_ClassId_For_Unconventional_OneToMany_Relationship()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Shipper, typeof(Shipper));

            var metadata = meta.OneToManyRelationships["Dogwood.Core.Test.Data.Shipper_OrdersShipped"];

            Assert.AreEqual((int)Classes.Order, metadata.ClassId);
        }

        [TestMethod]
        public void Create_Sets_ForeignKey_For_Unconventional_OneToMany_Relationship()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Shipper, typeof(Shipper));

            var metadata = meta.OneToManyRelationships["Dogwood.Core.Test.Data.Shipper_OrdersShipped"];

            Assert.AreEqual(typeof(Order).GetProperty("CarrierId"), metadata.ForeignKey);
        }

        [TestMethod]
        public void Create_Sets_ForeignKeyProperty_For_Unconventional_OneToMany_Relationship()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Shipper, typeof(Shipper));

            var metadata = meta.OneToManyRelationships["Dogwood.Core.Test.Data.Shipper_OrdersShipped"];

            Assert.AreEqual(typeof(Order).GetProperty("Carrier"), metadata.ForeignKeyProperty);
        }

        [TestMethod]
        public void Create_Sets_ForeignKeyAttributeId_For_Unconventional_OneToMany_Relationship()
        {
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type>(),
                              new EcomPersistenceAdapter());

            var meta = EntityMetadataFactory.Create((int)Classes.Shipper, typeof(Shipper));

            var metadata = meta.OneToManyRelationships["Dogwood.Core.Test.Data.Shipper_OrdersShipped"];

            Assert.AreEqual((int)Attributes.CarrierId, metadata.ForeignKeyAttributeId);
        }
    }
}
