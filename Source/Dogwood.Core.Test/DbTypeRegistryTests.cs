﻿using System;
using Dogwood.Core.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class DbTypeRegistryTests
    {
        public DbTypeRegistryTests()
        {
            DbTypeRegistry.Instance.Clear();

            if (!DbTypeRegistry.Instance.ContainsKey(99))
                DbTypeRegistry.Instance.Add(99, new ClassEntity{Id= 99, Name = "Customer"});
            if (!DbTypeRegistry.Instance.ContainsKey(44))
                DbTypeRegistry.Instance.Add(44, new ClassEntity { Id = 44, Name = "Order" });
            if (!DbTypeRegistry.Instance.ContainsKey(22))
                DbTypeRegistry.Instance.Add(22, new ClassEntity { Id = 22, Name = "OrderLine" });
        }

        [TestMethod]
        public void GetKeyReturnsIdOfRegistryEntry()
        {
            int id = DbTypeRegistry.GetTypeId("Customer");
            Assert.AreEqual(99, id);
        }

        [TestMethod]
        public void GetKeyReturnsThrowsInformativeException()
        {
            bool exceptionThrown = false;
            string message = null;
            try
            {
                DbTypeRegistry.GetTypeId("Product");
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                message = ex.Message;
            }

            Assert.AreEqual(true, exceptionThrown);
            Assert.AreEqual(
                "A class type named Product was expected in the database but was not found.  Please reconcile the collection of entities passed to the bootstrapper reboot the application.",
                message);
        }
    }
}
