﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class ManyToOneRelationAttributeTests
    {
        [TestMethod]
        public void Default_Ctor_Inits_RelatedClassId_To_Zero()
        {
            var x = new ManyToOneRelationAttribute();
            Assert.AreEqual(0, x.RelatedClassId);
        }

        [TestMethod]
        public void Ctor_Inits_RelatedClassId_To_Value_Passed()
        {
            var x = new ManyToOneRelationAttribute(55);
            Assert.AreEqual(55, x.RelatedClassId);
        }
    }
}
