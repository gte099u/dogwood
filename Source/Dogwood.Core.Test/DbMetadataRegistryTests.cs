﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class DbMetadataRegistryTests
    {
        public DbMetadataRegistryTests()
        {
            if (!DbMetadataRegistry.Instance.ContainsKey(99))
                DbMetadataRegistry.Instance.Add(99, new AttributeMetadataEntity
                {
                    AttributeId = 5,
                    ClassId = 40,
                    DataTypeId = (int)DogwoodDataType.DateTime,
                    IsNullable = false,
                    IsImmutable = true
                });

            if (!DbMetadataRegistry.Instance.ContainsKey(55))
                DbMetadataRegistry.Instance.Add(55, new AttributeMetadataEntity
                {
                    AttributeId = 6,
                    ClassId = 44,
                    DataTypeId = (int)DogwoodDataType.Bool,
                    IsNullable = true,
                    IsImmutable = false
                });

            if (!DbMetadataRegistry.Instance.ContainsKey(2))
                DbMetadataRegistry.Instance.Add(2, new AttributeMetadataEntity
                {
                    AttributeId = 6,
                    ClassId = 33,
                    DataTypeId = (int)DogwoodDataType.Bool,
                    IsNullable = true,
                    IsImmutable = false
                });
        }

        [TestMethod]
        public void GetKeyReturnsIdOfRegistryEntry()
        {
            int id = DbMetadataRegistry.GetKey(44, 6);
            Assert.AreEqual(55, id);
        }

        [TestMethod]
        public void GetKeyReturnsThrowsInformativeException()
        {
            bool exceptionThrown = false;
            string message = null;
            try
            {
                DbMetadataRegistry.GetKey(44, 7);
            }
            catch (Exception ex)
            {
                exceptionThrown = true;
                message = ex.Message;
            }

            Assert.AreEqual(true, exceptionThrown);
            Assert.AreEqual(
                "An attempt by the application to bind an attribute failed because it has not been registered in the database for the given class. ClassId: 44. AttributeId: 7",
                message);
        }
    }
}
