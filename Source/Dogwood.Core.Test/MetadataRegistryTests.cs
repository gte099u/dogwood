﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class MetadataRegistryTests
    {
        private class OrderEntity { }

        private class Customer { }

        private class Mail { }

        private class OrderLineEntity { }


        [TestMethod]
        public void GetNameAsPersisted_Trims_Entity()
        {
            Assert.AreEqual("Order", EntityMetadata.GetNameAsPersisted(typeof (OrderEntity)));
            Assert.AreEqual("OrderLine", EntityMetadata.GetNameAsPersisted(typeof(OrderLineEntity)));
        }

        [TestMethod]
        public void GetNameAsPersisted_Returns_Name()
        {
            Assert.AreEqual("Customer", EntityMetadata.GetNameAsPersisted(typeof(Customer)));
        }

        [TestMethod]
        public void GetNameAsPersisted_Works_For_Entity_With_Short_Name()
        {
            Assert.AreEqual("Mail", EntityMetadata.GetNameAsPersisted(typeof(Mail)));
        }
    }
}
