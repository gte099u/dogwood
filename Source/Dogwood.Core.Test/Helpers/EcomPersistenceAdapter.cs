﻿using System;
using System.Linq;
using Dogwood.Core.Entities;
using Dogwood.Core.Test.Data;
using Dogwood.Core.TestHelpers;

namespace Dogwood.Core.Test
{
    public class EcomPersistenceAdapter : FakePersistenceAdapter
    {
        public const int INITIAL_REVISION_COUNT = 3;
        public const int INITIAL_BRANCH_COUNT = 2;

        public EcomPersistenceAdapter()
        {
            // populate revisions
            _revisions.Add(0, new RevisionEntity
            {
                Id = 0,
                BranchId = 0,
                CreatedBy = 9999,
                Tag = "ROOT"
            });

            _revisions.Add(1, new RevisionEntity
            {
                Id = 1,
                BranchId = 0,
                CreatedBy = 9999,
                Tag = "Branch 1 Origin Point"
            });

            _revisions.Add(2, new RevisionEntity
            {
                Id = 2,
                BranchId = 1,
                CreatedBy = 9999
            });

            PopulateClassData();

            PopulateAttributeData();

            PopulateAttributeMetadata();

            PopulateRelationshipMeatadata();
        }


        private void PopulateAttributeMetadata()
        {
            _attributeMetadata.Add((int)AttributeMetadata.Customer_Birthday, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Customer_Birthday,
                ClassId = (int)Classes.Customer,
                AttributeId = (int)Attributes.Birthday,
                DataTypeId = (int)DogwoodDataType.DateTime,
                IsNullable = true,
                IsImmutable = true
            });

            _attributeMetadata.Add((int)AttributeMetadata.Customer_IsMale, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Customer_IsMale,
                ClassId = (int)Classes.Customer,
                AttributeId = (int)Attributes.IsMale,
                DataTypeId = (int)DogwoodDataType.Bool,
                IsNullable = false,
                IsImmutable = true
            });

            _attributeMetadata.Add((int)AttributeMetadata.Customer_Name, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Customer_Name,
                ClassId = (int)Classes.Customer,
                AttributeId = (int)Attributes.Name,
                DataTypeId = (int)DogwoodDataType.String,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.Customer_NickName, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Customer_NickName,
                ClassId = (int)Classes.Customer,
                AttributeId = (int)Attributes.Nickname,
                DataTypeId = (int)DogwoodDataType.String,
                IsNullable = true,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.Order_OrderDate, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Order_OrderDate,
                ClassId = (int)Classes.Order,
                AttributeId = (int)Attributes.OrderDate,
                DataTypeId = (int)DogwoodDataType.DateTime,
                IsNullable = false,
                IsImmutable = true
            });

            _attributeMetadata.Add((int)AttributeMetadata.Order_ShippedDate, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Order_ShippedDate,
                ClassId = (int)Classes.Order,
                AttributeId = (int)Attributes.ShippedDate,
                DataTypeId = (int)DogwoodDataType.DateTime,
                IsNullable = false,
                IsImmutable = true
            });

            _attributeMetadata.Add((int)AttributeMetadata.OrderLine_Quantity, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.OrderLine_Quantity,
                ClassId = (int)Classes.OrderLine,
                AttributeId = (int)Attributes.Quantity,
                DataTypeId = (int)DogwoodDataType.Int,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.Product_IsDiscontinued, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Product_IsDiscontinued,
                ClassId = (int)Classes.Product,
                AttributeId = (int)Attributes.IsDiscontinued,
                DataTypeId = (int)DogwoodDataType.Bool,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.Product_Name, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Product_Name,
                ClassId = (int)Classes.Product,
                AttributeId = (int)Attributes.Name,
                DataTypeId = (int)DogwoodDataType.String,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.Product_UnitPrice, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Product_UnitPrice,
                ClassId = (int)Classes.Product,
                AttributeId = (int)Attributes.UnitPrice,
                DataTypeId = (int)DogwoodDataType.Long,
                IsNullable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.Product_UnitsInStock, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Product_UnitsInStock,
                ClassId = (int)Classes.Product,
                AttributeId = (int)Attributes.UnitsInStock,
                DataTypeId = (int)DogwoodDataType.Int,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.Shipper_IsPrefered, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Shipper_IsPrefered,
                ClassId = (int)Classes.Shipper,
                AttributeId = (int)Attributes.IsPreferred,
                DataTypeId = (int)DogwoodDataType.Bool,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.Shipper_Name, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Shipper_Name,
                ClassId = (int)Classes.Shipper,
                AttributeId = (int)Attributes.Name,
                DataTypeId = (int)DogwoodDataType.String,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.Shipper_Phone, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.Shipper_Phone,
                ClassId = (int)Classes.Shipper,
                AttributeId = (int)Attributes.Phone,
                DataTypeId = (int)DogwoodDataType.String,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.HyperUniqueClass_Birthday, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.HyperUniqueClass_Birthday,
                ClassId = (int)Classes.HyperUniqueClass,
                AttributeId = (int)Attributes.Birthday,
                DataTypeId = (int)DogwoodDataType.DateTime,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.HyperUniqueClass_IsMale, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.HyperUniqueClass_IsMale,
                ClassId = (int)Classes.HyperUniqueClass,
                AttributeId = (int)Attributes.IsMale,
                DataTypeId = (int)DogwoodDataType.Bool,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.HyperUniqueClass_Name, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.HyperUniqueClass_Name,
                ClassId = (int)Classes.HyperUniqueClass,
                AttributeId = (int)Attributes.Name,
                DataTypeId = (int)DogwoodDataType.String,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.HyperUniqueClass_Quantity, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.HyperUniqueClass_Quantity,
                ClassId = (int)Classes.HyperUniqueClass,
                AttributeId = (int)Attributes.Quantity,
                DataTypeId = (int)DogwoodDataType.Long,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.HyperUniqueClass_UnitPrice, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.HyperUniqueClass_UnitPrice,
                ClassId = (int)Classes.HyperUniqueClass,
                AttributeId = (int)Attributes.UnitPrice,
                DataTypeId = (int)DogwoodDataType.Int,
                IsNullable = true,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.HyperUniqueClass_UnitsInStock, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.HyperUniqueClass_UnitsInStock,
                ClassId = (int)Classes.HyperUniqueClass,
                AttributeId = (int)Attributes.UnitsInStock,
                DataTypeId = (int)DogwoodDataType.Int,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.HyperUniqueClass_Phone, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.HyperUniqueClass_Phone,
                ClassId = (int)Classes.HyperUniqueClass,
                AttributeId = (int)Attributes.Phone,
                DataTypeId = (int)DogwoodDataType.String,
                IsNullable = false,
                IsImmutable = false
            });

            _attributeMetadata.Add((int)AttributeMetadata.HyperUniqueClass_ShippedDate, new AttributeMetadataEntity
            {
                Id = (int)AttributeMetadata.HyperUniqueClass_ShippedDate,
                ClassId = (int)Classes.HyperUniqueClass,
                AttributeId = (int)Attributes.ShippedDate,
                DataTypeId = (int)DogwoodDataType.DateTime,
                IsNullable = false,
                IsImmutable = false
            });
        }

        private void PopulateAttributeData()
        {
            foreach (var result in Enum.GetValues(typeof(Attributes)).Cast<Attributes>().Select(
                    m => new AttributeEntity { Id = (int)m, Value = m.ToString() }))
            {
                _attributes.Add(result.Id, result);
            }
        }

        private void PopulateRelationshipMeatadata()
        {
            _relationMetadata.Add((int)RelationshipMetadata.Order_CustomerId, new RelationshipMetadataEntity
            {
                Id = (int)RelationshipMetadata.Order_CustomerId,
                FkClassId = (int)Classes.Order,
                PkClassId = (int)Classes.Customer,
                AttributeId = (int)Attributes.CustomerId,
                CascadeBehavior = CascadeBehaviorType.Delete,
                CascadeDefaultValue = null,
                IsNullable = false,
                IsImmutable = true,
                OneToManyValue = "Orders",
            });

            _relationMetadata.Add((int)RelationshipMetadata.Order_CarrierId, new RelationshipMetadataEntity
            {
                Id = (int)RelationshipMetadata.Order_CarrierId,
                FkClassId = (int)Classes.Order,
                PkClassId = (int)Classes.Shipper,
                AttributeId = (int)Attributes.CarrierId,
                CascadeBehavior = CascadeBehaviorType.SetToDefault,
                CascadeDefaultValue = 8989,
                IsNullable = true,
                IsImmutable = false,
                OneToManyValue = "OrdersShipped",
            });

            _relationMetadata.Add((int)RelationshipMetadata.OrderLine_OrderId, new RelationshipMetadataEntity
            {
                Id = (int)RelationshipMetadata.OrderLine_OrderId,
                FkClassId = (int)Classes.OrderLine,
                PkClassId = (int)Classes.Order,
                AttributeId = (int)Attributes.OrderId,
                CascadeBehavior = CascadeBehaviorType.Delete,
                CascadeDefaultValue = null,
                IsNullable = false,
                IsImmutable = true,
                OneToManyValue = "OrderLines",
            });

            _relationMetadata.Add((int)RelationshipMetadata.OrderLine_ProductId, new RelationshipMetadataEntity
            {
                Id = (int)RelationshipMetadata.OrderLine_ProductId,
                FkClassId = (int)Classes.OrderLine,
                PkClassId = (int)Classes.Product,
                AttributeId = (int)Attributes.ProductId,
                CascadeBehavior = CascadeBehaviorType.Delete,
                CascadeDefaultValue = null,
                IsNullable = false,
                IsImmutable = true,
                OneToManyValue = "OrderLines",
            });

            _relationMetadata.Add((int)RelationshipMetadata.HyperUniqueClass_ProductId, new RelationshipMetadataEntity
            {
                Id = (int)RelationshipMetadata.HyperUniqueClass_ProductId,
                FkClassId = (int)Classes.HyperUniqueClass,
                PkClassId = (int)Classes.Product,
                AttributeId = (int)Attributes.ProductId,
                CascadeBehavior = CascadeBehaviorType.Delete,
                CascadeDefaultValue = null,
                IsNullable = false,
                IsImmutable = false,
                OneToManyValue = null
            });

            _relationMetadata.Add((int)RelationshipMetadata.Employee_DepartmentId, new RelationshipMetadataEntity
            {
                Id = (int)RelationshipMetadata.Employee_DepartmentId,
                FkClassId = (int)Classes.Employee,
                PkClassId = (int)Classes.Department,
                AttributeId = (int)Attributes.DepartmentId,
                CascadeBehavior = CascadeBehaviorType.Delete,
                CascadeDefaultValue = null,
                IsNullable = false,
                IsImmutable = false,
                OneToManyValue = null
            });
        }

        private void PopulateClassData()
        {
            foreach (var result in Enum.GetValues(typeof(Classes)).Cast<Classes>().Select(
                    m => new ClassEntity { Id = (int)m, Name = m.ToString() }))
            {
                _classes.Add(result.Id, result);
            }
        }
    }
}
