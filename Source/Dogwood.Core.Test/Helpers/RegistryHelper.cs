﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dogwood.Core.Test
{
    public static class RegistryHelper
    {
        public static void EmptyRegistries()
        {
            DbAttributeRegistry.Instance.Clear();
            DbFkMetadataRegistry.Instance.Clear();
            DbMetadataRegistry.Instance.Clear();
            DbTypeRegistry.Instance.Clear();
        }
    }
}
