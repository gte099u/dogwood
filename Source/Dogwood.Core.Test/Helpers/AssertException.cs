﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    public static class AssertException
    {
        public static void OfType<T>(Action cmd, string expectedMessage) where T : Exception
        {
            var exceptionThrown = false;
            var exceptionMessage = string.Empty;

            try
            {
                cmd();
            }
            catch (Exception ex)
            {
                
                exceptionThrown = true;
                exceptionMessage = ex.Message;
                Assert.IsInstanceOfType(ex, typeof(T));
            }
            
            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual(expectedMessage, exceptionMessage);
        }       

        public static void IsThrown(Action cmd, string expectedMessage)
        {
            var exceptionThrown = false;
            var exceptionMessage = string.Empty;

            try
            {
                cmd();
            }
            catch (Exception ex)
            {

                exceptionThrown = true;
                exceptionMessage = ex.Message;
            }

            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual(expectedMessage, exceptionMessage);
        }

        public static void IsThrown(Action cmd)
        {
            var exceptionThrown = false;

            try
            {
                cmd();
            }
            catch (Exception)
            {
                exceptionThrown = true;
            }

            Assert.IsTrue(exceptionThrown);
        }
    }
}
