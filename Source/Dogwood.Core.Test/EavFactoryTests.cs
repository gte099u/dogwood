﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Test.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class EavFactoryTests
    {
        [TestInitialize]
        public void TestInit()
        {
            RegistryHelper.EmptyRegistries();
        }

        [TestMethod]
        public void Create_Throws_Exception_If_Value_Not_Provided()
        {
            var adapter = new EcomPersistenceAdapter();
            Bootstrapper.Init(false, typeof(EcomPersistenceAdapter), new List<Type> { typeof(Customer) }, adapter);

            var objRev = new Entities.ObjectRevisionEntity
                             {
                                 Id = 1,
                                 IsDeleted = false,
                                 ObjectId = 111,
                                 RevisionId = 44,
                                 AttributeValues = new List<Entities.AttributeValueEntity>
                                    {
                                        new Entities.AttributeValueEntity
                                                {
                                                    Id = 1,
                                                    AttributeMetadataId =(int)AttributeMetadata.Customer_Birthday,
                                                    ObjectRevisionId = 1,
                                                    Value =DataConverter.DateTimeToBytes(DateTime.Now)
                                                },
                                        new Entities.AttributeValueEntity
                                        {
                                            Id = 2,
                                            AttributeMetadataId =(int)AttributeMetadata.Customer_IsMale,
                                            ObjectRevisionId = 1,
                                            Value =DataConverter.BoolToBytes(true)
                                        }
                                        //~ name is not provided, but is not-nullable
                                        //,
                                        //new Entities.AttributeValueEntity
                                        //{
                                        //    Id = 3,
                                        //    AttributeMetadataId =(int)AttributeMetadata.Customer_Name,
                                        //    ObjectRevisionId = 1,
                                        //    Value =DataConverter.StringToBytes("Ben")
                                        //}
                                    }
                             };

            var factory = new EavFactory<Customer>();

            AssertException.IsThrown(() => factory.Create(objRev), "Required field for object was not provided");
        }
    }
}
