﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dogwood.Core.Test
{
    [TestClass]
    public class UniqueConstraintAttributeTests
    {
        [TestMethod]
        public void Default_Ctor_Inits_ExceptionMessage()
        {
            var x = new UniqueConstraintAttribute("Exception message here", "Field1", "Field2");
            Assert.AreEqual("Exception message here", x.ExceptionMessage);
        }

        [TestMethod]
        public void Default_Ctor_Inits_UniqueComboFields()
        {
            var x = new UniqueConstraintAttribute("Exception message here", "Field1", "Field2");
            Assert.AreEqual(2, x.UniqueComboFields.Length);
        }

        [TestMethod]
        public void Ctor_Throws_Exception_If_Combo_Fields_Is_Empty()
        {
            bool exceptionThrown = false;
            string exceptionMessage = string.Empty;

            try
            {
                new UniqueConstraintAttribute("Exception message here");
            }
            catch (Exception ex)
            {
                exceptionMessage = ex.Message;
                exceptionThrown = true;
            }
           
            Assert.IsTrue(exceptionThrown);
            Assert.AreEqual("Unique constraints must have at least one field defined", exceptionMessage);
        }        
    }
}
