﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Dogwood.Core
{
    public static class DataConverter
    {
        public static object ConvertToTypedData(byte[] bytes, DogwoodDataType dataType)
        {
            switch (dataType)
            {
                case DogwoodDataType.String:
                    return BytesToString(bytes);
                case DogwoodDataType.Int:
                    return BytesToInt32(bytes);
                case DogwoodDataType.Long:
                    return BytesToInt64(bytes);
                case DogwoodDataType.Bool:
                    return BytesToBool(bytes);
                case DogwoodDataType.DateTime:
                    return BytesToDateTime(bytes);
            }

            throw new Exception("Could not convert the provided type");
        }

        public static byte[] ConvertToBytes(object value, DogwoodDataType dataType)
        {
            switch (dataType)
            {
                case DogwoodDataType.String:
                    return StringToBytes((string)value);
                case DogwoodDataType.Int:
                    return Int32ToBytes((int)value);
                case DogwoodDataType.Long:
                    return Int64ToBytes((long)value);
                case DogwoodDataType.Bool:
                    return BoolToBytes((bool)value);
                case DogwoodDataType.DateTime:
                    return DateTimeToBytes((DateTime)value);
            }

            throw new Exception("Unable to convert the provided type");
        }

        public static bool BytesToBool(byte[] bytes)
        {
            return BitConverter.ToBoolean(bytes, 0);
        }

        public static char BytesToChar(byte[] bytes)
        {
            return BitConverter.ToChar(bytes, 0);
        }

        public static string BytesToString(byte[] bytes)
        {
            return System.Text.Encoding.UTF8.GetString(bytes);
        }

        public static short BytesToInt16(byte[] bytes)
        {
            return IPAddress.NetworkToHostOrder(BitConverter.ToInt16(bytes, bytes.Length - 2));
        }

        public static int BytesToInt32(byte[] bytes)
        {
            return IPAddress.NetworkToHostOrder(BitConverter.ToInt32(bytes, 0));
        }

        public static long BytesToInt64(byte[] bytes)
        {
            return IPAddress.NetworkToHostOrder(BitConverter.ToInt64(bytes, 0));
        }

        public static DateTime BytesToDateTime(byte[] bytes)
        {
            return DateTime.FromBinary(BytesToInt64(bytes));
        }

        public static byte[] BoolToBytes(bool value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] CharToBytes(char value)
        {
            return BitConverter.GetBytes(value);
        }

        public static byte[] DateTimeToBytes(DateTime value)
        {
            return Int64ToBytes(value.Ticks);
        }

        public static byte[] Int16ToBytes(short value)
        {        
            return BitConverter.GetBytes(IPAddress.HostToNetworkOrder(value));
        }

        public static byte[] Int32ToBytes(int value)
        {
            return BitConverter.GetBytes(IPAddress.HostToNetworkOrder(value));
        }

        public static byte[] Int64ToBytes(long value)
        {
            return BitConverter.GetBytes(IPAddress.HostToNetworkOrder(value));
        }

        public static byte[] StringToBytes(string value)
        {
            return System.Text.Encoding.UTF8.GetBytes(value);
        }
    }
}
