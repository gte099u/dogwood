﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public class EntityValidation<T> : IEntityValidation<T> where T : IDogwoodEntity
    {
        private readonly IEntityMetaData _entityMetadata;
        private readonly int _branchId;
        private readonly ObjectCache _cache;
        private readonly IPersistenceAdapter _adapter;

        public EntityValidation(IPersistenceAdapter adapter, IEntityMetaData entityMetadata, int branchId, ObjectCache cache)
        {
            _adapter = adapter;
            _entityMetadata = entityMetadata;
            _branchId = branchId;
            _cache = cache;
        }

        public void ValidateBeforeUpdate(T toSave, T existing)
        {
            ValidateRelationships(toSave);

            ValidateUniqueConstraints(toSave);

            ValidateCustomValidationAttributes(toSave);

            ValidateImmutability(toSave, existing); //~ immutability only checked on updates

            ValidateNullableValue(toSave);
        }

        public void ValidateBeforeCreate(T toSave)
        {
            ValidateRelationships(toSave);

            ValidateUniqueConstraints(toSave);

            ValidateCustomValidationAttributes(toSave);

            ValidateNullableValue(toSave);
        }

        public void ValidateRelationships(T toSave)
        {
            //~ validate that all foreign key references point to objects that
            //~ exist in the ancestry of the object being saved, and that the object
            //~ is of the type declared in the FK metadata
            foreach (var fk in _entityMetadata.ManyToOneRelationships)
            {
                var info = toSave.GetType().GetProperty(fk.Value.Property.Name);
                var x = (int?)info.GetValue(toSave, null);

                if (x != null)
                {
                    BranchHeadObjectEntity headRecord = _cache.GetHeadObject(_branchId, (int)x);

                    if (headRecord == null)
                    {
                        headRecord = _adapter.GetValueForObjectInBranch(_branchId, (int)x, false);

                        if (headRecord == null)
                        {
                            throw new Exception(string.Format("Foreign key attribute {0} refers to an entity that does not exist in this branch", info.Name));
                        }

                        _cache.SetHeadObject(headRecord);
                    }

                    if (headRecord.ClassId != fk.Value.ClassId)
                    {
                        throw new Exception(string.Format("The foreign key reference \"{0}\" of the object is pointing to an object of the wrong type", info.Name));
                    }
                }
            }
        }

        public void ValidateUniqueConstraints(T toSave)
        {
            //~ validate uniqueness for the given branch according to unique constraint metadata
            //~ on the object being saved
            foreach (var constraint in _entityMetadata.UniqueConstraints)
            {
                var filter = new PredicateBucket<T>(_branchId);

                foreach (var property in constraint) //~ some constraints are multi-part (e.g. OrderLineId, ProductId)
                {
                    //~ if attempting to save null value, skip the unique check - nulls are always permitted
                    if (property.GetValue(toSave, null) == null) break;

                    if (property.GetCustomAttributes(typeof(MappableAttribute), true).Any()) //~ mappable data type property (string, int, long, bool, etc)
                    {                        
                        if (property.PropertyType == typeof(string))
                        {                            
                            filter.AddAttributeComparePredicate(property, (string)property.GetValue(toSave, null));
                        }

                        else if (property.PropertyType == typeof(bool))
                        {
                            filter.AddAttributeComparePredicate(property, (bool)property.GetValue(toSave, null));
                        }

                        else if (property.PropertyType == typeof(DateTime))
                        {
                            filter.AddAttributeComparePredicate(property, (DateTime)property.GetValue(toSave, null));
                        }

                        else if (property.PropertyType == typeof(long))
                        {
                            filter.AddAttributeComparePredicate(property, (long)property.GetValue(toSave, null));
                        }

                        else if (property.PropertyType == typeof(int))
                        {
                            filter.AddAttributeComparePredicate(property, (int)property.GetValue(toSave, null));
                        }

                        else if (property.PropertyType == typeof(bool?))
                        {
                            filter.AddAttributeComparePredicate(property, (bool?)property.GetValue(toSave, null));
                        }

                        else if (property.PropertyType == typeof(DateTime?))
                        {
                            filter.AddAttributeComparePredicate(property, (DateTime?)property.GetValue(toSave, null));
                        }

                        else if (property.PropertyType == typeof(long?))
                        {
                            filter.AddAttributeComparePredicate(property, (long?)property.GetValue(toSave, null));
                        }

                        else if (property.PropertyType == typeof(int?))
                        {
                            filter.AddAttributeComparePredicate(property, (int?)property.GetValue(toSave, null));
                        }  
                    }
                    else //~ foreign Key attribute
                    {
                        filter.AddRelationPredicate(property, (int)property.GetValue(toSave, null));
                    }                    
                }

                if (filter.IsEmpty) break;

                var objRevs = _adapter.GetBranchHeadObjectsWithFilter(filter).Where(m=>m.ObjectId != toSave.Id);

                if (objRevs.Any())
                {
                    throw new Exception(constraint.ExceptionMessage ?? "Unique constraint violated");
                }
            }
        }

        public static void ValidateCustomValidationAttributes(T toSave)
        {
            //~ validate any custom validation checks as defined by the object's metadata
            foreach (var property in toSave.GetType().GetProperties())
            {
                foreach (var validationAttribute in property.GetCustomAttributes(typeof(ValidationAttribute), false))
                {
                    var att = (ValidationAttribute)validationAttribute;

                    if (att.GetValidationResult(property.GetValue(toSave, null), new ValidationContext(toSave, null, null)) != ValidationResult.Success)
                    {
                        throw new Exception(att.ErrorMessage ?? string.Format("Validation for field {0} failed", property.Name));
                    }
                }
            }
        }

        public void ValidateImmutability(T toSave, T existing)
        {
            foreach (var immutableAttributes in MetadataRegistry.Instance[typeof(T)].Attributes.Where(m => m.IsImmutable))
            {
                if (!immutableAttributes.Property.GetValue(toSave, null).Equals(immutableAttributes.Property.GetValue(existing, null)))
                {
                    throw new Exception(string.Format("The {0} value is read-only and may not be changed", immutableAttributes.Property.Name));
                }
            }

            foreach (var immutableRelationship in MetadataRegistry.Instance[typeof(T)].ManyToOneRelationships.Where(m => m.Value.IsImmutable))
            {
                if (immutableRelationship.Value.Property.GetValue(toSave, null) != immutableRelationship.Value.Property.GetValue(existing, null))
                {
                    throw new Exception(string.Format("{0} is readonly and may not be changed", immutableRelationship.Value.Property.Name));
                }
            }
        }

        public void ValidateNullableValue(IDogwoodEntity toSave)
        {
            foreach (var nonNullableAttributes in MetadataRegistry.Instance[typeof(T)].Attributes.Where(m => !m.IsNullable))
            {
                if (nonNullableAttributes.Property.GetValue(toSave, null) == null)
                {
                    throw new Exception(string.Format("{0} is required", nonNullableAttributes.Property.Name));
                }
            }

            foreach (var immutableRelationship in MetadataRegistry.Instance[typeof(T)].ManyToOneRelationships.Where(m => m.Value.IsImmutable))
            {
                if (immutableRelationship.Value.Property.GetValue(toSave, null) == null)
                {
                    throw new Exception(string.Format("{0} is required", immutableRelationship.Value.Property.Name));
                }
            }
        }
    }
}
