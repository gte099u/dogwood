﻿using System;
using System.Collections.Generic;
using StructureMap;
using StructureMap.Configuration.DSL;

namespace Dogwood.Core
{
    public static class Bootstrapper
    {
        /// <summary>
        /// Initializes the Dogwood framework for use.  This method must be called at the application entry point before any other Dogwood 
        /// functionality may be used.
        /// </summary>
        /// <param name="isHttpContextScoped">If Dogwood is being used within an HTTP application context, this should be set to true.  This specifies which
        /// type of caching scheme will be used by Dogwood to reduce overhead.</param>
        /// <param name="adapterType"> </param>
        /// <param name="entityTypes"> </param>
        /// <param name="adapterToUse"> </param>              
        public static void Init(bool isHttpContextScoped, Type adapterType, IEnumerable<Type> entityTypes, IPersistenceAdapter adapterToUse = null)
        {
            if (isHttpContextScoped)
            {
                var r = new Registry();
                r.For(typeof(IPersistenceAdapter)).HttpContextScoped().Use(adapterType);

                ObjectFactory.Configure(x => x.AddRegistry(r));
            }
            else
            {
                var r = new Registry();
                r.For(typeof(IPersistenceAdapter)).Use(adapterType);

                ObjectFactory.Configure(x => x.AddRegistry(r));
            }

            if (adapterToUse == null)
            {
                var adapter = ObjectFactory.GetInstance<IPersistenceAdapter>();
                RegistryLoader.LoadAll(adapter);
            }
            else
            {
                RegistryLoader.LoadAll(adapterToUse);
            }

            MetadataRegistry.Fill(entityTypes);
        }            
    }
} 
