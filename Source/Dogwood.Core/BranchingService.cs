﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public class BranchingService
    {
        private readonly IPersistenceAdapter _adapter;

        public BranchingService(IPersistenceAdapter adapter)
        {
            _adapter = adapter;
        }

        public BranchData GetBranch(int branchId)
        {
            RevisionEntity toRet = _adapter.GetRevision(branchId);

            return new BranchData(toRet.Id, toRet.Tag, toRet.CreatedBy ?? 0, toRet.BranchId);
        }

        public int CreateBranch(int revisionId, int userId, string tag)
        {
            if (revisionId == 0)
            {
                throw new Exception("Can not branch off ROOT");
            }

            var existing = _adapter.GetRevision(revisionId);

            if(existing == null)
            {
                throw new Exception("The revisionId provided does not represent an existing revision");
            }

            //~ if the revision provided already serves as a branch
            var revisionsInBranch = _adapter.GetAllRevisionsByBranchId(revisionId).ToList();
            if (revisionsInBranch.Any())
            {
                throw new Exception(
                    string.Format(
                        "The revision provided as a point of branching already is a branch point.  Possibly consider using revision {0} as a branch point instead",
                        revisionsInBranch.Min(m => m.Id)));
            }

            //~ create a new branch off that point            
            var branchRev = new RevisionEntity { BranchId = revisionId, Tag = tag, TimeStamp = DateTime.Now, CreatedBy = userId };

            _adapter.SaveRevision(branchRev);

            _adapter.FillHeadObjects(); //~ refreshes head object table

            return revisionId; //~ the branchId is the revisionId that was branched from
        }

        public int GetDeviationPoint(int sourceBranchId, int targetBranchId)
        {
            int toRet = sourceBranchId;
            int previousBranchId = GetBranchPoint(toRet);

            while (previousBranchId != targetBranchId)
            {
                if (toRet == 1)
                {
                    throw new Exception("source did not branch from the target");
                }

                toRet = previousBranchId;
                previousBranchId = GetBranchPoint(toRet);
            }

            return toRet;
        }

        private int GetBranchPoint(int revisionId)
        {
            var rev = _adapter.GetRevision(revisionId);
            return rev.BranchId;
        }

        public int GetLatestRevisionForBranch(int branchId)
        {
            return _adapter.GetAllRevisionsByBranchId(branchId).OrderByDescending(m => m.Id).First().Id;
        }

        public List<int> GetRevisionsWithTag(int branchId, string tag)
        {
            IEnumerable<RevisionEntity> revisions = _adapter.GetAllRevisionsHavingTag(branchId, tag);
            return revisions.Select(m => m.Id).ToList();
        }

        public int CreateRevision(int branchId, int userId, string tag, string comments)
        {
            //~ can't operate off of ROOT
            if (branchId <= 1)
            {
                throw new Exception("not a valid branch");
            }

            //~ save a new revision
            var revision = new RevisionEntity
                               {BranchId = branchId, TimeStamp = DateTime.Now, Tag = tag, CreatedBy = userId, Comments = comments};

            _adapter.SaveRevision(revision);
            return revision.Id;
        }
    }
}
