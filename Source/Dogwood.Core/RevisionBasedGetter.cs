﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dogwood.Core
{
    public class RevisionBasedGetter<T> : IVersionedRetrieval<T> where T : class, IDogwoodEntity, new()
    {
        private readonly ObjectCache _objectCache;
        private readonly IEntityMetaData _entityMetadata;
        private readonly IObjectBuilder<T> _objectBuilder;
        private readonly IPersistenceAdapter _adapter;
        private readonly int _ContextId;
        private readonly int _branchId;

        public RevisionBasedGetter(IDogwoodDataContext context)
        {
            if (context.ContextType != EavContextType.Revision)
            {
                throw new Exception("Revision context type expected but not provided");
            }

            _objectCache = context.ObjectCache;
            _adapter = context.PersistenceAdapter;

            _objectBuilder = new VerboseObjectBuilder<T>(context.ObjectRevisionCache, _adapter);

            _ContextId = context.ContextId;
            _branchId = _adapter.GetRevision(_ContextId).BranchId;
            _entityMetadata = MetadataRegistry.Instance[typeof(T)];

            _adapter.AddRevisionToHeadObjects(_branchId, _ContextId);
        }

        /// <summary>
        /// Retreives the value of the entity of type T at a specific point in time.  Deleted entities are not
        /// included in the list.
        /// </summary>
        /// <param name="id">primary key of the entity</param>
        /// <returns>entity of type T if current value exists, or null if entity is not found at the revision or has been deleted</returns>
        public T Get(int id)
        {
            var toRet = default(T);

            // check our cache of all objects for the provided id
            if (_objectCache.ContainsObject(id))
            {
                toRet = (T)_objectCache.GetObject(id);
            }

            // if not already in the cache, fetch from the db
            if (toRet == null)
            {
                var currentObjectRev = _adapter.GetValueForObjectAtRevision(_branchId, id, _ContextId);
                toRet = _objectBuilder.BuildObject(currentObjectRev.ObjectRevision);

                // add it to the cache for future quick fetching
                _objectCache.SetObject(toRet);
            }

            return toRet;
        }

        /// <summary>
        /// Retreives a list of current values for all entities of type T at a given point in time
        /// </summary>
        /// <returns>list of all T entities whose state is not deleted</returns>
        public List<T> GetAll()
        {
            var dt = _adapter.GetAllDataForRevisionByType(_branchId, _ContextId, _entityMetadata.ClassId);
            var toRet = _objectBuilder.BuildCollection(dt);

            // add each individual item to the "AllItems" Cache
            foreach (var item in toRet)
            {
                if (!_objectCache.ContainsObject(item.Id))
                {
                    _objectCache.SetObject(item);
                }
            }

            return toRet;
        }

        public List<T> GetAll(IList<int> idList)
        {
            var toRet = new List<T>();

            for (var i = idList.Count - 1; i >= 0; i--)
            {
                if (_objectCache.ContainsObject(idList[i]))
                {
                    toRet.Add((T)_objectCache.GetObject(idList[i]));
                    idList.Remove(idList[i]);
                }
            }

            if (idList.Count != 0)
            {
                var dt = _adapter.GetAllAtRevisionWithId(_branchId, _ContextId, idList);
                toRet.AddRange(_objectBuilder.BuildCollection(dt));

                foreach (var item in toRet)
                {
                    if (!_objectCache.ContainsObject(item.Id))
                    {
                        _objectCache.SetObject(item);
                    }
                }
            }

            return toRet;
        }

        public List<T> GetAll(IPredicateBucket filterBucket)
        {
            if(filterBucket == null)
            {
                throw new NullReferenceException("FilterBucket may not be null");
            }
            var objectRevisions = _adapter.GetRevisionHeadObjectsWithFilter(filterBucket);
            var factory = new EavFactory<T>();
            return factory.CreateMulti(objectRevisions).ToList();
        }


        public IEnumerable<ObjectVersion<T>> GetObjectHistory(int objectId, int contextId)
        {
            throw new NotImplementedException();
        }
    }
}
