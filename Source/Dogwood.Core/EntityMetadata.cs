﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Dogwood.Core
{
    public class EntityMetadata : IEntityMetaData
    {
        public int ClassId { get; set; }
        public List<UniqueConstraint> UniqueConstraints { get; private set; }
        public List<MemberMap> Attributes { get; private set; }
        public Dictionary<string, ManyToOneMap> ManyToOneRelationships { get; private set; }
        public Dictionary<string, OneToManyMap> OneToManyRelationships { get; private set; }

        public EntityMetadata()
        {
            Attributes = new List<MemberMap>();
            OneToManyRelationships = new Dictionary<string, OneToManyMap>();
            ManyToOneRelationships = new Dictionary<string, ManyToOneMap>();
            UniqueConstraints = new List<UniqueConstraint>();
        }

        public int GetFkMetadataId(int pkClassId, int fkAttributeId)
        {
            return DbFkMetadataRegistry.GetKey(ClassId, fkAttributeId);
        }

        public int GetMetadataId(int attributeId)
        {
            return DbMetadataRegistry.GetKey(ClassId, attributeId);
        }

        public void AddAttribute(PropertyInfo property)
        {
            if (property.PropertyType.IsAvailableInDogwood())
            {
                DogwoodDataType dataType = property.PropertyType.ToDogwoodDataType();

                if (property.GetCustomAttributes(typeof(MappableAttribute), true).Any())
                {
                    var map = new MemberMap
                    {
                        DataType = dataType,
                        AttributeMetadataId = GetMetadataId(DbAttributeRegistry.GetKey(property.Name))
                    };
                    map.IsNullable = DbMetadataRegistry.Instance[map.AttributeMetadataId].IsNullable;
                    map.IsImmutable = DbMetadataRegistry.Instance[map.AttributeMetadataId].IsImmutable;
                    map.Property = property;
                    Attributes.Add(map);
                }
            }
        }

        public static string GetNameAsPersisted(Type type)
        {
            if (type.Name.Length <= 6)
            {
                return type.Name;
            }

            return type.Name.Substring(type.Name.Length - 6) == "Entity"
                                        ? type.Name.Remove(type.Name.Length - 6)
                                        : type.Name;
        }
    }
}
