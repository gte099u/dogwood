﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dogwood.Core
{
    /// <summary>
    /// A singleton dictionary serving as the in-memory store for Attribute records that have been retrieved from
    /// the database on application start.  Because attributes should not change during application runtime, this 
    /// data may be stored in memory to eliminate unnecessary database round trips.
    /// </summary>
    public class DbAttributeRegistry : Dictionary<int, string>
    {
        static DbAttributeRegistry instance;
        static readonly object padlock = new object();

        private DbAttributeRegistry() { }

        public static DbAttributeRegistry Instance
        {
            get
            {
                lock (padlock)
                {
                    return instance ?? (instance = new DbAttributeRegistry());
                }
            }
        }

        public static int GetKey(string propertyName)
        {
            try
            {
                return Instance.Single(m => m.Value == propertyName).Key;
            }
            catch (Exception)
            {
                throw new Exception(string.Format("A property named {0} was expected in the database attribute table but was not found. Please reconcile any entity metadata specifications and reboot the application", propertyName));                
            }            
        }
    }
}
