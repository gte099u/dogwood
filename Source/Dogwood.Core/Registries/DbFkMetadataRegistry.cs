﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    /// <summary>
    /// A singleton dictionary serving as the in-memory store for Relationship Attribute Metadata records that have been retrieved from
    /// the database on application start.  Because relation attribute metadata should not change during application runtime, this 
    /// data may be stored in memory to eliminate unnecessary database round trips.
    /// </summary>
    public class DbFkMetadataRegistry : Dictionary<int, RelationshipMetadataEntity>
    {
        static DbFkMetadataRegistry instance;
        static readonly object padlock = new object();

        private DbFkMetadataRegistry() { }

        public static DbFkMetadataRegistry Instance
        {
            get
            {
                lock (padlock)
                {
                    return instance ?? (instance = new DbFkMetadataRegistry());
                }
            }
        }

        public static int GetKey(int fkClassId, int AttributeId)
        {
            try
            {
                return Instance.Single(m => m.Value.FkClassId == fkClassId && m.Value.AttributeId == AttributeId).Key;
            }
            catch (Exception)
            {
                throw new Exception(string.Format("An attempt by the application to bind the {0} relation property on class type {1} failed because it has not been registered in the database.", AttributeId, fkClassId));
            }
        }
    }
}
