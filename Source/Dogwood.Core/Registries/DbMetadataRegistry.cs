﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    /// <summary>
    /// A singleton dictionary serving as the in-memory store for Attribute Metadata records that have been retrieved from
    /// the database on application start.  Because attribute metadata should not change during application runtime, this 
    /// data may be stored in memory to eliminate unnecessary database round trips.
    /// </summary>
    public class DbMetadataRegistry : Dictionary<int, AttributeMetadataEntity>
    {
        static DbMetadataRegistry instance;
        static readonly object padlock = new object();

        private DbMetadataRegistry() { }

        public static DbMetadataRegistry Instance
        {
            get
            {
                lock (padlock)
                {
                    return instance ?? (instance = new DbMetadataRegistry());
                }
            }
        }

        /// <summary>
        /// Given a class id and attribute id, returns the corresponding AttributeMetadataId from the in memory registry.  If no corresponding key is found,
        /// an exception will be thrown.
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="AttributeId"></param>
        /// <returns></returns>
        public static int GetKey(int classId, int AttributeId)
        {
            try
            {
                return Instance.Single(m => m.Value.ClassId == classId && m.Value.AttributeId == AttributeId).Key;
            }
            catch (Exception)
            {
                throw new Exception(string.Format("An attempt by the application to bind an attribute failed because it has not been registered in the database for the given class. ClassId: {0}. AttributeId: {1}", classId, AttributeId));
            }
        }        
    }
}
