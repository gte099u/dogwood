﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    /// <summary>
    /// A singleton registry of Id, Name pairs for all classes defined in the db.  Used by Dogwood to interchange class Names with Ids without a db round trip
    /// </summary>
    public class DbTypeRegistry : Dictionary<int, ClassEntity>
    {
        static DbTypeRegistry instance;
        static readonly object padlock = new object();

        private DbTypeRegistry() { }

        public static DbTypeRegistry Instance
        {
            get
            {
                lock (padlock)
                {
                    return instance ?? (instance = new DbTypeRegistry());
                }
            }
        }

        public static int GetTypeId(string name)
        {
            try
            {
                return Instance.Single(m => m.Value.Name == name).Key;
            }
            catch (Exception)
            {
                throw new Exception(string.Format("A class type named {0} was expected in the database but was not found.  Please reconcile the collection of entities passed to the bootstrapper reboot the application.", name));
            }
        }
    }
}
