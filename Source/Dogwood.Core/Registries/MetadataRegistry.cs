﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dogwood.Core
{
    public class MetadataRegistry : Dictionary<Type, IEntityMetaData>
    {
        static MetadataRegistry instance;
        static readonly object padlock = new object();

        private MetadataRegistry() { }

        public static MetadataRegistry Instance
        {
            get
            {
                lock (padlock)
                {
                    return instance ?? (instance = new MetadataRegistry());
                }
            }
        }

        public static void Fill(IEnumerable<Type> entityTypes)
        {
            // scan all classes in the executing assembly that implement IDogwoodEntity
            foreach (var typ in entityTypes)
            {
                string entityName = EntityMetadata.GetNameAsPersisted(typ);
                int classId = DbTypeRegistry.Instance.Single(m => m.Value.Name == entityName).Key;
                IEntityMetaData meta = EntityMetadataFactory.Create(classId, typ);

                //~ add Type/Metadata combo to MetadataRegistry
                if (!Instance.ContainsKey(typ))
                {
                    Instance.Add(typ, meta);
                }
            }
        }
    }
}
