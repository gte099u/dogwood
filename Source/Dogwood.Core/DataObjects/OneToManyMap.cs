﻿using System.Reflection;

namespace Dogwood.Core
{
    public class OneToManyMap : IRelationshipMap
    {
        public int ClassId { get; set; }
        public PropertyInfo ForeignKey { get; set; }
        public PropertyInfo ForeignKeyProperty { get; set; }
        public int ForeignKeyAttributeId { get; set; }
        public PropertyInfo PrimaryKey { get; set; }
        public PropertyInfo RelationProperty { get; set; }
    }
}
