﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dogwood.Core
{
    public class RelationPredicate
    {
        public int RelationMetadatatId { get; set; }
        public bool Negate { get; set; }
        public int? Value { get; set; }
    }
}
