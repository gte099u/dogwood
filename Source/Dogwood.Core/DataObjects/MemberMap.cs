﻿using System.Reflection;

namespace Dogwood.Core
{
    public class MemberMap : IMemberMap
    {
        public DogwoodDataType DataType { get; set; }
        public int AttributeMetadataId { get; set; }
        public PropertyInfo Property { get; set; }
        public bool IsNullable { get; set; }
        public bool IsImmutable { get; set; }
    }
}
