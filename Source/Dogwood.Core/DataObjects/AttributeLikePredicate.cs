﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dogwood.Core
{
    public class AttributeLikePredicate
    {
        public int? AttributeMetadatatId { get; set; }
        public LikeComparisonOperator CompOperator { get; set; }
        public string Value { get; set; }
    }
}
