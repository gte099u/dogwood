﻿namespace Dogwood.Core
{
    public class PrefetchElement
    {
        private PrefetchPath _SubPath;
        public IRelationshipMap FkMap { get; set; }
        public PrefetchPath SubPath
        {
            get { return _SubPath ?? (_SubPath = new PrefetchPath()); }
        }

        public PrefetchElement() { }

        public PrefetchElement(IRelationshipMap relationship)
        {
            FkMap = relationship;
        }
    }
}
