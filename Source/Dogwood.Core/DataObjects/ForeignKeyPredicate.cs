﻿using System.Collections.Generic;

namespace Dogwood.Core
{
    public class ForeignKeyPredicate
    {
        public int FkAttributeId { get; set; }
        public IList<int> PrimaryKeyValues { get; set; }
    }
}
