﻿namespace Dogwood.Core
{
    public class PrefetchSpec
    {
        public bool PrefetchAttributes { get; set; }
        public bool PrefetchForeignKey { get; set; }

        public PrefetchSpec(bool pfAtt, bool pfForeignKey)
        {
            PrefetchForeignKey = pfForeignKey;
            PrefetchAttributes = pfAtt;
        }
    }
}
