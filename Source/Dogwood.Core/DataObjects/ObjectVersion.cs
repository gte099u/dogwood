﻿using System;

namespace Dogwood.Core
{
    public class ObjectVersion<T> where T : IDogwoodEntity
    {
        public DateTime TimeStamp { get; set; }
        public T EntityState { get; set; }
        public int RevisionId { get; set; }
        public int BranchId { get; set; }
        public string RevisionComments { get; set; }
        public bool IsDeleted { get; set; }
    }
}
