﻿using System.Reflection;

namespace Dogwood.Core
{    
    public class ManyToOneMap : IMemberMap, IRelationshipMap
    {
        public int ClassId { get; set; }
        public int FkAttributeMetadataId { get; set; }
        public bool IsNullable { get; set; }
        public bool IsImmutable { get; set; }
        public PropertyInfo Property { get; set; }
        public PropertyInfo PrimaryKey { get; set; }
        public PropertyInfo RelationProperty { get; set; }
    }
}
