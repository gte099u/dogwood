﻿namespace Dogwood.Core
{
    public class BranchData
    {
        private int Id { get; set; }
        private string Description { get; set; }
        private int CreatedBy { get; set; }
        private int BranchPointId { get; set; }

        public BranchData(int id, string description, int createdBy, int branchPointId)
        {
            Id = id;
            Description = description;
            CreatedBy = createdBy;
            BranchPointId = branchPointId;
        }
    }
}
