﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dogwood.Core
{
    public class RelationInPredicate
    {
        public int RelationMetadatatId { get; set; }
        public bool Negate { get; set; }
        public IEnumerable<int> Values { get; set; }
    }
}
