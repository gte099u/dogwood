﻿using System.Collections.Generic;
using System.Collections;

namespace Dogwood.Core
{
    public class PrefetchPath : IEnumerable<PrefetchElement>
    {
        private readonly List<PrefetchElement> _Elements;

        public PrefetchPath()
        {
            _Elements = new List<PrefetchElement>();
        }

        public PrefetchElement Add(PrefetchElement toAdd)
        {
            _Elements.Add(toAdd);
            return toAdd;
        }

        public IEnumerator<PrefetchElement> GetEnumerator()
        {
            return ((IEnumerable<PrefetchElement>) _Elements).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
