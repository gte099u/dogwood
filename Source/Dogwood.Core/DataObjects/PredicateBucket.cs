﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Reflection;

namespace Dogwood.Core
{
    public class PredicateBucket<T> : IPredicateBucket where T : IDogwoodEntity
    {
        private readonly IList<RelationPredicate> _relationPredicates;
        private readonly IList<AttributeComparePredicate> _attributeComparePredicates;
        private readonly IList<AttributeLikePredicate> _attributeLikePredicates;
        private readonly IList<RelationInPredicate> _relationInPredicates;

        public int ContextId { get; private set; }
        public int ClassId { get; private set; }

        public PredicateBucket(int contextId)
            : this(contextId, MetadataRegistry.Instance[typeof(T)].ClassId)
        { }

        public PredicateBucket(int contextId, int classId)
        {
            ContextId = contextId;
            ClassId = classId;
            _relationPredicates = new List<RelationPredicate>();
            _relationInPredicates = new List<RelationInPredicate>();
            _attributeComparePredicates = new List<AttributeComparePredicate>();
            _attributeLikePredicates = new List<AttributeLikePredicate>();
        }

        public void AddRelationPredicate(Expression<Func<T, int?>> exp, int? value, bool negate = false)
        {
            var propInfo = ExpressionHelper.GetPropertyInfo(exp);

            AddRelationPredicate(propInfo, value, negate);
        }

        public void AddRelationPredicate(PropertyInfo property, int? value, bool negate = false)
        {
            var predicate = new RelationPredicate();

            var attributeId = DbAttributeRegistry.Instance.Single(m => m.Value == property.Name).Key;

            predicate.RelationMetadatatId = DbFkMetadataRegistry.GetKey(ClassId, attributeId);
            predicate.Value = value;
            predicate.Negate = negate;

            _relationPredicates.Add(predicate);
        }

        public void AddRelationInPredicate(Expression<Func<T, int?>> exp, IEnumerable<int> values, bool negate = false)
        {
            var predicate = new RelationInPredicate();

            var propInfo = ExpressionHelper.GetPropertyInfo(exp);

            var attributeId = DbAttributeRegistry.Instance.Single(m => m.Value == propInfo.Name).Key;

            predicate.RelationMetadatatId = DbFkMetadataRegistry.GetKey(ClassId, attributeId);
            predicate.Values = values;
            predicate.Negate = negate;

            _relationInPredicates.Add(predicate);
        }

        public void AddRelationPredicate(Expression<Func<T, int>> exp, int value, bool negate = false)
        {
            var predicate = new RelationPredicate();

            var propInfo = ExpressionHelper.GetPropertyInfo(exp);

            var attributeId = DbAttributeRegistry.Instance.Single(m => m.Value == propInfo.Name).Key;

            predicate.RelationMetadatatId = DbFkMetadataRegistry.GetKey(ClassId, attributeId);
            predicate.Value = value;
            predicate.Negate = negate;

            _relationPredicates.Add(predicate);
        }

        public void AddRelationInPredicate(Expression<Func<T, int>> exp, IEnumerable<int> values, bool negate = false)
        {
            var predicate = new RelationInPredicate();

            var propInfo = ExpressionHelper.GetPropertyInfo(exp);

            var attributeId = DbAttributeRegistry.Instance.Single(m => m.Value == propInfo.Name).Key;

            predicate.RelationMetadatatId = DbFkMetadataRegistry.GetKey(ClassId, attributeId);
            predicate.Values = values;
            predicate.Negate = negate;

            _relationInPredicates.Add(predicate);
        }

        public void AddAttributeLikePredicate(Expression<Func<T, string>> exp, string value, LikeComparisonOperator likeOperator = LikeComparisonOperator.Contains)
        {
            var propInfo = ExpressionHelper.GetPropertyInfo(exp);

            var predicate = new AttributeLikePredicate();

            var attributeId = DbAttributeRegistry.Instance.Single(m => m.Value == propInfo.Name).Key;

            predicate.AttributeMetadatatId = DbMetadataRegistry.GetKey(ClassId, attributeId);
            predicate.Value = value;
            predicate.CompOperator = likeOperator;

            _attributeLikePredicates.Add(predicate);
        }

        public void AddAttributeLikePredicate(string value, LikeComparisonOperator likeOperator = LikeComparisonOperator.Contains)
        {
            var predicate = new AttributeLikePredicate { AttributeMetadatatId = null, Value = value, CompOperator = likeOperator };

            _attributeLikePredicates.Add(predicate);
        }

        public void AddAttributeComparePredicate<TProp>(Expression<Func<T, TProp>> exp, TProp value, ComparisonOperator likeOperator = ComparisonOperator.Equal)
        {
            var propInfo = ExpressionHelper.GetPropertyInfo(exp);

            AddAttributeComparePredicate(propInfo, value, likeOperator);
        }

        public void AddAttributeComparePredicate<TProp>(PropertyInfo property, TProp value, ComparisonOperator likeOperator = ComparisonOperator.Equal)
        {
            var predicate = new AttributeComparePredicate();

            var attributeId = DbAttributeRegistry.Instance.Single(m => m.Value == property.Name).Key;

            predicate.AttributeMetadatatId = DbMetadataRegistry.GetKey(ClassId, attributeId);
            predicate.Value = value;
            predicate.CompOperator = likeOperator;

            _attributeComparePredicates.Add(predicate);
        }

        public IEnumerable<RelationPredicate> GetRelationPredicates()
        {
            return _relationPredicates;
        }

        public IEnumerable<AttributeComparePredicate> GetAttributeComparePredicates()
        {
            return _attributeComparePredicates;
        }

        public IEnumerable<AttributeLikePredicate> GetAttributeLikePredicates()
        {
            return _attributeLikePredicates;
        }

        public IEnumerable<RelationInPredicate> GetRelationInPredicates()
        {
            return _relationInPredicates;
        }


        public void AddRelationInPredicate(RelationInPredicate toAdd)
        {
            _relationInPredicates.Add(toAdd);
        }

        public void AddRelationPredicate(RelationPredicate toAdd)
        {
            _relationPredicates.Add(toAdd);
        }

        public bool IsEmpty
        {
            get
            {
                return _attributeComparePredicates.Count == 0 &&
                       _attributeLikePredicates.Count==0 &&
                       _relationInPredicates.Count == 0 &&
                       _relationPredicates.Count == 0;
            }
        }
    }
}
