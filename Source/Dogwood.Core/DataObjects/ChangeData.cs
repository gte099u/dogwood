﻿namespace Dogwood.Core
{
    public class ChangeData
    {
        public int ObjectId { get; set; }
        public int RevisionId { get; set; }
        public int ObjectRevisionId { get; set; }

        public ChangeData(int objId, int revId, int objRevId)
        {
            ObjectId = objId;
            RevisionId = revId;
            ObjectRevisionId = objRevId;
        }
    }
}
