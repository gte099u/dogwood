﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dogwood.Core
{
    public class AttributeComparePredicate
    {
        public int AttributeMetadatatId { get; set; }
        public ComparisonOperator CompOperator { get; set; }
        public object Value { get; set; }
    }
}
