﻿namespace Dogwood.Core.Entities
{
    public class ClassEntity : DogwoodEntityBase
    {
        public string Name { get; set; }
    }
}