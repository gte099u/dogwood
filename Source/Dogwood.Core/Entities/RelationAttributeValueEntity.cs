﻿namespace Dogwood.Core.Entities
{
    public class RelationAttributeValueEntity : DogwoodEntityBase
    {
        public int ObjectRevisionId { get; set; }
        public ObjectRevisionEntity ObjectRevision { get; set; }
        public int RelationshipMetadataId { get; set; }
        public int? Value { get; set; }
    }
}