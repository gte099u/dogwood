﻿using System;

namespace Dogwood.Core
{    
    [Serializable]
    public abstract class DogwoodEntityBase : IDogwoodEntity
    {
        public int Id { get; set; }
        public bool IsNew { get { return Id == 0; } }
    }
}
