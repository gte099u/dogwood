﻿namespace Dogwood.Core.Entities
{
    public class AttributeMetadataEntity : DogwoodEntityBase
    {
        public int DataTypeId { get; set; }
        public int AttributeId { get; set; }
        public int ClassId { get; set; }
        public bool IsNullable { get; set; }
        public bool IsImmutable { get; set; }
    }
}