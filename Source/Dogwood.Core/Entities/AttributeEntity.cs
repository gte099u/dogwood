﻿namespace Dogwood.Core.Entities
{
    public class AttributeEntity : DogwoodEntityBase
    {
        public string Value { get; set; }
    }
}