﻿namespace Dogwood.Core.Entities
{
    public class BranchHeadObjectEntity : DogwoodEntityBase
    {
        public int ClassId { get; set; }
        public int BranchId { get; set; }
        public int ObjectId { get; set; }
        public int ObjectRevisionId { get; set; }
        public int RevisionId { get; set; }
        public ObjectRevisionEntity ObjectRevision { get; set; }
    }
}
