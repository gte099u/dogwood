﻿namespace Dogwood.Core.Entities
{
    public class ObjectEntity : DogwoodEntityBase
    {
        public int ClassId { get; set; }
        public int RevisionId { get; set; }
    }
}