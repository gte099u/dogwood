﻿namespace Dogwood.Core.Entities
{
    public class AttributeValueEntity : DogwoodEntityBase
    {
        public int ObjectRevisionId { get; set; }
        public ObjectRevisionEntity ObjectRevision { get; set; }
        public int AttributeMetadataId { get; set; }
        public byte[] Value { get; set; }
    }
}