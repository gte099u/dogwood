﻿using System.Collections.Generic;

namespace Dogwood.Core.Entities
{
    public class ObjectRevisionEntity : DogwoodEntityBase
    {
        public int ObjectId { get; set; }
        public int RevisionId { get; set; }
        public RevisionEntity Revision { get; set; }
        public bool IsDeleted { get; set; }
        public IList<RelationAttributeValueEntity> ForeignKeyAttributeValues { get; set; }
        public IList<AttributeValueEntity> AttributeValues { get; set; }       
    }
}