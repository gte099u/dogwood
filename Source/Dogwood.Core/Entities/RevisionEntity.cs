﻿using System;

namespace Dogwood.Core.Entities
{
    [Serializable]
    public class RevisionEntity : DogwoodEntityBase
    {        
        public string Tag { get; set; }
        public int? CreatedBy { get; set; }
        public int BranchId { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Comments { get; set; }
    }
}