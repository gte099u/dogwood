﻿namespace Dogwood.Core.Entities
{
    public class RelationshipMetadataEntity : DogwoodEntityBase
    {
        public int AttributeId { get; set; }
        public int PkClassId { get; set; }
        public int FkClassId { get; set; }
        public bool IsNullable { get; set; }
        public bool IsImmutable { get; set; }
        public string OneToManyValue { get; set; }
        public CascadeBehaviorType CascadeBehavior { get; set; }
        public int? CascadeDefaultValue { get; set; }
    }
}