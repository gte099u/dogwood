﻿using System;
using System.Data;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public class RevisionBasedPersistence<T> : IVersionedPersistence<T> where T : class, IDogwoodEntity, new()
    {
        private readonly int _revisionId;
        private readonly IEntityMetaData _entityMetadata;
        private readonly IEntityValidation<T> _validation;
        private readonly IPersistenceAdapter _adapter;
        private readonly PersistenceHelper _helper;
        private readonly RevisionBasedGetter<T> _getter;

        public RevisionBasedPersistence(IDogwoodDataContext context)
        {
            _revisionId = context.ContextId;
            _entityMetadata = MetadataRegistry.Instance[typeof(T)];

            _adapter = context.PersistenceAdapter;
            _getter = new RevisionBasedGetter<T>(context);
            _helper = new PersistenceHelper(_adapter, context.ObjectCache);

            _validation = new EntityValidation<T>(_adapter, _entityMetadata, context.ContextId, context.ObjectCache);
        }

        public ChangeData Save(T toSave)
        {
            return Save(toSave, null);
        }

        public ChangeData Save(T toSave, string comments)
        {
            return toSave.IsNew ? CreateAtRevision(toSave, false) : ReviseAtRevision(toSave, false);
        }

        public ChangeData Save(T toSave, string comments, bool openNewTransaction)
        {
            return toSave.IsNew ? CreateAtRevision(toSave, openNewTransaction) : ReviseAtRevision(toSave, openNewTransaction);
        }

        private ChangeData CreateAtRevision(T toSave, bool openNewTransaction)
        {
            _validation.ValidateBeforeCreate(toSave);

            try
            {
                if (openNewTransaction)
                {
                    _adapter.StartTransaction(IsolationLevel.ReadCommitted, "ReviseInBranch");
                }

                var objId = _helper.SaveNewObject(_entityMetadata.ClassId, _revisionId);
                toSave.Id = objId;
                var objRevId = _helper.SaveNewObjectRevision(objId, _revisionId, false);

                _helper.SaveAttributeValues(toSave, objRevId);

                _adapter.AddRootObjectToHeadObjects(_entityMetadata.ClassId, objId, objRevId);

                if (openNewTransaction)
                {
                    _adapter.Commit();
                }

                return new ChangeData(objId, _revisionId, objRevId);
            }
            catch (Exception)
            {
                if (openNewTransaction)
                {
                    _adapter.Rollback();
                }

                throw;
            }
        }

        private ChangeData ReviseAtRevision(T toSave, bool openNewTransaction)
        {
            if (_revisionId != 0)
            {
                throw new Exception("Revising objects in the past is not supported.");
            }

            //~ verify that the object exists in the root revision
            var existing = _getter.Get(toSave.Id);

            if (existing == null)
            {
                throw new Exception("Unable to revise the object in this branch because the object does not exist in the branch's ancestry");
            }

            var objRev = _adapter.GetObjectRevisionEagerAtRoot(toSave.Id);

            if (objRev.Id == 0) throw new Exception("The object being saved does not exist at root");

            _validation.ValidateBeforeUpdate(toSave, existing);

            try
            {
                if (openNewTransaction)
                {
                    _adapter.StartTransaction(IsolationLevel.ReadCommitted, "ReviseInBranch");
                }

                SyncAttributeValues(objRev, toSave);

                //~ no need to update head object tables because root objects maintain the same object revision,
                //~ only attribute values have been impacted

                if (openNewTransaction)
                {
                    _adapter.Commit();
                }

                return new ChangeData(toSave.Id, _revisionId, objRev.Id);
            }
            catch (Exception)
            {
                if (openNewTransaction)
                {
                    _adapter.Rollback();
                }

                throw;
            }
        }

        private void SyncAttributeValues(ObjectRevisionEntity objRev, T toSave)
        {
            // delete all existing attribute value pairs
            _adapter.DeleteRelationAttributeValues(objRev.ForeignKeyAttributeValues);
            _adapter.DeleteAttributeValues(objRev.AttributeValues);

            // save new attribute value pairs
            _helper.SaveAttributeValues(toSave, objRev.Id);
        }

        public bool Delete(int toDeleteId)
        {
            return Delete(toDeleteId, null);
        }

        public bool Delete(int toDeleteId, string comments)
        {
            return Delete(toDeleteId, null, false);
        }

        public bool Delete(int toDeleteId, string comments, bool openNewTransaction)
        {
            if (_revisionId == 0)
            {
                return DestroyObject(toDeleteId);
            }

            throw new Exception("Deleting objects at a specific revision is not supported");
       }

        protected bool ObjectIsUsed(int id)
        {
            var fkavs = _adapter.GetAllForeignKeyReferencesTo(id);

            return fkavs.Count > 0;
        }

        public bool DestroyObject(int toDestroyId)
        {
            if (ObjectIsUsed(toDestroyId))
            {
                throw new Exception("That object is referenced by other objects in the application and may not be deleted");
            }

            try
            {
                _adapter.StartTransaction(IsolationLevel.ReadCommitted, "DestroyObject");

                _adapter.RemoveRootObjectFromHeadObjects(toDestroyId);

                _adapter.DeleteAllObjectRevisionsHavingObjectId(toDestroyId);

                _adapter.DeleteObject(toDestroyId);

                _helper.PurgeOrphanRevisions();

                _adapter.Commit();
            }
            catch (Exception)
            {
                _adapter.Rollback();

                throw;
            }

            return true;
        }

        public int CreateRevision()
        {
            return _revisionId;
        }

        public int CreateRevision(string tag, string comments)
        {
            return _revisionId;
        }

        public ChangeData SaveWithinBatch(T toSave, int revisionId)
        {
            if (toSave.IsNew)
            {
                return CreateAtRevision(toSave, false);
            }

            return ReviseAtRevision(toSave, false);
        }

        public bool DeleteWithinBatch(int toDeleteId, int revisionId)
        {
            throw new Exception("Deleting objects at a specific revision is not supported");
        }
    }
}
