﻿namespace Dogwood.Core
{
    public static class RegistryLoader
    {
        public static void LoadAll(IPersistenceAdapter adapter)
        {
            LoadClasses(adapter);
            LoadAttributes(adapter);
            LoadAttributeMetadata(adapter);
            LoadRelationshipMetadata(adapter);
        }

        public static void LoadRelationshipMetadata(IPersistenceAdapter adapter)
        {
            foreach (var item in adapter.GetAllRelationshipMetadata())
            {
                if (!DbFkMetadataRegistry.Instance.ContainsKey(item.Id))
                {
                    DbFkMetadataRegistry.Instance.Add(item.Id, item);
                }
            }
        }

        public static void LoadAttributeMetadata(IPersistenceAdapter adapter)
        {
            foreach (var item in adapter.GetAllAttributeMetadata())
            {
                if (!DbMetadataRegistry.Instance.ContainsKey(item.Id))
                {
                    DbMetadataRegistry.Instance.Add(item.Id, item);
                }
            }
        }

        public static void LoadAttributes(IPersistenceAdapter adapter)
        {           
            foreach (var att in adapter.GetAllAttributes())
            {
                DbAttributeRegistry.Instance.Add(att.Id, att.Value);
            }
        }

        public static void LoadClasses(IPersistenceAdapter adapter)
        {
            foreach (var cla in  adapter.GetAllClasses())
            {
                DbTypeRegistry.Instance.Add(cla.Id, cla);
            }
        }
    }
}
