﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public class ObjectRepository<T> : IRetrievalService, IObjectRepository<T> where T : class, IDogwoodEntity, new()
    {
        private IVersionedRetrieval<T> _getter;
        private IVersionedPersistence<T> _persistence;

        private IDogwoodDataContext _context;
        public IDogwoodDataContext Context
        {
            get
            {
                return _context;
            }
            set
            {
                switch (value.ContextType)
                {
                    case EavContextType.Branch:
                        _getter = new BranchBasedGetter<T>(value);
                        _persistence = new BranchBasedPersistence<T>(value);
                        break;
                    case EavContextType.Revision:
                        _getter = new RevisionBasedGetter<T>(value);
                        _persistence = new RevisionBasedPersistence<T>(value);
                        break;
                }

                _context = value;
            }
        }

        public ObjectRepository(IDogwoodDataContext context)
        {
            _context = context;

            switch (context.ContextType)
            {
                case EavContextType.Branch:
                    _getter = new BranchBasedGetter<T>(context);
                    _persistence = new BranchBasedPersistence<T>(context);
                    break;
                case EavContextType.Revision:
                    _getter = new RevisionBasedGetter<T>(context);
                    _persistence = new RevisionBasedPersistence<T>(context);
                    break;
            }
        }

        public ObjectRepository(IDogwoodDataContext context, IVersionedRetrieval<T> getter, IVersionedPersistence<T> persistence)
        {
            _context = context;
            _getter = getter;
            _persistence = persistence;
        }

        public virtual IList<T> GetAll()
        {
            return _getter.GetAll();
        }

        public virtual T Get(int id)
        {
            return _getter.Get(id);
        }

        public virtual T Get(int id, PrefetchPath prefetchPath)
        {
            var toRet = _getter.Get(id);

            if (toRet != null) //~ don't attempt to prefetch if the entity was not found
            {
                FetchGraph(prefetchPath, new List<T> { toRet });
            }

            return toRet;
        }

        public virtual IDogwoodEntity GetAsIDogwoodEntity(int id, PrefetchPath prefetchPath)
        {
            return Get(id, prefetchPath);
        }

        public virtual IList<T> GetAll(PrefetchPath prefetchPath)
        {
            var toRet = GetAll(null, prefetchPath);

            return toRet;
        }

        public virtual IList<T> GetAll(IPredicateBucket filterBucket)
        {
            var toRet = GetAll(filterBucket, null);

            return toRet;
        }

        public virtual IList<T> GetAll(IPredicateBucket filterBucket, IEnumerable<PrefetchElement> prefetchPath)
        {
            var toRet = filterBucket == null ? _getter.GetAll() :  _getter.GetAll(filterBucket);

            if (toRet.Any())
            {
                FetchGraph(prefetchPath, toRet);
            }

            foreach (var obj in toRet)
            {
                Context.ObjectCache.SetObject(obj);
            }

            return toRet;
        }

        public IEnumerable<T> GetMulti(IEnumerable<ObjectRevisionEntity> filteredObjects, IEnumerable<PrefetchElement> path)
        {
            var factory = new EavFactory<T>();
            var toRet = factory.CreateMulti(filteredObjects).ToList();

            if(toRet.Any())
            {
                FetchGraph(path, toRet);
            }

            foreach (var obj in toRet)
            {
                Context.ObjectCache.SetObject(obj);
            }

            return toRet;
        }

        private void FetchGraph(IEnumerable<PrefetchElement> prefetchPath, IEnumerable<T> toRet)
        {
            toRet = toRet.ToList();

            if (prefetchPath != null)
            {
                foreach (var element in prefetchPath)
                {
                    if (element.FkMap is OneToManyMap)
                    {
                        var fkMap = element.FkMap as OneToManyMap;

                        var service = GetReadOnlyService(element);

                        var filterBucket = GetPredicateBucket(element.FkMap.ClassId);

                        var predicate = new RelationInPredicate();

                        predicate.RelationMetadatatId = DbFkMetadataRegistry.GetKey(element.FkMap.ClassId, fkMap.ForeignKeyAttributeId);
                        predicate.Values = toRet.Select(m=>m.Id);
                        predicate.Negate = false;

                        filterBucket.AddRelationInPredicate(predicate);

                        var relatedItems = service.GetAllAsIDogwoodEntity(element.SubPath, filterBucket);

                        foreach (var item in toRet)
                        {
                            SetOneToManyRelationProperty(fkMap, item, relatedItems);
                        }
                    }
                    else
                    {
                        SetManyToOneRelationProperty(element, toRet);
                    }
                }
            }
        }

        private IPredicateBucket GetPredicateBucket(int classId)
        {
            var genericService = typeof(PredicateBucket<>);
            Type[] typeArgs = { MetadataRegistry.Instance.Single(m => m.Value.ClassId == classId).Key };
            var constructed = genericService.MakeGenericType(typeArgs);
            return Activator.CreateInstance(constructed, Context.ContextId) as IPredicateBucket;
        }

        public virtual IList<IDogwoodEntity> GetAllAsIDogwoodEntity(PrefetchPath prefetchPath, IPredicateBucket filterBucket)
        {
            return GetAll(filterBucket, prefetchPath).Cast<IDogwoodEntity>().ToList();
        }

        public virtual IList<IDogwoodEntity> GetAllAsIDogwoodEntity(PrefetchPath prefetchPath, IList<int> idFilter)
        {
            var toRet = _getter.GetAll(idFilter);

            if (toRet.Any())
            {
                FetchGraph(prefetchPath, toRet);
            }

            foreach (var obj in toRet)
            {
                Context.ObjectCache.SetObject(obj);
            }

            return toRet.Cast<IDogwoodEntity>().ToList();
        }        

        public virtual ChangeData Save(T toSave)
        {
            return Save(toSave, null);
        }

        public virtual ChangeData Save(T toSave, string comments)
        {
            return _persistence.Save(toSave, comments);
        }

        public virtual ChangeData Save(T toSave, string comments, bool openNewTransaction)
        {
            return _persistence.Save(toSave, comments, openNewTransaction);
        }

        public virtual ChangeData SaveWithinBatch(T toSave, int revisionId)
        {
            return _persistence.SaveWithinBatch(toSave, revisionId);
        }

        public virtual bool Delete(int toDeleteId)
        {
            return Delete(toDeleteId, null);
        }

        public virtual bool Delete(int toDeleteId, string comments)
        {
            return Delete(toDeleteId, comments, true);
        }

        public virtual bool Delete(int toDeleteId, string comments, bool openNewTransaction)
        {
            var toDelete = Get(toDeleteId);

            return toDelete != null && _persistence.Delete(toDeleteId, comments, openNewTransaction);
        }

        public virtual bool SaveMulti(IList<T> toSave)
        {
            return SaveMulti(toSave, null);
        }

        public virtual bool SaveMulti(IList<T> toSave, string comments)
        {
            Func<bool> func = () =>
            {
                var revisionId = _persistence.CreateRevision(null, comments);

                foreach (var limit in toSave)
                {
                    _persistence.SaveWithinBatch(limit, revisionId);
                }

                return true;
            };

            return ExecuteWithinTransaction(() => func());
        }

        public virtual bool ExecuteWithinTransaction(Expression<Func<bool>> cmd)
        {
            try
            {
                Context.PersistenceAdapter.StartTransaction(IsolationLevel.ReadCommitted, "transaction");

                var compiledExpression = cmd.Compile();

                var result = compiledExpression();

                Context.PersistenceAdapter.Commit();
                return result;
            }
            catch
            {
                Context.PersistenceAdapter.Rollback();
                throw;
            }
        }

        public virtual int CreateRevision(string tag, string comments)
        {
            return _persistence.CreateRevision(tag, comments);
        }

        public virtual bool IsTransactionInProgress
        {
            get { return Context.PersistenceAdapter.IsTransactionInProgress; }
        }

        public virtual void StartTransaction(IsolationLevel isolationLevel, string name)
        {
            Context.PersistenceAdapter.StartTransaction(isolationLevel, name);
        }

        public virtual void Commit()
        {
            Context.PersistenceAdapter.Commit();
        }

        public virtual void Rollback()
        {
            Context.PersistenceAdapter.Rollback();
        }

        public bool DeleteWithinBatch(int toDeleteId, int revisionId)
        {
            return _persistence.DeleteWithinBatch(toDeleteId, revisionId);
        }


        public IEnumerable<IDogwoodEntity> GetAllRelatedObjects(int id)
        {
            var baseEntity = Get(id);

            if (baseEntity == null)
            {
                throw new NullReferenceException(
                    string.Format("Trying to fetch all items related to {0} but that Id does not match an object of type {1} in this branch", id, typeof(T)));
            }

            var toRet = new List<IDogwoodEntity>();

            // add many objects related to this one
            foreach (var relation in MetadataRegistry.Instance[typeof(T)].OneToManyRelationships)
            {
                var readOnlyService = GetReadOnlyService(relation.Value.ClassId);

                //var fkFilter = new ForeignKeyPredicate { FkAttributeId = relation.Value.ForeignKeyAttributeId, PrimaryKeyValues = new List<int> { id } };

                var filterBucket = GetPredicateBucket(relation.Value.ClassId);

                var predicate = new RelationPredicate();

                predicate.RelationMetadatatId = DbFkMetadataRegistry.GetKey(relation.Value.ClassId, relation.Value.ForeignKeyAttributeId);
                predicate.Value = id;
                predicate.Negate = false;

                filterBucket.AddRelationPredicate(predicate);

                toRet.AddRange(readOnlyService.GetAllAsIDogwoodEntity(null, filterBucket));
            }

            // add single objects that this one is related to
            foreach (var relation in MetadataRegistry.Instance[typeof(T)].ManyToOneRelationships)
            {
                var readOnlyService = GetReadOnlyService(relation.Value.ClassId);
                var fk = (int?)relation.Value.Property.GetValue(baseEntity, null);
                if (fk != null)
                {
                    toRet.Add(readOnlyService.GetAsIDogwoodEntity((int)fk, null));
                }
            }


            return toRet;
        }

        private IRetrievalService GetReadOnlyService(PrefetchElement element)
        {
            return GetReadOnlyService(element.FkMap.ClassId);
        }

        private IRetrievalService GetReadOnlyService(int classId)
        {
            var genericService = typeof(ObjectRepository<>);
            Type[] typeArgs = { MetadataRegistry.Instance.Single(m=>m.Value.ClassId == classId).Key };
            var constructed = genericService.MakeGenericType(typeArgs);
            return Activator.CreateInstance(constructed, Context) as IRetrievalService;
        }

        private static void SetOneToManyRelationProperty(OneToManyMap fkMap, T toRet, IEnumerable<IDogwoodEntity> relatedItems)
        {
            var relatedEntities = relatedItems.Where(m => (int)fkMap.ForeignKey.GetValue(m, null) == (int)fkMap.PrimaryKey.GetValue(toRet, null)).ToList(); // todo: this seems redundant

            var generic = typeof(List<>);
            var specific = generic.MakeGenericType(MetadataRegistry.Instance.Single(m => m.Value.ClassId == fkMap.ClassId).Key);
            var ci = specific.GetConstructor(Type.EmptyTypes);
            if (ci == null) throw new Exception("No constructor exists for the given type");
            var typedList = ci.Invoke(new object[] { });

            foreach (var x in relatedEntities)
            {
                fkMap.ForeignKeyProperty.SetValue(x, toRet, null);
                ((IList)typedList).Add(x);
            }

            fkMap.RelationProperty.SetValue(toRet, typedList, null);
        }

        private void SetManyToOneRelationProperty(PrefetchElement element, IEnumerable<T> toRet)
        {
            toRet = toRet.ToList();
            var service = GetReadOnlyService(element);
            var fkMap = (ManyToOneMap)element.FkMap;

            var idList = new List<int>();

            foreach (var item in toRet)
            {
                var fk = (int?)fkMap.Property.GetValue(item, null);
                if (fk != null && !idList.Contains((int)fk))
                {
                    idList.Add((int)fk);
                }
            }

            if (idList.Count <= 0) return;

            var relatedEntities = service.GetAllAsIDogwoodEntity(element.SubPath, idList);

            foreach (var item in toRet)
            {
                var fk = (int?)fkMap.Property.GetValue(item, null);
                if (fk != null && fk != 0)
                {
                    fkMap.RelationProperty.SetValue(item, relatedEntities.Single(m => m.Id == fk), null);
                }
            }
        }

        public PredicateBucket<T> CreateNewPredicateBucket()
        {
            return new PredicateBucket<T>(Context.ContextId);
        }

        public IEnumerable<ObjectVersion<T>> GetObjectHistory(int id)
        {
            return _getter.GetObjectHistory(id, _context.ContextId);
        }
    }
}
