﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public class EavFactory<T> where T : IDogwoodEntity, new()
    {        
        public T Create(ObjectRevisionEntity objectRevision)
        {
            var toRet = new T {Id = objectRevision.ObjectId};

            var metaData = MetadataRegistry.Instance[typeof(T)];

            foreach (var item in metaData.Attributes)
            {
                var attValue = objectRevision.AttributeValues.SingleOrDefault(m => m.AttributeMetadataId == item.AttributeMetadataId);

                if (attValue != null)
                {
                    var info = typeof(T).GetProperty(item.Property.Name);
                    info.SetValue(toRet, DataConverter.ConvertToTypedData(attValue.Value, item.DataType), null);
                }
                else
                {
                    if (!item.IsNullable)
                    {
                        throw new Exception("Required field for object was not provided");
                    }
                }
            }

            foreach (var item in metaData.ManyToOneRelationships)
            {
                var attValue = objectRevision.ForeignKeyAttributeValues.SingleOrDefault(m => m.RelationshipMetadataId == item.Value.FkAttributeMetadataId);

                if (attValue != null)
                {
                    var info = typeof(T).GetProperty(item.Value.Property.Name);
                    info.SetValue(toRet, attValue.Value, null);
                }
                else
                {
                    if (!item.Value.IsNullable)
                    {
                        throw new Exception("Required field for object was not provided");
                    }
                }
            }
            
            return toRet;
        }

        public IEnumerable<T> CreateMulti(IEnumerable<ObjectRevisionEntity> objectRevisions)
        {
            return objectRevisions.Select(Create);
        }
    }
}
