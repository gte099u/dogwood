﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Dogwood.Core
{
    public static class EntityMetadataFactory
    {
        public static EntityMetadata Create(int classId, Type type)
        {
            // the entity type provided must match the pattern XXX or XXXEntity
            // maybe consider providint configuration options to allow user to specify custom entity name format?
            // scrape the entity type's name - ex. CustomerEntity would return "Customer", Person would return "Person"

            string entityTypeName = EntityMetadata.GetNameAsPersisted(type);

            var meta = new EntityMetadata();

            // identify the corresponding class Id in the database and stamp the metadata as such

            if (DbTypeRegistry.Instance.SingleOrDefault(m => m.Value.Name == entityTypeName).Value == null)
            {
                throw new Exception(string.Format("The entity of type {0} was not defined in the database.", entityTypeName));
            }

            meta.ClassId = classId;

            foreach (var property in type.GetProperties())
            {
                meta.AddAttribute(property);
            }

            foreach (var property in type.GetProperties().Where(m => m.PropertyType == typeof(int) || m.PropertyType == typeof(int?)))
            {
                foreach (var attribute in property.GetCustomAttributes(typeof(ManyToOneRelationAttribute), true))
                {
                    // many to one attributes
                    string className = property.Name.Remove(property.Name.Length - 2);

                    var mAtt = (ManyToOneRelationAttribute)attribute;
                    var map = new ManyToOneMap
                                  {
                                      FkAttributeMetadataId = meta.GetFkMetadataId(mAtt.RelatedClassId == 0 ? DbTypeRegistry.GetTypeId(className)
                                     : mAtt.RelatedClassId, DbAttributeRegistry.GetKey(property.Name))
                                  };

                    map.IsNullable = DbFkMetadataRegistry.Instance[map.FkAttributeMetadataId].IsNullable;
                    map.IsImmutable = DbFkMetadataRegistry.Instance[map.FkAttributeMetadataId].IsImmutable;

                    map.ClassId = mAtt.RelatedClassId == 0 ? DbTypeRegistry.GetTypeId(className) : mAtt.RelatedClassId;

                    map.RelationProperty = type.GetProperty(className);
                    map.Property = property;
                    meta.ManyToOneRelationships.Add(RelationshipTag(type, className), map);
                }
            }

            foreach (var fkAtt in type.GetProperties().Where(m => m.PropertyType.SupportsInterface(typeof(IList<>)))) // todo: use IEnumerable instead
            {
                foreach (var attribute in fkAtt.GetCustomAttributes(typeof(OneToManyRelationAttribute), true))
                {
                    var map = new OneToManyMap();
                    var mAtt = (OneToManyRelationAttribute)attribute;
                    Type relatedType = fkAtt.PropertyType.GetGenericArguments()[0];

                    if (mAtt.RelatedClassId == 0)
                    {
                        string relatedEntityName = EntityMetadata.GetNameAsPersisted(fkAtt.PropertyType.GetGenericArguments()[0]);
                        map.ClassId = DbTypeRegistry.GetTypeId(relatedEntityName);

                        PropertyInfo relatedIdProperty = relatedType.GetProperties().SingleOrDefault(m => m.Name == entityTypeName + "Id");

                        if (relatedIdProperty == null)
                        {
                            throw new Exception(string.Format("A relationship between {0} and {1} was defined unidirectionally in the application metadata.  Please check these entities and correct the missing relationship", entityTypeName, relatedEntityName));
                        }

                        map.ForeignKey = relatedIdProperty;

                        PropertyInfo relatedObjectProperty = relatedType.GetProperties().SingleOrDefault(m => m.Name == entityTypeName);

                        if (relatedObjectProperty == null)
                        {
                            throw new Exception(string.Format("A relationship between {0} and {1} was defined unidirectionally in the application metadata.  Please check these entities and correct the missing relationship", entityTypeName, relatedEntityName));
                        }

                        map.ForeignKeyProperty = relatedObjectProperty;
                        map.ForeignKeyAttributeId = DbAttributeRegistry.GetKey(entityTypeName + "Id");
                    }
                    else
                    {
                        map.ClassId = mAtt.RelatedClassId;

                        string fkAttributeName = DbAttributeRegistry.Instance[mAtt.FkAttributeId];

                        map.ForeignKey = relatedType.GetProperties().Single(m => m.Name == fkAttributeName);
                        map.ForeignKeyProperty = relatedType.GetProperties().Single(m => m.Name == fkAttributeName.Remove(fkAttributeName.Length - 2));
                        map.ForeignKeyAttributeId = mAtt.FkAttributeId;
                    }

                    map.PrimaryKey = type.GetProperty("Id");
                    map.RelationProperty = fkAtt;
                    meta.OneToManyRelationships.Add(RelationshipTag(type, fkAtt.Name), map);
                }
            }

            // add unique constraints
            foreach (var constraintAtt in type.GetCustomAttributes(typeof(UniqueConstraintAttribute), true))
            {
                var uniqueConstraintAtt = (UniqueConstraintAttribute)constraintAtt;

                var constraint = new UniqueConstraint { ExceptionMessage = uniqueConstraintAtt.ExceptionMessage };
                constraint.AddRange(uniqueConstraintAtt.UniqueComboFields.Select(type.GetProperty));

                meta.UniqueConstraints.Add(constraint);
            }

            return meta;
        }

        private static string RelationshipTag(Type t, string property)
        {
            return string.Format("{0}_{1}", t, property);
        }
    }
}
