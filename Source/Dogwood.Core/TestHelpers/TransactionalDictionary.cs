﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Dogwood.Core.TestHelpers
{
    public class TransactionalDictionary<T> : IDictionary<int, T> where T : DogwoodEntityBase
    {
        public event EventHandler<EventArgs> BeforeAdd;
        public event EventHandler<EventArgs> AfterAdd;

        public event EventHandler<EventArgs> BeforeRemove;
        public event EventHandler<EventArgs> AfterRemove;

        private readonly IDictionary<int, T> _dataStore;
        private readonly List<int> _toRemoveTemp;
        private readonly IDictionary<int, T> _toUpdateTemp;
        private readonly IDictionary<int, T> _toAddTemp;
        private bool _transactionInProgress;

        private IDictionary<int, T> UncommittedData
        {
            get
            {
                var toRet = new Dictionary<int, T>();

                foreach (var item in _dataStore.Where(m => !_toRemoveTemp.Contains(m.Key)))
                {
                    if (_toUpdateTemp.ContainsKey(item.Key))
                    {
                        toRet.Add(item.Key, _toUpdateTemp[item.Key]);
                    }
                    else
                    {
                        toRet.Add(item.Key, item.Value);
                    }
                }

                foreach (var item in _toAddTemp)
                {
                    toRet.Add(item.Key, item.Value);
                }

                return toRet;
            }
        }

        public TransactionalDictionary()
        {
            _dataStore = new Dictionary<int, T>();
            _toAddTemp = new Dictionary<int, T>();
            _toUpdateTemp = new Dictionary<int, T>();
            _toRemoveTemp = new List<int>();
            _transactionInProgress = false;
        }

        public bool ContainsKey(int key)
        {
            return _dataStore.ContainsKey(key);
        }

        public void Add(int key, T value)
        {
            OnBeforeAdd(EventArgs.Empty);

            if (_transactionInProgress)
            {
                _toAddTemp.Add(key, value);
            }
            else
            {
                _dataStore.Add(key, value);
            }

            OnAfterAdd(EventArgs.Empty);
        }

        public bool TryGetValue(int key, out T value)
        {
            if (_transactionInProgress)
            {
                if (_toRemoveTemp.Contains(key))
                {
                    value = null;
                    return false;
                }

                if (_toUpdateTemp.ContainsKey(key))
                {
                    value = _toUpdateTemp[key];
                    return true;
                }

                if (_toAddTemp.ContainsKey(key))
                {
                    value = _toAddTemp[key];
                    return true;
                }
            }

            return _dataStore.TryGetValue(key, out value);
        }

        public T this[int key]
        {
            get
            {
                if (_transactionInProgress)
                {
                    return UncommittedData[key];
                }

                return _dataStore[key];
            }
            set
            {
                if (_transactionInProgress)
                {
                    _toUpdateTemp.Add(key, value);
                }
                else
                {
                    _dataStore[key] = value;
                }
            }
        }

        public ICollection<int> Keys
        {
            get
            {
                if (_transactionInProgress)
                {
                    return UncommittedData.Keys;
                }

                return _dataStore.Keys;
            }
        }

        public ICollection<T> Values
        {
            get
            {
                if (_transactionInProgress)
                {
                    return UncommittedData.Values;
                }

                return _dataStore.Values;
            }
        }

        public void Commit()
        {
            _toRemoveTemp.ForEach(m => _dataStore.Remove(m));

            foreach (var toAdd in _toAddTemp)
            {
                _dataStore.Add(toAdd);
            }

            foreach (var newValue in _toUpdateTemp)
            {
                _dataStore[newValue.Key] = newValue.Value;
            }

            _toUpdateTemp.Clear();
            _toRemoveTemp.Clear();
            _toAddTemp.Clear();

            _transactionInProgress = false;
        }

        public void Rollback()
        {
            _toUpdateTemp.Clear();
            _toRemoveTemp.Clear();
            _toAddTemp.Clear();

            _transactionInProgress = false;
        }

        public void StartTransaction()
        {
            _toUpdateTemp.Clear();
            _toRemoveTemp.Clear();
            _toAddTemp.Clear();

            _transactionInProgress = true;
        }

        public IEnumerator<KeyValuePair<int, T>> GetEnumerator()
        {
            return UncommittedData.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(KeyValuePair<int, T> item)
        {
            if (_transactionInProgress)
            {
                _toAddTemp.Add(item);
            }
            else
            {
                _dataStore.Add(item);
            }
        }

        public void Clear()
        {
            if (_transactionInProgress)
            {
                foreach (var toRemove in _dataStore)
                {
                    _toRemoveTemp.Add(toRemove.Key);
                }
            }
            else
            {
                _dataStore.Clear();
            }
        }

        public bool Contains(KeyValuePair<int, T> item)
        {
            if (_transactionInProgress)
            {
                return UncommittedData.Contains(item);
            }

            return _dataStore.Contains(item);
        }

        public void CopyTo(KeyValuePair<int, T>[] array, int arrayIndex)
        {
            _dataStore.CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<int, T> item)
        {
            OnBeforeRemove(EventArgs.Empty);

            if (_transactionInProgress)
            {
                _toRemoveTemp.Add(item.Key);
                return true;
            }

            OnAfterRemove(EventArgs.Empty);

            return _dataStore.Remove(item);
        }

        public int Count
        {
            get
            {
                if(_transactionInProgress)
                {
                    return UncommittedData.Count;
                }

                return _dataStore.Count;
            }
        }

        public bool IsReadOnly
        {
            get { return _dataStore.IsReadOnly; }
        }

        public bool Remove(int key)
        {
            OnBeforeRemove(EventArgs.Empty);

            if (_transactionInProgress)
            {
                _toRemoveTemp.Add(key);
                return true;
            }

            OnAfterRemove(EventArgs.Empty);

            return _dataStore.Remove(key);
        }

        public void Save(T toSave)
        {
            if (toSave.IsNew)
            {
                toSave.Id = GetNewKey();
                Add(toSave.Id, toSave);
            }
            else
            {
                this[toSave.Id] = toSave;
            }
        }

        private int GetNewKey()
        {
            if (_transactionInProgress)
            {
                return UncommittedData.Any() ? UncommittedData.Max(m => m.Key) + 1 : 1;
            }

            return _dataStore.Any() ? _dataStore.Max(m => m.Key) + 1 : 1;
        }

        private void OnBeforeAdd(EventArgs e)
        {
            if (BeforeAdd != null)
                BeforeAdd(this, e);
        }

        private void OnAfterAdd(EventArgs e)
        {
            if (AfterAdd != null)
                AfterAdd(this, e);
        }

        private void OnBeforeRemove(EventArgs e)
        {
            if (BeforeRemove != null)
                BeforeRemove(this, e);
        }

        private void OnAfterRemove(EventArgs e)
        {
            if (AfterRemove != null)
                AfterRemove(this, e);
        }
    }
}
