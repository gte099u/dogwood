﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dogwood.Core.Entities;

namespace Dogwood.Core.TestHelpers
{
    public class FakePersistenceAdapter : IPersistenceAdapter
    {
        public readonly TransactionalDictionary<ObjectRevisionEntity> _objectRevisions;
        public readonly TransactionalDictionary<ClassEntity> _classes;
        public readonly TransactionalDictionary<AttributeEntity> _attributes;
        public readonly TransactionalDictionary<RelationshipMetadataEntity> _relationMetadata;
        public readonly TransactionalDictionary<AttributeMetadataEntity> _attributeMetadata;
        public readonly TransactionalDictionary<RevisionEntity> _revisions;
        public readonly TransactionalDictionary<ObjectEntity> _objects;
        public readonly TransactionalDictionary<BranchHeadObjectEntity> _branchHeadObjects;
        public readonly TransactionalDictionary<BranchHeadObjectEntity> _revisionHeadObjects;
        public readonly TransactionalDictionary<AttributeValueEntity> _attributeValues;
        public readonly TransactionalDictionary<RelationAttributeValueEntity> _relationAttributeValue;

        private bool _transactionInProgress;

        public FakePersistenceAdapter()
        {
            _objectRevisions = new TransactionalDictionary<ObjectRevisionEntity>();
            _classes = new TransactionalDictionary<ClassEntity>();
            _attributes = new TransactionalDictionary<AttributeEntity>();
            _relationMetadata = new TransactionalDictionary<RelationshipMetadataEntity>();
            _attributeMetadata = new TransactionalDictionary<AttributeMetadataEntity>();
            _revisions = new TransactionalDictionary<RevisionEntity>();
            _objects = new TransactionalDictionary<ObjectEntity>();
            _branchHeadObjects = new TransactionalDictionary<BranchHeadObjectEntity>();
            _revisionHeadObjects = new TransactionalDictionary<BranchHeadObjectEntity>();
            _attributeValues = new TransactionalDictionary<AttributeValueEntity>();
            _relationAttributeValue = new TransactionalDictionary<RelationAttributeValueEntity>();
        }

        public IEnumerable<ObjectRevisionEntity> GetAllRevisionsForObject(int objectId, int branchId)
        {
            return _objectRevisions.Where(m => m.Value.ObjectId == objectId).Select(m => m.Value);
        }

        public IList<ObjectRevisionEntity> GetObjectRevisionsEager(IEnumerable<int> idList, IEntityMetaData entityMetadata)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ClassEntity> GetAllClasses()
        {
            return _classes.Values;
        }

        public IEnumerable<AttributeEntity> GetAllAttributes()
        {
            return _attributes.Values;
        }

        public IEnumerable<RelationshipMetadataEntity> GetAllRelationshipMetadata()
        {
            return _relationMetadata.Values;
        }

        public IEnumerable<AttributeMetadataEntity> GetAllAttributeMetadata()
        {
            return _attributeMetadata.Values;
        }

        public RevisionEntity GetRevision(int id)
        {
            if (!_revisions.ContainsKey(id))
            {
                return null;
            }

            return _revisions[id];
        }

        public IEnumerable<RevisionEntity> GetAllRevisionsByBranchId(int branchId)
        {
            return _revisions.Where(m => m.Value.BranchId == branchId).Select(m => m.Value);
        }

        public IEnumerable<RevisionEntity> GetAllRevisionsHavingTag(int branchId, string tag)
        {
            return GetAllRevisionsByBranchId(branchId).Where(m => m.Tag == tag);
        }

        public IEnumerable<RevisionEntity> GetAllOrphanRevisions()
        {
            var revisionsThatArentBranchPoints = _revisions
                .Where(m => !_revisions.Values
                                 .GroupBy(n => n.BranchId)
                                 .Select(n => n.Key).Contains(m.Key));

            var orphanRevisions = revisionsThatArentBranchPoints.Where(m =>
                                                                       !_objectRevisions
                                                                            .Select(n => n.Value.RevisionId)
                                                                            .Contains(m.Key))
                .Select(m => m.Value);

            return orphanRevisions;
        }

        public void StartTransaction(System.Data.IsolationLevel isolationLevel, string name)
        {
            if (_transactionInProgress)
            {
                throw new Exception("A transaction is already in progress");
            }

            _attributeMetadata.StartTransaction();
            _attributes.StartTransaction();
            _attributeValues.StartTransaction();
            _branchHeadObjects.StartTransaction();
            _classes.StartTransaction();
            _objectRevisions.StartTransaction();
            _objects.StartTransaction();
            _relationAttributeValue.StartTransaction();
            _relationMetadata.StartTransaction();
            _revisions.StartTransaction();

            _transactionInProgress = true;
        }

        public void Commit()
        {
            _attributeMetadata.Commit();
            _attributes.Commit();
            _attributeValues.Commit();
            _branchHeadObjects.Commit();
            _classes.Commit();
            _objectRevisions.Commit();
            _objects.Commit();
            _relationAttributeValue.Commit();
            _relationMetadata.Commit();
            _revisions.Commit();

            _transactionInProgress = false;
        }

        public void Rollback()
        {
            _attributeMetadata.Rollback();
            _attributes.Rollback();
            _attributeValues.Rollback();
            _branchHeadObjects.Rollback();
            _classes.Rollback();
            _objectRevisions.Rollback();
            _objects.Rollback();
            _relationAttributeValue.Rollback();
            _relationMetadata.Rollback();
            _revisions.Rollback();

            _transactionInProgress = false;
        }

        public bool IsTransactionInProgress
        {
            get { return _transactionInProgress; }
        }

        public void SaveRevision(RevisionEntity toSave)
        {
            _revisions.Save(toSave);
        }

        public void SaveObject(ObjectEntity toSave)
        {
            _objects.Save(toSave);
        }

        public void SaveObjectRevision(ObjectRevisionEntity toSave)
        {
            _objectRevisions.Save(toSave);
        }

        public void DeleteRevision(RevisionEntity toDelete)
        {
            _revisions.Remove(toDelete.Id);
        }

        public void DeleteObject(int toDeleteId)
        {
            _objects.Remove(toDeleteId);
        }

        public int DeleteAllObjectRevisionsHavingObjectId(int objectId)
        {
            _objectRevisions.Remove(objectId);
            return 0;
        }

        public bool RemoveObjectFromBranchHeadObjects(int objectId, int branchId)
        {
            var itemsToRemove = _branchHeadObjects
                .Where(m => m.Value.ObjectId == objectId && m.Value.BranchId == branchId)
                .Select(m => m.Value);

            foreach (var branchHeadObjectEntity in itemsToRemove)
            {
                _branchHeadObjects.Remove(branchHeadObjectEntity.Id);
            }

            return true;
        }

        public bool RemoveObjectFromBranchHeadObjects(int objectId)
        {
            var itemsToRemove = _branchHeadObjects.Where(m => m.Value.ObjectId == objectId).Select(m => m.Value);

            foreach (var branchHeadObjectEntity in itemsToRemove)
            {
                _branchHeadObjects.Remove(branchHeadObjectEntity.Id);
            }

            return true;
        }

        public bool UpdateObjectInBranchHeadObjects(BranchHeadObjectEntity toSave)
        {
            _branchHeadObjects.Save(toSave);
            return true;
        }

        public bool AddObjectToBranchHeadObjects(BranchHeadObjectEntity toSave)
        {
            _branchHeadObjects.Save(toSave);
            return true;
        }

        public void FillHeadObjects()
        {
            _branchHeadObjects.Clear();
            var branches = _revisions.GroupBy(m => m.Value.BranchId).Select(m => m.Key);

            foreach (var branch in branches)
            {
                var branchHeadObjs = BranchHeadObjects(branch);

                foreach (var branchHeadObj in branchHeadObjs)
                {
                    _branchHeadObjects.Save(branchHeadObj);
                }
            }
        }

        public void AddRootObjectToHeadObjects(int classId, int objectId, int objectRevisionId)
        {
            //~ add record to BranchHeadObjects for each indexed branch
            var branches = _revisions.Values.GroupBy(m => m.BranchId).Select(m => m.Key);

            foreach (var branchId in branches)
            {
                var toCreate = new BranchHeadObjectEntity
                {
                    BranchId = branchId,
                    ClassId = classId,
                    ObjectId = objectId,
                    ObjectRevisionId = objectRevisionId,
                    RevisionId = 0
                };

                AddObjectToBranchHeadObjects(toCreate);
            }

            //~ add record to RevisionHeadObjects for each indexed revision
            var revisions = _revisionHeadObjects.Values.GroupBy(m => m.RevisionId).Select(m => new { RevisionId = m.Key, m.First().BranchId });

            foreach (var revision in revisions)
            {
                var toCreate = new BranchHeadObjectEntity
                {
                    BranchId = revision.BranchId,
                    ClassId = classId,
                    ObjectId = objectId,
                    ObjectRevisionId = objectRevisionId,
                    RevisionId = 0
                };

                _revisionHeadObjects.Save(toCreate);
            }
        }

        public BranchHeadObjectEntity GetValueForObjectInBranch(int branchId, int objectId)
        {
            return GetValueForObjectInBranch(branchId, objectId, true);
        }

        public BranchHeadObjectEntity GetValueForObjectInBranch(int branchId, int objectId, bool prefetchValues)
        {
            if (prefetchValues)
            {
                var result = _branchHeadObjects.SingleOrDefault(m => m.Value.BranchId == branchId && m.Value.ObjectId == objectId);

                if(result.Value == null){ return null;}

                var toRet = result.Value;
                toRet.ObjectRevision = _objectRevisions.Single(m => m.Value.Id == toRet.ObjectRevisionId).Value;
                PrefetchValues(toRet.ObjectRevision);
                return toRet;
            }

            return _branchHeadObjects.Single(m => m.Value.BranchId == branchId && m.Value.ObjectId == objectId).Value;
        }

        public IEnumerable<ObjectRevisionEntity> GetAllDataForBranchByType(int branchId, int classId)
        {
            var toRet = _branchHeadObjects
                .Where(m => m.Value.BranchId == branchId && m.Value.ClassId == classId).Select(m => _objectRevisions.Single(n => n.Value.Id == m.Value.ObjectRevisionId).Value).ToList();

            PrefetchValues(toRet);
            return toRet;
        }

        private void PrefetchValues(IEnumerable<ObjectRevisionEntity> toRet)
        {
            foreach (var objectRevisionEntity in toRet.ToList())
            {
                PrefetchValues(objectRevisionEntity);
            }
        }

        private void PrefetchValues(ObjectRevisionEntity objectRevisionEntity)
        {
            objectRevisionEntity.ForeignKeyAttributeValues =
                    _relationAttributeValue.Where(m => m.Value.ObjectRevisionId == objectRevisionEntity.Id).Select(m => m.Value).ToList();

            objectRevisionEntity.AttributeValues =
                _attributeValues.Where(m => m.Value.ObjectRevisionId == objectRevisionEntity.Id).Select(m => m.Value).ToList();
        }

        public IEnumerable<ObjectRevisionEntity> GetAllByTypeWithFkRelation(int branchId, int classId, int fkAttributeId, IEnumerable<int> fkList)
        {
            var toRet = from objectRevisions in _objectRevisions
                        join branchObjs in _branchHeadObjects on objectRevisions.Value.Id equals branchObjs.Value.ObjectRevisionId
                        join relationValues in _relationAttributeValue on objectRevisions.Value.Id equals
                            relationValues.Value.ObjectRevisionId
                        join relationMetadata in _relationMetadata on relationValues.Value.RelationshipMetadataId equals
                            relationMetadata.Value.Id
                        where
                            branchObjs.Value.BranchId == branchId && branchObjs.Value.ClassId == classId &&
                            relationMetadata.Value.AttributeId == fkAttributeId
                            && fkList.Contains(relationValues.Value.Value ?? -1)
                        select objectRevisions.Value;

            PrefetchValues(toRet.ToList());

            return toRet;
        }

        

        public IEnumerable<ObjectRevisionEntity> GetAllByTypeWithId(int branchId, IEnumerable<int> idList)
        {
            var toRet = _branchHeadObjects
               .Where(m => m.Value.BranchId == branchId && idList.Contains(m.Value.Id))
               .Select(m => _objectRevisions.Single(n => n.Value.Id == m.Value.ObjectRevisionId).Value);

            PrefetchValues(toRet.ToList());
            return toRet;
        }

        public System.Data.DataTable GetAllRelatedToObject(int branchId, int objectId)
        {
            var relatedToObject = _relationAttributeValue.Where(m => m.Value.Value == objectId);
            var relatedInBranch =
                _branchHeadObjects.Where(
                    m =>
                    m.Value.BranchId == branchId &&
                    relatedToObject.Select(n => n.Value.ObjectRevisionId).Contains(m.Value.ObjectRevisionId));

            var dt = new DataTable();
            dt.Columns.Add("ObjectRevisionId", typeof(int));
            dt.Columns.Add("ObjectId", typeof(int));
            dt.Columns.Add("RevisionId", typeof(int));
            dt.Columns.Add("RelationMetadataId", typeof(int));

            foreach (var item in relatedInBranch)
            {
                int metaDataId =
                    _relationAttributeValue.First(m => m.Value.ObjectRevisionId == item.Value.ObjectRevisionId &&
                                                       m.Value.Value == objectId).Value.RelationshipMetadataId;


                dt.Rows.Add( item.Value.ObjectRevisionId, item.Value.ObjectId, item.Value.RevisionId, metaDataId );
            }

            return dt;
        }

        public BranchHeadObjectEntity GetValueForObjectAtRevision(int branchId, int objectId, int revisionId)
        {
            var toRet =
                _revisionHeadObjects.Single(
                    m =>
                    m.Value.BranchId == branchId && m.Value.ObjectId == objectId && m.Value.RevisionId == revisionId);

            toRet.Value.ObjectRevision = _objectRevisions[toRet.Value.ObjectRevisionId];
            PrefetchValues(toRet.Value.ObjectRevision);

            return toRet.Value;
        }

        public IEnumerable<ObjectRevisionEntity> GetAllDataForRevisionByType(int branchId, int revisionId, int classId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ObjectRevisionEntity> GetAllForRevisionWithFkRelation(int branchId, int revisionId, int classId, int fkAttributeId, IEnumerable<int> fkList)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ObjectRevisionEntity> GetAllAtRevisionWithId(int branchId, int revisionId, IEnumerable<int> idList)
        {
            throw new NotImplementedException();
        }

        public ObjectEntity GetObject(int id)
        {
            return _objects[id];
        }

        public IList<RelationAttributeValueEntity> GetAllForeignKeyReferencesTo(int objectId)
        {
            return _relationAttributeValue.Where(m => m.Value.Value == objectId).Select(m => m.Value).ToList();
        }

        public void SaveRelationAttributeValue(RelationAttributeValueEntity toSave)
        {
            _relationAttributeValue.Save(toSave);
        }

        public void SaveAttributeValue(AttributeValueEntity toSave)
        {
            _attributeValues.Save(toSave);
        }

        public ObjectRevisionEntity GetObjectRevisionEager(int id, IEntityMetaData entityMetadata)
        {
            var toRet = _objectRevisions[id];
            PrefetchValues(toRet);
            return toRet;
        }

        public ObjectRevisionEntity GetObjectRevisionEagerAtRoot(int id)
        {
            var toRet = _objectRevisions.Single(m => m.Value.ObjectId == id && m.Value.RevisionId == 0).Value;
            PrefetchValues(toRet);
            return toRet;
        }

        public void DeleteAttributeValues(IEnumerable<AttributeValueEntity> toDelete)
        {
            foreach (var attributeValueEntity in toDelete)
            {
                _attributeValues.Remove(attributeValueEntity.Id);
            }
        }

        public void DeleteRelationAttributeValues(IEnumerable<RelationAttributeValueEntity> toDelete)
        {
            foreach (var relationValueEntity in toDelete)
            {
                _relationAttributeValue.Remove(relationValueEntity.Id);
            }
        }

        public void Dispose() { }


        public IEnumerable<ObjectRevisionEntity> GetBranchHeadObjectsWithFilter(IPredicateBucket filterBucket)
        {
            IEnumerable<ObjectRevisionEntity> filteredList = _branchHeadObjects
                .Where(n=>n.Value.BranchId == filterBucket.ContextId && n.Value.ClassId == filterBucket.ClassId)
                .SelectMany(m=> _objectRevisions.Where(n=>n.Value.Id == m.Value.ObjectRevisionId).Select(n=>n.Value)).ToList();

            #region CompareValueFilters
            foreach (var compareValueFilter in filterBucket.GetAttributeComparePredicates())
            {
                var dataType = (DogwoodDataType)DbMetadataRegistry.Instance[compareValueFilter.AttributeMetadatatId].DataTypeId;

                var list = filteredList;
                var filter = compareValueFilter;

                switch (compareValueFilter.CompOperator)
                {
                    case ComparisonOperator.Equal:

                        switch (dataType)
                        {
                            case DogwoodDataType.String:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToString(m.Value.Value) == (string)filter.Value));
                                break;
                            case DogwoodDataType.Int:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt32(m.Value.Value) == (int?)filter.Value));
                                break;
                            case DogwoodDataType.Long:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt64(m.Value.Value) == (long?)filter.Value));
                                break;
                            case DogwoodDataType.Bool:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToBool(m.Value.Value) == (bool?)filter.Value));
                                break;
                            case DogwoodDataType.DateTime:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToDateTime(m.Value.Value) == (DateTime?)filter.Value));
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                        break;
                    case ComparisonOperator.NotEqual:
                        switch (dataType)
                        {
                            case DogwoodDataType.String:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToString(m.Value.Value) != (string)filter.Value));
                                break;
                            case DogwoodDataType.Int:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt32(m.Value.Value) != (int?)filter.Value));
                                break;
                            case DogwoodDataType.Long:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt64(m.Value.Value) != (long?)filter.Value));
                                break;
                            case DogwoodDataType.Bool:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToBool(m.Value.Value) != (bool?)filter.Value));
                                break;
                            case DogwoodDataType.DateTime:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToDateTime(m.Value.Value) != (DateTime?)filter.Value));
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        break;
                    case ComparisonOperator.LessThan:
                        switch (dataType)
                        {
                            case DogwoodDataType.String:
                                throw new Exception("Can not compare less than on strings");
                            case DogwoodDataType.Int:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt32(m.Value.Value) < (int?)filter.Value));
                                break;
                            case DogwoodDataType.Long:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt64(m.Value.Value) < (long?)filter.Value));
                                break;
                            case DogwoodDataType.Bool:
                                throw new Exception("Can not compare less than on bools");
                            case DogwoodDataType.DateTime:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToDateTime(m.Value.Value) < (DateTime?)filter.Value));
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        break;
                    case ComparisonOperator.GreaterThan:
                        switch (dataType)
                        {
                            case DogwoodDataType.String:
                                throw new Exception("Can not compare greater than on strings");
                            case DogwoodDataType.Int:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt32(m.Value.Value) > (int?)filter.Value));
                                break;
                            case DogwoodDataType.Long:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt64(m.Value.Value) > (long?)filter.Value));
                                break;
                            case DogwoodDataType.Bool:
                                throw new Exception("Can not compare greater than on bools");
                            case DogwoodDataType.DateTime:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToDateTime(m.Value.Value) > (DateTime?)filter.Value));
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        break;
                    case ComparisonOperator.LessThanOrEqualTo:
                        switch (dataType)
                        {
                            case DogwoodDataType.String:
                                throw new Exception("Can not compare less than on strings");
                            case DogwoodDataType.Int:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt32(m.Value.Value) <= (int?)filter.Value));
                                break;
                            case DogwoodDataType.Long:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt64(m.Value.Value) <= (long?)filter.Value));
                                break;
                            case DogwoodDataType.Bool:
                                throw new Exception("Can not compare less than on bools");
                            case DogwoodDataType.DateTime:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToDateTime(m.Value.Value) <= (DateTime?)filter.Value));
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        break;
                    case ComparisonOperator.GreaterThanOrEqualTo:
                        switch (dataType)
                        {
                            case DogwoodDataType.String:
                                throw new Exception("Can not compare greater than on strings");
                            case DogwoodDataType.Int:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt32(m.Value.Value) >= (int?)filter.Value));
                                break;
                            case DogwoodDataType.Long:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToInt64(m.Value.Value) >= (long?)filter.Value));
                                break;
                            case DogwoodDataType.Bool:
                                throw new Exception("Can not compare greater than on bools");
                            case DogwoodDataType.DateTime:
                                filteredList = _attributeValues
                                    .SelectMany(m => list
                                    .Where(n => m.Value.ObjectRevisionId == n.Id &&
                                                m.Value.AttributeMetadataId ==
                                                filter.AttributeMetadatatId &&
                                                DataConverter.BytesToDateTime(m.Value.Value) >= (DateTime?)filter.Value));
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            #endregion
            
            return filteredList;
        }

        public IEnumerable<ObjectRevisionEntity> GetRevisionHeadObjectsWithFilter(IPredicateBucket filterBucket)
        {
            throw new NotImplementedException();
        }

        public void AddRevisionToHeadObjects(int branchId, int revisionId)
        {
            if (!_revisionHeadObjects.Any(m => m.Value.RevisionId == revisionId))
            {
                foreach (var headObj in RevisionHeadObjects(branchId, revisionId))
                {
                    _revisionHeadObjects.Save(headObj);
                }
            }
        }

        public void RemoveRootObjectFromHeadObjects(int objectId)
        {
            //~ DELETE FROM tblBranchHeadObject WHERE ObjectId = objectId
            var keysToRemoveFromBranchHead = _branchHeadObjects.Where(m => m.Value.ObjectId == objectId).Select(m => m.Key);
            foreach (var toRemoveFromBranchHead in keysToRemoveFromBranchHead)
            {
                _branchHeadObjects.Remove(toRemoveFromBranchHead);
            }

            //~ DELETE FROM tblRevisionHeadObject WHERE ObjectId = objectId
            var keysToRemoveFromRevisionHead = _revisionHeadObjects.Where(m => m.Value.ObjectId == objectId).Select(m => m.Key);
            foreach (var toRemoveFromRevisionHead in keysToRemoveFromRevisionHead)
            {
                _revisionHeadObjects.Remove(toRemoveFromRevisionHead);
            }
        }

        private IEnumerable<HiLoRow> BranchTable(int branchId)
        {
            IList<HiLoRow> toRet = new List<HiLoRow>();

            var hi = int.MaxValue;
            var lo = branchId;

            toRet.Add(new HiLoRow(branchId, hi, lo));

            while (branchId > 0)
            {
                branchId = _revisions[branchId].BranchId;
                hi = lo;
                lo = branchId;

                toRet.Add(new HiLoRow(branchId, hi, lo));
            }

            return toRet;
        }

        private IEnumerable<int> RevisionBound(int branchId, int revisionId)
        {
            var revs = BranchTable(branchId)
                .SelectMany(b => _revisions.Where(r =>b.BranchId == r.Value.BranchId && b.Lo <= r.Key && b.Hi >= r.Key &&r.Key <= revisionId));

            var objRevs = revs.SelectMany(r => _objectRevisions.Where(objRev => r.Key == objRev.Value.RevisionId));

            var notDeletedObjRevs = objRevs.Where(m => !m.Value.IsDeleted);

            var annon = notDeletedObjRevs.Select(m => new
                                  {
                                      m.Value.ObjectId,
                                      _objects[m.Value.ObjectId].ClassId,
                                      ObjectRevisionId = m.Key
                                  });

            return annon.GroupBy(m => new { m.ClassId, m.ObjectId }).Select(m => m.Max(n => n.ObjectRevisionId));
        }

        private IEnumerable<int> BranchBound(int branchId)
        {
            var revs = BranchTable(branchId)
                .SelectMany(b => _revisions.Where(r =>b.BranchId == r.Value.BranchId && b.Lo <= r.Key && b.Hi >= r.Key));

            var objRevs = revs.SelectMany(r => _objectRevisions.Where(objRev => r.Key == objRev.Value.RevisionId));

            var notDeletedObjRevs = objRevs.Where(m => !m.Value.IsDeleted);

            var annon = notDeletedObjRevs.Select(m => new
                  {
                      m.Value.ObjectId,
                      _objects[m.Value.ObjectId].ClassId,
                      ObjectRevisionId = m.Key
                  });

            return annon.GroupBy(m => new { m.ClassId, m.ObjectId }).Select(m => m.Max(n => n.ObjectRevisionId));
        }

        private IEnumerable<BranchHeadObjectEntity> RevisionHeadObjects(int branchId, int revisionId)
        {
            return RevisionBound(branchId, revisionId)
                .Select(m => new BranchHeadObjectEntity
                {
                    BranchId = branchId,
                    ClassId = _objects[_objectRevisions[m].ObjectId].ClassId,
                    ObjectId = _objectRevisions[m].ObjectId,
                    ObjectRevisionId = m,
                    RevisionId = _objectRevisions[m].RevisionId
                });
        }

        private IEnumerable<BranchHeadObjectEntity> BranchHeadObjects(int branchId)
        {
            return BranchBound(branchId)
                .Select(m => new BranchHeadObjectEntity
                {
                    BranchId = branchId,
                    ClassId = _objects[_objectRevisions[m].ObjectId].ClassId,
                    ObjectId = _objectRevisions[m].ObjectId,
                    ObjectRevisionId = m,
                    RevisionId = _objectRevisions[m].RevisionId
                });
        }
    }

    public class HiLoRow
    {
        public int Hi { get; set; }
        public int Lo { get; set; }
        public int BranchId { get; set; }

        public HiLoRow(int branchId, int hi, int lo)
        {
            Hi = hi;
            Lo = lo;
            BranchId = branchId;
        }
    }
}
