﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Dogwood.Core
{
    public class ExpressionHelper
    {
        /// <summary>
        /// Return the PropertyInfo that corresponds to the given a member expression based on the object type T.  Throws an ArgumentException if
        /// the provided expression is not a MemberExpression.
        /// </summary>
        /// <typeparam name="T">The class type serving as the root of the expression</typeparam>
        /// <typeparam name="TProperty">The property type of the expression result</typeparam>
        /// <param name="propertyLambda">The expression to evaluate</param>
        /// <returns>PropertyInfo object.</returns>
        public static PropertyInfo GetPropertyInfo<T, TProperty>(Expression<Func<T, TProperty>> propertyLambda)
        {
            var type = typeof(T);

            var member = propertyLambda.Body as MemberExpression;
            if (member == null)
                throw new ArgumentException(string.Format("Expression '{0}' refers to a method, not a property.", propertyLambda));

            var propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
                throw new ArgumentException(string.Format("Expression '{0}' refers to a field, not a property.", propertyLambda));

            if (type != propInfo.ReflectedType &&
                !type.IsSubclassOf(propInfo.ReflectedType))
                throw new ArgumentException(string.Format("Expresion '{0}' refers to a property that is not from type {1}.", propertyLambda, type));

            return propInfo;
        }
    }
}
