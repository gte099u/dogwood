﻿using StructureMap;

namespace Dogwood.Core
{
    public class RootDataContext : DogwoodDataContext
    {
        public RootDataContext(IPersistenceAdapter database, int requestingUserId)
            : base(database, 0, requestingUserId, EavContextType.Revision)
        { }

        public RootDataContext(int requestingUserId)
            : base(ObjectFactory.GetInstance<IPersistenceAdapter>(), 0, requestingUserId, EavContextType.Revision) { }
    }
}
