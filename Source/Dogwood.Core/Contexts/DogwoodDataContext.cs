﻿using System.Collections.Generic;
using StructureMap;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public class DogwoodDataContext : IDogwoodDataContext
    {
        private IDictionary<int, ObjectRevisionEntity> _ObjetRevisionCache;
        public IDictionary<int, ObjectRevisionEntity> ObjectRevisionCache
        {
            get { return _ObjetRevisionCache ?? (_ObjetRevisionCache = new Dictionary<int, ObjectRevisionEntity>()); }
            set
            {
                _ObjetRevisionCache = value;
            }
        }

        private ObjectCache _ObjectCache;
        public ObjectCache ObjectCache
        {
            get { return _ObjectCache ?? (_ObjectCache = new ObjectCache()); }
        }

        public IPersistenceAdapter PersistenceAdapter { get; set; }
        public int ContextId { get; private set; }
        public int RequestingUserId { get; private set; }
        public EavContextType ContextType { get; set; }

        public DogwoodDataContext(IPersistenceAdapter database, int contextId, int requestingUserId, EavContextType contextType)
        {
            PersistenceAdapter = database;
            ContextType = contextType;
            RequestingUserId = requestingUserId;
            ContextId = contextId;
        }

        public DogwoodDataContext(int contextId, int requestingUserId, EavContextType contextType) :
            this(ObjectFactory.GetInstance<IPersistenceAdapter>(), contextId, requestingUserId, contextType)
        { }
    }

   
}
