﻿namespace Dogwood.Core
{
    public class DogwoodBranchContext : DogwoodDataContext
    {
        public DogwoodBranchContext(int contextId, int requestingUserId)
            : base(contextId, requestingUserId, EavContextType.Branch)
        { }

        public DogwoodBranchContext(IPersistenceAdapter adapter, int contextId, int requestingUserId)
            : base(adapter, contextId, requestingUserId, EavContextType.Branch)
        { }
    }
}
