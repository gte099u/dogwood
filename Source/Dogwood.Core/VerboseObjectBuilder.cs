﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Diagnostics;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public class VerboseObjectBuilder<T> : IObjectBuilder<T> where T : IDogwoodEntity, new()
    {
        private readonly IDictionary<int, ObjectRevisionEntity> _ObjectRevisionCache;
        private readonly IEntityMetaData _EntityMetadata;
        private readonly IPersistenceAdapter _Adapter;
        private readonly EavFactory<T> _ObjectFactory;

        public VerboseObjectBuilder(IDictionary<int, ObjectRevisionEntity> objRevCache, IPersistenceAdapter adapter)
        {
            _ObjectFactory = new EavFactory<T>();
            _ObjectRevisionCache = objRevCache;

            try
            {
                _EntityMetadata = MetadataRegistry.Instance[typeof(T)];
            }
            catch (Exception)
            {
                throw new Exception(string.Format("Metadata for type {0} was not found in the registry. Make sure that the {1} type was included in the entityTypes bootstrapper parameter", typeof(T), typeof(T)));
            }

            _Adapter = adapter;
        }

        public T BuildObject(DataTable currentObjectRev)
        {
            var sw = Stopwatch.StartNew();

            // we expect only one object in the provided data table, if zero exist return default null
            if (currentObjectRev.Rows.Count == 0)
            {
                return default(T);
            }
            // if multiple rows exist in the datatable, we have a problem - throw
            if (currentObjectRev.Rows.Count > 1)
            {
                throw new Exception("An unexpected number of records were returned for a single item query");
            }
            
            ObjectRevisionEntity objRev;

            if (_ObjectRevisionCache.ContainsKey((int)currentObjectRev.Rows[0]["Id"]))
            {
                objRev = _ObjectRevisionCache[(int)currentObjectRev.Rows[0]["Id"]];
            }
            else
            {
                objRev = _Adapter.GetObjectRevisionEager((int)currentObjectRev.Rows[0]["Id"], _EntityMetadata);

                if (!_ObjectRevisionCache.ContainsKey(objRev.Id))
                {
                    _ObjectRevisionCache.Add(objRev.Id, objRev);
                }
            }

            var toRet = _ObjectFactory.Create(objRev);

            sw.Stop();

            return toRet;
        }

        public List<T> BuildCollection(DataTable dt)
        {

            var sw = Stopwatch.StartNew();

            var idRange = new List<int>();
            var objectRevisions = new List<ObjectRevisionEntity>();

            foreach (DataRow row in dt.Rows)
            {
                if (_ObjectRevisionCache.ContainsKey((int)row["ObjectRevisionId"]))
                {
                    objectRevisions.Add(_ObjectRevisionCache[(int)row["ObjectRevisionId"]]);
                }
                else
                {
                    idRange.Add((int)row["ObjectRevisionId"]);
                }
            }

            if (idRange.Count != 0)
            {
                var objectVersions = _Adapter.GetObjectRevisionsEager(idRange, _EntityMetadata);

                objectRevisions.AddRange(objectVersions);

                foreach (var item in objectVersions)
                {
                    _ObjectRevisionCache.Add(item.Id, item);
                }
            }

            var toRet = objectRevisions.Select(objRev => _ObjectFactory.Create(objRev)).ToList();

            sw.Stop();

            return toRet;
        }

        public T BuildObject(ObjectRevisionEntity headObject)
        {
            return _ObjectFactory.Create(headObject);
        }

        public List<T> BuildCollection(IEnumerable<ObjectRevisionEntity> branchHeadObjects)
        {
            return branchHeadObjects.Select(objRev => _ObjectFactory.Create(objRev)).ToList();
        }
    }
}
