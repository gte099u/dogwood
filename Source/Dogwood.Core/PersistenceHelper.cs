﻿using System;
using System.Collections.Generic;
using Dogwood.Core.Entities;
using System.Linq;

namespace Dogwood.Core
{
    /// <summary>
    /// Provides common persistence functions for use by persistence services
    /// </summary>
    internal class PersistenceHelper
    {
        private List<int> _DeletedObjects;
        private readonly IPersistenceAdapter _Adapter;
        private readonly ObjectCache _cache;
        private readonly AttributePersistenceManager _AttManager;

        public PersistenceHelper(IPersistenceAdapter adapter, ObjectCache cache)
        {
            _DeletedObjects = new List<int>();
            _Adapter = adapter;
            _cache = cache;
            _AttManager = new AttributePersistenceManager(_Adapter);            
        }

        public void PurgeOrphanRevisions()
        {
            var orphanRevisions = _Adapter.GetAllOrphanRevisions();

            foreach (var rev in orphanRevisions.OrderByDescending(m => m.Id))
            {
                _Adapter.DeleteRevision(rev);
            }
        }

        public void DestroyObjects(int toDestroyId)
        {
            //~ reverse-cascade delete all related objects

            //~ identify foreign key attributes that point to the "to-be" deleted entity
            var referencesToDeletedObj = _Adapter.GetAllForeignKeyReferencesTo(toDestroyId);

            foreach (var reference in referencesToDeletedObj)
            {
                DestroyObjects(reference.ObjectRevision.ObjectId);
            }

            //~ delete object revisions, which will cascade delete all attribute values
            _Adapter.DeleteAllObjectRevisionsHavingObjectId(toDestroyId);

            //~ delete the entity
            _Adapter.DeleteObject(toDestroyId);
        }

        public void SaveAttributeValues(IDogwoodEntity toSave, int objectRevisionId)
        {
            _AttManager.SaveAttributeValues(toSave, objectRevisionId);
        }

        /// <summary>
        /// Creates a new Revision record using the provided branchId and tag string.  Returns the PK of the newly created
        /// revision record.
        /// </summary>
        /// <param name="branchId">PK of the branch associated with the revision</param>
        /// <param name="userId"> </param>
        /// <param name="tag">String description or identifier for the revision</param>
        /// <param name="comments"> </param>
        /// <returns>the PK of the newly created revision record</returns>
        public int SaveNewRevision(int branchId, int userId, string tag, string comments)
        {
            if (branchId == 0)
            {
                return 0; //~ dont save another revision for root-based objects
            }

            //~ save a new revision
            var revision = new RevisionEntity { BranchId = branchId, TimeStamp = DateTime.Now, Tag = tag, CreatedBy = userId, Comments = comments };

            _Adapter.SaveRevision(revision);
            return revision.Id;
        }

        public int SaveNewObject(int classId, int revisionId)
        {
            //~ save a new object entry
            var obj = new ObjectEntity { ClassId = classId, RevisionId = revisionId };
            _Adapter.SaveObject(obj);

            return obj.Id;
        }

        public int SaveNewObjectRevision(int objectId, int revisionId, bool isDeleted)
        {
            //~ save a new object revision entry
            var objRev = new ObjectRevisionEntity { ObjectId = objectId, RevisionId = revisionId, IsDeleted = isDeleted };
            _Adapter.SaveObjectRevision(objRev);
            return objRev.Id;
        }

        public void DeleteObject(int toDeleteId, int revisionId, int branchId)
        {
            if (!_DeletedObjects.Contains(toDeleteId))
            {
                SaveNewObjectRevision(toDeleteId, revisionId, true);

                _Adapter.RemoveObjectFromBranchHeadObjects(toDeleteId, branchId);
                
                _DeletedObjects.Add(toDeleteId);

                //~ delete all related objects 
                var relatedObjects = _Adapter.GetAllRelatedToObject(branchId, toDeleteId);

                for (var i = 0; i < relatedObjects.Rows.Count; i++)
                {
                    var currentObjectRevisionId = (int)relatedObjects.Rows[i][0];
                    var relatedObjectId = (int)relatedObjects.Rows[i][1];
                    var fkMetadataId = (int)relatedObjects.Rows[i][3];

                    var spec = DbFkMetadataRegistry.Instance[fkMetadataId];

                    switch (spec.CascadeBehavior)
                    {
                        case CascadeBehaviorType.Delete:
                            DeleteObject(relatedObjectId, revisionId, branchId);
                            break;
                        case CascadeBehaviorType.SetToDefault:
                            {
                                var objRev = _Adapter.GetObjectRevisionEager(currentObjectRevisionId, MetadataRegistry.Instance.Single(m => m.Value.ClassId == spec.FkClassId).Value);
                                var fkToUpdate = objRev.ForeignKeyAttributeValues.Single(m => m.RelationshipMetadataId == fkMetadataId);
                                fkToUpdate.Value = spec.CascadeDefaultValue;
                                _Adapter.SaveRelationAttributeValue(fkToUpdate);
                                _DeletedObjects.Add(relatedObjectId);
                            }
                            break;
                        case CascadeBehaviorType.SetToNull:
                            {
                                SaveNewObjectRevision(relatedObjectId, revisionId, false);

                                var objRev = _Adapter.GetObjectRevisionEager(currentObjectRevisionId, MetadataRegistry.Instance.Single(m => m.Value.ClassId == spec.FkClassId).Value);
                                var fkToUpdate = objRev.ForeignKeyAttributeValues.Single(m => m.RelationshipMetadataId == fkMetadataId);
                                fkToUpdate.Value = null;
                                _Adapter.SaveRelationAttributeValue(fkToUpdate);
                                _DeletedObjects.Add(relatedObjectId);
                            }
                            break;
                    }
                }
            }
        }

        public void ResetDeletedObjectsList()
        {
            _DeletedObjects = new List<int>();
        }

        public int GetBranchIdForRevision(int revisionId)
        {
            if (_cache.ContainsRevision(revisionId))
            {
                return _cache.GetRevision(revisionId).BranchId;
            }

            var revision = _Adapter.GetRevision(revisionId);

            _cache.AddRevision(revision);

            return revision.BranchId;
        }
    }
}
