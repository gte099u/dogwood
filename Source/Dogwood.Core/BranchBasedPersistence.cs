﻿using System;
using System.Data;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public class BranchBasedPersistence<T> : IVersionedPersistence<T> where T : class, IDogwoodEntity, new()
    {
        private readonly BranchBasedGetter<T> _getter;
        private readonly ObjectCache _objectCache;
        private readonly IPersistenceAdapter _adapter;
        private readonly PersistenceHelper _helper;
        private readonly IEntityMetaData _entityMetadata;
        private readonly int _contextId;
        private readonly IEntityValidation<T> _validator;
        private readonly int _userId;

        public BranchBasedPersistence(IDogwoodDataContext context)
        {
            if (context.ContextType != EavContextType.Branch)
            {
                throw new Exception("Branch context type expected but not provided");
            }

            _adapter = context.PersistenceAdapter;
            _entityMetadata = MetadataRegistry.Instance[typeof(T)];
            _contextId = context.ContextId;
            _userId = context.RequestingUserId;
            _objectCache = context.ObjectCache;
            _helper = new PersistenceHelper(_adapter, _objectCache);
            _getter = new BranchBasedGetter<T>(context);
            _validator = new EntityValidation<T>(_adapter, _entityMetadata, _contextId, _objectCache);
            new VerboseObjectBuilder<T>(context.ObjectRevisionCache, _adapter);
        }

        /// <summary>
        /// Initializes a new revision in the database.   
        /// </summary>
        /// <remarks>Use this function if you wish to execute batch persistence commands under the label of a single revision. 
        /// When used in this fashion, this function and the associated batch persistence commands should be wrapped in a database transaction.
        /// This fuction does not initiate its own database transaction, but instead leaves that responsibility to the consumer</remarks>
        /// <returns></returns>
        public int CreateRevision()
        {
            return CreateRevision(null, null);
        }

        public int CreateRevision(string tag, string comments)
        {
            return _helper.SaveNewRevision(_contextId, _userId, tag, comments);
        }

        public ChangeData Save(T toSave)
        {
            return Save(toSave, null);
        }

        public ChangeData Save(T toSave, string comments)
        {
            return Save(toSave, comments, false);
        }

        public ChangeData Save(T toSave, string comments, bool openNewTransaction)
        {
            ChangeData toRet;

            try
            {
                if (openNewTransaction)
                {
                    CheckTransactionState();

                    _adapter.StartTransaction(IsolationLevel.ReadCommitted, "DeleteObject");
                }

                toRet = toSave.IsNew ? CreateInBranch(toSave, comments) : ReviseInBranch(toSave, comments);

                if (openNewTransaction)
                {
                    _adapter.Commit();
                }
            }
            catch (Exception)
            {
                if (openNewTransaction)
                {
                    _adapter.Rollback();
                }

                throw;
            }
            return toRet;

        }

        public bool Delete(int toDeleteId)
        {
            return Delete(toDeleteId, null);
        }

        public bool Delete(int toDeleteId, string comments)
        {
            return Delete(toDeleteId, comments, true);
        }

        public bool Delete(int toDeleteId, string comments, bool openNewTransaction)
        {
            try
            {
                if (openNewTransaction)
                {
                    CheckTransactionState();
                    _adapter.StartTransaction(IsolationLevel.ReadCommitted, "DeleteObject");
                }

                //~ create new revision
                var revisionId = _helper.SaveNewRevision(_contextId, _userId, null, comments);

                //~ delete the object within the new revision (will cascade delete related objects)
                _helper.DeleteObject(toDeleteId, revisionId, _contextId);
                _helper.ResetDeletedObjectsList();

                _objectCache.Remove(toDeleteId, _entityMetadata.ClassId);
                _objectCache.Clear();

                if (openNewTransaction)
                {
                    _adapter.Commit();
                }
            }
            catch (Exception)
            {
                if (openNewTransaction)
                {
                    _adapter.Rollback();
                }

                throw;
            }
            return true;
        }

        public bool DestroyObject(int toDestroyId)
        {
            try
            {
                _adapter.StartTransaction(IsolationLevel.ReadCommitted, "DestroyObject");

                _helper.DestroyObjects(toDestroyId);
                _helper.PurgeOrphanRevisions();

                _objectCache.Remove(toDestroyId, _entityMetadata.ClassId);

                _adapter.Commit();
            }
            catch (Exception)
            {
                _adapter.Rollback();

                throw;
            }

            return true;
        }

        public bool DeleteWithinBatch(int toDeleteId, int revisionId)
        {
            //~ delete the object within the new revision (will cascade delete related objects)
            _helper.DeleteObject(toDeleteId, revisionId, _contextId);
            _helper.ResetDeletedObjectsList();

            _objectCache.Remove(toDeleteId, _entityMetadata.ClassId);

            _adapter.RemoveObjectFromBranchHeadObjects(toDeleteId, _contextId);

            return true;
        }

        /// <summary>
        /// Saves a new object to the database under the label of an existing revision.
        /// </summary>
        /// <remarks>Use this function to create objects as part of a batch commit under the label of a single revision.
        /// This fuction does not initiate its own database transaction, but instead leaves that responsibility to the consumer.
        /// It is advised that this function be called within a database transaction that the consumer has already initialized.</remarks>
        /// <param name="toSave">entity of type T to save.  This entity must be new and should not have an Id value yet.</param>
        /// <param name="revisionId">the id of the revision context to save the entity within</param>
        /// <returns>data about the new ObjectId, RevisionId, and ObjectRevisionId</returns>
        public ChangeData SaveWithinBatch(T toSave, int revisionId)
        {
            if (!_adapter.IsTransactionInProgress)
            {
                throw new Exception("SaveWithinBatch must be wrapped within a transaction.");
            }

            if (toSave.IsNew)
            {
                return CreateAtRevision(toSave, revisionId);
            }

            return ReviseAtRevision(toSave, revisionId);
        }

        private ChangeData CreateAtRevision(T toSave, int revisionId)
        {
            var branchId = _helper.GetBranchIdForRevision(revisionId);

            if (_contextId != branchId)
            {
                throw new Exception("Bad Revision");
            }

            _validator.ValidateBeforeCreate(toSave);

            var objId = _helper.SaveNewObject(_entityMetadata.ClassId, revisionId);
            var objRevId = _helper.SaveNewObjectRevision(objId, revisionId, false);

            _helper.SaveAttributeValues(toSave, objRevId);

            var headObject = new BranchHeadObjectEntity
            {
                BranchId = _contextId,
                ClassId = _entityMetadata.ClassId,
                ObjectId = objId,
                ObjectRevisionId = objRevId,
                RevisionId = revisionId
            };

            _adapter.AddObjectToBranchHeadObjects(headObject);

            _objectCache.SetHeadObject(headObject);

            toSave.Id = objId;
            _objectCache.SetObject(toSave);

            var toRet = new ChangeData(objId, revisionId, objRevId);

            return toRet;
        }

        private ChangeData ReviseAtRevision(T toSave, int revisionId)
        {
            var branchId = _helper.GetBranchIdForRevision(revisionId);

            if (_contextId != branchId)
            {
                throw new Exception("Bad Revision");
            }

            //~ verify that the object exists in the branch's ancestry
            var existing = _getter.Get(toSave.Id);

            if (existing == null)
            {
                throw new Exception("Unable to revise the object in this branch because the object does not exist in the branch's ancestry");
            }

            //~ perform any checks to ensure uniqueness, etc.
            _validator.ValidateBeforeUpdate(toSave, existing);

            var objRevId = _helper.SaveNewObjectRevision(toSave.Id, revisionId, false);

            _helper.SaveAttributeValues(toSave, objRevId);

            var headObject = _adapter.GetValueForObjectInBranch(_contextId, toSave.Id, false);
            headObject.RevisionId = revisionId;
            headObject.ObjectRevisionId = objRevId;
            _adapter.UpdateObjectInBranchHeadObjects(headObject);

            if (_objectCache.ContainsHeadObject(toSave.Id))
            {
                _objectCache.SetHeadObject(headObject);
            }
            else
            {
                _objectCache.SetHeadObject(headObject);
            }

            _objectCache.SetObject(toSave);

            return new ChangeData(toSave.Id, revisionId, objRevId);
        }

        private void CheckTransactionState()
        {
            if (_adapter.IsTransactionInProgress)
            {
                throw new Exception("Unable to execute this action because a transaction is already open");
            }
        }

        private ChangeData CreateInBranch(T toAdd, string comments)
        {
            _validator.ValidateBeforeCreate(toAdd);

            var revisionId = _helper.SaveNewRevision(_contextId, _userId, null, comments);
            var objId = _helper.SaveNewObject(_entityMetadata.ClassId, revisionId);
            var objRevId = _helper.SaveNewObjectRevision(objId, revisionId, false);

            _helper.SaveAttributeValues(toAdd, objRevId);

            var headObject = new BranchHeadObjectEntity
            {
                BranchId = _contextId,
                ClassId = _entityMetadata.ClassId,
                ObjectId = objId,
                ObjectRevisionId = objRevId,
                RevisionId = revisionId
            };

            _adapter.AddObjectToBranchHeadObjects(headObject);

            _objectCache.SetHeadObject(headObject);

            toAdd.Id = objId;

            _objectCache.SetObject(toAdd);

            var toRet = new ChangeData(objId, revisionId, objRevId);

            return toRet;
        }

        private ChangeData ReviseInBranch(T toSave, string comments)
        {
            var existing = _getter.Get(toSave.Id);

            //~ verify that the object exists in the branch's ancestry - bypass cache to ensure most recent existing version

            if (existing == null)
            {
                throw new Exception("Unable to revise the object in this branch because the object does not exist in the branch's ancestry");
            }

            if (existing.Equals(toSave))
            {
                //~ the object tosave is unchanged since the last persisted state
                //~ don't save a new revision, instead return a null change data
                return null;
            }

            //~ perform any checks to ensure uniqueness, etc.
            _validator.ValidateBeforeUpdate(toSave, existing);

            var revisionId = _helper.SaveNewRevision(_contextId, _userId, null, comments);
            var objRevId = _helper.SaveNewObjectRevision(toSave.Id, revisionId, false);

            _helper.SaveAttributeValues(toSave, objRevId);

            var headObject = _adapter.GetValueForObjectInBranch(_contextId, toSave.Id, false);
            headObject.RevisionId = revisionId;
            headObject.ObjectRevisionId = objRevId;
            _adapter.UpdateObjectInBranchHeadObjects(headObject);

            if (_objectCache.ContainsHeadObject(toSave.Id))
            {
                _objectCache.SetHeadObject(headObject);
            }
            else
            {
                _objectCache.SetHeadObject(headObject);
            }

            _objectCache.SetObject(toSave);

            return new ChangeData(toSave.Id, revisionId, objRevId);
        }
    }
}
