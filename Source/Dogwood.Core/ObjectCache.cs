﻿using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public class ObjectCache
    {
        private readonly IDictionary<int, IDogwoodEntity> _objects;
        private readonly IDictionary<int, Dictionary<int, IDogwoodEntity>> _classSets;
        private readonly IDictionary<int, RevisionEntity> _revisions;
        private readonly IDictionary<int, BranchHeadObjectEntity> _headObjects;

        public ObjectCache()
        {
            _objects = new Dictionary<int, IDogwoodEntity>();
            _classSets = new Dictionary<int, Dictionary<int, IDogwoodEntity>>();
            _revisions = new Dictionary<int, RevisionEntity>();
            _headObjects = new Dictionary<int, BranchHeadObjectEntity>();
        }

        public void SetObject(IDogwoodEntity entity)
        {
            var metaData = MetadataRegistry.Instance[entity.GetType()];
            if (_objects.ContainsKey(entity.Id))
            {
                _objects[entity.Id] = entity.DeepClone();
            }
            else
            {
                _objects.Add(entity.Id, entity.DeepClone());
            }

            if (_classSets.ContainsKey(metaData.ClassId))
            {
                if (_classSets[metaData.ClassId].ContainsKey(entity.Id))
                {
                    _classSets[metaData.ClassId][entity.Id] = entity.DeepClone();
                }
                else
                {
                    _classSets[metaData.ClassId].Add(entity.Id, entity.DeepClone());
                }
            }
        }

        public void Remove(int id, int classId)
        {
            if (_objects.ContainsKey(id))
            {
                _objects.Remove(id);
            }

            if (_classSets.ContainsKey(classId))
            {
                if (_classSets[classId].ContainsKey(id))
                {
                    _classSets[classId].Remove(id);
                }
            }

            _headObjects.Clear();
        }

        public void AddClassSet(int id, Dictionary<int, IDogwoodEntity> classSet)
        {
            _classSets.Add(id, classSet.DeepClone());

            // add each individual item to the "AllItems" Cache
            foreach (var item in classSet)
            {
                if (!ContainsObject(item.Key))
                {
                    SetObject(item.Value);
                }
            }
        }

        public IDogwoodEntity GetObject(int id)
        {
            return _objects[id].DeepClone();
        }

        public RevisionEntity GetRevision(int id)
        {
            return _revisions[id].DeepClone();
        }

        public void AddRevision(RevisionEntity revision)
        {
            _revisions.Add(revision.Id, revision.DeepClone());
        }

        public BranchHeadObjectEntity GetHeadObject(int id)
        {
            return _headObjects[id];
        }

        public BranchHeadObjectEntity GetHeadObject(int branchId, int objectId)
        {
            return _headObjects.SingleOrDefault(m => m.Value.BranchId == branchId && m.Value.ObjectId == objectId).Value;
        }

        public void SetHeadObject(BranchHeadObjectEntity value)
        {
            if (value != null)
            {
                if (ContainsHeadObject(value.Id))
                {
                    _headObjects[value.Id] = value;
                }
                else
                {
                    _headObjects.Add(value.Id, value);
                }
            }
        }

        public Dictionary<int, IDogwoodEntity> GetClassSet(int id)
        {
            return _classSets[id].DeepClone();
        }

        public bool ContainsObject(int id)
        {
            return _objects.ContainsKey(id);
        }

        public bool ContainsRevision(int id)
        {
            return _revisions.ContainsKey(id);
        }

        public bool ContainsClassSet(int id)
        {
            return _classSets.ContainsKey(id);
        }

        public bool ContainsHeadObject(int id)
        {
            return _headObjects.ContainsKey(id);
        }

        public void Clear()
        {
            _classSets.Clear();
            _objects.Clear();
            _headObjects.Clear();            
        }
    }
}
