﻿using System.Reflection;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public class AttributePersistenceManager
    {
        private readonly IPersistenceAdapter _Adapter;

        public AttributePersistenceManager(IPersistenceAdapter adapter)
        {
            _Adapter = adapter;
        }

        private static bool AttributeMustBePersisted(IDogwoodEntity toSave, PropertyInfo info)
        {
            if (info.GetValue(toSave, null) == null) //~ nullable attributes are not persisted
                return false;

            //~ note: immutable attributes are still persisted, immutability is enforced in validation

            return true;
        }

        public void SaveAttributeValues(IDogwoodEntity toSave, int objectRevisionId)
        {
            var entityMetadata = MetadataRegistry.Instance[toSave.GetType()];

            //~ save foreign key references
            foreach (var fk in entityMetadata.ManyToOneRelationships)
            {
                var info = toSave.GetType().GetProperty(fk.Value.Property.Name);

                if (AttributeMustBePersisted(toSave, info))
                {
                    var fkAttr = new RelationAttributeValueEntity
                                                              {
                                                                  ObjectRevisionId = objectRevisionId,
                                                                  RelationshipMetadataId = fk.Value.FkAttributeMetadataId,
                                                                  Value = (int) info.GetValue(toSave, null)
                                                              };
                    _Adapter.SaveRelationAttributeValue(fkAttr);
                }
            }

            //~ save string key references
            foreach (var att in entityMetadata.Attributes)
            {
                var info = toSave.GetType().GetProperty(att.Property.Name);

                if (AttributeMustBePersisted(toSave, info))
                {
                    var strAttr = new AttributeValueEntity
                                                       {
                                                           ObjectRevisionId = objectRevisionId,
                                                           AttributeMetadataId = att.AttributeMetadataId,
                                                           Value =
                                                               DataConverter.ConvertToBytes(
                                                                   info.GetValue(toSave, null), att.DataType)
                                                       };

                    _Adapter.SaveAttributeValue(strAttr);
                }
            }            
        }
    }
}
