﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public class BranchBasedGetter<T> : IVersionedRetrieval<T> where T : class, IDogwoodEntity, new()
    {
        private readonly ObjectCache _ObjectCache;
        private readonly IEntityMetaData _EntityMetadata;
        private readonly IObjectBuilder<T> _ObjectBuilder;
        private readonly IPersistenceAdapter _Adapter;
        private readonly int _BranchId;

        public BranchBasedGetter(IDogwoodDataContext context)
        {
            if (context.ContextType != EavContextType.Branch)
            {
                throw new Exception("Branch context type expected but not provided");
            }

            _Adapter = context.PersistenceAdapter;
            _ObjectCache = context.ObjectCache;

            _ObjectBuilder = new VerboseObjectBuilder<T>(context.ObjectRevisionCache, _Adapter);

            _BranchId = context.ContextId;
            _EntityMetadata = MetadataRegistry.Instance[typeof(T)];
        }

        /// <summary>
        /// Retreives the current value of the entity of type T within the given branch.
        /// Returns NULL if the current state of the entity is "deleted".
        /// </summary>
        /// <param name="id">primary key of the entity</param>
        /// <returns>entity of type T if current value exists, or null if entity is not found in the branch or has been deleted</returns>
        public T Get(int id)
        {
            T toRet = default(T);

            // check our cache of all objects for the provided id
            if (_ObjectCache.ContainsObject(id))
            {
                toRet = (T)_ObjectCache.GetObject(id);
            }

            // if not already in the cache, fetch from the db
            if (toRet == null)
            {
                BranchHeadObjectEntity currentObjectRev;

                if (_ObjectCache.ContainsHeadObject(id))
                {
                    if (_ObjectCache.GetHeadObject(id).ObjectRevision != null)
                    {
                        currentObjectRev = _ObjectCache.GetHeadObject(id);
                    }
                    else
                    {
                        currentObjectRev = _Adapter.GetValueForObjectInBranch(_BranchId, id);
                        _ObjectCache.SetHeadObject(currentObjectRev);
                    }
                }
                else
                {
                    currentObjectRev = _Adapter.GetValueForObjectInBranch(_BranchId, id);
                    _ObjectCache.SetHeadObject(currentObjectRev);
                }

                toRet = _ObjectBuilder.BuildObject(currentObjectRev.ObjectRevision);

                // add it to the cache for future quick fetching
                _ObjectCache.SetObject(toRet);
            }

            return toRet;
        }

        /// <summary>
        /// Retreives a list of current values for all entities of type T within the given branch
        /// </summary>
        /// <returns>list of all T entities whose state is not deleted</returns>
        public List<T> GetAll()
        {
            // check the AllBranchObjects cache for the given branch id
            if (_ObjectCache.ContainsClassSet(_EntityMetadata.ClassId))
            {
                return _ObjectCache.GetClassSet(_EntityMetadata.ClassId).Select(m => m.Value).Cast<T>().ToList();
            }

            // if not already in the cache, fetch from the database
            var dt = _Adapter.GetAllDataForBranchByType(_BranchId, _EntityMetadata.ClassId);
            var toRet = _ObjectBuilder.BuildCollection(dt);

            // add the complete branch set to the cache
            _ObjectCache.AddClassSet(_EntityMetadata.ClassId, toRet.Cast<IDogwoodEntity>().ToDictionary(m => m.Id));

            return toRet;
        }

        public List<T> GetAll(IList<int> idList)
        {
            var toRet = new List<T>();

            for (int i = idList.Count - 1; i >= 0; i--)
            {
                if (_ObjectCache.ContainsObject(idList[i]))
                {
                    toRet.Add((T)_ObjectCache.GetObject(idList[i]));
                    idList.Remove(idList[i]);
                }
            }

            if (idList.Count != 0)
            {
                var dt = _Adapter.GetAllByTypeWithId(_BranchId, idList);
                toRet.AddRange(_ObjectBuilder.BuildCollection(dt));

                foreach (var item in toRet)
                {
                    if (!_ObjectCache.ContainsObject(item.Id))
                    {
                        _ObjectCache.SetObject(item);
                    }
                }
            }

            return toRet;
        }

        /// <summary>
        /// Retreives a list of values for a specific object representing its state as it has changed over time
        /// </summary>
        /// <returns>list of T entities, each representing the same object's state over time</returns>
        public IEnumerable<ObjectVersion<T>> GetObjectHistory(int objectId, int contextId)
        {
            var objectRevisions = _Adapter.GetAllRevisionsForObject(objectId, contextId);

            return objectRevisions.Select(objectRevisionEntity => new ObjectVersion<T>
            {
                BranchId = objectRevisionEntity.Revision.BranchId, 
                EntityState = _ObjectBuilder.BuildObject(objectRevisionEntity), 
                IsDeleted = objectRevisionEntity.IsDeleted, 
                RevisionComments = objectRevisionEntity.Revision.Comments, 
                RevisionId = objectRevisionEntity.RevisionId, 
                TimeStamp = objectRevisionEntity.Revision.TimeStamp
            }).ToList();
        }

        public List<T> GetAll(IPredicateBucket filterBucket)
        {
            if (filterBucket == null)
            {
                throw new NullReferenceException("FilterBucket may not be null");
            }

            var objectRevisions = _Adapter.GetBranchHeadObjectsWithFilter(filterBucket);
            var factory = new EavFactory<T>();
            return factory.CreateMulti(objectRevisions).ToList();
        }
    }
}
