﻿namespace Dogwood.Core
{
    public enum CascadeBehaviorType
    {
        Delete = 1,
        SetToNull = 2,
        SetToDefault = 3
    }

    public enum DogwoodDataType
    {
        String = 1,
        Int = 2,
        Long = 3,
        Bool = 4,
        DateTime = 5,
    }

    public enum ComparisonOperator
    {
        Equal,
        NotEqual,
        LessThan,
        GreaterThan,
        LessThanOrEqualTo,
        GreaterThanOrEqualTo
    }

    public enum LikeComparisonOperator
    {
        BeginsWith,
        Contains,
        EndsWith
    }

}
