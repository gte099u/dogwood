﻿using System;
using System.Linq;

namespace Dogwood.Core
{
    public static class TypeExtensions
    {
        public static DogwoodDataType ToDogwoodDataType(this Type type)
        {
            if (type == typeof(string)) return DogwoodDataType.String;

            if (type == typeof(int) || type == typeof(int?)) return DogwoodDataType.Int;

            if (type == typeof(long) || type == typeof(long?)) return DogwoodDataType.Long;

            if (type == typeof(bool) || type == typeof(bool?)) return DogwoodDataType.Bool;

            if (type == typeof(DateTime) || type == typeof(DateTime?)) return DogwoodDataType.DateTime;

            throw new Exception("Type does not map to a Dogwood Data Type");
        }

        public static bool IsAvailableInDogwood(this Type type)
        {
            return type == typeof(string) ||
               type == typeof(int) || type == typeof(int?) ||
               type == typeof(long) || type == typeof(long?) ||
               type == typeof(bool) || type == typeof(bool?) ||
               type == typeof(DateTime) || type == typeof(DateTime?);
        }

        public static bool SupportsInterface(this Type type, Type interfaceType)
        {
            if (interfaceType.IsAssignableFrom(type))
                return true;
            if (type.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == interfaceType))
                return true;
            if (type.IsGenericType && type.GetGenericTypeDefinition() == interfaceType)
                return true;
            return false;
        }
    }
}
