﻿using System;
using System.Collections.Generic;
using Dogwood.Core.Entities;
using System.Data;

namespace Dogwood.Core
{
    public interface IPersistenceAdapter : IDisposable
    {
        /// <summary>
        /// SELECT * FROM tblObjectRevision WHERE ObjectId = objectId
        /// Prefetches all EAV tables and related Revision entity
        /// </summary>
        /// <param name="objectId"></param>
        /// <param name="branchId"> </param>
        /// <returns></returns>
        IEnumerable<ObjectRevisionEntity> GetAllRevisionsForObject(int objectId, int branchId);
        IList<ObjectRevisionEntity> GetObjectRevisionsEager(IEnumerable<int> idList, IEntityMetaData entityMetadata);

        /// <summary>
        /// Translates the provided IPredicateBucket into implementation-specific (ORM) predicates and fetches a filtered list of
        /// ObjectRevision entities with AttributeValues and RelationValues prefetched.
        /// </summary>
        /// <param name="filterBucket"></param>
        /// <returns></returns>
        IEnumerable<ObjectRevisionEntity> GetBranchHeadObjectsWithFilter(IPredicateBucket filterBucket);

        /// <summary>
        /// Translates the provided IPredicateBucket into implementation-specific (ORM) predicates and fetches a filtered list of
        /// ObjectRevision entities with AttributeValues and RelationValues prefetched.
        /// </summary>
        /// <param name="filterBucket"></param>
        /// <returns></returns>
        IEnumerable<ObjectRevisionEntity> GetRevisionHeadObjectsWithFilter(IPredicateBucket filterBucket);

        /// <summary>
        /// SELECT * FROM tblClass
        /// </summary>
        /// <returns></returns>
        IEnumerable<ClassEntity> GetAllClasses();

        /// <summary>
        /// SELECT * FROM tblStringAttribute
        /// </summary>
        /// <returns></returns>
        IEnumerable<AttributeEntity> GetAllAttributes();

        IEnumerable<RelationshipMetadataEntity> GetAllRelationshipMetadata();
        IEnumerable<AttributeMetadataEntity> GetAllAttributeMetadata();

        /// <summary>
        /// SELECT * FROM tblRevision WHERE Id = id
        /// </summary>
        /// <returns></returns>
        RevisionEntity GetRevision(int id);

        /// <summary>
        /// SELECT * FROM tblRevision WHERE BranchId = branchId
        /// </summary>
        /// <returns></returns>
        IEnumerable<RevisionEntity> GetAllRevisionsByBranchId(int branchId);

        /// <summary>
        /// SELECT * FROM tblRevision WHERE BranchId = branchId AND Tag = tag
        /// </summary>
        /// <returns></returns>
        IEnumerable<RevisionEntity> GetAllRevisionsHavingTag(int branchId, string tag);

        /// <summary>
        /// Returns a list of RevisionEntities representing revisions that do not have any associated object revision records
        /// </summary>
        /// <returns></returns>
        IEnumerable<RevisionEntity> GetAllOrphanRevisions();

        void StartTransaction(IsolationLevel isolationLevel, string name);
        void Commit();
        void Rollback();
        bool IsTransactionInProgress { get; }

        void SaveRevision(RevisionEntity toSave);
        void SaveObject(ObjectEntity toSave);
        void SaveObjectRevision(ObjectRevisionEntity toSave);
        void DeleteRevision(RevisionEntity toDelete);
        void DeleteObject(int toDeleteId);
        int DeleteAllObjectRevisionsHavingObjectId(int objectId);

        bool RemoveObjectFromBranchHeadObjects(int objectId, int branchId);
        bool RemoveObjectFromBranchHeadObjects(int objectId);
        bool UpdateObjectInBranchHeadObjects(BranchHeadObjectEntity toSave);
        bool AddObjectToBranchHeadObjects(BranchHeadObjectEntity toSave);

        /// <summary>
        /// Repopulates the HeadObjects table with head object data for all branches
        /// </summary>
        void FillHeadObjects();

        /// <summary>
        /// Inserts records for the given root object into the HeadObjects tables (Branch and Revision) for all branches and revisions
        /// </summary>
        void AddRootObjectToHeadObjects(int classId, int objectId, int objectRevisionId);
        /// <summary>
        /// Removes all records from BranchHeadObjects and RevisionHeadObjects where ObjectId = objectId
        /// </summary>
        void RemoveRootObjectFromHeadObjects(int objectId);

        /// <summary>
        /// Indexes all data as of the given revision if the revision has not alredy been indexed in the head objects table
        /// </summary>
        /// <param name="branchId"></param>
        /// <param name="revisionId"></param>
        void AddRevisionToHeadObjects(int branchId, int revisionId);

        BranchHeadObjectEntity GetValueForObjectInBranch(int branchId, int objectId, bool prefetchValues = true);
        BranchHeadObjectEntity GetValueForObjectAtRevision(int branchId, int objectId, int revisionId);
       
        IEnumerable<ObjectRevisionEntity> GetAllDataForBranchByType(int branchId, int classId);
        IEnumerable<ObjectRevisionEntity> GetAllByTypeWithFkRelation(int branchId, int classId, int fkAttributeId, IEnumerable<int> fkList);
        IEnumerable<ObjectRevisionEntity> GetAllByTypeWithId(int branchId, IEnumerable<int> idList);

        DataTable GetAllRelatedToObject(int branchId, int objectId);


        IEnumerable<ObjectRevisionEntity> GetAllDataForRevisionByType(int branchId, int revisionId, int classId);
        IEnumerable<ObjectRevisionEntity> GetAllForRevisionWithFkRelation(int branchId, int revisionId, int classId, int fkAttributeId, IEnumerable<int> fkList);
        IEnumerable<ObjectRevisionEntity> GetAllAtRevisionWithId(int branchId, int revisionId, IEnumerable<int> idList);

        ObjectEntity GetObject(int id);

        IList<RelationAttributeValueEntity> GetAllForeignKeyReferencesTo(int objectId);

        void SaveRelationAttributeValue(RelationAttributeValueEntity toSave);
        void SaveAttributeValue(AttributeValueEntity toSave);

        ObjectRevisionEntity GetObjectRevisionEager(int id, IEntityMetaData entityMetadata);
        
        ObjectRevisionEntity GetObjectRevisionEagerAtRoot(int id);

        void DeleteAttributeValues(IEnumerable<AttributeValueEntity> toDelete);
        void DeleteRelationAttributeValues(IEnumerable<RelationAttributeValueEntity> toDelete);
    }
}
