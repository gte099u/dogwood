﻿namespace Dogwood.Core
{
    public interface IDogwoodEntity
    {
        int Id { get; set; }
        bool IsNew { get; }
    }
}
