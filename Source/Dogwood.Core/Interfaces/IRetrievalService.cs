﻿using System.Collections.Generic;

namespace Dogwood.Core
{
    public interface IRetrievalService
    {
        IDogwoodEntity GetAsIDogwoodEntity(int id, PrefetchPath prefetchPath);
        IList<IDogwoodEntity> GetAllAsIDogwoodEntity(PrefetchPath prefetchPath, IPredicateBucket filterBucket);
        IList<IDogwoodEntity> GetAllAsIDogwoodEntity(PrefetchPath prefetchPath, IList<int> idFilter);
        IEnumerable<IDogwoodEntity> GetAllRelatedObjects(int id);
    }
}
