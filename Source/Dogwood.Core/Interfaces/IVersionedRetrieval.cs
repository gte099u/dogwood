﻿

using System.Collections.Generic;
namespace Dogwood.Core
{
    public interface IVersionedRetrieval<T> where T : IDogwoodEntity
    {
        T Get(int id);
        List<T> GetAll();
        List<T> GetAll(IPredicateBucket filterBucket);
        List<T> GetAll(IList<int> idList);
        IEnumerable<ObjectVersion<T>> GetObjectHistory(int objectId, int contextId);
    }
}
