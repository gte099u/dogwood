﻿namespace Dogwood.Core
{
    public interface IEntityValidation<T> where T : IDogwoodEntity
    {
        void ValidateBeforeCreate(T toSave);
        void ValidateBeforeUpdate(T toSave, T existing);
    }
}
