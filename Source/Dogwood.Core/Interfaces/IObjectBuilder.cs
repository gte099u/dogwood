﻿using System.Collections.Generic;
using System.Data;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    internal interface IObjectBuilder<T> where T : IDogwoodEntity
    {
        T BuildObject(DataTable objRevTable);
        T BuildObject(ObjectRevisionEntity headObject);
        List<T> BuildCollection(DataTable objRevTable);
        List<T> BuildCollection(IEnumerable<ObjectRevisionEntity> branchHeadObjects);
    }
}
