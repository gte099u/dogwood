﻿using System.Collections.Generic;

namespace Dogwood.Core
{
    public interface IPredicateBucket
    {
        IEnumerable<RelationPredicate> GetRelationPredicates();
        IEnumerable<AttributeComparePredicate> GetAttributeComparePredicates();
        IEnumerable<AttributeLikePredicate> GetAttributeLikePredicates();
        IEnumerable<RelationInPredicate> GetRelationInPredicates();

        void AddRelationInPredicate(RelationInPredicate toAdd);
        void AddRelationPredicate(RelationPredicate toAdd);

        int ContextId { get; }
        int ClassId { get; }
    }
}
