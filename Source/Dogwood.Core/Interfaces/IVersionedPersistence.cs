﻿namespace Dogwood.Core
{
    public interface IVersionedPersistence<T> where T : IDogwoodEntity
    {
        ChangeData Save(T toSave);
        ChangeData Save(T toSave, string comments);
        ChangeData Save(T toSave, string comments, bool openNewTransaction);
        bool Delete(int toDeleteId);
        bool Delete(int toDeleteId, string comments);
        bool Delete(int toDeleteId, string comments, bool openNewTransaction);
        bool DeleteWithinBatch(int toDeleteId, int revisionId);
        bool DestroyObject(int toDestroyId);
        int CreateRevision();
        int CreateRevision(string tag, string comments);
        ChangeData SaveWithinBatch(T toSave, int revisionId);
    }
}
