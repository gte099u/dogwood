﻿using System.Collections.Generic;
using System.Reflection;

namespace Dogwood.Core
{
    public interface IEntityMetaData
    {
        int ClassId { get; set; }
        List<UniqueConstraint> UniqueConstraints { get; }
        List<MemberMap> Attributes { get; }
        Dictionary<string, OneToManyMap> OneToManyRelationships { get; }
        Dictionary<string, ManyToOneMap> ManyToOneRelationships { get; }

        int GetMetadataId(int attributeId);
        int GetFkMetadataId(int pkClassId, int fkAttributeId);
        void AddAttribute(PropertyInfo property);
    }
}
