﻿using System.Reflection;

namespace Dogwood.Core
{
    public interface IRelationshipMap
    {
        int ClassId { get; set; }
        PropertyInfo PrimaryKey { get; set; }
        PropertyInfo RelationProperty { get; set; }
    }
}
