﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Data;

namespace Dogwood.Core
{
    public interface IObjectRepository<T> where T : IDogwoodEntity
    {
        bool IsTransactionInProgress { get; }
        void StartTransaction(IsolationLevel isolationLevel, string name);
        void Commit();
        void Rollback();

        IDogwoodDataContext Context { get; set; }
        T Get(int id);
        IList<T> GetAll();
        T Get(int id, PrefetchPath prefetchPath);
        IList<T> GetAll(PrefetchPath prefetchPath);
        IList<T> GetAll(IPredicateBucket filterBucket);
        IList<T> GetAll(IPredicateBucket filterBucket, IEnumerable<PrefetchElement> prefetchPath);
        IEnumerable<ObjectVersion<T>> GetObjectHistory(int id);

        /// <summary>
        /// Persists the provided object
        /// </summary>
        /// <param name="toSave">object of type T to save</param>
        /// <returns>ChangeData describing the details of the persistence revision.  In "Lean" persistence this
        /// value will be null if the object to save was unchanged since the last revision, and therefore was not persisted</returns>
        ChangeData Save(T toSave);
        ChangeData Save(T toSave, string comments);
        ChangeData Save(T toSave, string comments, bool openNewTransaction);
        ChangeData SaveWithinBatch(T toSave, int revisionId);
        bool Delete(int toDeleteId);
        bool Delete(int toDeleteId, string comments);
        bool Delete(int toDeleteId, string comments, bool openNewTransaction);
        bool DeleteWithinBatch(int toDeleteId, int revisionId);
        bool SaveMulti(IList<T> toSave);
        bool SaveMulti(IList<T> toSave, string comments);
        int CreateRevision(string tag, string comments);
        PredicateBucket<T> CreateNewPredicateBucket();
        bool ExecuteWithinTransaction(Expression<Func<bool>> cmd);
    }
}
