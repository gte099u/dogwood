﻿namespace Dogwood.Core
{
    public interface IMemberMap
    {
        bool IsImmutable { get; set; }
        bool IsNullable { get; set; }
    }
}
