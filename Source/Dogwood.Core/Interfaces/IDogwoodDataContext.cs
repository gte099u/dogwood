﻿using System.Collections.Generic;
using Dogwood.Core.Entities;

namespace Dogwood.Core
{
    public interface IDogwoodDataContext
    {
        IPersistenceAdapter PersistenceAdapter { get; set; }
        IDictionary<int, ObjectRevisionEntity> ObjectRevisionCache { get; set; }
        ObjectCache ObjectCache { get; }
        int RequestingUserId { get; }
        int ContextId { get; }
        EavContextType ContextType { get; set; }
    }

    public enum EavContextType
    {
        Branch,
        Revision
    }
}
