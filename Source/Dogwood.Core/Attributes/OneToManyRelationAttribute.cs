﻿using System;

namespace Dogwood.Core
{   
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class OneToManyRelationAttribute : Attribute
    {
        public int RelatedClassId { get; private set; }
        public int FkAttributeId { get; private set; }

        public OneToManyRelationAttribute() { }

        public OneToManyRelationAttribute(int relatedClassId, int fkAttributeId)
        {
            RelatedClassId = relatedClassId;
            FkAttributeId = fkAttributeId;
        }
    }
}
