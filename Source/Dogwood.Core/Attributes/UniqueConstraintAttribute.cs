﻿using System;

namespace Dogwood.Core
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class UniqueConstraintAttribute : Attribute
    {
        public string[] UniqueComboFields { get; private set; }
        public string ExceptionMessage { get; set; }

        public UniqueConstraintAttribute(string exceptionMessage, params string[] uniqueFields)
        {
            if(uniqueFields.Length == 0)
            {
                throw new Exception("Unique constraints must have at least one field defined");
            }

            UniqueComboFields = uniqueFields;
            ExceptionMessage = exceptionMessage;
        }
    }
}
