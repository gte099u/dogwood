﻿using System;

namespace Dogwood.Core
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class MappableAttribute : Attribute { }
}
