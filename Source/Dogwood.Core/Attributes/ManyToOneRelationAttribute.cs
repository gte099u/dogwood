﻿using System;

namespace Dogwood.Core
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class ManyToOneRelationAttribute : Attribute
    {
        public int RelatedClassId { get; private set; }

        public ManyToOneRelationAttribute() { }

        public ManyToOneRelationAttribute(int relatedClassId)
        {
            RelatedClassId = relatedClassId;
        }
    }
}
