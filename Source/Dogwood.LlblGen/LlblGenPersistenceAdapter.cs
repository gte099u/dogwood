﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dogwood.Core;
using Dogwood.Data.DatabaseSpecific;
using Dogwood.Data.HelperClasses;
using Dogwood.Data.TypedViewClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using AutoMapper;
using Dogwood.Data;

namespace Dogwood.LlblGen
{
    public class LlblGenPersistenceAdapter : IPersistenceAdapter
    {
        public readonly IDataAccessAdapter _Adapter;

        public LlblGenPersistenceAdapter()
        {
            _Adapter = new DataAccessAdapter(true);
        }

        public void Dispose() { _Adapter.Dispose(); }

        public void Commit() { _Adapter.Commit(); }

        public void Rollback() { _Adapter.Rollback(); }

        public void StartTransaction(IsolationLevel isolationLevel, string name)
        {
            _Adapter.StartTransaction(isolationLevel, name);
        }

        public bool IsTransactionInProgress
        {
            get { return _Adapter.IsTransactionInProgress; }
        }

        public IEnumerable<Core.Entities.ObjectRevisionEntity> GetAllRevisionsForObject(int objectId, int branchId)
        {
            var result = RetrievalProcedures.GetObjectHistory(objectId, branchId);

            var objRevIds = new List<int>();

            for (int i = 0; i < result.Rows.Count; i++)
            {
                objRevIds.Add((int)result.Rows[i][0]);
            }

            var prefetchPath = PrefetchConstants.EavPrefetchGraph;
            prefetchPath.Add(Data.EntityClasses.ObjectRevisionEntity.PrefetchPathRevision);

            IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ObjectRevisionFields.Id == objRevIds.ToArray());

            var objectRevisions = new EntityCollection<Data.EntityClasses.ObjectRevisionEntity>();
            _Adapter.FetchEntityCollection(objectRevisions, filterBucket, prefetchPath);

            return objectRevisions.Select(Mapper.Map<Data.EntityClasses.ObjectRevisionEntity, Core.Entities.ObjectRevisionEntity>).ToList();            
        }

        public IEnumerable<Core.Entities.ClassEntity> GetAllClasses()
        {
            var classes = new EntityCollection<Data.EntityClasses.ClassEntity>();
            _Adapter.FetchEntityCollection(classes, null);

            return classes.Select(Mapper.Map<Data.EntityClasses.ClassEntity, Core.Entities.ClassEntity>).ToList();
        }

        public IEnumerable<Core.Entities.AttributeEntity> GetAllAttributes()
        {
            var classes = new EntityCollection<Data.EntityClasses.AttributeEntity>();
            _Adapter.FetchEntityCollection(classes, null);

            return classes.Select(Mapper.Map<Data.EntityClasses.AttributeEntity, Core.Entities.AttributeEntity>).ToList();
        }

        public IEnumerable<Core.Entities.RelationshipMetadataEntity> GetAllRelationshipMetadata()
        {
            var toRet = new EntityCollection<Data.EntityClasses.RelationshipMetadataEntity>();
            _Adapter.FetchEntityCollection(toRet, null);

            return toRet.Select(Mapper.Map<Data.EntityClasses.RelationshipMetadataEntity, Core.Entities.RelationshipMetadataEntity>).ToList();
        }

        public IEnumerable<Core.Entities.AttributeMetadataEntity> GetAllAttributeMetadata()
        {
            var toRet = new EntityCollection<Data.EntityClasses.AttributeMetadataEntity>();
            _Adapter.FetchEntityCollection(toRet, null);

            return toRet.Select(Mapper.Map<Data.EntityClasses.AttributeMetadataEntity, Core.Entities.AttributeMetadataEntity>).ToList();
        }

        public IEnumerable<Core.Entities.RevisionEntity> GetAllRevisionsByBranchId(int branchId)
        {
            var branches = new EntityCollection<Data.EntityClasses.RevisionEntity>();

            IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(RevisionFields.BranchId == branchId);

            _Adapter.FetchEntityCollection(branches, filterBucket);

            return branches.Select(Mapper.Map<Data.EntityClasses.RevisionEntity, Core.Entities.RevisionEntity>).ToList();
        }

        public IEnumerable<Core.Entities.RevisionEntity> GetAllRevisionsHavingTag(int branchId, string tag)
        {
            var revisions = new EntityCollection<Data.EntityClasses.RevisionEntity>();

            IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.AddWithAnd(RevisionFields.BranchId == branchId);
            filterBucket.PredicateExpression.AddWithAnd(RevisionFields.Tag == tag);

            _Adapter.FetchEntityCollection(revisions, filterBucket);

            return revisions.Select(Mapper.Map<Data.EntityClasses.RevisionEntity, Core.Entities.RevisionEntity>).ToList();
        }

        public IEnumerable<Core.Entities.RevisionEntity> GetAllOrphanRevisions()
        {
            IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
            IGroupByCollection groupCol = new GroupByCollection();
            groupCol.Add(RevisionFields.BranchId);
            IPredicate branchFilter = new FieldCompareSetPredicate(RevisionFields.Id, null, RevisionFields.BranchId, null, SetOperator.In, null, null, null, 0, null, true, groupCol);

            filterBucket.PredicateExpression.AddWithAnd(branchFilter);

            filterBucket.PredicateExpression.AddWithAnd(new FieldCompareNullPredicate(ObjectRevisionFields.Id, null));
            filterBucket.PredicateExpression.AddWithAnd(new FieldCompareNullPredicate(RevisionFields.Tag, null));
            filterBucket.PredicateExpression.AddWithAnd(new FieldCompareNullPredicate(RevisionFields.Comments, null));

            filterBucket.Relations.Add(Data.EntityClasses.RevisionEntity.Relations.ObjectRevisionEntityUsingRevisionId, JoinHint.Left);

            var orphanRevisions = new EntityCollection<Data.EntityClasses.RevisionEntity>();
            _Adapter.FetchEntityCollection(orphanRevisions, filterBucket);

            return orphanRevisions.Select(Mapper.Map<Data.EntityClasses.RevisionEntity, Core.Entities.RevisionEntity>).ToList();
        }

        public IList<Core.Entities.RelationAttributeValueEntity> GetAllForeignKeyReferencesTo(int objectId)
        {
            IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.RelationAttributeValueEntity);
            prefetchPath.Add(Data.EntityClasses.RelationAttributeValueEntity.PrefetchPathObjectRevision);

            var references = new EntityCollection<Data.EntityClasses.RelationAttributeValueEntity>();
            IRelationPredicateBucket filterBucket = new RelationPredicateBucket(RelationAttributeValueFields.Value == objectId);
            _Adapter.FetchEntityCollection(references, filterBucket, prefetchPath);

            return references.Select(Mapper.Map<Data.EntityClasses.RelationAttributeValueEntity, Core.Entities.RelationAttributeValueEntity>).ToList();
        }

        public IList<Core.Entities.ObjectRevisionEntity> GetObjectRevisionsEager(IEnumerable<int> idList, IEntityMetaData entityMetadata)
        {
            var objectVersions = new EntityCollection<Data.EntityClasses.ObjectRevisionEntity>();

            var prefetchPath = LeanPrefetchFactory.LeanPrefetchPath(entityMetadata);

            IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ObjectRevisionFields.Id == idList.ToArray());
            _Adapter.FetchEntityCollection(objectVersions, filterBucket, prefetchPath);

            return objectVersions.Select(Mapper.Map<Data.EntityClasses.ObjectRevisionEntity, Core.Entities.ObjectRevisionEntity>).ToList();
        }

        public Core.Entities.RevisionEntity GetRevision(int id)
        {
            var toRet = new Data.EntityClasses.RevisionEntity(id);
            _Adapter.FetchEntity(toRet);

            return Mapper.Map<Data.EntityClasses.RevisionEntity, Core.Entities.RevisionEntity>(toRet);
        }

        public Core.Entities.ObjectEntity GetObject(int id)
        {
            var obj = new Data.EntityClasses.ObjectEntity(id);
            _Adapter.FetchEntity(obj);
            return Mapper.Map<Data.EntityClasses.ObjectEntity, Core.Entities.ObjectEntity>(obj);
        }

        public Core.Entities.ObjectRevisionEntity GetObjectRevisionEager(int id, IEntityMetaData entityMetadata)
        {
            var objRev = new Data.EntityClasses.ObjectRevisionEntity(id);
            var prefetchPath = LeanPrefetchFactory.LeanPrefetchPath(entityMetadata);
            _Adapter.FetchEntity(objRev, prefetchPath);

            return Mapper.Map<Data.EntityClasses.ObjectRevisionEntity, Core.Entities.ObjectRevisionEntity>(objRev);
        }

        public Core.Entities.ObjectRevisionEntity GetObjectRevisionEagerAtRoot(int id)
        {
            var prefetchPath = PrefetchConstants.EavPrefetchGraph;
            prefetchPath.Add(Data.EntityClasses.ObjectRevisionEntity.PrefetchPathRevision);

            var objRev = new Data.EntityClasses.ObjectRevisionEntity { ObjectId = id, RevisionId = 0 };
            _Adapter.FetchEntityUsingUniqueConstraint(objRev, objRev.ConstructFilterForUCObjectIdRevisionId(), prefetchPath);

            return Mapper.Map<Data.EntityClasses.ObjectRevisionEntity, Core.Entities.ObjectRevisionEntity>(objRev);
        }

        public void SaveRevision(Core.Entities.RevisionEntity toSave)
        {
            var rev = Mapper.Map<Core.Entities.RevisionEntity, Data.EntityClasses.RevisionEntity>(toSave);
            _Adapter.SaveEntity(rev);
            toSave.Id = rev.Id;
        }

        public void SaveRelationAttributeValue(Core.Entities.RelationAttributeValueEntity toSave)
        {
            var a = Mapper.Map<Core.Entities.RelationAttributeValueEntity, Data.EntityClasses.RelationAttributeValueEntity>(toSave);
            _Adapter.SaveEntity(a);
            toSave.Id = a.Id;
        }

        public void SaveObject(Core.Entities.ObjectEntity toSave)
        {
            var a = Mapper.Map<Core.Entities.ObjectEntity, Data.EntityClasses.ObjectEntity>(toSave);
            _Adapter.SaveEntity(a);
            toSave.Id = a.Id;
        }

        public void SaveAttributeValue(Core.Entities.AttributeValueEntity toSave)
        {
            var a = Mapper.Map<Core.Entities.AttributeValueEntity, Data.EntityClasses.AttributeValueEntity>(toSave);
            _Adapter.SaveEntity(a);
            toSave.Id = a.Id;
        }

        public void SaveObjectRevision(Core.Entities.ObjectRevisionEntity toSave)
        {
            var a = Mapper.Map<Core.Entities.ObjectRevisionEntity, Data.EntityClasses.ObjectRevisionEntity>(toSave);
            _Adapter.SaveEntity(a);
            toSave.Id = a.Id;
        }

        public void DeleteRevision(Core.Entities.RevisionEntity toDelete)
        {
            var rev = new Data.EntityClasses.RevisionEntity { Id = toDelete.Id };
            _Adapter.DeleteEntity(rev);
        }

        public int DeleteAllObjectRevisionsHavingObjectId(int objectId)
        {
            IRelationPredicateBucket objReviFilter = new RelationPredicateBucket();
            objReviFilter.PredicateExpression.Add(ObjectRevisionFields.ObjectId == objectId);
            return _Adapter.DeleteEntitiesDirectly(typeof(Data.EntityClasses.ObjectRevisionEntity), objReviFilter);
        }

        public void DeleteObject(int toDeleteId)
        {
            var toDelete = new Data.EntityClasses.ObjectEntity(toDeleteId);
            _Adapter.DeleteEntity(toDelete);
        }

        public void DeleteAttributeValues(IEnumerable<Core.Entities.AttributeValueEntity> toDelete)
        {
            foreach (var attributeValueEntity in toDelete)
            {
                _Adapter.DeleteEntity(new Data.EntityClasses.AttributeValueEntity(attributeValueEntity.Id));
            }
        }

        public void DeleteRelationAttributeValues(IEnumerable<Core.Entities.RelationAttributeValueEntity> toDelete)
        {
            foreach (var relationAttributeValueEntity in toDelete)
            {
                _Adapter.DeleteEntity(new Data.EntityClasses.RelationAttributeValueEntity(relationAttributeValueEntity.Id));
            }
        }

        public bool RemoveObjectFromBranchHeadObjects(int objectId, int branchId)
        {
            var toRet = new Data.EntityClasses.BranchHeadObjectEntity();

            var predicateExpression = new PredicateExpression { BranchHeadObjectFields.BranchId == branchId };
            predicateExpression.AddWithAnd(BranchHeadObjectFields.ObjectId == objectId);

            _Adapter.FetchEntityUsingUniqueConstraint(toRet, predicateExpression);

            var success = _Adapter.DeleteEntity(toRet);

            if (!success)
            {
                throw new Exception("The object to be deleted was not found in the branch head objects table");
            }

            return true;
        }

        public bool RemoveObjectFromBranchHeadObjects(int objectId)
        {
            var filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(BranchHeadObjectFields.ObjectId == objectId);

            _Adapter.DeleteEntitiesDirectly(typeof(Data.EntityClasses.BranchHeadObjectEntity), filterBucket);

            return true;
        }

        public bool UpdateObjectInBranchHeadObjects(Core.Entities.BranchHeadObjectEntity toSave)
        {
            var toUpdate =
                Mapper.Map<Core.Entities.BranchHeadObjectEntity, Data.EntityClasses.BranchHeadObjectEntity>(toSave);

            _Adapter.SaveEntity(toUpdate);

            return true;
        }

        public bool AddObjectToBranchHeadObjects(Core.Entities.BranchHeadObjectEntity toSave)
        {
            var toUpdate =
                Mapper.Map<Core.Entities.BranchHeadObjectEntity, Data.EntityClasses.BranchHeadObjectEntity>(toSave);

            _Adapter.SaveEntity(toUpdate);

            return true;
        }

        public Core.Entities.BranchHeadObjectEntity GetValueForObjectInBranch(int branchId, int objectId, bool prefetchValues = true)
        {
            var toRet = new Data.EntityClasses.BranchHeadObjectEntity();

            var predicateExpression = new PredicateExpression { BranchHeadObjectFields.BranchId == branchId, BranchHeadObjectFields.ObjectId == objectId };

            if (prefetchValues)
            {
                var prefetchPath = new PrefetchPath2(EntityType.BranchHeadObjectEntity);
                var objRevElement = prefetchPath.Add(Data.EntityClasses.BranchHeadObjectEntity.PrefetchPathObjectRevision);
                objRevElement.SubPath.Add(Data.EntityClasses.ObjectRevisionEntity.PrefetchPathAttributeValue);
                objRevElement.SubPath.Add(Data.EntityClasses.ObjectRevisionEntity.PrefetchPathRelationAttributeValue);
                _Adapter.FetchEntityUsingUniqueConstraint(toRet, predicateExpression, prefetchPath);
            }
            else
            {
                _Adapter.FetchEntityUsingUniqueConstraint(toRet, predicateExpression);
            }

            return Mapper.Map<Data.EntityClasses.BranchHeadObjectEntity, Core.Entities.BranchHeadObjectEntity>(toRet);
        }

        public IEnumerable<Core.Entities.ObjectRevisionEntity> GetAllDataForBranchByType(int branchId, int classId)
        {
            var filterBucket = new RelationPredicateBucket();

            filterBucket.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.BranchHeadObjectEntityUsingObjectRevisionId);

            filterBucket.PredicateExpression.Add(BranchHeadObjectFields.BranchId == branchId);
            filterBucket.PredicateExpression.AddWithAnd(BranchHeadObjectFields.ClassId == classId);

            return FetchHeadObjects(filterBucket);
        }

        public IEnumerable<Core.Entities.ObjectRevisionEntity> GetAllByTypeWithFkRelation(int branchId, int classId, int fkAttributeId, IEnumerable<int> fkList)
        {
            var filterBucket = new RelationPredicateBucket();

            filterBucket.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.BranchHeadObjectEntityUsingObjectRevisionId);
            filterBucket.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.RelationAttributeValueEntityUsingObjectRevisionId);
            filterBucket.Relations.Add(Data.EntityClasses.RelationAttributeValueEntity.Relations.RelationshipMetadataEntityUsingRelationshipMetadataId);

            filterBucket.PredicateExpression.Add(BranchHeadObjectFields.BranchId == branchId);
            filterBucket.PredicateExpression.AddWithAnd(BranchHeadObjectFields.ClassId == classId);
            filterBucket.PredicateExpression.AddWithAnd(RelationshipMetadataFields.AttributeId == fkAttributeId);
            filterBucket.PredicateExpression.AddWithAnd(new FieldCompareRangePredicate(RelationAttributeValueFields.Value, null, fkList));

            return FetchHeadObjects(filterBucket);
        }

        public IEnumerable<Core.Entities.ObjectRevisionEntity> GetBranchHeadObjectsWithFilter(IPredicateBucket filterBucket)
        {
            var objectRevisions = new EntityCollection<Data.EntityClasses.ObjectRevisionEntity>();

            _Adapter.FetchEntityCollection(objectRevisions, PredicateTranslator.Convert(filterBucket, EavContextType.Branch), PrefetchConstants.EavPrefetchGraph);

            return objectRevisions.Select(Mapper.Map<Data.EntityClasses.ObjectRevisionEntity, Core.Entities.ObjectRevisionEntity>).ToList();
        }

        public IEnumerable<Core.Entities.ObjectRevisionEntity> GetRevisionHeadObjectsWithFilter(IPredicateBucket filterBucket)
        {
            var objectRevisions = new EntityCollection<Data.EntityClasses.ObjectRevisionEntity>();

            _Adapter.FetchEntityCollection(objectRevisions, PredicateTranslator.Convert(filterBucket, EavContextType.Revision), PrefetchConstants.EavPrefetchGraph);

            return objectRevisions.Select(Mapper.Map<Data.EntityClasses.ObjectRevisionEntity, Core.Entities.ObjectRevisionEntity>).ToList();
        }

        public IEnumerable<Core.Entities.ObjectRevisionEntity> GetAllByTypeWithId(int branchId, IEnumerable<int> idList)
        {
            idList = idList.ToList(); //~ enumerate into list

            var toRet = new List<Core.Entities.ObjectRevisionEntity>();

            while (idList.Count() > 2000) //~ break into batches of 2000 to avoid exceeding sql "where in" clause capacity
            {
                toRet.AddRange(GetAllByTypeWithId(branchId, idList.Take(2000)));
                idList = idList.Skip(2000).ToList();
            }

            var filterBucket = new RelationPredicateBucket();

            filterBucket.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.BranchHeadObjectEntityUsingObjectRevisionId);

            filterBucket.PredicateExpression.Add(BranchHeadObjectFields.BranchId == branchId);
            filterBucket.PredicateExpression.AddWithAnd(new FieldCompareRangePredicate(BranchHeadObjectFields.ObjectId, null, idList));

            toRet.AddRange(FetchHeadObjects(filterBucket));

            return toRet;
        }

        private IEnumerable<Core.Entities.ObjectRevisionEntity> FetchHeadObjects(IRelationPredicateBucket filterBucket)
        {
            var toRet = new EntityCollection<Data.EntityClasses.ObjectRevisionEntity>();

            _Adapter.FetchEntityCollection(toRet, filterBucket, PrefetchConstants.EavPrefetchGraph);

            return toRet.Select(Mapper.Map<Data.EntityClasses.ObjectRevisionEntity, Core.Entities.ObjectRevisionEntity>).ToList();
        }

        public DataTable GetAllRelatedToObject(int branchId, int objectId)
        {
            return RetrievalProcedures.GetAllRelatedToObject(branchId, objectId, (DataAccessAdapter)_Adapter);
        }

        public Core.Entities.BranchHeadObjectEntity GetValueForObjectAtRevision(int branchId, int objectId, int revisionId)
        {
            var toRet = new Data.EntityClasses.RevisionHeadObjectEntity();

            var predicateExpression = new PredicateExpression
                                          {
                                              RevisionHeadObjectFields.BranchId == branchId,
                                              RevisionHeadObjectFields.RevisionId == revisionId,
                                              RevisionHeadObjectFields.ObjectId == objectId
                                          };

            var prefetchPath = new PrefetchPath2(EntityType.RevisionHeadObjectEntity);
            var objRevElement = prefetchPath.Add(Data.EntityClasses.RevisionHeadObjectEntity.PrefetchPathObjectRevision);
            objRevElement.SubPath.Add(Data.EntityClasses.ObjectRevisionEntity.PrefetchPathAttributeValue);
            objRevElement.SubPath.Add(Data.EntityClasses.ObjectRevisionEntity.PrefetchPathRelationAttributeValue);
            _Adapter.FetchEntityUsingUniqueConstraint(toRet, predicateExpression, prefetchPath);

            return Mapper.Map<Data.EntityClasses.RevisionHeadObjectEntity, Core.Entities.BranchHeadObjectEntity>(toRet);
        }

        public IEnumerable<Core.Entities.ObjectRevisionEntity> GetAllDataForRevisionByType(int branchId, int revisionId, int classId)
        {
            var filterBucket = new RelationPredicateBucket();

            filterBucket.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId);

            filterBucket.PredicateExpression.Add(RevisionHeadObjectFields.RevisionId == revisionId);
            filterBucket.PredicateExpression.Add(RevisionHeadObjectFields.BranchId == branchId);
            filterBucket.PredicateExpression.AddWithAnd(RevisionHeadObjectFields.ClassId == classId);

            return FetchHeadObjects(filterBucket);
        }

        public IEnumerable<Core.Entities.ObjectRevisionEntity> GetAllForRevisionWithFkRelation(int branchId, int revisionId, int classId, int fkAttributeId, IEnumerable<int> fkList)
        {
            var filterBucket = new RelationPredicateBucket();

            filterBucket.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId);
            filterBucket.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.RelationAttributeValueEntityUsingObjectRevisionId);
            filterBucket.Relations.Add(Data.EntityClasses.RelationAttributeValueEntity.Relations.RelationshipMetadataEntityUsingRelationshipMetadataId);

            filterBucket.PredicateExpression.Add(RevisionHeadObjectFields.RevisionId == revisionId);
            filterBucket.PredicateExpression.Add(RevisionHeadObjectFields.BranchId == branchId);
            filterBucket.PredicateExpression.AddWithAnd(RevisionHeadObjectFields.ClassId == classId);
            filterBucket.PredicateExpression.AddWithAnd(RelationshipMetadataFields.AttributeId == fkAttributeId);
            filterBucket.PredicateExpression.AddWithAnd(new FieldCompareRangePredicate(RelationAttributeValueFields.Value, null, fkList));

            return FetchHeadObjects(filterBucket);
        }

        public IEnumerable<Core.Entities.ObjectRevisionEntity> GetAllAtRevisionWithId(int branchId, int revisionId, IEnumerable<int> idList)
        {
            idList = idList.ToList(); //~ enumerate into list

            var toRet = new List<Core.Entities.ObjectRevisionEntity>();

            while (idList.Count() > 2000) //~ break into batches of 2000 to avoid exceeding sql "where in" clause capacity
            {
                toRet.AddRange(GetAllAtRevisionWithId(branchId, revisionId, idList.Take(2000)));
                idList = idList.Skip(2000).ToList();
            }

            var filterBucket = new RelationPredicateBucket();

            filterBucket.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId);

            filterBucket.PredicateExpression.Add(RevisionHeadObjectFields.BranchId == branchId);
            filterBucket.PredicateExpression.Add(RevisionHeadObjectFields.RevisionId == revisionId);
            filterBucket.PredicateExpression.AddWithAnd(new FieldCompareRangePredicate(RevisionHeadObjectFields.ObjectId, null, idList));

            toRet.AddRange(FetchHeadObjects(filterBucket));

            return toRet;
        }

        public void FillHeadObjects()
        {
            ActionProcedures.FillHeadObjects((DataAccessAdapter)_Adapter);
        }

        public void AddRevisionToHeadObjects(int branchId, int revisionId)
        {
            //~ check if revision has already been indexed
            var revEntity = new EntityCollection<Data.EntityClasses.RevisionHeadObjectEntity>();

            var filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(RevisionHeadObjectFields.RevisionId == revisionId);

            var count = _Adapter.GetDbCount(revEntity, filterBucket);

            if (count == 0)
            {
                ActionProcedures.AddRevisionToHeadObjects(branchId, revisionId, (DataAccessAdapter)_Adapter);
            }
        }

        public void AddRootObjectToHeadObjects(int classId, int objectId, int objectRevisionId)
        {
            //~ add record to BranchHeadObjects for each indexed branch
            var branches = new BranchesTypedView();
            _Adapter.FetchTypedView(branches);

            foreach (var branch in branches)
            {
                var toCreate = new Core.Entities.BranchHeadObjectEntity
                                   {
                                       BranchId = branch.BranchId,
                                       ClassId = classId,
                                       ObjectId = objectId,
                                       ObjectRevisionId = objectRevisionId,
                                       RevisionId = 0
                                   };

                AddObjectToBranchHeadObjects(toCreate);
            }

            //~ add record to RevisionHeadObjects for each indexed revision
            var revisions = new IndexedRevisionsTypedView();
            _Adapter.FetchTypedView(revisions);

            foreach (var revision in revisions)
            {
                var toSave = new Data.EntityClasses.RevisionHeadObjectEntity
                                 {
                                     BranchId = revision.BranchId,
                                     ClassId = classId,
                                     ObjectId = objectId,
                                     ObjectRevisionId = objectRevisionId,
                                     RevisionId = revision.RevisionId
                                 };

                _Adapter.SaveEntity(toSave);
            }
        }

        public void RemoveRootObjectFromHeadObjects(int objectId)
        {
            //~ DELETE FROM tblBranchHeadObject WHERE ObjectId = objectId
            var filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(BranchHeadObjectFields.ObjectId == objectId);
            _Adapter.DeleteEntitiesDirectly(typeof(Data.EntityClasses.BranchHeadObjectEntity), filterBucket);

            //~ DELETE FROM tblRevisionHeadObject WHERE ObjectId = objectId
            var filterBucket2 = new RelationPredicateBucket();
            filterBucket2.PredicateExpression.Add(RevisionHeadObjectFields.ObjectId == objectId);
            _Adapter.DeleteEntitiesDirectly(typeof(Data.EntityClasses.RevisionHeadObjectEntity), filterBucket2);
        }
    }
}
