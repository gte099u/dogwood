﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Core;

namespace Dogwood.LlblGen
{
    internal static class LeanPrefetchFactory
    {
        public static IPrefetchPath2 LeanPrefetchPath(IEntityMetaData metaData)
        {
            var spec = new PrefetchSpec(metaData.Attributes.Count != 0,               
                metaData.OneToManyRelationships.Count != 0 || metaData.ManyToOneRelationships.Count != 0);

            return PrefetchConstants.GetPrefetchGraph(spec);
        }
    }
}
