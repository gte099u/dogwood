﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.EntityClasses;
using Dogwood.Data;
using Dogwood.Core;

namespace Dogwood.LlblGen
{
    internal static class PrefetchConstants
    {
        public static IPrefetchPath2 EavPrefetchGraph
        {
            get
            {
                IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.ObjectRevisionEntity);
                prefetchPath.Add(ObjectRevisionEntity.PrefetchPathAttributeValue);
                prefetchPath.Add(ObjectRevisionEntity.PrefetchPathRelationAttributeValue);
                return prefetchPath;
            }
        }

        public static IPrefetchPath2 GetPrefetchGraph(PrefetchSpec spec)
        {
            IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.ObjectRevisionEntity);

            if (spec.PrefetchAttributes)
                prefetchPath.Add(ObjectRevisionEntity.PrefetchPathAttributeValue);
            if (spec.PrefetchForeignKey)
                prefetchPath.Add(ObjectRevisionEntity.PrefetchPathRelationAttributeValue);
           
            return prefetchPath;
        }
    }
}
