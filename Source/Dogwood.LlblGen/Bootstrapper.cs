﻿using AutoMapper;
using Dogwood.Core.Entities;

namespace Dogwood.LlblGen
{
    public static class Bootstrapper
    {
        public static void Init()
        {
            Mapper.CreateMap<Data.EntityClasses.RevisionEntity, RevisionEntity>();
            Mapper.CreateMap<Data.EntityClasses.BranchHeadObjectEntity, BranchHeadObjectEntity>();
            Mapper.CreateMap<Data.EntityClasses.RevisionHeadObjectEntity, BranchHeadObjectEntity>();

            Mapper.CreateMap<Data.EntityClasses.ObjectEntity, ObjectEntity>();
            Mapper.CreateMap<Data.EntityClasses.ClassEntity, ClassEntity>();

            Mapper.CreateMap<Data.EntityClasses.ObjectRevisionEntity, ObjectRevisionEntity>()
                .ForMember(m => m.AttributeValues, n => n.MapFrom(x => x.AttributeValue))
                .ForMember(m => m.ForeignKeyAttributeValues, n => n.MapFrom(x => x.RelationAttributeValue));

            Mapper.CreateMap<Data.EntityClasses.RelationshipMetadataEntity, RelationshipMetadataEntity>();
            Mapper.CreateMap<Data.EntityClasses.AttributeMetadataEntity, AttributeMetadataEntity>();

            Mapper.CreateMap<Data.EntityClasses.AttributeValueEntity, AttributeValueEntity>();
            Mapper.CreateMap<Data.EntityClasses.RelationAttributeValueEntity, RelationAttributeValueEntity>();

            Mapper.CreateMap<Data.EntityClasses.AttributeEntity, AttributeEntity>();

            Mapper.CreateMap<RevisionEntity, Data.EntityClasses.RevisionEntity>()
                .IgnoreLlblStuff()
                .ForMember(m => m.ObjectRevision, x => x.Ignore());

            Mapper.CreateMap<BranchHeadObjectEntity, Data.EntityClasses.BranchHeadObjectEntity>()
                .IgnoreLlblStuff()
                .ForMember(m => m.Class, x => x.Ignore())
                .ForMember(m => m.Object, x => x.Ignore())
                .ForMember(m => m.Revision, x => x.Ignore())
                .ForMember(m => m.Revision_, x => x.Ignore());

            Mapper.CreateMap<BranchHeadObjectEntity, Data.EntityClasses.RevisionHeadObjectEntity>()
               .IgnoreLlblStuff()
               .ForMember(m => m.Class, x => x.Ignore())
               .ForMember(m => m.Object, x => x.Ignore())
               .ForMember(m => m.Revision, x => x.Ignore())
               .ForMember(m => m.Revision_, x => x.Ignore());

            Mapper.CreateMap<ObjectEntity, Data.EntityClasses.ObjectEntity>()
                .IgnoreLlblStuff()
                .ForMember(m => m.Class, x => x.Ignore())
                .ForMember(m => m.RelationAttributeValue, x => x.Ignore())
                .ForMember(m => m.ObjectRevision, x => x.Ignore());

            Mapper.CreateMap<ClassEntity, Data.EntityClasses.ClassEntity>()
                .IgnoreLlblStuff()
                .ForMember(m => m.RelationshipMetadataAsFk, x => x.Ignore())
                .ForMember(m => m.RelationshipMetadataAsPk, x => x.Ignore())
                .ForMember(m => m.AttributeMetadata, x => x.Ignore())
                .ForMember(m => m.Object, x => x.Ignore());

            Mapper.CreateMap<ObjectRevisionEntity, Data.EntityClasses.ObjectRevisionEntity>()
                .IgnoreLlblStuff()
                .ForMember(m => m.Object, x => x.Ignore())
                .ForMember(m => m.Revision, x => x.Ignore())
                .ForMember(m => m.AttributeValue, x => x.Ignore())
                .ForMember(m => m.RelationAttributeValue, x => x.Ignore());

            Mapper.CreateMap<RelationshipMetadataEntity, Data.EntityClasses.RelationshipMetadataEntity>()
                .IgnoreLlblStuff()
                .ForMember(m => m.Attribute, x => x.Ignore())
                .ForMember(m => m.RelationAttributeValue, x => x.Ignore())
                .ForMember(m => m.FkClass, x => x.Ignore())
                .ForMember(m => m.PkClass, x => x.Ignore());

            Mapper.CreateMap<AttributeMetadataEntity, Data.EntityClasses.AttributeMetadataEntity>()
                .IgnoreLlblStuff()
                .ForMember(m => m.Attribute, x => x.Ignore())
                .ForMember(m => m.AttributeValue, x => x.Ignore())
                .ForMember(m => m.Class, x => x.Ignore());

            Mapper.CreateMap<AttributeValueEntity, Data.EntityClasses.AttributeValueEntity>()
                .IgnoreLlblStuff()
                .ForMember(m => m.AttributeMetadata, x => x.Ignore())
                .ForMember(m => m.ObjectRevision, x => x.Ignore());

            Mapper.CreateMap<RelationAttributeValueEntity, Data.EntityClasses.RelationAttributeValueEntity>()
                .IgnoreLlblStuff()
                .ForMember(m => m.Object, x => x.Ignore())
                .ForMember(m => m.RelationshipMetadata, x => x.Ignore())
                .ForMember(m => m.ObjectRevision, x => x.Ignore());

            Mapper.CreateMap<AttributeEntity, Data.EntityClasses.AttributeEntity>()
                .IgnoreLlblStuff()
                .ForMember(m => m.AttributeMetadata, x => x.Ignore());
        }

        private static IMappingExpression<TSource, TDestination> IgnoreLlblStuff<TSource, TDestination>(this IMappingExpression<TSource, TDestination> exp) where TDestination : Data.EntityClasses.CommonEntityBase
        {
            return exp.ForMember(m => m.ActiveContext, x => x.Ignore())
                .ForMember(m => m.IsDirty, m => m.UseValue(true))
                .ForMember(m => m.AuditorToUse, x => x.Ignore())
                .ForMember(m => m.AuthorizerToUse, x => x.Ignore())
                .ForMember(m => m.ConcurrencyPredicateFactoryToUse, x => x.Ignore())
                .ForMember(m => m.CustomPropertiesOfType, x => x.Ignore())
                .ForMember(m => m.Fields, x => x.Ignore())
                .ForMember(m => m.FieldsCustomPropertiesOfType, x => x.Ignore())
                .ForMember(m => m.LLBLGenProEntityTypeValue, x => x.Ignore())
                .ForMember(m => m.ObjectID, x => x.Ignore())
                .ForMember(m => m.Transaction, x => x.Ignore())
                .ForMember(m => m.TypeDefaultValueProviderToUse, x => x.Ignore())
                .ForMember(m => m.Validator, x => x.Ignore())
                .ForMember(m => m.PrimaryKeyFields, x => x.Ignore())
                .ForMember(m => m.IsDeserializing, x => x.Ignore());
        }
    }
}
