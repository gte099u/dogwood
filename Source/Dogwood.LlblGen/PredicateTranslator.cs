﻿using System;
using System.Linq;
using Dogwood.Core;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ComparisonOperator = Dogwood.Core.ComparisonOperator;

namespace Dogwood.LlblGen
{
    public class PredicateTranslator
    {
        public static IRelationPredicateBucket Convert(IPredicateBucket coreBucket, EavContextType contextType)
        {
            var toRet = new RelationPredicateBucket();

            switch (contextType)
            {
                case EavContextType.Branch:
                    toRet.PredicateExpression.Add(BranchHeadObjectFields.BranchId == coreBucket.ContextId & BranchHeadObjectFields.ClassId == coreBucket.ClassId);
                    toRet.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.BranchHeadObjectEntityUsingObjectRevisionId);
                    break;
                case EavContextType.Revision:
                    toRet.PredicateExpression.Add(RevisionHeadObjectFields.RevisionId == coreBucket.ContextId & RevisionHeadObjectFields.ClassId == coreBucket.ClassId);
                    toRet.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("contextType");
            }
            
            foreach (var predicate in coreBucket.GetAttributeComparePredicates())
            {
                var objAlias = string.Format("AttributeCompare_{0}", predicate.AttributeMetadatatId);

                toRet.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.AttributeValueEntityUsingObjectRevisionId, objAlias, JoinHint.Left);

                var dataType = (DogwoodDataType)DbMetadataRegistry.Instance[predicate.AttributeMetadatatId].DataTypeId;

                string castExpression;

                switch (dataType)
                {
                    case DogwoodDataType.String:
                        castExpression = "CAST({0} AS varchar(max))";
                        break;
                    case DogwoodDataType.Int:
                        castExpression = "CAST({0} AS int)";
                        break;
                    case DogwoodDataType.Long:
                        castExpression = "CAST({0} AS bigint)";
                        break;
                    case DogwoodDataType.Bool:
                        castExpression = "CAST({0} AS bit)";
                        break;
                    case DogwoodDataType.DateTime:
                        castExpression = "CAST({0} AS DateTime)";
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                var fieldExpression = new EntityField2("CastedValue", new DbFunctionCall(castExpression, new object[] { AttributeValueFields.Value.SetObjectAlias(objAlias) }));
                var attMetaExp = AttributeValueFields.AttributeMetadataId.SetObjectAlias(objAlias);                
               
                switch (predicate.CompOperator)
                {
                    case ComparisonOperator.Equal:
                        toRet.PredicateExpression.Add(attMetaExp == predicate.AttributeMetadatatId & new FieldCompareValuePredicate(AttributeValueFields.Value.SetObjectAlias(objAlias), null, SD.LLBLGen.Pro.ORMSupportClasses.ComparisonOperator.Equal, DataConverter.ConvertToBytes(predicate.Value, dataType)));
                        break;
                    case ComparisonOperator.NotEqual:
                        toRet.PredicateExpression.Add(attMetaExp == predicate.AttributeMetadatatId & new FieldCompareValuePredicate(AttributeValueFields.Value.SetObjectAlias(objAlias), null, SD.LLBLGen.Pro.ORMSupportClasses.ComparisonOperator.NotEqual, DataConverter.ConvertToBytes(predicate.Value, dataType)));
                        break;
                    case ComparisonOperator.LessThan:
                        toRet.PredicateExpression.Add(attMetaExp == predicate.AttributeMetadatatId & fieldExpression < predicate.Value);
                        break;
                    case ComparisonOperator.GreaterThan:
                        toRet.PredicateExpression.Add(attMetaExp == predicate.AttributeMetadatatId & fieldExpression > predicate.Value);
                        break;
                    case ComparisonOperator.LessThanOrEqualTo:
                        toRet.PredicateExpression.Add(attMetaExp == predicate.AttributeMetadatatId & fieldExpression <= predicate.Value);
                        break;
                    case ComparisonOperator.GreaterThanOrEqualTo:
                        toRet.PredicateExpression.Add(attMetaExp == predicate.AttributeMetadatatId & fieldExpression >= predicate.Value);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("coreBucket");
                }
            }

            foreach (var predicate in coreBucket.GetAttributeLikePredicates())
            {
                var objAlias = string.Format("AttributeLike_{0}", predicate.AttributeMetadatatId ?? 0);

                toRet.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.AttributeValueEntityUsingObjectRevisionId, objAlias, JoinHint.Left);

                var fieldExpression = new EntityField2("CastedValue", new DbFunctionCall("CAST({0} AS varchar(max))", new object[] { AttributeValueFields.Value.SetObjectAlias(objAlias) }));
              
                if (predicate.AttributeMetadatatId == null)
                {
                    switch (predicate.CompOperator)
                    {
                        case LikeComparisonOperator.Contains:
                            toRet.PredicateExpression.Add(fieldExpression % string.Format("%{0}%", predicate.Value));
                            break;
                        case LikeComparisonOperator.BeginsWith:
                            toRet.PredicateExpression.Add(fieldExpression % string.Format("{0}%", predicate.Value));
                            break;
                        case LikeComparisonOperator.EndsWith:
                            toRet.PredicateExpression.Add(fieldExpression % string.Format("%{0}", predicate.Value));
                            break;
                        default:
                            throw new ArgumentOutOfRangeException("coreBucket");
                    }
                }
                else
                {
                    var attMetaExp = AttributeValueFields.AttributeMetadataId.SetObjectAlias(objAlias);
                    switch (predicate.CompOperator)
                    {
                        case LikeComparisonOperator.Contains:
                            toRet.PredicateExpression.Add(attMetaExp == predicate.AttributeMetadatatId & fieldExpression % string.Format("%{0}%", predicate.Value));
                            break;
                        case LikeComparisonOperator.BeginsWith:
                            toRet.PredicateExpression.Add(attMetaExp == predicate.AttributeMetadatatId & fieldExpression % string.Format("{0}%", predicate.Value));
                            break;
                        case LikeComparisonOperator.EndsWith:
                            toRet.PredicateExpression.Add(attMetaExp == predicate.AttributeMetadatatId & fieldExpression % string.Format("%{0}", predicate.Value));
                            break;
                        default:
                            throw new ArgumentOutOfRangeException("coreBucket");
                    }
                }
            }

            foreach (var predicate in coreBucket.GetRelationPredicates())
            {
                var objAlias = string.Format("Relation_{0}", predicate.RelationMetadatatId);
                toRet.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.RelationAttributeValueEntityUsingObjectRevisionId, objAlias, JoinHint.Left);

                if (predicate.Negate)
                {
                    toRet.PredicateExpression.Add(RelationAttributeValueFields.RelationshipMetadataId.SetObjectAlias(objAlias) == predicate.RelationMetadatatId &
                        RelationAttributeValueFields.Value.SetObjectAlias(objAlias) != predicate.Value);
                }
                else
                {
                    toRet.PredicateExpression.Add(RelationAttributeValueFields.RelationshipMetadataId.SetObjectAlias(objAlias) == predicate.RelationMetadatatId &
                        RelationAttributeValueFields.Value.SetObjectAlias(objAlias) == predicate.Value);
                }
            }

            foreach (var predicate in coreBucket.GetRelationInPredicates())
            {
                var objAlias = string.Format("RelationIn_{0}", predicate.RelationMetadatatId);
                toRet.Relations.Add(Data.EntityClasses.ObjectRevisionEntity.Relations.RelationAttributeValueEntityUsingObjectRevisionId, objAlias, JoinHint.Left);

                if (predicate.Negate)
                {
                    toRet.PredicateExpression.Add(RelationAttributeValueFields.RelationshipMetadataId.SetObjectAlias(objAlias) == predicate.RelationMetadatatId &
                        RelationAttributeValueFields.Value.SetObjectAlias(objAlias) != predicate.Values.ToArray());
                }
                else
                {
                    toRet.PredicateExpression.Add(RelationAttributeValueFields.RelationshipMetadataId.SetObjectAlias(objAlias) == predicate.RelationMetadatatId &
                        RelationAttributeValueFields.Value.SetObjectAlias(objAlias) == predicate.Values.ToArray());
                }
            }

            return toRet;
        }
    }
}
