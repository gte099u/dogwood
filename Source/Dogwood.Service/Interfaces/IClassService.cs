﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dogwood.Data.EntityClasses;

namespace Dogwood.Service
{
    public interface IClassService
    {
        bool Update(int id, string name);
        bool Create(string name);
        ClassEntity Get(int id);
        IEnumerable<ClassEntity> GetAll();
    }
}
