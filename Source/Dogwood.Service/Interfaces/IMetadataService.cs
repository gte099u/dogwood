﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dogwood.Data.EntityClasses;

namespace Dogwood.Service
{
    public interface IMetadataService
    {
        AttributeMetadataEntity Get(int id);
        bool Create(int classId, int dataType, int attributeId, bool isNullable, bool isImmutable, byte[] defaultValue);
        bool Update(int id, int dataType, bool isNullable, bool isImmutable, byte[] defaultValue);
    }
}
