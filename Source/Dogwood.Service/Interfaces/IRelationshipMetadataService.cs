﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dogwood.Data.EntityClasses;

namespace Dogwood.Service
{
    public interface IRelationshipMetadataService
    {
        bool Create(RelationshipMetadataDto toSave);
        bool Update(RelationshipMetadataDto toSave);
        RelationshipMetadataEntity Get(int id);
    }
}
