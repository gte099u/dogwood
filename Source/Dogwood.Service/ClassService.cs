﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data;
using Dogwood.Data.DatabaseSpecific;

namespace Dogwood.Service
{
    public class ClassService : IClassService
    {
        public bool Create(string name)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var cla = new ClassEntity {Name = name};


                adapter.SaveEntity(cla);
            }

            return true;
        }

        public bool Update(int id, string name)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var cla = new ClassEntity(id);
                adapter.FetchEntity(cla, MetadataPath());

                cla.Name = name;

                adapter.SaveEntity(cla);
            }

            return true;
        }       

        private static IPrefetchPath2 MetadataPath()
        {
            IPrefetchPath2 metadataPath = new PrefetchPath2(EntityType.ClassEntity);
            metadataPath.Add(ClassEntity.PrefetchPathRelationshipMetadataAsFk);
            metadataPath.Add(ClassEntity.PrefetchPathAttributeMetadata);

            return metadataPath;
        }
        
        public ClassEntity Get(int id)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var toRet = new ClassEntity(id);

                IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.ClassEntity);

                prefetchPath.Add(ClassEntity.PrefetchPathAttributeMetadata).SubPath.Add(AttributeMetadataEntity.PrefetchPathAttribute);

                var element = prefetchPath.Add(ClassEntity.PrefetchPathRelationshipMetadataAsFk);
                element.SubPath.Add(RelationshipMetadataEntity.PrefetchPathAttribute);
                element.SubPath.Add(RelationshipMetadataEntity.PrefetchPathPkClass);

                var element2 = prefetchPath.Add(ClassEntity.PrefetchPathRelationshipMetadataAsPk);
                element2.SubPath.Add(RelationshipMetadataEntity.PrefetchPathFkClass);
                element2.SubPath.Add(RelationshipMetadataEntity.PrefetchPathAttribute);

                prefetchPath.Add(ClassEntity.PrefetchPathObject);

                adapter.FetchEntity(toRet, prefetchPath);

                return toRet;
            }
        }

        public IEnumerable<ClassEntity> GetAll()
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                var allClasses = new EntityCollection<ClassEntity>();

                IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.ClassEntity);

                prefetchPath.Add(ClassEntity.PrefetchPathAttributeMetadata).SubPath.Add(AttributeMetadataEntity.PrefetchPathAttribute);

                var element = prefetchPath.Add(ClassEntity.PrefetchPathRelationshipMetadataAsFk);
                element.SubPath.Add(RelationshipMetadataEntity.PrefetchPathAttribute);
                element.SubPath.Add(RelationshipMetadataEntity.PrefetchPathPkClass);

                var element2 = prefetchPath.Add(ClassEntity.PrefetchPathRelationshipMetadataAsPk);
                element2.SubPath.Add(RelationshipMetadataEntity.PrefetchPathFkClass);
                element2.SubPath.Add(RelationshipMetadataEntity.PrefetchPathAttribute);

                prefetchPath.Add(ClassEntity.PrefetchPathObject);

                adapter.FetchEntityCollection(allClasses, null, prefetchPath);

                return allClasses;
            }
        }
    }
}
