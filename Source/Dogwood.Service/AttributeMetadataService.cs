﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using System.Data;
using Dogwood.Data.EntityClasses;
using Dogwood.Data;
using Dogwood.Core;

namespace Dogwood.Service
{
    public class AttributeMetadataService : IMetadataService
    {
        public static string GetDataType(DogwoodDataType dataType)
        {
            switch (dataType)
            {
                case DogwoodDataType.String:
                    return "string";
                case DogwoodDataType.Int:
                    return "int";
                case DogwoodDataType.Long:
                    return "long";
                case DogwoodDataType.Bool:
                    return "bool";
                case DogwoodDataType.DateTime:
                    return "DateTime";
                default:
                    return "";
            }
        }

        public bool Create(int classId, int dataType, int stringAttributeId, bool isNullable, bool isImmutable, byte[] defaultValue)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                try
                {
                    adapter.StartTransaction(IsolationLevel.ReadCommitted, "CreateMetadata");

                    IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.ClassEntity);
                    prefetchPath.Add(ClassEntity.PrefetchPathAttributeMetadata);
                    ClassEntity cla = new ClassEntity(classId);
                    adapter.FetchEntity(cla, prefetchPath);

                    AttributeMetadataEntity meta = new AttributeMetadataEntity();

                    meta.ClassId = classId;
                    meta.AttributeId = stringAttributeId;
                    meta.IsNullable = isNullable;
                    meta.DataTypeId = dataType;

                    adapter.SaveEntity(meta);

                    //~ if not nullable, update all existing object revisions of class type with default value
                    if (!isNullable)
                    {
                        ObjectRevisionService service = new ObjectRevisionService(adapter);

                        foreach (var objRev in service.GetAllNotDeletedForClass(classId))
                        {
                            AttributeValueEntity attVal = new AttributeValueEntity();
                            attVal.AttributeMetadataId = meta.Id;
                            attVal.Value = defaultValue;
                            attVal.ObjectRevisionId = objRev.Id;
                            adapter.SaveEntity(attVal);
                        }
                    }

                    adapter.Commit();
                }
                catch (Exception)
                {
                    adapter.Rollback();
                    throw;
                }
            }

            return true;
        }

        public bool Update(int id, int dataType, bool isNullable, bool isImmutable, byte[] defaultValue)
        {
            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                try
                {
                    adapter.StartTransaction(IsolationLevel.ReadCommitted, "CreateMetadata");

                    var meta = new AttributeMetadataEntity(id);
                    adapter.FetchEntity(meta);
                    bool wasNullable = meta.IsNullable;

                    meta.IsNullable = isNullable;
                    meta.IsImmutable = isImmutable;
                    meta.DataTypeId = dataType;

                    adapter.SaveEntity(meta);

                    //~ if nullable state has been activated, update all existing object revisions of class type with default value
                    if (wasNullable && !isNullable)
                    {
                        var service = new ObjectRevisionService(adapter);

                        IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.ObjectRevisionEntity);
                        prefetchPath.Add(ObjectRevisionEntity.PrefetchPathAttributeValue);

                        foreach (var objRev in service.GetAllNotDeletedForClass(meta.ClassId, prefetchPath))
                        {
                            if (!objRev.AttributeValue.Where(m => m.AttributeMetadataId == meta.Id).Any())
                            {
                                var attVal = new AttributeValueEntity();
                                attVal.AttributeMetadataId = meta.Id;
                                attVal.Value = defaultValue;
                                attVal.ObjectRevisionId = objRev.Id;
                                adapter.SaveEntity(attVal);
                            }
                        }
                    }

                    adapter.Commit();
                }
                catch (Exception)
                {
                    adapter.Rollback();
                    throw;
                }
            }

            return true;
        }

        public AttributeMetadataEntity Get(int id)
        {
            using (var adapter = new DataAccessAdapter(true))
            {
                var path = new PrefetchPath2(EntityType.AttributeMetadataEntity) { AttributeMetadataEntity.PrefetchPathAttribute };
                var toRet = new AttributeMetadataEntity(id);
                adapter.FetchEntity(toRet, path);
                return toRet;
            }
        }
    }
}
