﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Service
{
    public class ObjectRevisionService
    {
        private IDataAccessAdapter _adapter;

        public ObjectRevisionService(IDataAccessAdapter adapter)
        {
            _adapter = adapter;
        }

        /// <summary>
        /// Returns a collection of all object revisions that are not deleting an object for the given class
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="adapter"></param>
        /// <returns></returns>
        public EntityCollection<ObjectRevisionEntity> GetAllNotDeletedForClass(int classId)
        {
            return GetAllNotDeletedForClass(classId, null);
        }

        public EntityCollection<ObjectRevisionEntity> GetAllNotDeletedForClass(int classId, IPrefetchPath2 prefetchPath)
        {
            EntityCollection<ObjectRevisionEntity> allObjectRevs = new EntityCollection<ObjectRevisionEntity>();
            IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
            filterBucket.PredicateExpression.Add(ObjectFields.ClassId == classId);
            filterBucket.PredicateExpression.Add(ObjectRevisionFields.IsDeleted == false);
            filterBucket.Relations.Add(ObjectRevisionEntity.Relations.ObjectEntityUsingObjectId);
            _adapter.FetchEntityCollection(allObjectRevs, filterBucket, prefetchPath);

            return allObjectRevs;
        }
    }
}