﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dogwood.Service
{
    public class RelationshipMetadataDto
    {
        public int Id { get; set; }
        public int BehaviorType { get; set; }
        public int? CascadeDefaultValue { get; set; }
        public bool IsNullable { get; set; }
        public int ClassId { get; set; }
        public int PkClassId { get; set; }
        public int DefaultValue { get; set; }
        public string OneToManyValue { get; set; }
        public int AttributeId { get; set; }
    }
}
