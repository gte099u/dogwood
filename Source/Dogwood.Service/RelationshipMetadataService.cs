﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.DatabaseSpecific;
using System.Data;
using Dogwood.Data.EntityClasses;
using Dogwood.Data;
using Dogwood.Data.HelperClasses;

namespace Dogwood.Service
{
    public class RelationshipMetadataService : IRelationshipMetadataService
    {
        private static void ValidateDefaultCascadeValue(IDataAccessAdapter adapter, int defaultObjectId, int classId)
        {
            ObjectEntity cascadeDefaultObject = new ObjectEntity(defaultObjectId);
            adapter.FetchEntity(cascadeDefaultObject);
            if (cascadeDefaultObject.ClassId != classId)
            {
                throw new Exception("The provided cascade default Id is not a valid Id for the chosen class type");
            }
        }

        private static void ValidateCascadeBehaviorSelections(int selectedBehaviorType, int? defaultValue, bool isNullable)
        {
            if (selectedBehaviorType == 2 && isNullable == false)
            {
                throw new Exception("Cascade set to null may only be selected for Nullable relationships.");
            }

            if (selectedBehaviorType == 3 && defaultValue == null)
            {
                throw new Exception("A cascade default value must be provided for cascade set to default.");
            }
        }

        public bool Create(RelationshipMetadataDto dto)
        {
            ValidateCascadeBehaviorSelections(dto.BehaviorType, dto.CascadeDefaultValue, dto.IsNullable);

            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                try
                {
                    adapter.StartTransaction(IsolationLevel.ReadCommitted, "CreateMetadata");

                    IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.ClassEntity);
                    prefetchPath.Add(ClassEntity.PrefetchPathRelationshipMetadataAsFk);
                    var cla = new ClassEntity(dto.ClassId);
                    adapter.FetchEntity(cla, prefetchPath);

                    if (dto.BehaviorType == 3)
                    {                        
                            ValidateDefaultCascadeValue(adapter, (int)dto.CascadeDefaultValue, dto.ClassId);
                    }

                    var meta = new RelationshipMetadataEntity();

                    meta.FkClassId = dto.ClassId;
                    meta.AttributeId = dto.AttributeId;
                    meta.IsNullable = dto.IsNullable;
                    meta.PkClassId = dto.PkClassId;
                    meta.CascadeDefaultValue = dto.CascadeDefaultValue;
                    meta.CascadeBehavior = dto.BehaviorType;

                    if (string.IsNullOrEmpty(dto.OneToManyValue))
                    {
                        meta.OneToManyValue = null;
                    }
                    else
                    {
                        var classesWithOtmValue = new EntityCollection<RelationshipMetadataEntity>();
                        IRelationPredicateBucket filterBucket = new RelationPredicateBucket();
                        filterBucket.PredicateExpression.Add(RelationshipMetadataFields.OneToManyValue == dto.OneToManyValue);
                        filterBucket.PredicateExpression.AddWithAnd(RelationshipMetadataFields.PkClassId == dto.PkClassId);
                        adapter.FetchEntityCollection(classesWithOtmValue, filterBucket);

                        if (classesWithOtmValue.Count != 0)
                        {
                            throw new Exception("A foreign key property with the same One-To-Many property name already exists for the given class");
                        }

                        meta.OneToManyValue = dto.OneToManyValue;
                    }

                    adapter.SaveEntity(meta);

                    //~ if not nullable, update all existing object revisions of class type with default value
                    if (!dto.IsNullable)
                    {
                        ObjectRevisionService service = new ObjectRevisionService(adapter);

                        foreach (var objRev in service.GetAllNotDeletedForClass(dto.ClassId))
                        {
                            RelationAttributeValueEntity attVal = new RelationAttributeValueEntity();
                            attVal.RelationshipMetadataId = meta.Id;
                            attVal.Value = dto.DefaultValue;
                            attVal.ObjectRevisionId = objRev.Id;
                            adapter.SaveEntity(attVal);
                        }
                    }

                    adapter.Commit();
                }
                catch (Exception)
                {
                    adapter.Rollback();
                }
            }

            return true;
        }

        public bool Update(RelationshipMetadataDto dto)
        {
            ValidateCascadeBehaviorSelections(dto.BehaviorType, dto.CascadeDefaultValue, dto.IsNullable);

            using (IDataAccessAdapter adapter = new DataAccessAdapter(true))
            {
                try
                {
                    adapter.StartTransaction(IsolationLevel.ReadCommitted, "CreateMetadata");

                    if (dto.BehaviorType == 3)
                    {
                        ValidateDefaultCascadeValue(adapter, (int)dto.CascadeDefaultValue, dto.ClassId);
                    }

                    RelationshipMetadataEntity meta = new RelationshipMetadataEntity(dto.Id);
                    adapter.FetchEntity(meta);
                    bool wasNullable = meta.IsNullable;

                    meta.IsNullable = dto.IsNullable;
                    meta.CascadeDefaultValue = dto.CascadeDefaultValue;
                    meta.CascadeBehavior = dto.BehaviorType;

                    if (string.IsNullOrEmpty(dto.OneToManyValue))
                    {
                        meta.OneToManyValue = null;
                    }
                    else
                    {
                        meta.OneToManyValue = dto.OneToManyValue;
                    }

                    adapter.SaveEntity(meta);

                    //~ if nullable state has been activated, update all existing object revisions of class type with default value
                    if (wasNullable && !dto.IsNullable)
                    {
                        ObjectRevisionService service = new ObjectRevisionService(adapter);

                        IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.ObjectRevisionEntity);
                        prefetchPath.Add(ObjectRevisionEntity.PrefetchPathRelationAttributeValue);

                        foreach (var objRev in service.GetAllNotDeletedForClass(meta.PkClassId, prefetchPath))
                        {
                            if (!objRev.RelationAttributeValue.Where(m => m.RelationshipMetadataId == meta.Id).Any())
                            {
                                RelationAttributeValueEntity attVal = new RelationAttributeValueEntity();
                                attVal.RelationshipMetadataId = meta.Id;
                                attVal.Value = dto.DefaultValue;
                                attVal.ObjectRevisionId = objRev.Id;
                                adapter.SaveEntity(attVal);
                            }
                        }
                    }

                    adapter.Commit();
                }
                catch (Exception)
                {
                    adapter.Rollback();
                }
            }

            return true;
        }


        public RelationshipMetadataEntity Get(int id)
        {
            using (var adapter = new DataAccessAdapter(true))
            {
                var path = new PrefetchPath2(EntityType.RelationshipMetadataEntity) { RelationshipMetadataEntity.PrefetchPathAttribute };
                var toRet = new RelationshipMetadataEntity(id);
                adapter.FetchEntity(toRet, path);
                return toRet;
            }
        }
    }
}
