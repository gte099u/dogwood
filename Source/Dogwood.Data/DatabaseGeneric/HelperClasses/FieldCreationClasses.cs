﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:04 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data;

namespace Dogwood.Data.HelperClasses
{
	/// <summary>Field Creation Class for entity AttributeEntity</summary>
	public partial class AttributeFields
	{
		/// <summary>Creates a new AttributeEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeFieldIndex.Id);}
		}
		/// <summary>Creates a new AttributeEntity.Value field instance</summary>
		public static EntityField2 Value
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeFieldIndex.Value);}
		}
	}

	/// <summary>Field Creation Class for entity AttributeMetadataEntity</summary>
	public partial class AttributeMetadataFields
	{
		/// <summary>Creates a new AttributeMetadataEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeMetadataFieldIndex.Id);}
		}
		/// <summary>Creates a new AttributeMetadataEntity.ClassId field instance</summary>
		public static EntityField2 ClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeMetadataFieldIndex.ClassId);}
		}
		/// <summary>Creates a new AttributeMetadataEntity.DataTypeId field instance</summary>
		public static EntityField2 DataTypeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeMetadataFieldIndex.DataTypeId);}
		}
		/// <summary>Creates a new AttributeMetadataEntity.AttributeId field instance</summary>
		public static EntityField2 AttributeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeMetadataFieldIndex.AttributeId);}
		}
		/// <summary>Creates a new AttributeMetadataEntity.IsNullable field instance</summary>
		public static EntityField2 IsNullable
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeMetadataFieldIndex.IsNullable);}
		}
		/// <summary>Creates a new AttributeMetadataEntity.IsImmutable field instance</summary>
		public static EntityField2 IsImmutable
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeMetadataFieldIndex.IsImmutable);}
		}
	}

	/// <summary>Field Creation Class for entity AttributeValueEntity</summary>
	public partial class AttributeValueFields
	{
		/// <summary>Creates a new AttributeValueEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeValueFieldIndex.Id);}
		}
		/// <summary>Creates a new AttributeValueEntity.ObjectRevisionId field instance</summary>
		public static EntityField2 ObjectRevisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeValueFieldIndex.ObjectRevisionId);}
		}
		/// <summary>Creates a new AttributeValueEntity.AttributeMetadataId field instance</summary>
		public static EntityField2 AttributeMetadataId
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeValueFieldIndex.AttributeMetadataId);}
		}
		/// <summary>Creates a new AttributeValueEntity.Value field instance</summary>
		public static EntityField2 Value
		{
			get { return (EntityField2)EntityFieldFactory.Create(AttributeValueFieldIndex.Value);}
		}
	}

	/// <summary>Field Creation Class for entity BranchHeadObjectEntity</summary>
	public partial class BranchHeadObjectFields
	{
		/// <summary>Creates a new BranchHeadObjectEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(BranchHeadObjectFieldIndex.Id);}
		}
		/// <summary>Creates a new BranchHeadObjectEntity.BranchId field instance</summary>
		public static EntityField2 BranchId
		{
			get { return (EntityField2)EntityFieldFactory.Create(BranchHeadObjectFieldIndex.BranchId);}
		}
		/// <summary>Creates a new BranchHeadObjectEntity.ClassId field instance</summary>
		public static EntityField2 ClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(BranchHeadObjectFieldIndex.ClassId);}
		}
		/// <summary>Creates a new BranchHeadObjectEntity.ObjectId field instance</summary>
		public static EntityField2 ObjectId
		{
			get { return (EntityField2)EntityFieldFactory.Create(BranchHeadObjectFieldIndex.ObjectId);}
		}
		/// <summary>Creates a new BranchHeadObjectEntity.ObjectRevisionId field instance</summary>
		public static EntityField2 ObjectRevisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(BranchHeadObjectFieldIndex.ObjectRevisionId);}
		}
		/// <summary>Creates a new BranchHeadObjectEntity.RevisionId field instance</summary>
		public static EntityField2 RevisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(BranchHeadObjectFieldIndex.RevisionId);}
		}
	}

	/// <summary>Field Creation Class for entity ClassEntity</summary>
	public partial class ClassFields
	{
		/// <summary>Creates a new ClassEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClassFieldIndex.Id);}
		}
		/// <summary>Creates a new ClassEntity.Name field instance</summary>
		public static EntityField2 Name
		{
			get { return (EntityField2)EntityFieldFactory.Create(ClassFieldIndex.Name);}
		}
	}

	/// <summary>Field Creation Class for entity DataTypeEntity</summary>
	public partial class DataTypeFields
	{
		/// <summary>Creates a new DataTypeEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(DataTypeFieldIndex.Id);}
		}
		/// <summary>Creates a new DataTypeEntity.Value field instance</summary>
		public static EntityField2 Value
		{
			get { return (EntityField2)EntityFieldFactory.Create(DataTypeFieldIndex.Value);}
		}
	}

	/// <summary>Field Creation Class for entity ObjectEntity</summary>
	public partial class ObjectFields
	{
		/// <summary>Creates a new ObjectEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(ObjectFieldIndex.Id);}
		}
		/// <summary>Creates a new ObjectEntity.ClassId field instance</summary>
		public static EntityField2 ClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ObjectFieldIndex.ClassId);}
		}
	}

	/// <summary>Field Creation Class for entity ObjectRevisionEntity</summary>
	public partial class ObjectRevisionFields
	{
		/// <summary>Creates a new ObjectRevisionEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(ObjectRevisionFieldIndex.Id);}
		}
		/// <summary>Creates a new ObjectRevisionEntity.ObjectId field instance</summary>
		public static EntityField2 ObjectId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ObjectRevisionFieldIndex.ObjectId);}
		}
		/// <summary>Creates a new ObjectRevisionEntity.RevisionId field instance</summary>
		public static EntityField2 RevisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(ObjectRevisionFieldIndex.RevisionId);}
		}
		/// <summary>Creates a new ObjectRevisionEntity.IsDeleted field instance</summary>
		public static EntityField2 IsDeleted
		{
			get { return (EntityField2)EntityFieldFactory.Create(ObjectRevisionFieldIndex.IsDeleted);}
		}
	}

	/// <summary>Field Creation Class for entity RelationAttributeValueEntity</summary>
	public partial class RelationAttributeValueFields
	{
		/// <summary>Creates a new RelationAttributeValueEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationAttributeValueFieldIndex.Id);}
		}
		/// <summary>Creates a new RelationAttributeValueEntity.ObjectRevisionId field instance</summary>
		public static EntityField2 ObjectRevisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationAttributeValueFieldIndex.ObjectRevisionId);}
		}
		/// <summary>Creates a new RelationAttributeValueEntity.RelationshipMetadataId field instance</summary>
		public static EntityField2 RelationshipMetadataId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationAttributeValueFieldIndex.RelationshipMetadataId);}
		}
		/// <summary>Creates a new RelationAttributeValueEntity.Value field instance</summary>
		public static EntityField2 Value
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationAttributeValueFieldIndex.Value);}
		}
	}

	/// <summary>Field Creation Class for entity RelationshipMetadataEntity</summary>
	public partial class RelationshipMetadataFields
	{
		/// <summary>Creates a new RelationshipMetadataEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationshipMetadataFieldIndex.Id);}
		}
		/// <summary>Creates a new RelationshipMetadataEntity.PkClassId field instance</summary>
		public static EntityField2 PkClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationshipMetadataFieldIndex.PkClassId);}
		}
		/// <summary>Creates a new RelationshipMetadataEntity.FkClassId field instance</summary>
		public static EntityField2 FkClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationshipMetadataFieldIndex.FkClassId);}
		}
		/// <summary>Creates a new RelationshipMetadataEntity.AttributeId field instance</summary>
		public static EntityField2 AttributeId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationshipMetadataFieldIndex.AttributeId);}
		}
		/// <summary>Creates a new RelationshipMetadataEntity.IsNullable field instance</summary>
		public static EntityField2 IsNullable
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationshipMetadataFieldIndex.IsNullable);}
		}
		/// <summary>Creates a new RelationshipMetadataEntity.OneToManyValue field instance</summary>
		public static EntityField2 OneToManyValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationshipMetadataFieldIndex.OneToManyValue);}
		}
		/// <summary>Creates a new RelationshipMetadataEntity.CascadeBehavior field instance</summary>
		public static EntityField2 CascadeBehavior
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationshipMetadataFieldIndex.CascadeBehavior);}
		}
		/// <summary>Creates a new RelationshipMetadataEntity.CascadeDefaultValue field instance</summary>
		public static EntityField2 CascadeDefaultValue
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationshipMetadataFieldIndex.CascadeDefaultValue);}
		}
		/// <summary>Creates a new RelationshipMetadataEntity.IsImmutable field instance</summary>
		public static EntityField2 IsImmutable
		{
			get { return (EntityField2)EntityFieldFactory.Create(RelationshipMetadataFieldIndex.IsImmutable);}
		}
	}

	/// <summary>Field Creation Class for entity RevisionEntity</summary>
	public partial class RevisionFields
	{
		/// <summary>Creates a new RevisionEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionFieldIndex.Id);}
		}
		/// <summary>Creates a new RevisionEntity.BranchId field instance</summary>
		public static EntityField2 BranchId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionFieldIndex.BranchId);}
		}
		/// <summary>Creates a new RevisionEntity.Tag field instance</summary>
		public static EntityField2 Tag
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionFieldIndex.Tag);}
		}
		/// <summary>Creates a new RevisionEntity.TimeStamp field instance</summary>
		public static EntityField2 TimeStamp
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionFieldIndex.TimeStamp);}
		}
		/// <summary>Creates a new RevisionEntity.CreatedBy field instance</summary>
		public static EntityField2 CreatedBy
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new RevisionEntity.Comments field instance</summary>
		public static EntityField2 Comments
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionFieldIndex.Comments);}
		}
	}

	/// <summary>Field Creation Class for entity RevisionHeadObjectEntity</summary>
	public partial class RevisionHeadObjectFields
	{
		/// <summary>Creates a new RevisionHeadObjectEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionHeadObjectFieldIndex.Id);}
		}
		/// <summary>Creates a new RevisionHeadObjectEntity.BranchId field instance</summary>
		public static EntityField2 BranchId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionHeadObjectFieldIndex.BranchId);}
		}
		/// <summary>Creates a new RevisionHeadObjectEntity.ClassId field instance</summary>
		public static EntityField2 ClassId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionHeadObjectFieldIndex.ClassId);}
		}
		/// <summary>Creates a new RevisionHeadObjectEntity.ObjectId field instance</summary>
		public static EntityField2 ObjectId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionHeadObjectFieldIndex.ObjectId);}
		}
		/// <summary>Creates a new RevisionHeadObjectEntity.ObjectRevisionId field instance</summary>
		public static EntityField2 ObjectRevisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionHeadObjectFieldIndex.ObjectRevisionId);}
		}
		/// <summary>Creates a new RevisionHeadObjectEntity.RevisionId field instance</summary>
		public static EntityField2 RevisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(RevisionHeadObjectFieldIndex.RevisionId);}
		}
	}
	

	/// <summary>Field Creation Class for typedview BranchesTypedView</summary>
	public partial class BranchesFields
	{
		/// <summary>Creates a new BranchesTypedView.BranchId field instance</summary>
		public static EntityField2 BranchId
		{
			get { return (EntityField2)EntityFieldFactory.Create(BranchesFieldIndex.BranchId);}
		}
	}

	/// <summary>Field Creation Class for typedview IndexedRevisionsTypedView</summary>
	public partial class IndexedRevisionsFields
	{
		/// <summary>Creates a new IndexedRevisionsTypedView.RevisionId field instance</summary>
		public static EntityField2 RevisionId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IndexedRevisionsFieldIndex.RevisionId);}
		}

		/// <summary>Creates a new IndexedRevisionsTypedView.BranchId field instance</summary>
		public static EntityField2 BranchId
		{
			get { return (EntityField2)EntityFieldFactory.Create(IndexedRevisionsFieldIndex.BranchId);}
		}
	}
}