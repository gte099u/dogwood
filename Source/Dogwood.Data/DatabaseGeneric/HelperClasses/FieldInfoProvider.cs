﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:02 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>
	/// Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.
	/// </summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal sealed class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion
		
		/// <summary>private ctor to prevent instances of this class.</summary>
		private FieldInfoProviderSingleton()
		{
		}

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			base.InitClass( (12 + 2));
			InitAttributeEntityInfos();
			InitAttributeMetadataEntityInfos();
			InitAttributeValueEntityInfos();
			InitBranchHeadObjectEntityInfos();
			InitClassEntityInfos();
			InitDataTypeEntityInfos();
			InitObjectEntityInfos();
			InitObjectRevisionEntityInfos();
			InitRelationAttributeValueEntityInfos();
			InitRelationshipMetadataEntityInfos();
			InitRevisionEntityInfos();
			InitRevisionHeadObjectEntityInfos();
			InitBranchesTypedViewInfos();
			InitIndexedRevisionsTypedViewInfos();
			base.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits AttributeEntity's FieldInfo objects</summary>
		private void InitAttributeEntityInfos()
		{
			base.AddElementFieldInfo("AttributeEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)AttributeFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("AttributeEntity", "Value", typeof(System.String), false, false, false, false,  (int)AttributeFieldIndex.Value, 50, 0, 0);
		}
		/// <summary>Inits AttributeMetadataEntity's FieldInfo objects</summary>
		private void InitAttributeMetadataEntityInfos()
		{
			base.AddElementFieldInfo("AttributeMetadataEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)AttributeMetadataFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("AttributeMetadataEntity", "ClassId", typeof(System.Int32), false, true, false, false,  (int)AttributeMetadataFieldIndex.ClassId, 0, 0, 10);
			base.AddElementFieldInfo("AttributeMetadataEntity", "DataTypeId", typeof(System.Int32), false, true, false, false,  (int)AttributeMetadataFieldIndex.DataTypeId, 0, 0, 10);
			base.AddElementFieldInfo("AttributeMetadataEntity", "AttributeId", typeof(System.Int32), false, true, false, false,  (int)AttributeMetadataFieldIndex.AttributeId, 0, 0, 10);
			base.AddElementFieldInfo("AttributeMetadataEntity", "IsNullable", typeof(System.Boolean), false, false, false, false,  (int)AttributeMetadataFieldIndex.IsNullable, 0, 0, 0);
			base.AddElementFieldInfo("AttributeMetadataEntity", "IsImmutable", typeof(System.Boolean), false, false, false, false,  (int)AttributeMetadataFieldIndex.IsImmutable, 0, 0, 0);
		}
		/// <summary>Inits AttributeValueEntity's FieldInfo objects</summary>
		private void InitAttributeValueEntityInfos()
		{
			base.AddElementFieldInfo("AttributeValueEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)AttributeValueFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("AttributeValueEntity", "ObjectRevisionId", typeof(System.Int32), false, true, false, false,  (int)AttributeValueFieldIndex.ObjectRevisionId, 0, 0, 10);
			base.AddElementFieldInfo("AttributeValueEntity", "AttributeMetadataId", typeof(System.Int32), false, true, false, false,  (int)AttributeValueFieldIndex.AttributeMetadataId, 0, 0, 10);
			base.AddElementFieldInfo("AttributeValueEntity", "Value", typeof(System.Byte[]), false, false, false, false,  (int)AttributeValueFieldIndex.Value, 2147483647, 0, 0);
		}
		/// <summary>Inits BranchHeadObjectEntity's FieldInfo objects</summary>
		private void InitBranchHeadObjectEntityInfos()
		{
			base.AddElementFieldInfo("BranchHeadObjectEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)BranchHeadObjectFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("BranchHeadObjectEntity", "BranchId", typeof(System.Int32), false, true, false, false,  (int)BranchHeadObjectFieldIndex.BranchId, 0, 0, 10);
			base.AddElementFieldInfo("BranchHeadObjectEntity", "ClassId", typeof(System.Int32), false, true, false, false,  (int)BranchHeadObjectFieldIndex.ClassId, 0, 0, 10);
			base.AddElementFieldInfo("BranchHeadObjectEntity", "ObjectId", typeof(System.Int32), false, true, false, false,  (int)BranchHeadObjectFieldIndex.ObjectId, 0, 0, 10);
			base.AddElementFieldInfo("BranchHeadObjectEntity", "ObjectRevisionId", typeof(System.Int32), false, true, false, false,  (int)BranchHeadObjectFieldIndex.ObjectRevisionId, 0, 0, 10);
			base.AddElementFieldInfo("BranchHeadObjectEntity", "RevisionId", typeof(System.Int32), false, true, false, false,  (int)BranchHeadObjectFieldIndex.RevisionId, 0, 0, 10);
		}
		/// <summary>Inits ClassEntity's FieldInfo objects</summary>
		private void InitClassEntityInfos()
		{
			base.AddElementFieldInfo("ClassEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)ClassFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("ClassEntity", "Name", typeof(System.String), false, false, false, false,  (int)ClassFieldIndex.Name, 50, 0, 0);
		}
		/// <summary>Inits DataTypeEntity's FieldInfo objects</summary>
		private void InitDataTypeEntityInfos()
		{
			base.AddElementFieldInfo("DataTypeEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)DataTypeFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("DataTypeEntity", "Value", typeof(System.String), false, false, false, false,  (int)DataTypeFieldIndex.Value, 50, 0, 0);
		}
		/// <summary>Inits ObjectEntity's FieldInfo objects</summary>
		private void InitObjectEntityInfos()
		{
			base.AddElementFieldInfo("ObjectEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)ObjectFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("ObjectEntity", "ClassId", typeof(System.Int32), false, true, false, false,  (int)ObjectFieldIndex.ClassId, 0, 0, 10);
		}
		/// <summary>Inits ObjectRevisionEntity's FieldInfo objects</summary>
		private void InitObjectRevisionEntityInfos()
		{
			base.AddElementFieldInfo("ObjectRevisionEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)ObjectRevisionFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("ObjectRevisionEntity", "ObjectId", typeof(System.Int32), false, true, false, false,  (int)ObjectRevisionFieldIndex.ObjectId, 0, 0, 10);
			base.AddElementFieldInfo("ObjectRevisionEntity", "RevisionId", typeof(System.Int32), false, true, false, false,  (int)ObjectRevisionFieldIndex.RevisionId, 0, 0, 10);
			base.AddElementFieldInfo("ObjectRevisionEntity", "IsDeleted", typeof(System.Boolean), false, false, false, false,  (int)ObjectRevisionFieldIndex.IsDeleted, 0, 0, 0);
		}
		/// <summary>Inits RelationAttributeValueEntity's FieldInfo objects</summary>
		private void InitRelationAttributeValueEntityInfos()
		{
			base.AddElementFieldInfo("RelationAttributeValueEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)RelationAttributeValueFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("RelationAttributeValueEntity", "ObjectRevisionId", typeof(System.Int32), false, true, false, false,  (int)RelationAttributeValueFieldIndex.ObjectRevisionId, 0, 0, 10);
			base.AddElementFieldInfo("RelationAttributeValueEntity", "RelationshipMetadataId", typeof(System.Int32), false, true, false, false,  (int)RelationAttributeValueFieldIndex.RelationshipMetadataId, 0, 0, 10);
			base.AddElementFieldInfo("RelationAttributeValueEntity", "Value", typeof(System.Int32), false, true, false, false,  (int)RelationAttributeValueFieldIndex.Value, 0, 0, 10);
		}
		/// <summary>Inits RelationshipMetadataEntity's FieldInfo objects</summary>
		private void InitRelationshipMetadataEntityInfos()
		{
			base.AddElementFieldInfo("RelationshipMetadataEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)RelationshipMetadataFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("RelationshipMetadataEntity", "PkClassId", typeof(System.Int32), false, true, false, false,  (int)RelationshipMetadataFieldIndex.PkClassId, 0, 0, 10);
			base.AddElementFieldInfo("RelationshipMetadataEntity", "FkClassId", typeof(System.Int32), false, true, false, false,  (int)RelationshipMetadataFieldIndex.FkClassId, 0, 0, 10);
			base.AddElementFieldInfo("RelationshipMetadataEntity", "AttributeId", typeof(System.Int32), false, true, false, false,  (int)RelationshipMetadataFieldIndex.AttributeId, 0, 0, 10);
			base.AddElementFieldInfo("RelationshipMetadataEntity", "IsNullable", typeof(System.Boolean), false, false, false, false,  (int)RelationshipMetadataFieldIndex.IsNullable, 0, 0, 0);
			base.AddElementFieldInfo("RelationshipMetadataEntity", "OneToManyValue", typeof(System.String), false, false, false, true,  (int)RelationshipMetadataFieldIndex.OneToManyValue, 50, 0, 0);
			base.AddElementFieldInfo("RelationshipMetadataEntity", "CascadeBehavior", typeof(System.Int32), false, false, false, false,  (int)RelationshipMetadataFieldIndex.CascadeBehavior, 0, 0, 10);
			base.AddElementFieldInfo("RelationshipMetadataEntity", "CascadeDefaultValue", typeof(Nullable<System.Int32>), false, true, false, true,  (int)RelationshipMetadataFieldIndex.CascadeDefaultValue, 0, 0, 10);
			base.AddElementFieldInfo("RelationshipMetadataEntity", "IsImmutable", typeof(System.Boolean), false, false, false, false,  (int)RelationshipMetadataFieldIndex.IsImmutable, 0, 0, 0);
		}
		/// <summary>Inits RevisionEntity's FieldInfo objects</summary>
		private void InitRevisionEntityInfos()
		{
			base.AddElementFieldInfo("RevisionEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)RevisionFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("RevisionEntity", "BranchId", typeof(System.Int32), false, true, false, false,  (int)RevisionFieldIndex.BranchId, 0, 0, 10);
			base.AddElementFieldInfo("RevisionEntity", "Tag", typeof(System.String), false, false, false, true,  (int)RevisionFieldIndex.Tag, 50, 0, 0);
			base.AddElementFieldInfo("RevisionEntity", "TimeStamp", typeof(System.DateTime), false, false, false, false,  (int)RevisionFieldIndex.TimeStamp, 0, 0, 0);
			base.AddElementFieldInfo("RevisionEntity", "CreatedBy", typeof(Nullable<System.Int32>), false, false, false, true,  (int)RevisionFieldIndex.CreatedBy, 0, 0, 10);
			base.AddElementFieldInfo("RevisionEntity", "Comments", typeof(System.String), false, false, false, true,  (int)RevisionFieldIndex.Comments, 255, 0, 0);
		}
		/// <summary>Inits RevisionHeadObjectEntity's FieldInfo objects</summary>
		private void InitRevisionHeadObjectEntityInfos()
		{
			base.AddElementFieldInfo("RevisionHeadObjectEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)RevisionHeadObjectFieldIndex.Id, 0, 0, 10);
			base.AddElementFieldInfo("RevisionHeadObjectEntity", "BranchId", typeof(System.Int32), false, true, false, false,  (int)RevisionHeadObjectFieldIndex.BranchId, 0, 0, 10);
			base.AddElementFieldInfo("RevisionHeadObjectEntity", "ClassId", typeof(System.Int32), false, true, false, false,  (int)RevisionHeadObjectFieldIndex.ClassId, 0, 0, 10);
			base.AddElementFieldInfo("RevisionHeadObjectEntity", "ObjectId", typeof(System.Int32), false, true, false, false,  (int)RevisionHeadObjectFieldIndex.ObjectId, 0, 0, 10);
			base.AddElementFieldInfo("RevisionHeadObjectEntity", "ObjectRevisionId", typeof(System.Int32), false, true, false, false,  (int)RevisionHeadObjectFieldIndex.ObjectRevisionId, 0, 0, 10);
			base.AddElementFieldInfo("RevisionHeadObjectEntity", "RevisionId", typeof(System.Int32), false, true, false, false,  (int)RevisionHeadObjectFieldIndex.RevisionId, 0, 0, 10);
		}

		/// <summary>Inits View's FieldInfo objects</summary>
		private void InitBranchesTypedViewInfos()
		{
			base.AddElementFieldInfo("BranchesTypedView", "BranchId", typeof(System.Int32), false, false, true, false, (int)BranchesFieldIndex.BranchId, 0, 0, 10);
		}
		/// <summary>Inits View's FieldInfo objects</summary>
		private void InitIndexedRevisionsTypedViewInfos()
		{
			base.AddElementFieldInfo("IndexedRevisionsTypedView", "RevisionId", typeof(System.Int32), false, false, true, false, (int)IndexedRevisionsFieldIndex.RevisionId, 0, 0, 10);
			base.AddElementFieldInfo("IndexedRevisionsTypedView", "BranchId", typeof(System.Int32), false, false, true, false, (int)IndexedRevisionsFieldIndex.BranchId, 0, 0, 10);
		}		
	}
}




