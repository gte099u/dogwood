﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:02 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace Dogwood.Data
{

	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Attribute.
	/// </summary>
	public enum AttributeFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AttributeMetadata.
	/// </summary>
	public enum AttributeMetadataFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>ClassId. </summary>
		ClassId,
		///<summary>DataTypeId. </summary>
		DataTypeId,
		///<summary>AttributeId. </summary>
		AttributeId,
		///<summary>IsNullable. </summary>
		IsNullable,
		///<summary>IsImmutable. </summary>
		IsImmutable,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AttributeValue.
	/// </summary>
	public enum AttributeValueFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>ObjectRevisionId. </summary>
		ObjectRevisionId,
		///<summary>AttributeMetadataId. </summary>
		AttributeMetadataId,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BranchHeadObject.
	/// </summary>
	public enum BranchHeadObjectFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>BranchId. </summary>
		BranchId,
		///<summary>ClassId. </summary>
		ClassId,
		///<summary>ObjectId. </summary>
		ObjectId,
		///<summary>ObjectRevisionId. </summary>
		ObjectRevisionId,
		///<summary>RevisionId. </summary>
		RevisionId,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Class.
	/// </summary>
	public enum ClassFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DataType.
	/// </summary>
	public enum DataTypeFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Object.
	/// </summary>
	public enum ObjectFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>ClassId. </summary>
		ClassId,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ObjectRevision.
	/// </summary>
	public enum ObjectRevisionFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>ObjectId. </summary>
		ObjectId,
		///<summary>RevisionId. </summary>
		RevisionId,
		///<summary>IsDeleted. </summary>
		IsDeleted,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RelationAttributeValue.
	/// </summary>
	public enum RelationAttributeValueFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>ObjectRevisionId. </summary>
		ObjectRevisionId,
		///<summary>RelationshipMetadataId. </summary>
		RelationshipMetadataId,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RelationshipMetadata.
	/// </summary>
	public enum RelationshipMetadataFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>PkClassId. </summary>
		PkClassId,
		///<summary>FkClassId. </summary>
		FkClassId,
		///<summary>AttributeId. </summary>
		AttributeId,
		///<summary>IsNullable. </summary>
		IsNullable,
		///<summary>OneToManyValue. </summary>
		OneToManyValue,
		///<summary>CascadeBehavior. </summary>
		CascadeBehavior,
		///<summary>CascadeDefaultValue. </summary>
		CascadeDefaultValue,
		///<summary>IsImmutable. </summary>
		IsImmutable,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Revision.
	/// </summary>
	public enum RevisionFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>BranchId. </summary>
		BranchId,
		///<summary>Tag. </summary>
		Tag,
		///<summary>TimeStamp. </summary>
		TimeStamp,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Comments. </summary>
		Comments,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RevisionHeadObject.
	/// </summary>
	public enum RevisionHeadObjectFieldIndex:int
	{
		///<summary>Id. </summary>
		Id,
		///<summary>BranchId. </summary>
		BranchId,
		///<summary>ClassId. </summary>
		ClassId,
		///<summary>ObjectId. </summary>
		ObjectId,
		///<summary>ObjectRevisionId. </summary>
		ObjectRevisionId,
		///<summary>RevisionId. </summary>
		RevisionId,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : Branches.
	/// </summary>
	public enum BranchesFieldIndex:int
	{
		///<summary>BranchId</summary>
		BranchId,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>
	/// Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : IndexedRevisions.
	/// </summary>
	public enum IndexedRevisionsFieldIndex:int
	{
		///<summary>RevisionId</summary>
		RevisionId,
		///<summary>BranchId</summary>
		BranchId,
		/// <summary></summary>
		AmountOfFields
	}




	/// <summary>
	/// Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.
	/// </summary>
	public enum EntityType:int
	{
		///<summary>Attribute</summary>
		AttributeEntity,
		///<summary>AttributeMetadata</summary>
		AttributeMetadataEntity,
		///<summary>AttributeValue</summary>
		AttributeValueEntity,
		///<summary>BranchHeadObject</summary>
		BranchHeadObjectEntity,
		///<summary>Class</summary>
		ClassEntity,
		///<summary>DataType</summary>
		DataTypeEntity,
		///<summary>Object</summary>
		ObjectEntity,
		///<summary>ObjectRevision</summary>
		ObjectRevisionEntity,
		///<summary>RelationAttributeValue</summary>
		RelationAttributeValueEntity,
		///<summary>RelationshipMetadata</summary>
		RelationshipMetadataEntity,
		///<summary>Revision</summary>
		RevisionEntity,
		///<summary>RevisionHeadObject</summary>
		RevisionHeadObjectEntity
	}



	/// <summary>
	/// Enum definition for all the typed view types defined in this namespace. Used by the entityfields factory.
	/// </summary>
	public enum TypedViewType:int
	{
		///<summary>Branches</summary>
		BranchesTypedView,
		///<summary>IndexedRevisions</summary>
		IndexedRevisionsTypedView
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	#endregion

	#region Included code

	#endregion
}


