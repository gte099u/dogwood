﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:04 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.HelperClasses;
using Dogwood.Data.RelationClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.FactoryClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>general base class for the generated factories</summary>
	[Serializable]
	public partial class EntityFactoryBase2 : EntityFactoryCore2
	{
		private string _entityName;
		private Dogwood.Data.EntityType _typeOfEntity;
		
		/// <summary>CTor</summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="typeOfEntity">The type of entity.</param>
		public EntityFactoryBase2(string entityName, Dogwood.Data.EntityType typeOfEntity)
		{
			_entityName = entityName;
			_typeOfEntity = typeOfEntity;
		}
		
		/// <summary>Creates, using the generated EntityFieldsFactory, the IEntityFields2 object for the entity to create.</summary>
		/// <returns>Empty IEntityFields2 object.</returns>
		public override IEntityFields2 CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(_typeOfEntity);
		}
		
		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity2 CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.Create((Dogwood.Data.EntityType)entityTypeValue);
		}

		/// <summary>Creates the relations collection to the entity to join all targets so this entity can be fetched. </summary>
		/// <param name="objectAlias">The object alias to use for the elements in the relations.</param>
		/// <returns>null if the entity isn't in a hierarchy of type TargetPerEntity, otherwise the relations collection needed to join all targets together to fetch all subtypes of this entity and this entity itself</returns>
		public override IRelationCollection CreateHierarchyRelations(string objectAlias) 
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetHierarchyRelations(_entityName, objectAlias);
		}

		/// <summary>This method retrieves, using the InheritanceInfoprovider, the factory for the entity represented by the values passed in.</summary>
		/// <param name="fieldValues">Field values read from the db, to determine which factory to return, based on the field values passed in.</param>
		/// <param name="entityFieldStartIndexesPerEntity">indexes into values where per entity type their own fields start.</param>
		/// <returns>the factory for the entity which is represented by the values passed in.</returns>
		public override IEntityFactory2 GetEntityFactory(object[] fieldValues, Dictionary<string, int> entityFieldStartIndexesPerEntity) 
		{
			IEntityFactory2 toReturn = (IEntityFactory2)InheritanceInfoProviderSingleton.GetInstance().GetEntityFactory(_entityName, fieldValues, entityFieldStartIndexesPerEntity);
			if(toReturn == null)
			{
				toReturn = this;
			}
			return toReturn;
		}
		
		/// <summary>Gets a predicateexpression which filters on the entity with type belonging to this factory.</summary>
		/// <param name="negate">Flag to produce a NOT filter, (true), or a normal filter (false). </param>
		/// <param name="objectAlias">The object alias to use for the predicate(s).</param>
		/// <returns>ready to use predicateexpression, or an empty predicate expression if the belonging entity isn't a hierarchical type.</returns>
		public override IPredicateExpression GetEntityTypeFilter(bool negate, string objectAlias) 
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetEntityTypeFilter(this.ForEntityName, objectAlias, negate);
		}
				
		/// <summary>returns the name of the entity this factory is for, e.g. "EmployeeEntity"</summary>
		public override string ForEntityName 
		{ 
			get { return _entityName; }
		}
	}
	
	/// <summary>Factory to create new, empty AttributeEntity objects.</summary>
	[Serializable]
	public partial class AttributeEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public AttributeEntityFactory() : base("AttributeEntity", Dogwood.Data.EntityType.AttributeEntity) { }

		/// <summary>Creates a new, empty AttributeEntity object.</summary>
		/// <returns>A new, empty AttributeEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new AttributeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttribute
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new AttributeEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new AttributeEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttributeUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<AttributeEntity>(this);
		}
		

		#region Included Code

		#endregion
	}	
	/// <summary>Factory to create new, empty AttributeMetadataEntity objects.</summary>
	[Serializable]
	public partial class AttributeMetadataEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public AttributeMetadataEntityFactory() : base("AttributeMetadataEntity", Dogwood.Data.EntityType.AttributeMetadataEntity) { }

		/// <summary>Creates a new, empty AttributeMetadataEntity object.</summary>
		/// <returns>A new, empty AttributeMetadataEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new AttributeMetadataEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttributeMetadata
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new AttributeMetadataEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new AttributeMetadataEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttributeMetadataUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<AttributeMetadataEntity>(this);
		}
		

		#region Included Code

		#endregion
	}	
	/// <summary>Factory to create new, empty AttributeValueEntity objects.</summary>
	[Serializable]
	public partial class AttributeValueEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public AttributeValueEntityFactory() : base("AttributeValueEntity", Dogwood.Data.EntityType.AttributeValueEntity) { }

		/// <summary>Creates a new, empty AttributeValueEntity object.</summary>
		/// <returns>A new, empty AttributeValueEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new AttributeValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttributeValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new AttributeValueEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new AttributeValueEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttributeValueUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<AttributeValueEntity>(this);
		}
		

		#region Included Code

		#endregion
	}	
	/// <summary>Factory to create new, empty BranchHeadObjectEntity objects.</summary>
	[Serializable]
	public partial class BranchHeadObjectEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public BranchHeadObjectEntityFactory() : base("BranchHeadObjectEntity", Dogwood.Data.EntityType.BranchHeadObjectEntity) { }

		/// <summary>Creates a new, empty BranchHeadObjectEntity object.</summary>
		/// <returns>A new, empty BranchHeadObjectEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new BranchHeadObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBranchHeadObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new BranchHeadObjectEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new BranchHeadObjectEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBranchHeadObjectUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<BranchHeadObjectEntity>(this);
		}
		

		#region Included Code

		#endregion
	}	
	/// <summary>Factory to create new, empty ClassEntity objects.</summary>
	[Serializable]
	public partial class ClassEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public ClassEntityFactory() : base("ClassEntity", Dogwood.Data.EntityType.ClassEntity) { }

		/// <summary>Creates a new, empty ClassEntity object.</summary>
		/// <returns>A new, empty ClassEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new ClassEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new ClassEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ClassEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClassUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<ClassEntity>(this);
		}
		

		#region Included Code

		#endregion
	}	
	/// <summary>Factory to create new, empty DataTypeEntity objects.</summary>
	[Serializable]
	public partial class DataTypeEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public DataTypeEntityFactory() : base("DataTypeEntity", Dogwood.Data.EntityType.DataTypeEntity) { }

		/// <summary>Creates a new, empty DataTypeEntity object.</summary>
		/// <returns>A new, empty DataTypeEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new DataTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDataType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new DataTypeEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new DataTypeEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDataTypeUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<DataTypeEntity>(this);
		}
		

		#region Included Code

		#endregion
	}	
	/// <summary>Factory to create new, empty ObjectEntity objects.</summary>
	[Serializable]
	public partial class ObjectEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public ObjectEntityFactory() : base("ObjectEntity", Dogwood.Data.EntityType.ObjectEntity) { }

		/// <summary>Creates a new, empty ObjectEntity object.</summary>
		/// <returns>A new, empty ObjectEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new ObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new ObjectEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ObjectEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewObjectUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<ObjectEntity>(this);
		}
		

		#region Included Code

		#endregion
	}	
	/// <summary>Factory to create new, empty ObjectRevisionEntity objects.</summary>
	[Serializable]
	public partial class ObjectRevisionEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public ObjectRevisionEntityFactory() : base("ObjectRevisionEntity", Dogwood.Data.EntityType.ObjectRevisionEntity) { }

		/// <summary>Creates a new, empty ObjectRevisionEntity object.</summary>
		/// <returns>A new, empty ObjectRevisionEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new ObjectRevisionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewObjectRevision
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new ObjectRevisionEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new ObjectRevisionEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewObjectRevisionUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<ObjectRevisionEntity>(this);
		}
		

		#region Included Code

		#endregion
	}	
	/// <summary>Factory to create new, empty RelationAttributeValueEntity objects.</summary>
	[Serializable]
	public partial class RelationAttributeValueEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public RelationAttributeValueEntityFactory() : base("RelationAttributeValueEntity", Dogwood.Data.EntityType.RelationAttributeValueEntity) { }

		/// <summary>Creates a new, empty RelationAttributeValueEntity object.</summary>
		/// <returns>A new, empty RelationAttributeValueEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new RelationAttributeValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRelationAttributeValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new RelationAttributeValueEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new RelationAttributeValueEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRelationAttributeValueUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<RelationAttributeValueEntity>(this);
		}
		

		#region Included Code

		#endregion
	}	
	/// <summary>Factory to create new, empty RelationshipMetadataEntity objects.</summary>
	[Serializable]
	public partial class RelationshipMetadataEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public RelationshipMetadataEntityFactory() : base("RelationshipMetadataEntity", Dogwood.Data.EntityType.RelationshipMetadataEntity) { }

		/// <summary>Creates a new, empty RelationshipMetadataEntity object.</summary>
		/// <returns>A new, empty RelationshipMetadataEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new RelationshipMetadataEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRelationshipMetadata
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new RelationshipMetadataEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new RelationshipMetadataEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRelationshipMetadataUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<RelationshipMetadataEntity>(this);
		}
		

		#region Included Code

		#endregion
	}	
	/// <summary>Factory to create new, empty RevisionEntity objects.</summary>
	[Serializable]
	public partial class RevisionEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public RevisionEntityFactory() : base("RevisionEntity", Dogwood.Data.EntityType.RevisionEntity) { }

		/// <summary>Creates a new, empty RevisionEntity object.</summary>
		/// <returns>A new, empty RevisionEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new RevisionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRevision
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new RevisionEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new RevisionEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRevisionUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<RevisionEntity>(this);
		}
		

		#region Included Code

		#endregion
	}	
	/// <summary>Factory to create new, empty RevisionHeadObjectEntity objects.</summary>
	[Serializable]
	public partial class RevisionHeadObjectEntityFactory : EntityFactoryBase2 {
		/// <summary>CTor</summary>
		public RevisionHeadObjectEntityFactory() : base("RevisionHeadObjectEntity", Dogwood.Data.EntityType.RevisionHeadObjectEntity) { }

		/// <summary>Creates a new, empty RevisionHeadObjectEntity object.</summary>
		/// <returns>A new, empty RevisionHeadObjectEntity object.</returns>
		public override IEntity2 Create() {
			IEntity2 toReturn = new RevisionHeadObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRevisionHeadObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new RevisionHeadObjectEntity instance but uses a special constructor which will set the Fields object of the new IEntity2 instance to the passed in fields object.</summary>
		/// <param name="fields">Populated IEntityFields2 object for the new IEntity2 to create</param>
		/// <returns>Fully created and populated (due to the IEntityFields2 object) IEntity2 object</returns>
		public override IEntity2 Create(IEntityFields2 fields) {
			IEntity2 toReturn = new RevisionHeadObjectEntity(fields);
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRevisionHeadObjectUsingFields
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}
		
		/// <summary>Creates a new generic EntityCollection(Of T) for the entity to which this factory belongs.</summary>
		/// <returns>ready to use generic EntityCollection(Of T) with this factory set as the factory</returns>
		public override IEntityCollection2 CreateEntityCollection()
		{
			return new EntityCollection<RevisionHeadObjectEntity>(this);
		}
		

		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new, empty Entity objects based on the entity type specified. Uses  entity specific factory objects</summary>
	[Serializable]
	public partial class GeneralEntityFactory
	{
		/// <summary>Creates a new, empty Entity object of the type specified</summary>
		/// <param name="entityTypeToCreate">The entity type to create.</param>
		/// <returns>A new, empty Entity object.</returns>
		public static IEntity2 Create(Dogwood.Data.EntityType entityTypeToCreate)
		{
			IEntityFactory2 factoryToUse = null;
			switch(entityTypeToCreate)
			{
				case Dogwood.Data.EntityType.AttributeEntity:
					factoryToUse = new AttributeEntityFactory();
					break;
				case Dogwood.Data.EntityType.AttributeMetadataEntity:
					factoryToUse = new AttributeMetadataEntityFactory();
					break;
				case Dogwood.Data.EntityType.AttributeValueEntity:
					factoryToUse = new AttributeValueEntityFactory();
					break;
				case Dogwood.Data.EntityType.BranchHeadObjectEntity:
					factoryToUse = new BranchHeadObjectEntityFactory();
					break;
				case Dogwood.Data.EntityType.ClassEntity:
					factoryToUse = new ClassEntityFactory();
					break;
				case Dogwood.Data.EntityType.DataTypeEntity:
					factoryToUse = new DataTypeEntityFactory();
					break;
				case Dogwood.Data.EntityType.ObjectEntity:
					factoryToUse = new ObjectEntityFactory();
					break;
				case Dogwood.Data.EntityType.ObjectRevisionEntity:
					factoryToUse = new ObjectRevisionEntityFactory();
					break;
				case Dogwood.Data.EntityType.RelationAttributeValueEntity:
					factoryToUse = new RelationAttributeValueEntityFactory();
					break;
				case Dogwood.Data.EntityType.RelationshipMetadataEntity:
					factoryToUse = new RelationshipMetadataEntityFactory();
					break;
				case Dogwood.Data.EntityType.RevisionEntity:
					factoryToUse = new RevisionEntityFactory();
					break;
				case Dogwood.Data.EntityType.RevisionHeadObjectEntity:
					factoryToUse = new RevisionHeadObjectEntityFactory();
					break;
			}
			IEntity2 toReturn = null;
			if(factoryToUse != null)
			{
				toReturn = factoryToUse.Create();
			}
			return toReturn;
		}		
	}
		
	/// <summary>Class which is used to obtain the entity factory based on the .NET type of the entity. </summary>
	[Serializable]
	public static class EntityFactoryFactory
	{
#if CF
		/// <summary>Gets the factory of the entity with the Dogwood.Data.EntityType specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory2 GetFactory(Dogwood.Data.EntityType typeOfEntity)
		{
			return GeneralEntityFactory.Create(typeOfEntity).GetEntityFactory();
		}
#else
		private static Dictionary<Type, IEntityFactory2> _factoryPerType = new Dictionary<Type, IEntityFactory2>();

		/// <summary>Initializes the <see cref="EntityFactoryFactory"/> class.</summary>
		static EntityFactoryFactory()
		{
			Array entityTypeValues = Enum.GetValues(typeof(Dogwood.Data.EntityType));
			foreach(int entityTypeValue in entityTypeValues)
			{
				IEntity2 dummy = GeneralEntityFactory.Create((Dogwood.Data.EntityType)entityTypeValue);
				_factoryPerType.Add(dummy.GetType(), dummy.GetEntityFactory());
			}
		}

		/// <summary>Gets the factory of the entity with the .NET type specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory2 GetFactory(Type typeOfEntity)
		{
			IEntityFactory2 toReturn = null;
			_factoryPerType.TryGetValue(typeOfEntity, out toReturn);
			return toReturn;
		}

		/// <summary>Gets the factory of the entity with the Dogwood.Data.EntityType specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory2 GetFactory(Dogwood.Data.EntityType typeOfEntity)
		{
			return GetFactory(GeneralEntityFactory.Create(typeOfEntity).GetType());
		}
#endif		
	}
		
	/// <summary>Element creator for creating project elements from somewhere else, like inside Linq providers.</summary>
	public class ElementCreator : ElementCreatorBase, IElementCreator2
	{
		/// <summary>Gets the factory of the Entity type with the Dogwood.Data.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory2 GetFactory(int entityTypeValue)
		{
			return (IEntityFactory2)this.GetFactoryImpl(entityTypeValue);
		}
		
		/// <summary>Gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory2 GetFactory(Type typeOfEntity)
		{
			return (IEntityFactory2)this.GetFactoryImpl(typeOfEntity);
		}

		/// <summary>Creates a new resultset fields object with the number of field slots reserved as specified</summary>
		/// <param name="numberOfFields">The number of fields.</param>
		/// <returns>ready to use resultsetfields object</returns>
		public IEntityFields2 CreateResultsetFields(int numberOfFields)
		{
			return new ResultsetFields(numberOfFields);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand)
		{
			return new DynamicRelation(leftOperand);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperand">The right operand.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, DerivedTableDefinition rightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, rightOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, string rightOperandEntityName, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, (Dogwood.Data.EntityType)Enum.Parse(typeof(Dogwood.Data.EntityType), rightOperandEntityName, false), aliasRightOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperandEntityName">Name of the entity which is used as the left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(string leftOperandEntityName, JoinHint joinType, string rightOperandEntityName, string aliasLeftOperand, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation((Dogwood.Data.EntityType)Enum.Parse(typeof(Dogwood.Data.EntityType), leftOperandEntityName, false), joinType, (Dogwood.Data.EntityType)Enum.Parse(typeof(Dogwood.Data.EntityType), rightOperandEntityName, false), aliasLeftOperand, aliasRightOperand, onClause);
		}
		
		/// <summary>Obtains the inheritance info provider instance from the singleton </summary>
		/// <returns>The singleton instance of the inheritance info provider</returns>
		public override IInheritanceInfoProvider ObtainInheritanceInfoProviderInstance()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}
		
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the Dogwood.Data.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(int entityTypeValue)
		{
			return EntityFactoryFactory.GetFactory((Dogwood.Data.EntityType)entityTypeValue);
		}
#if !CF		
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(Type typeOfEntity)
		{
			return EntityFactoryFactory.GetFactory(typeOfEntity);
		}
#endif
	}
}
