﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:04 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

using Dogwood.Data;
using Dogwood.Data.HelperClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.FactoryClasses
{
	/// <summary>
	/// Factory class for IEntityField2 instances, used in IEntityFields2 instances.
	/// </summary>
	public partial class EntityFieldFactory
	{
		/// <summary> Private CTor, no instantiation possible.</summary>
		private EntityFieldFactory()
		{
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the AttributeEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(AttributeFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("AttributeEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the AttributeMetadataEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(AttributeMetadataFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("AttributeMetadataEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the AttributeValueEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(AttributeValueFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("AttributeValueEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the BranchHeadObjectEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(BranchHeadObjectFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("BranchHeadObjectEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the ClassEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(ClassFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("ClassEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the DataTypeEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(DataTypeFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("DataTypeEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the ObjectEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(ObjectFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("ObjectEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the ObjectRevisionEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(ObjectRevisionFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("ObjectRevisionEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the RelationAttributeValueEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(RelationAttributeValueFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("RelationAttributeValueEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the RelationshipMetadataEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(RelationshipMetadataFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("RelationshipMetadataEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the RevisionEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(RevisionFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("RevisionEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the RevisionHeadObjectEntity. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(RevisionHeadObjectFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("RevisionHeadObjectEntity", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the Branches TypedView. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(BranchesFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("BranchesTypedView", (int)fieldIndex));
		}

		/// <summary> Creates a new IEntityField2 instance for usage in the EntityFields object for the IndexedRevisions TypedView. Which EntityField is created is specified by fieldIndex</summary>
		/// <param name="fieldIndex">The field which IEntityField2 instance should be created</param>
		/// <returns>The IEntityField2 instance for the field specified in fieldIndex</returns>
		public static IEntityField2 Create(IndexedRevisionsFieldIndex fieldIndex)
		{
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo("IndexedRevisionsTypedView", (int)fieldIndex));
		}

		/// <summary>Creates a new IEntityField2 instance, which represents the field objectName.fieldName</summary>
		/// <param name="objectName">the name of the object the field belongs to, like CustomerEntity or OrdersTypedView</param>
		/// <param name="fieldName">the name of the field to create</param>
		public static IEntityField2 Create(string objectName, string fieldName)
        {
			return new EntityField2(FieldInfoProviderSingleton.GetInstance().GetFieldInfo(objectName, fieldName));
        }

		#region Included Code

		#endregion
	}
}
