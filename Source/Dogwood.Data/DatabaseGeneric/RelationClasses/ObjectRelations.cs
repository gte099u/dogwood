﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:03 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Dogwood.Data;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.RelationClasses
{
	/// <summary>Implements the static Relations variant for the entity: Object. </summary>
	public partial class ObjectRelations
	{
		/// <summary>CTor</summary>
		public ObjectRelations()
		{
		}

		/// <summary>Gets all relations of the ObjectEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BranchHeadObjectEntityUsingObjectId);
			toReturn.Add(this.ObjectRevisionEntityUsingObjectId);
			toReturn.Add(this.RelationAttributeValueEntityUsingValue);
			toReturn.Add(this.RelationshipMetadataEntityUsingCascadeDefaultValue);
			toReturn.Add(this.RevisionHeadObjectEntityUsingObjectId);

			toReturn.Add(this.ClassEntityUsingClassId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ObjectEntity and BranchHeadObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Object.Id - BranchHeadObject.ObjectId
		/// </summary>
		public virtual IEntityRelation BranchHeadObjectEntityUsingObjectId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BranchHeadObject" , true);
				relation.AddEntityFieldPair(ObjectFields.Id, BranchHeadObjectFields.ObjectId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BranchHeadObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ObjectEntity and ObjectRevisionEntity over the 1:n relation they have, using the relation between the fields:
		/// Object.Id - ObjectRevision.ObjectId
		/// </summary>
		public virtual IEntityRelation ObjectRevisionEntityUsingObjectId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ObjectRevision" , true);
				relation.AddEntityFieldPair(ObjectFields.Id, ObjectRevisionFields.ObjectId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectRevisionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ObjectEntity and RelationAttributeValueEntity over the 1:n relation they have, using the relation between the fields:
		/// Object.Id - RelationAttributeValue.Value
		/// </summary>
		public virtual IEntityRelation RelationAttributeValueEntityUsingValue
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RelationAttributeValue" , true);
				relation.AddEntityFieldPair(ObjectFields.Id, RelationAttributeValueFields.Value);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationAttributeValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ObjectEntity and RelationshipMetadataEntity over the 1:n relation they have, using the relation between the fields:
		/// Object.Id - RelationshipMetadata.CascadeDefaultValue
		/// </summary>
		public virtual IEntityRelation RelationshipMetadataEntityUsingCascadeDefaultValue
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RelationshipMetadata" , true);
				relation.AddEntityFieldPair(ObjectFields.Id, RelationshipMetadataFields.CascadeDefaultValue);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationshipMetadataEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ObjectEntity and RevisionHeadObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Object.Id - RevisionHeadObject.ObjectId
		/// </summary>
		public virtual IEntityRelation RevisionHeadObjectEntityUsingObjectId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RevisionHeadObject" , true);
				relation.AddEntityFieldPair(ObjectFields.Id, RevisionHeadObjectFields.ObjectId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionHeadObjectEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ObjectEntity and ClassEntity over the m:1 relation they have, using the relation between the fields:
		/// Object.ClassId - Class.Id
		/// </summary>
		public virtual IEntityRelation ClassEntityUsingClassId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Class", false);
				relation.AddEntityFieldPair(ClassFields.Id, ObjectFields.ClassId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}

		#endregion

		#region Included Code

		#endregion
	}
}
