﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:03 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Dogwood.Data;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.RelationClasses
{
	/// <summary>Implements the static Relations variant for the entity: RelationAttributeValue. </summary>
	public partial class RelationAttributeValueRelations
	{
		/// <summary>CTor</summary>
		public RelationAttributeValueRelations()
		{
		}

		/// <summary>Gets all relations of the RelationAttributeValueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();


			toReturn.Add(this.ObjectEntityUsingValue);
			toReturn.Add(this.ObjectRevisionEntityUsingObjectRevisionId);
			toReturn.Add(this.RelationshipMetadataEntityUsingRelationshipMetadataId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between RelationAttributeValueEntity and ObjectEntity over the m:1 relation they have, using the relation between the fields:
		/// RelationAttributeValue.Value - Object.Id
		/// </summary>
		public virtual IEntityRelation ObjectEntityUsingValue
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Object", false);
				relation.AddEntityFieldPair(ObjectFields.Id, RelationAttributeValueFields.Value);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationAttributeValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RelationAttributeValueEntity and ObjectRevisionEntity over the m:1 relation they have, using the relation between the fields:
		/// RelationAttributeValue.ObjectRevisionId - ObjectRevision.Id
		/// </summary>
		public virtual IEntityRelation ObjectRevisionEntityUsingObjectRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ObjectRevision", false);
				relation.AddEntityFieldPair(ObjectRevisionFields.Id, RelationAttributeValueFields.ObjectRevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectRevisionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationAttributeValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RelationAttributeValueEntity and RelationshipMetadataEntity over the m:1 relation they have, using the relation between the fields:
		/// RelationAttributeValue.RelationshipMetadataId - RelationshipMetadata.Id
		/// </summary>
		public virtual IEntityRelation RelationshipMetadataEntityUsingRelationshipMetadataId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RelationshipMetadata", false);
				relation.AddEntityFieldPair(RelationshipMetadataFields.Id, RelationAttributeValueFields.RelationshipMetadataId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationshipMetadataEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationAttributeValueEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}

		#endregion

		#region Included Code

		#endregion
	}
}
