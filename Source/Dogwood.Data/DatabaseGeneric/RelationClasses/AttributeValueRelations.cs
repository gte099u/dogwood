﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:03 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Dogwood.Data;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.RelationClasses
{
	/// <summary>Implements the static Relations variant for the entity: AttributeValue. </summary>
	public partial class AttributeValueRelations
	{
		/// <summary>CTor</summary>
		public AttributeValueRelations()
		{
		}

		/// <summary>Gets all relations of the AttributeValueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();


			toReturn.Add(this.AttributeMetadataEntityUsingAttributeMetadataId);
			toReturn.Add(this.ObjectRevisionEntityUsingObjectRevisionId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and AttributeMetadataEntity over the m:1 relation they have, using the relation between the fields:
		/// AttributeValue.AttributeMetadataId - AttributeMetadata.Id
		/// </summary>
		public virtual IEntityRelation AttributeMetadataEntityUsingAttributeMetadataId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeMetadata", false);
				relation.AddEntityFieldPair(AttributeMetadataFields.Id, AttributeValueFields.AttributeMetadataId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeMetadataEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and ObjectRevisionEntity over the m:1 relation they have, using the relation between the fields:
		/// AttributeValue.ObjectRevisionId - ObjectRevision.Id
		/// </summary>
		public virtual IEntityRelation ObjectRevisionEntityUsingObjectRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ObjectRevision", false);
				relation.AddEntityFieldPair(ObjectRevisionFields.Id, AttributeValueFields.ObjectRevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectRevisionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}

		#endregion

		#region Included Code

		#endregion
	}
}
