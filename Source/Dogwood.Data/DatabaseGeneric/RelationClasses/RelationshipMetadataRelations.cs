﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:03 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Dogwood.Data;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.RelationClasses
{
	/// <summary>Implements the static Relations variant for the entity: RelationshipMetadata. </summary>
	public partial class RelationshipMetadataRelations
	{
		/// <summary>CTor</summary>
		public RelationshipMetadataRelations()
		{
		}

		/// <summary>Gets all relations of the RelationshipMetadataEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RelationAttributeValueEntityUsingRelationshipMetadataId);

			toReturn.Add(this.AttributeEntityUsingAttributeId);
			toReturn.Add(this.ClassEntityUsingPkClassId);
			toReturn.Add(this.ClassEntityUsingFkClassId);
			toReturn.Add(this.ObjectEntityUsingCascadeDefaultValue);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RelationshipMetadataEntity and RelationAttributeValueEntity over the 1:n relation they have, using the relation between the fields:
		/// RelationshipMetadata.Id - RelationAttributeValue.RelationshipMetadataId
		/// </summary>
		public virtual IEntityRelation RelationAttributeValueEntityUsingRelationshipMetadataId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RelationAttributeValue" , true);
				relation.AddEntityFieldPair(RelationshipMetadataFields.Id, RelationAttributeValueFields.RelationshipMetadataId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationshipMetadataEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationAttributeValueEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RelationshipMetadataEntity and AttributeEntity over the m:1 relation they have, using the relation between the fields:
		/// RelationshipMetadata.AttributeId - Attribute.Id
		/// </summary>
		public virtual IEntityRelation AttributeEntityUsingAttributeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Attribute", false);
				relation.AddEntityFieldPair(AttributeFields.Id, RelationshipMetadataFields.AttributeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationshipMetadataEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RelationshipMetadataEntity and ClassEntity over the m:1 relation they have, using the relation between the fields:
		/// RelationshipMetadata.PkClassId - Class.Id
		/// </summary>
		public virtual IEntityRelation ClassEntityUsingPkClassId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PkClass", false);
				relation.AddEntityFieldPair(ClassFields.Id, RelationshipMetadataFields.PkClassId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationshipMetadataEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RelationshipMetadataEntity and ClassEntity over the m:1 relation they have, using the relation between the fields:
		/// RelationshipMetadata.FkClassId - Class.Id
		/// </summary>
		public virtual IEntityRelation ClassEntityUsingFkClassId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FkClass", false);
				relation.AddEntityFieldPair(ClassFields.Id, RelationshipMetadataFields.FkClassId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationshipMetadataEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RelationshipMetadataEntity and ObjectEntity over the m:1 relation they have, using the relation between the fields:
		/// RelationshipMetadata.CascadeDefaultValue - Object.Id
		/// </summary>
		public virtual IEntityRelation ObjectEntityUsingCascadeDefaultValue
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Object", false);
				relation.AddEntityFieldPair(ObjectFields.Id, RelationshipMetadataFields.CascadeDefaultValue);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationshipMetadataEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}

		#endregion

		#region Included Code

		#endregion
	}
}
