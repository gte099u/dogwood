﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:03 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Dogwood.Data;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.RelationClasses
{
	/// <summary>Implements the static Relations variant for the entity: Class. </summary>
	public partial class ClassRelations
	{
		/// <summary>CTor</summary>
		public ClassRelations()
		{
		}

		/// <summary>Gets all relations of the ClassEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeMetadataEntityUsingClassId);
			toReturn.Add(this.BranchHeadObjectEntityUsingClassId);
			toReturn.Add(this.ObjectEntityUsingClassId);
			toReturn.Add(this.RelationshipMetadataEntityUsingPkClassId);
			toReturn.Add(this.RelationshipMetadataEntityUsingFkClassId);
			toReturn.Add(this.RevisionHeadObjectEntityUsingClassId);


			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ClassEntity and AttributeMetadataEntity over the 1:n relation they have, using the relation between the fields:
		/// Class.Id - AttributeMetadata.ClassId
		/// </summary>
		public virtual IEntityRelation AttributeMetadataEntityUsingClassId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeMetadata" , true);
				relation.AddEntityFieldPair(ClassFields.Id, AttributeMetadataFields.ClassId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeMetadataEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClassEntity and BranchHeadObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Class.Id - BranchHeadObject.ClassId
		/// </summary>
		public virtual IEntityRelation BranchHeadObjectEntityUsingClassId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BranchHeadObject" , true);
				relation.AddEntityFieldPair(ClassFields.Id, BranchHeadObjectFields.ClassId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BranchHeadObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClassEntity and ObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Class.Id - Object.ClassId
		/// </summary>
		public virtual IEntityRelation ObjectEntityUsingClassId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Object" , true);
				relation.AddEntityFieldPair(ClassFields.Id, ObjectFields.ClassId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClassEntity and RelationshipMetadataEntity over the 1:n relation they have, using the relation between the fields:
		/// Class.Id - RelationshipMetadata.PkClassId
		/// </summary>
		public virtual IEntityRelation RelationshipMetadataEntityUsingPkClassId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RelationshipMetadataAsPk" , true);
				relation.AddEntityFieldPair(ClassFields.Id, RelationshipMetadataFields.PkClassId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationshipMetadataEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClassEntity and RelationshipMetadataEntity over the 1:n relation they have, using the relation between the fields:
		/// Class.Id - RelationshipMetadata.FkClassId
		/// </summary>
		public virtual IEntityRelation RelationshipMetadataEntityUsingFkClassId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RelationshipMetadataAsFk" , true);
				relation.AddEntityFieldPair(ClassFields.Id, RelationshipMetadataFields.FkClassId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationshipMetadataEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClassEntity and RevisionHeadObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Class.Id - RevisionHeadObject.ClassId
		/// </summary>
		public virtual IEntityRelation RevisionHeadObjectEntityUsingClassId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RevisionHeadObject" , true);
				relation.AddEntityFieldPair(ClassFields.Id, RevisionHeadObjectFields.ClassId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionHeadObjectEntity", false);
				return relation;
			}
		}



		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}

		#endregion

		#region Included Code

		#endregion
	}
}
