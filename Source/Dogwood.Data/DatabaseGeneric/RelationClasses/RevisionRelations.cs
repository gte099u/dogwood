﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:03 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Dogwood.Data;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.RelationClasses
{
	/// <summary>Implements the static Relations variant for the entity: Revision. </summary>
	public partial class RevisionRelations
	{
		/// <summary>CTor</summary>
		public RevisionRelations()
		{
		}

		/// <summary>Gets all relations of the RevisionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BranchHeadObjectEntityUsingRevisionId);
			toReturn.Add(this.BranchHeadObjectEntityUsingBranchId);
			toReturn.Add(this.ObjectRevisionEntityUsingRevisionId);
			toReturn.Add(this.RevisionEntityUsingBranchId);
			toReturn.Add(this.RevisionHeadObjectEntityUsingBranchId);
			toReturn.Add(this.RevisionHeadObjectEntityUsingRevisionId);

			toReturn.Add(this.RevisionEntityUsingIdBranchId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RevisionEntity and BranchHeadObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Revision.Id - BranchHeadObject.RevisionId
		/// </summary>
		public virtual IEntityRelation BranchHeadObjectEntityUsingRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "" , true);
				relation.AddEntityFieldPair(RevisionFields.Id, BranchHeadObjectFields.RevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BranchHeadObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RevisionEntity and BranchHeadObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Revision.Id - BranchHeadObject.BranchId
		/// </summary>
		public virtual IEntityRelation BranchHeadObjectEntityUsingBranchId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "" , true);
				relation.AddEntityFieldPair(RevisionFields.Id, BranchHeadObjectFields.BranchId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BranchHeadObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RevisionEntity and ObjectRevisionEntity over the 1:n relation they have, using the relation between the fields:
		/// Revision.Id - ObjectRevision.RevisionId
		/// </summary>
		public virtual IEntityRelation ObjectRevisionEntityUsingRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ObjectRevision" , true);
				relation.AddEntityFieldPair(RevisionFields.Id, ObjectRevisionFields.RevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectRevisionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RevisionEntity and RevisionEntity over the 1:n relation they have, using the relation between the fields:
		/// Revision.Id - Revision.BranchId
		/// </summary>
		public virtual IEntityRelation RevisionEntityUsingBranchId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "" , true);
				relation.AddEntityFieldPair(RevisionFields.Id, RevisionFields.BranchId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RevisionEntity and RevisionHeadObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Revision.Id - RevisionHeadObject.BranchId
		/// </summary>
		public virtual IEntityRelation RevisionHeadObjectEntityUsingBranchId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RevisionHeadObject_" , true);
				relation.AddEntityFieldPair(RevisionFields.Id, RevisionHeadObjectFields.BranchId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionHeadObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RevisionEntity and RevisionHeadObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Revision.Id - RevisionHeadObject.RevisionId
		/// </summary>
		public virtual IEntityRelation RevisionHeadObjectEntityUsingRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RevisionHeadObject" , true);
				relation.AddEntityFieldPair(RevisionFields.Id, RevisionHeadObjectFields.RevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionHeadObjectEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RevisionEntity and RevisionEntity over the m:1 relation they have, using the relation between the fields:
		/// Revision.BranchId - Revision.Id
		/// </summary>
		public virtual IEntityRelation RevisionEntityUsingIdBranchId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "", false);
				relation.AddEntityFieldPair(RevisionFields.Id, RevisionFields.BranchId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}

		#endregion

		#region Included Code

		#endregion
	}
}
