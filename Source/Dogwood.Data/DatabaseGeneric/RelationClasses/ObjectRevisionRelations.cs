﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:03 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Dogwood.Data;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.RelationClasses
{
	/// <summary>Implements the static Relations variant for the entity: ObjectRevision. </summary>
	public partial class ObjectRevisionRelations
	{
		/// <summary>CTor</summary>
		public ObjectRevisionRelations()
		{
		}

		/// <summary>Gets all relations of the ObjectRevisionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeValueEntityUsingObjectRevisionId);
			toReturn.Add(this.BranchHeadObjectEntityUsingObjectRevisionId);
			toReturn.Add(this.RelationAttributeValueEntityUsingObjectRevisionId);
			toReturn.Add(this.RevisionHeadObjectEntityUsingObjectRevisionId);

			toReturn.Add(this.ObjectEntityUsingObjectId);
			toReturn.Add(this.RevisionEntityUsingRevisionId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ObjectRevisionEntity and AttributeValueEntity over the 1:n relation they have, using the relation between the fields:
		/// ObjectRevision.Id - AttributeValue.ObjectRevisionId
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingObjectRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeValue" , true);
				relation.AddEntityFieldPair(ObjectRevisionFields.Id, AttributeValueFields.ObjectRevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectRevisionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ObjectRevisionEntity and BranchHeadObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// ObjectRevision.Id - BranchHeadObject.ObjectRevisionId
		/// </summary>
		public virtual IEntityRelation BranchHeadObjectEntityUsingObjectRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BranchHeadObject" , true);
				relation.AddEntityFieldPair(ObjectRevisionFields.Id, BranchHeadObjectFields.ObjectRevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectRevisionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BranchHeadObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ObjectRevisionEntity and RelationAttributeValueEntity over the 1:n relation they have, using the relation between the fields:
		/// ObjectRevision.Id - RelationAttributeValue.ObjectRevisionId
		/// </summary>
		public virtual IEntityRelation RelationAttributeValueEntityUsingObjectRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RelationAttributeValue" , true);
				relation.AddEntityFieldPair(ObjectRevisionFields.Id, RelationAttributeValueFields.ObjectRevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectRevisionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RelationAttributeValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ObjectRevisionEntity and RevisionHeadObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// ObjectRevision.Id - RevisionHeadObject.ObjectRevisionId
		/// </summary>
		public virtual IEntityRelation RevisionHeadObjectEntityUsingObjectRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RevisionHeadObject" , true);
				relation.AddEntityFieldPair(ObjectRevisionFields.Id, RevisionHeadObjectFields.ObjectRevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectRevisionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionHeadObjectEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ObjectRevisionEntity and ObjectEntity over the m:1 relation they have, using the relation between the fields:
		/// ObjectRevision.ObjectId - Object.Id
		/// </summary>
		public virtual IEntityRelation ObjectEntityUsingObjectId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Object", false);
				relation.AddEntityFieldPair(ObjectFields.Id, ObjectRevisionFields.ObjectId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectRevisionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ObjectRevisionEntity and RevisionEntity over the m:1 relation they have, using the relation between the fields:
		/// ObjectRevision.RevisionId - Revision.Id
		/// </summary>
		public virtual IEntityRelation RevisionEntityUsingRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Revision", false);
				relation.AddEntityFieldPair(RevisionFields.Id, ObjectRevisionFields.RevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectRevisionEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}

		#endregion

		#region Included Code

		#endregion
	}
}
