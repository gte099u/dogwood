﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:03 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Dogwood.Data;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.RelationClasses
{
	/// <summary>Implements the static Relations variant for the entity: RevisionHeadObject. </summary>
	public partial class RevisionHeadObjectRelations
	{
		/// <summary>CTor</summary>
		public RevisionHeadObjectRelations()
		{
		}

		/// <summary>Gets all relations of the RevisionHeadObjectEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();


			toReturn.Add(this.ClassEntityUsingClassId);
			toReturn.Add(this.ObjectEntityUsingObjectId);
			toReturn.Add(this.ObjectRevisionEntityUsingObjectRevisionId);
			toReturn.Add(this.RevisionEntityUsingBranchId);
			toReturn.Add(this.RevisionEntityUsingRevisionId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between RevisionHeadObjectEntity and ClassEntity over the m:1 relation they have, using the relation between the fields:
		/// RevisionHeadObject.ClassId - Class.Id
		/// </summary>
		public virtual IEntityRelation ClassEntityUsingClassId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Class", false);
				relation.AddEntityFieldPair(ClassFields.Id, RevisionHeadObjectFields.ClassId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionHeadObjectEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RevisionHeadObjectEntity and ObjectEntity over the m:1 relation they have, using the relation between the fields:
		/// RevisionHeadObject.ObjectId - Object.Id
		/// </summary>
		public virtual IEntityRelation ObjectEntityUsingObjectId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Object", false);
				relation.AddEntityFieldPair(ObjectFields.Id, RevisionHeadObjectFields.ObjectId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionHeadObjectEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RevisionHeadObjectEntity and ObjectRevisionEntity over the m:1 relation they have, using the relation between the fields:
		/// RevisionHeadObject.ObjectRevisionId - ObjectRevision.Id
		/// </summary>
		public virtual IEntityRelation ObjectRevisionEntityUsingObjectRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ObjectRevision", false);
				relation.AddEntityFieldPair(ObjectRevisionFields.Id, RevisionHeadObjectFields.ObjectRevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ObjectRevisionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionHeadObjectEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RevisionHeadObjectEntity and RevisionEntity over the m:1 relation they have, using the relation between the fields:
		/// RevisionHeadObject.BranchId - Revision.Id
		/// </summary>
		public virtual IEntityRelation RevisionEntityUsingBranchId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Revision_", false);
				relation.AddEntityFieldPair(RevisionFields.Id, RevisionHeadObjectFields.BranchId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionHeadObjectEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RevisionHeadObjectEntity and RevisionEntity over the m:1 relation they have, using the relation between the fields:
		/// RevisionHeadObject.RevisionId - Revision.Id
		/// </summary>
		public virtual IEntityRelation RevisionEntityUsingRevisionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Revision", false);
				relation.AddEntityFieldPair(RevisionFields.Id, RevisionHeadObjectFields.RevisionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevisionHeadObjectEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}

		#endregion

		#region Included Code

		#endregion
	}
}
