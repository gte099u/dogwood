﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:04 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using Dogwood.Data;
using Dogwood.Data.HelperClasses;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>
	/// Entity class which represents the entity 'RelationAttributeValue'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RelationAttributeValueEntity : CommonEntityBase, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations


		private ObjectEntity _object;
		private ObjectRevisionEntity _objectRevision;
		private RelationshipMetadataEntity _relationshipMetadata;

		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Object</summary>
			public static readonly string Object = "Object";
			/// <summary>Member name ObjectRevision</summary>
			public static readonly string ObjectRevision = "ObjectRevision";
			/// <summary>Member name RelationshipMetadata</summary>
			public static readonly string RelationshipMetadata = "RelationshipMetadata";



		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RelationAttributeValueEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public RelationAttributeValueEntity():base("RelationAttributeValueEntity")
		{
			InitClassEmpty(null, CreateFields());
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public RelationAttributeValueEntity(IEntityFields2 fields):base("RelationAttributeValueEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this RelationAttributeValueEntity</param>
		public RelationAttributeValueEntity(IValidator validator):base("RelationAttributeValueEntity")
		{
			InitClassEmpty(validator, CreateFields());
		}
				

		/// <summary> CTor</summary>
		/// <param name="id">PK value for RelationAttributeValue which data should be fetched into this RelationAttributeValue object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public RelationAttributeValueEntity(System.Int32 id):base("RelationAttributeValueEntity")
		{
			InitClassEmpty(null, CreateFields());
			this.Id = id;
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for RelationAttributeValue which data should be fetched into this RelationAttributeValue object</param>
		/// <param name="validator">The custom validator object for this RelationAttributeValueEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public RelationAttributeValueEntity(System.Int32 id, IValidator validator):base("RelationAttributeValueEntity")
		{
			InitClassEmpty(validator, CreateFields());
			this.Id = id;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected RelationAttributeValueEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{


				_object = (ObjectEntity)info.GetValue("_object", typeof(ObjectEntity));
				if(_object!=null)
				{
					_object.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_objectRevision = (ObjectRevisionEntity)info.GetValue("_objectRevision", typeof(ObjectRevisionEntity));
				if(_objectRevision!=null)
				{
					_objectRevision.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_relationshipMetadata = (RelationshipMetadataEntity)info.GetValue("_relationshipMetadata", typeof(RelationshipMetadataEntity));
				if(_relationshipMetadata!=null)
				{
					_relationshipMetadata.AfterSave+=new EventHandler(OnEntityAfterSave);
				}

				base.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RelationAttributeValueFieldIndex)fieldIndex)
			{
				case RelationAttributeValueFieldIndex.ObjectRevisionId:
					DesetupSyncObjectRevision(true, false);
					break;
				case RelationAttributeValueFieldIndex.RelationshipMetadataId:
					DesetupSyncRelationshipMetadata(true, false);
					break;
				case RelationAttributeValueFieldIndex.Value:
					DesetupSyncObject(true, false);
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}
				
		/// <summary>Gets the inheritance info provider instance of the project this entity instance is located in. </summary>
		/// <returns>ready to use inheritance info provider instance.</returns>
		protected override IInheritanceInfoProvider GetInheritanceInfoProvider()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntityProperty(string propertyName, IEntity2 entity)
		{
			switch(propertyName)
			{
				case "Object":
					this.Object = (ObjectEntity)entity;
					break;
				case "ObjectRevision":
					this.ObjectRevision = (ObjectRevisionEntity)entity;
					break;
				case "RelationshipMetadata":
					this.RelationshipMetadata = (RelationshipMetadataEntity)entity;
					break;



				default:
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return RelationAttributeValueEntity.GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Object":
					toReturn.Add(RelationAttributeValueEntity.Relations.ObjectEntityUsingValue);
					break;
				case "ObjectRevision":
					toReturn.Add(RelationAttributeValueEntity.Relations.ObjectRevisionEntityUsingObjectRevisionId);
					break;
				case "RelationshipMetadata":
					toReturn.Add(RelationAttributeValueEntity.Relations.RelationshipMetadataEntityUsingRelationshipMetadataId);
					break;



				default:

					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it
		/// will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override bool CheckOneWayRelations(string propertyName)
		{
			// use template trick to calculate the # of single-sided / oneway relations
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));




				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntity(IEntity2 relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Object":
					SetupSyncObject(relatedEntity);
					break;
				case "ObjectRevision":
					SetupSyncObjectRevision(relatedEntity);
					break;
				case "RelationshipMetadata":
					SetupSyncRelationshipMetadata(relatedEntity);
					break;


				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void UnsetRelatedEntity(IEntity2 relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Object":
					DesetupSyncObject(false, true);
					break;
				case "ObjectRevision":
					DesetupSyncObjectRevision(false, true);
					break;
				case "RelationshipMetadata":
					DesetupSyncRelationshipMetadata(false, true);
					break;


				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		public override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();

			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		public override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			if(_object!=null)
			{
				toReturn.Add(_object);
			}
			if(_objectRevision!=null)
			{
				toReturn.Add(_objectRevision);
			}
			if(_relationshipMetadata!=null)
			{
				toReturn.Add(_relationshipMetadata);
			}

			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. The contents of the ArrayList is used by the DataAccessAdapter to perform recursive saves. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		public override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();


			return toReturn;
		}
		


		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{


				info.AddValue("_object", (!this.MarkedForDeletion?_object:null));
				info.AddValue("_objectRevision", (!this.MarkedForDeletion?_objectRevision:null));
				info.AddValue("_relationshipMetadata", (!this.MarkedForDeletion?_relationshipMetadata:null));

			}
			
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}

		/// <summary> Method which will construct a filter (predicate expression) for the unique constraint defined on the fields:
		/// ObjectRevisionId , RelationshipMetadataId .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCObjectRevisionIdRelationshipMetadataId()
		{
			IPredicateExpression filter = new PredicateExpression();
			filter.Add(new FieldCompareValuePredicate(base.Fields[(int)RelationAttributeValueFieldIndex.ObjectRevisionId], null, ComparisonOperator.Equal));
			filter.Add(new FieldCompareValuePredicate(base.Fields[(int)RelationAttributeValueFieldIndex.RelationshipMetadataId], null, ComparisonOperator.Equal)); 
			return filter;
		}

		/// <summary>Returns true if the original value for the field with the fieldIndex passed in, read from the persistent storage was NULL, false otherwise.
		/// Should not be used for testing if the current value is NULL, use <see cref="TestCurrentFieldValueForNull"/> for that.</summary>
		/// <param name="fieldIndex">Index of the field to test if that field was NULL in the persistent storage</param>
		/// <returns>true if the field with the passed in index was NULL in the persistent storage, false otherwise</returns>
		public bool TestOriginalFieldValueForNull(RelationAttributeValueFieldIndex fieldIndex)
		{
			return base.Fields[(int)fieldIndex].IsNull;
		}
		
		/// <summary>Returns true if the current value for the field with the fieldIndex passed in represents null/not defined, false otherwise.
		/// Should not be used for testing if the original value (read from the db) is NULL</summary>
		/// <param name="fieldIndex">Index of the field to test if its currentvalue is null/undefined</param>
		/// <returns>true if the field's value isn't defined yet, false otherwise</returns>
		public bool TestCurrentFieldValueForNull(RelationAttributeValueFieldIndex fieldIndex)
		{
			return base.CheckIfCurrentFieldValueIsNull((int)fieldIndex);
		}

				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			return new RelationAttributeValueRelations().GetAllRelations();
		}
		



		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entity of type 'Object' to this entity. Use DataAccessAdapter.FetchNewEntity() to fetch this related entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ObjectFields.Id, null, ComparisonOperator.Equal, this.Value));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entity of type 'ObjectRevision' to this entity. Use DataAccessAdapter.FetchNewEntity() to fetch this related entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObjectRevision()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ObjectRevisionFields.Id, null, ComparisonOperator.Equal, this.ObjectRevisionId));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entity of type 'RelationshipMetadata' to this entity. Use DataAccessAdapter.FetchNewEntity() to fetch this related entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRelationshipMetadata()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RelationshipMetadataFields.Id, null, ComparisonOperator.Equal, this.RelationshipMetadataId));
			return bucket;
		}

	
		
		/// <summary>Creates entity fields object for this entity. Used in constructor to setup this entity in a polymorphic scenario.</summary>
		protected virtual IEntityFields2 CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(Dogwood.Data.EntityType.RelationAttributeValueEntity);
		}

		/// <summary>
		/// Creates the ITypeDefaultValue instance used to provide default values for value types which aren't of type nullable(of T)
		/// </summary>
		/// <returns></returns>
		protected override ITypeDefaultValue CreateTypeDefaultValueProvider()
		{
			return new TypeDefaultValue();
		}

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(RelationAttributeValueEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);


		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);


		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{


			return base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);


		}
#endif
		/// <summary>
		/// Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element. 
		/// </summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		public override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Object", _object);
			toReturn.Add("ObjectRevision", _objectRevision);
			toReturn.Add("RelationshipMetadata", _relationshipMetadata);



			return toReturn;
		}
		
		/// <summary> Adds the internals to the active context. </summary>
		protected override void AddInternalsToContext()
		{


			if(_object!=null)
			{
				_object.ActiveContext = base.ActiveContext;
			}
			if(_objectRevision!=null)
			{
				_objectRevision.ActiveContext = base.ActiveContext;
			}
			if(_relationshipMetadata!=null)
			{
				_relationshipMetadata.ActiveContext = base.ActiveContext;
			}

		}

		/// <summary> Initializes the class members</summary>
		protected virtual void InitClassMembers()
		{



			_object = null;
			_objectRevision = null;
			_relationshipMetadata = null;

			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();

			Dictionary<string, string> fieldHashtable = null;
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("ObjectRevisionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("RelationshipMetadataId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Value", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _object</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncObject(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _object, new PropertyChangedEventHandler( OnObjectPropertyChanged ), "Object", RelationAttributeValueEntity.Relations.ObjectEntityUsingValue, true, signalRelatedEntity, "RelationAttributeValue", resetFKFields, new int[] { (int)RelationAttributeValueFieldIndex.Value } );		
			_object = null;
		}

		/// <summary> setups the sync logic for member _object</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncObject(IEntity2 relatedEntity)
		{
			if(_object!=relatedEntity)
			{
				DesetupSyncObject(true, true);
				_object = (ObjectEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _object, new PropertyChangedEventHandler( OnObjectPropertyChanged ), "Object", RelationAttributeValueEntity.Relations.ObjectEntityUsingValue, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnObjectPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _objectRevision</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncObjectRevision(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _objectRevision, new PropertyChangedEventHandler( OnObjectRevisionPropertyChanged ), "ObjectRevision", RelationAttributeValueEntity.Relations.ObjectRevisionEntityUsingObjectRevisionId, true, signalRelatedEntity, "RelationAttributeValue", resetFKFields, new int[] { (int)RelationAttributeValueFieldIndex.ObjectRevisionId } );		
			_objectRevision = null;
		}

		/// <summary> setups the sync logic for member _objectRevision</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncObjectRevision(IEntity2 relatedEntity)
		{
			if(_objectRevision!=relatedEntity)
			{
				DesetupSyncObjectRevision(true, true);
				_objectRevision = (ObjectRevisionEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _objectRevision, new PropertyChangedEventHandler( OnObjectRevisionPropertyChanged ), "ObjectRevision", RelationAttributeValueEntity.Relations.ObjectRevisionEntityUsingObjectRevisionId, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnObjectRevisionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _relationshipMetadata</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRelationshipMetadata(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _relationshipMetadata, new PropertyChangedEventHandler( OnRelationshipMetadataPropertyChanged ), "RelationshipMetadata", RelationAttributeValueEntity.Relations.RelationshipMetadataEntityUsingRelationshipMetadataId, true, signalRelatedEntity, "RelationAttributeValue", resetFKFields, new int[] { (int)RelationAttributeValueFieldIndex.RelationshipMetadataId } );		
			_relationshipMetadata = null;
		}

		/// <summary> setups the sync logic for member _relationshipMetadata</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRelationshipMetadata(IEntity2 relatedEntity)
		{
			if(_relationshipMetadata!=relatedEntity)
			{
				DesetupSyncRelationshipMetadata(true, true);
				_relationshipMetadata = (RelationshipMetadataEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _relationshipMetadata, new PropertyChangedEventHandler( OnRelationshipMetadataPropertyChanged ), "RelationshipMetadata", RelationAttributeValueEntity.Relations.RelationshipMetadataEntityUsingRelationshipMetadataId, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRelationshipMetadataPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}


		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this RelationAttributeValueEntity</param>
		/// <param name="fields">Fields of this entity</param>
		protected virtual void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			base.Fields = fields;
			base.IsNew=true;
			base.Validator = validator;
			InitClassMembers();

			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RelationAttributeValueRelations Relations
		{
			get	{ return new RelationAttributeValueRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}



		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Object' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObject
		{
			get
			{
				return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))),
					(IEntityRelation)GetRelationsForField("Object")[0], (int)Dogwood.Data.EntityType.RelationAttributeValueEntity, (int)Dogwood.Data.EntityType.ObjectEntity, 0, null, null, null, null, "Object", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ObjectRevision' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObjectRevision
		{
			get
			{
				return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory))),
					(IEntityRelation)GetRelationsForField("ObjectRevision")[0], (int)Dogwood.Data.EntityType.RelationAttributeValueEntity, (int)Dogwood.Data.EntityType.ObjectRevisionEntity, 0, null, null, null, null, "ObjectRevision", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'RelationshipMetadata' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRelationshipMetadata
		{
			get
			{
				return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(RelationshipMetadataEntityFactory))),
					(IEntityRelation)GetRelationsForField("RelationshipMetadata")[0], (int)Dogwood.Data.EntityType.RelationAttributeValueEntity, (int)Dogwood.Data.EntityType.RelationshipMetadataEntity, 0, null, null, null, null, "RelationshipMetadata", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return RelationAttributeValueEntity.CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value
		/// pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return RelationAttributeValueEntity.FieldsCustomProperties;}
		}

		/// <summary> The Id property of the Entity RelationAttributeValue<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationAttributeValue"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)RelationAttributeValueFieldIndex.Id, true); }
			set	{ SetValue((int)RelationAttributeValueFieldIndex.Id, value); }
		}

		/// <summary> The ObjectRevisionId property of the Entity RelationAttributeValue<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationAttributeValue"."ObjectRevisionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ObjectRevisionId
		{
			get { return (System.Int32)GetValue((int)RelationAttributeValueFieldIndex.ObjectRevisionId, true); }
			set	{ SetValue((int)RelationAttributeValueFieldIndex.ObjectRevisionId, value); }
		}

		/// <summary> The RelationshipMetadataId property of the Entity RelationAttributeValue<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationAttributeValue"."RelationshipMetadataId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RelationshipMetadataId
		{
			get { return (System.Int32)GetValue((int)RelationAttributeValueFieldIndex.RelationshipMetadataId, true); }
			set	{ SetValue((int)RelationAttributeValueFieldIndex.RelationshipMetadataId, value); }
		}

		/// <summary> The Value property of the Entity RelationAttributeValue<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationAttributeValue"."Value"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Value
		{
			get { return (System.Int32)GetValue((int)RelationAttributeValueFieldIndex.Value, true); }
			set	{ SetValue((int)RelationAttributeValueFieldIndex.Value, value); }
		}



		/// <summary> Gets / sets related entity of type 'ObjectEntity' which has to be set using a fetch action earlier. If no related entity
		/// is set for this property, null is returned. This property is not visible in databound grids.</summary>
		[Browsable(false)]
		public virtual ObjectEntity Object
		{
			get
			{
				return _object;
			}
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncObject(value);
				}
				else
				{
					if(value==null)
					{
						if(_object != null)
						{
							_object.UnsetRelatedEntity(this, "RelationAttributeValue");
						}
					}
					else
					{
						if(_object!=value)
						{
							((IEntity2)value).SetRelatedEntity(this, "RelationAttributeValue");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'ObjectRevisionEntity' which has to be set using a fetch action earlier. If no related entity
		/// is set for this property, null is returned. This property is not visible in databound grids.</summary>
		[Browsable(false)]
		public virtual ObjectRevisionEntity ObjectRevision
		{
			get
			{
				return _objectRevision;
			}
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncObjectRevision(value);
				}
				else
				{
					if(value==null)
					{
						if(_objectRevision != null)
						{
							_objectRevision.UnsetRelatedEntity(this, "RelationAttributeValue");
						}
					}
					else
					{
						if(_objectRevision!=value)
						{
							((IEntity2)value).SetRelatedEntity(this, "RelationAttributeValue");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'RelationshipMetadataEntity' which has to be set using a fetch action earlier. If no related entity
		/// is set for this property, null is returned. This property is not visible in databound grids.</summary>
		[Browsable(false)]
		public virtual RelationshipMetadataEntity RelationshipMetadata
		{
			get
			{
				return _relationshipMetadata;
			}
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncRelationshipMetadata(value);
				}
				else
				{
					if(value==null)
					{
						if(_relationshipMetadata != null)
						{
							_relationshipMetadata.UnsetRelatedEntity(this, "RelationAttributeValue");
						}
					}
					else
					{
						if(_relationshipMetadata!=value)
						{
							((IEntity2)value).SetRelatedEntity(this, "RelationAttributeValue");
						}
					}
				}
			}
		}

	
		
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the Dogwood.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		public override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Dogwood.Data.EntityType.RelationAttributeValueEntity; }
		}
		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
