﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:04 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using Dogwood.Data;
using Dogwood.Data.HelperClasses;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>
	/// Entity class which represents the entity 'ObjectRevision'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ObjectRevisionEntity : CommonEntityBase, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EntityCollection<AttributeValueEntity> _attributeValue;
		private EntityCollection<BranchHeadObjectEntity> _branchHeadObject;
		private EntityCollection<RelationAttributeValueEntity> _relationAttributeValue;
		private EntityCollection<RevisionHeadObjectEntity> _revisionHeadObject;
		private EntityCollection<ClassEntity> _classCollectionViaRevisionHeadObject;
		private EntityCollection<ObjectEntity> _objectCollectionViaRevisionHeadObject;
		private EntityCollection<RevisionEntity> _revisionCollectionViaRevisionHeadObject_;
		private EntityCollection<RevisionEntity> _revisionCollectionViaRevisionHeadObject;
		private ObjectEntity _object;
		private RevisionEntity _revision;

		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Object</summary>
			public static readonly string Object = "Object";
			/// <summary>Member name Revision</summary>
			public static readonly string Revision = "Revision";
			/// <summary>Member name AttributeValue</summary>
			public static readonly string AttributeValue = "AttributeValue";
			/// <summary>Member name BranchHeadObject</summary>
			public static readonly string BranchHeadObject = "BranchHeadObject";
			/// <summary>Member name RelationAttributeValue</summary>
			public static readonly string RelationAttributeValue = "RelationAttributeValue";
			/// <summary>Member name RevisionHeadObject</summary>
			public static readonly string RevisionHeadObject = "RevisionHeadObject";
			/// <summary>Member name ClassCollectionViaRevisionHeadObject</summary>
			public static readonly string ClassCollectionViaRevisionHeadObject = "ClassCollectionViaRevisionHeadObject";
			/// <summary>Member name ObjectCollectionViaRevisionHeadObject</summary>
			public static readonly string ObjectCollectionViaRevisionHeadObject = "ObjectCollectionViaRevisionHeadObject";
			/// <summary>Member name RevisionCollectionViaRevisionHeadObject_</summary>
			public static readonly string RevisionCollectionViaRevisionHeadObject_ = "RevisionCollectionViaRevisionHeadObject_";
			/// <summary>Member name RevisionCollectionViaRevisionHeadObject</summary>
			public static readonly string RevisionCollectionViaRevisionHeadObject = "RevisionCollectionViaRevisionHeadObject";

		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ObjectRevisionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public ObjectRevisionEntity():base("ObjectRevisionEntity")
		{
			InitClassEmpty(null, CreateFields());
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ObjectRevisionEntity(IEntityFields2 fields):base("ObjectRevisionEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ObjectRevisionEntity</param>
		public ObjectRevisionEntity(IValidator validator):base("ObjectRevisionEntity")
		{
			InitClassEmpty(validator, CreateFields());
		}
				

		/// <summary> CTor</summary>
		/// <param name="id">PK value for ObjectRevision which data should be fetched into this ObjectRevision object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ObjectRevisionEntity(System.Int32 id):base("ObjectRevisionEntity")
		{
			InitClassEmpty(null, CreateFields());
			this.Id = id;
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for ObjectRevision which data should be fetched into this ObjectRevision object</param>
		/// <param name="validator">The custom validator object for this ObjectRevisionEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ObjectRevisionEntity(System.Int32 id, IValidator validator):base("ObjectRevisionEntity")
		{
			InitClassEmpty(validator, CreateFields());
			this.Id = id;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected ObjectRevisionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_attributeValue = (EntityCollection<AttributeValueEntity>)info.GetValue("_attributeValue", typeof(EntityCollection<AttributeValueEntity>));
				_branchHeadObject = (EntityCollection<BranchHeadObjectEntity>)info.GetValue("_branchHeadObject", typeof(EntityCollection<BranchHeadObjectEntity>));
				_relationAttributeValue = (EntityCollection<RelationAttributeValueEntity>)info.GetValue("_relationAttributeValue", typeof(EntityCollection<RelationAttributeValueEntity>));
				_revisionHeadObject = (EntityCollection<RevisionHeadObjectEntity>)info.GetValue("_revisionHeadObject", typeof(EntityCollection<RevisionHeadObjectEntity>));
				_classCollectionViaRevisionHeadObject = (EntityCollection<ClassEntity>)info.GetValue("_classCollectionViaRevisionHeadObject", typeof(EntityCollection<ClassEntity>));
				_objectCollectionViaRevisionHeadObject = (EntityCollection<ObjectEntity>)info.GetValue("_objectCollectionViaRevisionHeadObject", typeof(EntityCollection<ObjectEntity>));
				_revisionCollectionViaRevisionHeadObject_ = (EntityCollection<RevisionEntity>)info.GetValue("_revisionCollectionViaRevisionHeadObject_", typeof(EntityCollection<RevisionEntity>));
				_revisionCollectionViaRevisionHeadObject = (EntityCollection<RevisionEntity>)info.GetValue("_revisionCollectionViaRevisionHeadObject", typeof(EntityCollection<RevisionEntity>));
				_object = (ObjectEntity)info.GetValue("_object", typeof(ObjectEntity));
				if(_object!=null)
				{
					_object.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_revision = (RevisionEntity)info.GetValue("_revision", typeof(RevisionEntity));
				if(_revision!=null)
				{
					_revision.AfterSave+=new EventHandler(OnEntityAfterSave);
				}

				base.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ObjectRevisionFieldIndex)fieldIndex)
			{
				case ObjectRevisionFieldIndex.ObjectId:
					DesetupSyncObject(true, false);
					break;
				case ObjectRevisionFieldIndex.RevisionId:
					DesetupSyncRevision(true, false);
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}
				
		/// <summary>Gets the inheritance info provider instance of the project this entity instance is located in. </summary>
		/// <returns>ready to use inheritance info provider instance.</returns>
		protected override IInheritanceInfoProvider GetInheritanceInfoProvider()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntityProperty(string propertyName, IEntity2 entity)
		{
			switch(propertyName)
			{
				case "Object":
					this.Object = (ObjectEntity)entity;
					break;
				case "Revision":
					this.Revision = (RevisionEntity)entity;
					break;
				case "AttributeValue":
					this.AttributeValue.Add((AttributeValueEntity)entity);
					break;
				case "BranchHeadObject":
					this.BranchHeadObject.Add((BranchHeadObjectEntity)entity);
					break;
				case "RelationAttributeValue":
					this.RelationAttributeValue.Add((RelationAttributeValueEntity)entity);
					break;
				case "RevisionHeadObject":
					this.RevisionHeadObject.Add((RevisionHeadObjectEntity)entity);
					break;
				case "ClassCollectionViaRevisionHeadObject":
					this.ClassCollectionViaRevisionHeadObject.IsReadOnly = false;
					this.ClassCollectionViaRevisionHeadObject.Add((ClassEntity)entity);
					this.ClassCollectionViaRevisionHeadObject.IsReadOnly = true;
					break;
				case "ObjectCollectionViaRevisionHeadObject":
					this.ObjectCollectionViaRevisionHeadObject.IsReadOnly = false;
					this.ObjectCollectionViaRevisionHeadObject.Add((ObjectEntity)entity);
					this.ObjectCollectionViaRevisionHeadObject.IsReadOnly = true;
					break;
				case "RevisionCollectionViaRevisionHeadObject_":
					this.RevisionCollectionViaRevisionHeadObject_.IsReadOnly = false;
					this.RevisionCollectionViaRevisionHeadObject_.Add((RevisionEntity)entity);
					this.RevisionCollectionViaRevisionHeadObject_.IsReadOnly = true;
					break;
				case "RevisionCollectionViaRevisionHeadObject":
					this.RevisionCollectionViaRevisionHeadObject.IsReadOnly = false;
					this.RevisionCollectionViaRevisionHeadObject.Add((RevisionEntity)entity);
					this.RevisionCollectionViaRevisionHeadObject.IsReadOnly = true;
					break;

				default:
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return ObjectRevisionEntity.GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Object":
					toReturn.Add(ObjectRevisionEntity.Relations.ObjectEntityUsingObjectId);
					break;
				case "Revision":
					toReturn.Add(ObjectRevisionEntity.Relations.RevisionEntityUsingRevisionId);
					break;
				case "AttributeValue":
					toReturn.Add(ObjectRevisionEntity.Relations.AttributeValueEntityUsingObjectRevisionId);
					break;
				case "BranchHeadObject":
					toReturn.Add(ObjectRevisionEntity.Relations.BranchHeadObjectEntityUsingObjectRevisionId);
					break;
				case "RelationAttributeValue":
					toReturn.Add(ObjectRevisionEntity.Relations.RelationAttributeValueEntityUsingObjectRevisionId);
					break;
				case "RevisionHeadObject":
					toReturn.Add(ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId);
					break;
				case "ClassCollectionViaRevisionHeadObject":
					toReturn.Add(ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId, "ObjectRevisionEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.ClassEntityUsingClassId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;
				case "ObjectCollectionViaRevisionHeadObject":
					toReturn.Add(ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId, "ObjectRevisionEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.ObjectEntityUsingObjectId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;
				case "RevisionCollectionViaRevisionHeadObject_":
					toReturn.Add(ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId, "ObjectRevisionEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.RevisionEntityUsingBranchId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;
				case "RevisionCollectionViaRevisionHeadObject":
					toReturn.Add(ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId, "ObjectRevisionEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.RevisionEntityUsingRevisionId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;

				default:

					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it
		/// will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override bool CheckOneWayRelations(string propertyName)
		{
			// use template trick to calculate the # of single-sided / oneway relations
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));



				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntity(IEntity2 relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Object":
					SetupSyncObject(relatedEntity);
					break;
				case "Revision":
					SetupSyncRevision(relatedEntity);
					break;
				case "AttributeValue":
					this.AttributeValue.Add((AttributeValueEntity)relatedEntity);
					break;
				case "BranchHeadObject":
					this.BranchHeadObject.Add((BranchHeadObjectEntity)relatedEntity);
					break;
				case "RelationAttributeValue":
					this.RelationAttributeValue.Add((RelationAttributeValueEntity)relatedEntity);
					break;
				case "RevisionHeadObject":
					this.RevisionHeadObject.Add((RevisionHeadObjectEntity)relatedEntity);
					break;

				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void UnsetRelatedEntity(IEntity2 relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Object":
					DesetupSyncObject(false, true);
					break;
				case "Revision":
					DesetupSyncRevision(false, true);
					break;
				case "AttributeValue":
					base.PerformRelatedEntityRemoval(this.AttributeValue, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BranchHeadObject":
					base.PerformRelatedEntityRemoval(this.BranchHeadObject, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RelationAttributeValue":
					base.PerformRelatedEntityRemoval(this.RelationAttributeValue, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RevisionHeadObject":
					base.PerformRelatedEntityRemoval(this.RevisionHeadObject, relatedEntity, signalRelatedEntityManyToOne);
					break;

				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		public override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();

			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		public override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			if(_object!=null)
			{
				toReturn.Add(_object);
			}
			if(_revision!=null)
			{
				toReturn.Add(_revision);
			}

			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. The contents of the ArrayList is used by the DataAccessAdapter to perform recursive saves. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		public override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.AttributeValue);
			toReturn.Add(this.BranchHeadObject);
			toReturn.Add(this.RelationAttributeValue);
			toReturn.Add(this.RevisionHeadObject);

			return toReturn;
		}
		


		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_attributeValue", ((_attributeValue!=null) && (_attributeValue.Count>0) && !this.MarkedForDeletion)?_attributeValue:null);
				info.AddValue("_branchHeadObject", ((_branchHeadObject!=null) && (_branchHeadObject.Count>0) && !this.MarkedForDeletion)?_branchHeadObject:null);
				info.AddValue("_relationAttributeValue", ((_relationAttributeValue!=null) && (_relationAttributeValue.Count>0) && !this.MarkedForDeletion)?_relationAttributeValue:null);
				info.AddValue("_revisionHeadObject", ((_revisionHeadObject!=null) && (_revisionHeadObject.Count>0) && !this.MarkedForDeletion)?_revisionHeadObject:null);
				info.AddValue("_classCollectionViaRevisionHeadObject", ((_classCollectionViaRevisionHeadObject!=null) && (_classCollectionViaRevisionHeadObject.Count>0) && !this.MarkedForDeletion)?_classCollectionViaRevisionHeadObject:null);
				info.AddValue("_objectCollectionViaRevisionHeadObject", ((_objectCollectionViaRevisionHeadObject!=null) && (_objectCollectionViaRevisionHeadObject.Count>0) && !this.MarkedForDeletion)?_objectCollectionViaRevisionHeadObject:null);
				info.AddValue("_revisionCollectionViaRevisionHeadObject_", ((_revisionCollectionViaRevisionHeadObject_!=null) && (_revisionCollectionViaRevisionHeadObject_.Count>0) && !this.MarkedForDeletion)?_revisionCollectionViaRevisionHeadObject_:null);
				info.AddValue("_revisionCollectionViaRevisionHeadObject", ((_revisionCollectionViaRevisionHeadObject!=null) && (_revisionCollectionViaRevisionHeadObject.Count>0) && !this.MarkedForDeletion)?_revisionCollectionViaRevisionHeadObject:null);
				info.AddValue("_object", (!this.MarkedForDeletion?_object:null));
				info.AddValue("_revision", (!this.MarkedForDeletion?_revision:null));

			}
			
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}

		/// <summary> Method which will construct a filter (predicate expression) for the unique constraint defined on the fields:
		/// ObjectId , RevisionId .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCObjectIdRevisionId()
		{
			IPredicateExpression filter = new PredicateExpression();
			filter.Add(new FieldCompareValuePredicate(base.Fields[(int)ObjectRevisionFieldIndex.ObjectId], null, ComparisonOperator.Equal));
			filter.Add(new FieldCompareValuePredicate(base.Fields[(int)ObjectRevisionFieldIndex.RevisionId], null, ComparisonOperator.Equal)); 
			return filter;
		}

		/// <summary>Returns true if the original value for the field with the fieldIndex passed in, read from the persistent storage was NULL, false otherwise.
		/// Should not be used for testing if the current value is NULL, use <see cref="TestCurrentFieldValueForNull"/> for that.</summary>
		/// <param name="fieldIndex">Index of the field to test if that field was NULL in the persistent storage</param>
		/// <returns>true if the field with the passed in index was NULL in the persistent storage, false otherwise</returns>
		public bool TestOriginalFieldValueForNull(ObjectRevisionFieldIndex fieldIndex)
		{
			return base.Fields[(int)fieldIndex].IsNull;
		}
		
		/// <summary>Returns true if the current value for the field with the fieldIndex passed in represents null/not defined, false otherwise.
		/// Should not be used for testing if the original value (read from the db) is NULL</summary>
		/// <param name="fieldIndex">Index of the field to test if its currentvalue is null/undefined</param>
		/// <returns>true if the field's value isn't defined yet, false otherwise</returns>
		public bool TestCurrentFieldValueForNull(ObjectRevisionFieldIndex fieldIndex)
		{
			return base.CheckIfCurrentFieldValueIsNull((int)fieldIndex);
		}

				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			return new ObjectRevisionRelations().GetAllRelations();
		}
		

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'AttributeValue' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAttributeValue()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(AttributeValueFields.ObjectRevisionId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'BranchHeadObject' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoBranchHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(BranchHeadObjectFields.ObjectRevisionId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'RelationAttributeValue' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRelationAttributeValue()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RelationAttributeValueFields.ObjectRevisionId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'RevisionHeadObject' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RevisionHeadObjectFields.ObjectRevisionId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Class' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClassCollectionViaRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("ClassCollectionViaRevisionHeadObject"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ObjectRevisionFields.Id, null, ComparisonOperator.Equal, this.Id, "ObjectRevisionEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Object' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObjectCollectionViaRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("ObjectCollectionViaRevisionHeadObject"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ObjectRevisionFields.Id, null, ComparisonOperator.Equal, this.Id, "ObjectRevisionEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Revision' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRevisionCollectionViaRevisionHeadObject_()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("RevisionCollectionViaRevisionHeadObject_"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ObjectRevisionFields.Id, null, ComparisonOperator.Equal, this.Id, "ObjectRevisionEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Revision' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRevisionCollectionViaRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("RevisionCollectionViaRevisionHeadObject"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ObjectRevisionFields.Id, null, ComparisonOperator.Equal, this.Id, "ObjectRevisionEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entity of type 'Object' to this entity. Use DataAccessAdapter.FetchNewEntity() to fetch this related entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ObjectFields.Id, null, ComparisonOperator.Equal, this.ObjectId));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entity of type 'Revision' to this entity. Use DataAccessAdapter.FetchNewEntity() to fetch this related entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRevision()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RevisionFields.Id, null, ComparisonOperator.Equal, this.RevisionId));
			return bucket;
		}

	
		
		/// <summary>Creates entity fields object for this entity. Used in constructor to setup this entity in a polymorphic scenario.</summary>
		protected virtual IEntityFields2 CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(Dogwood.Data.EntityType.ObjectRevisionEntity);
		}

		/// <summary>
		/// Creates the ITypeDefaultValue instance used to provide default values for value types which aren't of type nullable(of T)
		/// </summary>
		/// <returns></returns>
		protected override ITypeDefaultValue CreateTypeDefaultValueProvider()
		{
			return new TypeDefaultValue();
		}

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._attributeValue);
			collectionsQueue.Enqueue(this._branchHeadObject);
			collectionsQueue.Enqueue(this._relationAttributeValue);
			collectionsQueue.Enqueue(this._revisionHeadObject);
			collectionsQueue.Enqueue(this._classCollectionViaRevisionHeadObject);
			collectionsQueue.Enqueue(this._objectCollectionViaRevisionHeadObject);
			collectionsQueue.Enqueue(this._revisionCollectionViaRevisionHeadObject_);
			collectionsQueue.Enqueue(this._revisionCollectionViaRevisionHeadObject);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._attributeValue = (EntityCollection<AttributeValueEntity>) collectionsQueue.Dequeue();
			this._branchHeadObject = (EntityCollection<BranchHeadObjectEntity>) collectionsQueue.Dequeue();
			this._relationAttributeValue = (EntityCollection<RelationAttributeValueEntity>) collectionsQueue.Dequeue();
			this._revisionHeadObject = (EntityCollection<RevisionHeadObjectEntity>) collectionsQueue.Dequeue();
			this._classCollectionViaRevisionHeadObject = (EntityCollection<ClassEntity>) collectionsQueue.Dequeue();
			this._objectCollectionViaRevisionHeadObject = (EntityCollection<ObjectEntity>) collectionsQueue.Dequeue();
			this._revisionCollectionViaRevisionHeadObject_ = (EntityCollection<RevisionEntity>) collectionsQueue.Dequeue();
			this._revisionCollectionViaRevisionHeadObject = (EntityCollection<RevisionEntity>) collectionsQueue.Dequeue();
		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			if (this._attributeValue != null)
			{
				return true;
			}
			if (this._branchHeadObject != null)
			{
				return true;
			}
			if (this._relationAttributeValue != null)
			{
				return true;
			}
			if (this._revisionHeadObject != null)
			{
				return true;
			}
			if (this._classCollectionViaRevisionHeadObject != null)
			{
				return true;
			}
			if (this._objectCollectionViaRevisionHeadObject != null)
			{
				return true;
			}
			if (this._revisionCollectionViaRevisionHeadObject_ != null)
			{
				return true;
			}
			if (this._revisionCollectionViaRevisionHeadObject != null)
			{
				return true;
			}
			return base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<AttributeValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AttributeValueEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<BranchHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(BranchHeadObjectEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RelationAttributeValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationAttributeValueEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ClassEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory))) : null);
		}
#endif
		/// <summary>
		/// Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element. 
		/// </summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		public override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Object", _object);
			toReturn.Add("Revision", _revision);
			toReturn.Add("AttributeValue", _attributeValue);
			toReturn.Add("BranchHeadObject", _branchHeadObject);
			toReturn.Add("RelationAttributeValue", _relationAttributeValue);
			toReturn.Add("RevisionHeadObject", _revisionHeadObject);
			toReturn.Add("ClassCollectionViaRevisionHeadObject", _classCollectionViaRevisionHeadObject);
			toReturn.Add("ObjectCollectionViaRevisionHeadObject", _objectCollectionViaRevisionHeadObject);
			toReturn.Add("RevisionCollectionViaRevisionHeadObject_", _revisionCollectionViaRevisionHeadObject_);
			toReturn.Add("RevisionCollectionViaRevisionHeadObject", _revisionCollectionViaRevisionHeadObject);

			return toReturn;
		}
		
		/// <summary> Adds the internals to the active context. </summary>
		protected override void AddInternalsToContext()
		{
			if(_attributeValue!=null)
			{
				_attributeValue.ActiveContext = base.ActiveContext;
			}
			if(_branchHeadObject!=null)
			{
				_branchHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_relationAttributeValue!=null)
			{
				_relationAttributeValue.ActiveContext = base.ActiveContext;
			}
			if(_revisionHeadObject!=null)
			{
				_revisionHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_classCollectionViaRevisionHeadObject!=null)
			{
				_classCollectionViaRevisionHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_objectCollectionViaRevisionHeadObject!=null)
			{
				_objectCollectionViaRevisionHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_revisionCollectionViaRevisionHeadObject_!=null)
			{
				_revisionCollectionViaRevisionHeadObject_.ActiveContext = base.ActiveContext;
			}
			if(_revisionCollectionViaRevisionHeadObject!=null)
			{
				_revisionCollectionViaRevisionHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_object!=null)
			{
				_object.ActiveContext = base.ActiveContext;
			}
			if(_revision!=null)
			{
				_revision.ActiveContext = base.ActiveContext;
			}

		}

		/// <summary> Initializes the class members</summary>
		protected virtual void InitClassMembers()
		{

			_attributeValue = null;
			_branchHeadObject = null;
			_relationAttributeValue = null;
			_revisionHeadObject = null;
			_classCollectionViaRevisionHeadObject = null;
			_objectCollectionViaRevisionHeadObject = null;
			_revisionCollectionViaRevisionHeadObject_ = null;
			_revisionCollectionViaRevisionHeadObject = null;
			_object = null;
			_revision = null;

			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();

			Dictionary<string, string> fieldHashtable = null;
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("ObjectId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("RevisionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("IsDeleted", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _object</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncObject(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _object, new PropertyChangedEventHandler( OnObjectPropertyChanged ), "Object", ObjectRevisionEntity.Relations.ObjectEntityUsingObjectId, true, signalRelatedEntity, "ObjectRevision", resetFKFields, new int[] { (int)ObjectRevisionFieldIndex.ObjectId } );		
			_object = null;
		}

		/// <summary> setups the sync logic for member _object</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncObject(IEntity2 relatedEntity)
		{
			if(_object!=relatedEntity)
			{
				DesetupSyncObject(true, true);
				_object = (ObjectEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _object, new PropertyChangedEventHandler( OnObjectPropertyChanged ), "Object", ObjectRevisionEntity.Relations.ObjectEntityUsingObjectId, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnObjectPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _revision</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRevision(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _revision, new PropertyChangedEventHandler( OnRevisionPropertyChanged ), "Revision", ObjectRevisionEntity.Relations.RevisionEntityUsingRevisionId, true, signalRelatedEntity, "ObjectRevision", resetFKFields, new int[] { (int)ObjectRevisionFieldIndex.RevisionId } );		
			_revision = null;
		}

		/// <summary> setups the sync logic for member _revision</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRevision(IEntity2 relatedEntity)
		{
			if(_revision!=relatedEntity)
			{
				DesetupSyncRevision(true, true);
				_revision = (RevisionEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _revision, new PropertyChangedEventHandler( OnRevisionPropertyChanged ), "Revision", ObjectRevisionEntity.Relations.RevisionEntityUsingRevisionId, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRevisionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}


		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ObjectRevisionEntity</param>
		/// <param name="fields">Fields of this entity</param>
		protected virtual void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			base.Fields = fields;
			base.IsNew=true;
			base.Validator = validator;
			InitClassMembers();

			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ObjectRevisionRelations Relations
		{
			get	{ return new ObjectRevisionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AttributeValue' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAttributeValue
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<AttributeValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AttributeValueEntityFactory))),
					(IEntityRelation)GetRelationsForField("AttributeValue")[0], (int)Dogwood.Data.EntityType.ObjectRevisionEntity, (int)Dogwood.Data.EntityType.AttributeValueEntity, 0, null, null, null, null, "AttributeValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}
		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'BranchHeadObject' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathBranchHeadObject
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<BranchHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(BranchHeadObjectEntityFactory))),
					(IEntityRelation)GetRelationsForField("BranchHeadObject")[0], (int)Dogwood.Data.EntityType.ObjectRevisionEntity, (int)Dogwood.Data.EntityType.BranchHeadObjectEntity, 0, null, null, null, null, "BranchHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}
		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'RelationAttributeValue' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRelationAttributeValue
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<RelationAttributeValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationAttributeValueEntityFactory))),
					(IEntityRelation)GetRelationsForField("RelationAttributeValue")[0], (int)Dogwood.Data.EntityType.ObjectRevisionEntity, (int)Dogwood.Data.EntityType.RelationAttributeValueEntity, 0, null, null, null, null, "RelationAttributeValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}
		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'RevisionHeadObject' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRevisionHeadObject
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory))),
					(IEntityRelation)GetRelationsForField("RevisionHeadObject")[0], (int)Dogwood.Data.EntityType.ObjectRevisionEntity, (int)Dogwood.Data.EntityType.RevisionHeadObjectEntity, 0, null, null, null, null, "RevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Class' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClassCollectionViaRevisionHeadObject
		{
			get
			{
				IEntityRelation intermediateRelation = ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<ClassEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.ObjectRevisionEntity, (int)Dogwood.Data.EntityType.ClassEntity, 0, null, null, GetRelationsForField("ClassCollectionViaRevisionHeadObject"), null, "ClassCollectionViaRevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Object' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObjectCollectionViaRevisionHeadObject
		{
			get
			{
				IEntityRelation intermediateRelation = ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.ObjectRevisionEntity, (int)Dogwood.Data.EntityType.ObjectEntity, 0, null, null, GetRelationsForField("ObjectCollectionViaRevisionHeadObject"), null, "ObjectCollectionViaRevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Revision' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRevisionCollectionViaRevisionHeadObject_
		{
			get
			{
				IEntityRelation intermediateRelation = ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.ObjectRevisionEntity, (int)Dogwood.Data.EntityType.RevisionEntity, 0, null, null, GetRelationsForField("RevisionCollectionViaRevisionHeadObject_"), null, "RevisionCollectionViaRevisionHeadObject_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Revision' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRevisionCollectionViaRevisionHeadObject
		{
			get
			{
				IEntityRelation intermediateRelation = ObjectRevisionEntity.Relations.RevisionHeadObjectEntityUsingObjectRevisionId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.ObjectRevisionEntity, (int)Dogwood.Data.EntityType.RevisionEntity, 0, null, null, GetRelationsForField("RevisionCollectionViaRevisionHeadObject"), null, "RevisionCollectionViaRevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Object' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObject
		{
			get
			{
				return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))),
					(IEntityRelation)GetRelationsForField("Object")[0], (int)Dogwood.Data.EntityType.ObjectRevisionEntity, (int)Dogwood.Data.EntityType.ObjectEntity, 0, null, null, null, null, "Object", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Revision' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRevision
		{
			get
			{
				return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory))),
					(IEntityRelation)GetRelationsForField("Revision")[0], (int)Dogwood.Data.EntityType.ObjectRevisionEntity, (int)Dogwood.Data.EntityType.RevisionEntity, 0, null, null, null, null, "Revision", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return ObjectRevisionEntity.CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value
		/// pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return ObjectRevisionEntity.FieldsCustomProperties;}
		}

		/// <summary> The Id property of the Entity ObjectRevision<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblObjectRevision"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)ObjectRevisionFieldIndex.Id, true); }
			set	{ SetValue((int)ObjectRevisionFieldIndex.Id, value); }
		}

		/// <summary> The ObjectId property of the Entity ObjectRevision<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblObjectRevision"."ObjectId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ObjectId
		{
			get { return (System.Int32)GetValue((int)ObjectRevisionFieldIndex.ObjectId, true); }
			set	{ SetValue((int)ObjectRevisionFieldIndex.ObjectId, value); }
		}

		/// <summary> The RevisionId property of the Entity ObjectRevision<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblObjectRevision"."RevisionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RevisionId
		{
			get { return (System.Int32)GetValue((int)ObjectRevisionFieldIndex.RevisionId, true); }
			set	{ SetValue((int)ObjectRevisionFieldIndex.RevisionId, value); }
		}

		/// <summary> The IsDeleted property of the Entity ObjectRevision<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblObjectRevision"."IsDeleted"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsDeleted
		{
			get { return (System.Boolean)GetValue((int)ObjectRevisionFieldIndex.IsDeleted, true); }
			set	{ SetValue((int)ObjectRevisionFieldIndex.IsDeleted, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'AttributeValueEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(AttributeValueEntity))]
		public virtual EntityCollection<AttributeValueEntity> AttributeValue
		{
			get
			{
				if(_attributeValue==null)
				{
					_attributeValue = new EntityCollection<AttributeValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AttributeValueEntityFactory)));
					_attributeValue.SetContainingEntityInfo(this, "ObjectRevision");
				}
				return _attributeValue;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'BranchHeadObjectEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(BranchHeadObjectEntity))]
		public virtual EntityCollection<BranchHeadObjectEntity> BranchHeadObject
		{
			get
			{
				if(_branchHeadObject==null)
				{
					_branchHeadObject = new EntityCollection<BranchHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(BranchHeadObjectEntityFactory)));
					_branchHeadObject.SetContainingEntityInfo(this, "ObjectRevision");
				}
				return _branchHeadObject;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'RelationAttributeValueEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RelationAttributeValueEntity))]
		public virtual EntityCollection<RelationAttributeValueEntity> RelationAttributeValue
		{
			get
			{
				if(_relationAttributeValue==null)
				{
					_relationAttributeValue = new EntityCollection<RelationAttributeValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationAttributeValueEntityFactory)));
					_relationAttributeValue.SetContainingEntityInfo(this, "ObjectRevision");
				}
				return _relationAttributeValue;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'RevisionHeadObjectEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RevisionHeadObjectEntity))]
		public virtual EntityCollection<RevisionHeadObjectEntity> RevisionHeadObject
		{
			get
			{
				if(_revisionHeadObject==null)
				{
					_revisionHeadObject = new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory)));
					_revisionHeadObject.SetContainingEntityInfo(this, "ObjectRevision");
				}
				return _revisionHeadObject;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ClassEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ClassEntity))]
		public virtual EntityCollection<ClassEntity> ClassCollectionViaRevisionHeadObject
		{
			get
			{
				if(_classCollectionViaRevisionHeadObject==null)
				{
					_classCollectionViaRevisionHeadObject = new EntityCollection<ClassEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory)));
					_classCollectionViaRevisionHeadObject.IsReadOnly=true;
				}
				return _classCollectionViaRevisionHeadObject;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ObjectEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ObjectEntity))]
		public virtual EntityCollection<ObjectEntity> ObjectCollectionViaRevisionHeadObject
		{
			get
			{
				if(_objectCollectionViaRevisionHeadObject==null)
				{
					_objectCollectionViaRevisionHeadObject = new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory)));
					_objectCollectionViaRevisionHeadObject.IsReadOnly=true;
				}
				return _objectCollectionViaRevisionHeadObject;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'RevisionEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RevisionEntity))]
		public virtual EntityCollection<RevisionEntity> RevisionCollectionViaRevisionHeadObject_
		{
			get
			{
				if(_revisionCollectionViaRevisionHeadObject_==null)
				{
					_revisionCollectionViaRevisionHeadObject_ = new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory)));
					_revisionCollectionViaRevisionHeadObject_.IsReadOnly=true;
				}
				return _revisionCollectionViaRevisionHeadObject_;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'RevisionEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RevisionEntity))]
		public virtual EntityCollection<RevisionEntity> RevisionCollectionViaRevisionHeadObject
		{
			get
			{
				if(_revisionCollectionViaRevisionHeadObject==null)
				{
					_revisionCollectionViaRevisionHeadObject = new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory)));
					_revisionCollectionViaRevisionHeadObject.IsReadOnly=true;
				}
				return _revisionCollectionViaRevisionHeadObject;
			}
		}

		/// <summary> Gets / sets related entity of type 'ObjectEntity' which has to be set using a fetch action earlier. If no related entity
		/// is set for this property, null is returned. This property is not visible in databound grids.</summary>
		[Browsable(false)]
		public virtual ObjectEntity Object
		{
			get
			{
				return _object;
			}
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncObject(value);
				}
				else
				{
					if(value==null)
					{
						if(_object != null)
						{
							_object.UnsetRelatedEntity(this, "ObjectRevision");
						}
					}
					else
					{
						if(_object!=value)
						{
							((IEntity2)value).SetRelatedEntity(this, "ObjectRevision");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'RevisionEntity' which has to be set using a fetch action earlier. If no related entity
		/// is set for this property, null is returned. This property is not visible in databound grids.</summary>
		[Browsable(false)]
		public virtual RevisionEntity Revision
		{
			get
			{
				return _revision;
			}
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncRevision(value);
				}
				else
				{
					if(value==null)
					{
						if(_revision != null)
						{
							_revision.UnsetRelatedEntity(this, "ObjectRevision");
						}
					}
					else
					{
						if(_revision!=value)
						{
							((IEntity2)value).SetRelatedEntity(this, "ObjectRevision");
						}
					}
				}
			}
		}

	
		
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the Dogwood.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		public override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Dogwood.Data.EntityType.ObjectRevisionEntity; }
		}
		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
