﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:04 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using Dogwood.Data;
using Dogwood.Data.HelperClasses;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>
	/// Entity class which represents the entity 'RelationshipMetadata'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RelationshipMetadataEntity : CommonEntityBase, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EntityCollection<RelationAttributeValueEntity> _relationAttributeValue;

		private AttributeEntity _attribute;
		private ClassEntity _pkClass;
		private ClassEntity _fkClass;
		private ObjectEntity _object;

		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Attribute</summary>
			public static readonly string Attribute = "Attribute";
			/// <summary>Member name PkClass</summary>
			public static readonly string PkClass = "PkClass";
			/// <summary>Member name FkClass</summary>
			public static readonly string FkClass = "FkClass";
			/// <summary>Member name Object</summary>
			public static readonly string Object = "Object";
			/// <summary>Member name RelationAttributeValue</summary>
			public static readonly string RelationAttributeValue = "RelationAttributeValue";


		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RelationshipMetadataEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public RelationshipMetadataEntity():base("RelationshipMetadataEntity")
		{
			InitClassEmpty(null, CreateFields());
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public RelationshipMetadataEntity(IEntityFields2 fields):base("RelationshipMetadataEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this RelationshipMetadataEntity</param>
		public RelationshipMetadataEntity(IValidator validator):base("RelationshipMetadataEntity")
		{
			InitClassEmpty(validator, CreateFields());
		}
				

		/// <summary> CTor</summary>
		/// <param name="id">PK value for RelationshipMetadata which data should be fetched into this RelationshipMetadata object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public RelationshipMetadataEntity(System.Int32 id):base("RelationshipMetadataEntity")
		{
			InitClassEmpty(null, CreateFields());
			this.Id = id;
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for RelationshipMetadata which data should be fetched into this RelationshipMetadata object</param>
		/// <param name="validator">The custom validator object for this RelationshipMetadataEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public RelationshipMetadataEntity(System.Int32 id, IValidator validator):base("RelationshipMetadataEntity")
		{
			InitClassEmpty(validator, CreateFields());
			this.Id = id;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected RelationshipMetadataEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_relationAttributeValue = (EntityCollection<RelationAttributeValueEntity>)info.GetValue("_relationAttributeValue", typeof(EntityCollection<RelationAttributeValueEntity>));

				_attribute = (AttributeEntity)info.GetValue("_attribute", typeof(AttributeEntity));
				if(_attribute!=null)
				{
					_attribute.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_pkClass = (ClassEntity)info.GetValue("_pkClass", typeof(ClassEntity));
				if(_pkClass!=null)
				{
					_pkClass.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_fkClass = (ClassEntity)info.GetValue("_fkClass", typeof(ClassEntity));
				if(_fkClass!=null)
				{
					_fkClass.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_object = (ObjectEntity)info.GetValue("_object", typeof(ObjectEntity));
				if(_object!=null)
				{
					_object.AfterSave+=new EventHandler(OnEntityAfterSave);
				}

				base.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RelationshipMetadataFieldIndex)fieldIndex)
			{
				case RelationshipMetadataFieldIndex.PkClassId:
					DesetupSyncPkClass(true, false);
					break;
				case RelationshipMetadataFieldIndex.FkClassId:
					DesetupSyncFkClass(true, false);
					break;
				case RelationshipMetadataFieldIndex.AttributeId:
					DesetupSyncAttribute(true, false);
					break;
				case RelationshipMetadataFieldIndex.CascadeDefaultValue:
					DesetupSyncObject(true, false);
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}
				
		/// <summary>Gets the inheritance info provider instance of the project this entity instance is located in. </summary>
		/// <returns>ready to use inheritance info provider instance.</returns>
		protected override IInheritanceInfoProvider GetInheritanceInfoProvider()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntityProperty(string propertyName, IEntity2 entity)
		{
			switch(propertyName)
			{
				case "Attribute":
					this.Attribute = (AttributeEntity)entity;
					break;
				case "PkClass":
					this.PkClass = (ClassEntity)entity;
					break;
				case "FkClass":
					this.FkClass = (ClassEntity)entity;
					break;
				case "Object":
					this.Object = (ObjectEntity)entity;
					break;
				case "RelationAttributeValue":
					this.RelationAttributeValue.Add((RelationAttributeValueEntity)entity);
					break;


				default:
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return RelationshipMetadataEntity.GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Attribute":
					toReturn.Add(RelationshipMetadataEntity.Relations.AttributeEntityUsingAttributeId);
					break;
				case "PkClass":
					toReturn.Add(RelationshipMetadataEntity.Relations.ClassEntityUsingPkClassId);
					break;
				case "FkClass":
					toReturn.Add(RelationshipMetadataEntity.Relations.ClassEntityUsingFkClassId);
					break;
				case "Object":
					toReturn.Add(RelationshipMetadataEntity.Relations.ObjectEntityUsingCascadeDefaultValue);
					break;
				case "RelationAttributeValue":
					toReturn.Add(RelationshipMetadataEntity.Relations.RelationAttributeValueEntityUsingRelationshipMetadataId);
					break;


				default:

					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it
		/// will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override bool CheckOneWayRelations(string propertyName)
		{
			// use template trick to calculate the # of single-sided / oneway relations
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));





				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntity(IEntity2 relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Attribute":
					SetupSyncAttribute(relatedEntity);
					break;
				case "PkClass":
					SetupSyncPkClass(relatedEntity);
					break;
				case "FkClass":
					SetupSyncFkClass(relatedEntity);
					break;
				case "Object":
					SetupSyncObject(relatedEntity);
					break;
				case "RelationAttributeValue":
					this.RelationAttributeValue.Add((RelationAttributeValueEntity)relatedEntity);
					break;

				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void UnsetRelatedEntity(IEntity2 relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Attribute":
					DesetupSyncAttribute(false, true);
					break;
				case "PkClass":
					DesetupSyncPkClass(false, true);
					break;
				case "FkClass":
					DesetupSyncFkClass(false, true);
					break;
				case "Object":
					DesetupSyncObject(false, true);
					break;
				case "RelationAttributeValue":
					base.PerformRelatedEntityRemoval(this.RelationAttributeValue, relatedEntity, signalRelatedEntityManyToOne);
					break;

				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		public override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();

			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		public override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			if(_attribute!=null)
			{
				toReturn.Add(_attribute);
			}
			if(_pkClass!=null)
			{
				toReturn.Add(_pkClass);
			}
			if(_fkClass!=null)
			{
				toReturn.Add(_fkClass);
			}
			if(_object!=null)
			{
				toReturn.Add(_object);
			}

			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. The contents of the ArrayList is used by the DataAccessAdapter to perform recursive saves. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		public override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.RelationAttributeValue);

			return toReturn;
		}
		


		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_relationAttributeValue", ((_relationAttributeValue!=null) && (_relationAttributeValue.Count>0) && !this.MarkedForDeletion)?_relationAttributeValue:null);

				info.AddValue("_attribute", (!this.MarkedForDeletion?_attribute:null));
				info.AddValue("_pkClass", (!this.MarkedForDeletion?_pkClass:null));
				info.AddValue("_fkClass", (!this.MarkedForDeletion?_fkClass:null));
				info.AddValue("_object", (!this.MarkedForDeletion?_object:null));

			}
			
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}

		/// <summary> Method which will construct a filter (predicate expression) for the unique constraint defined on the fields:
		/// PkClassId , FkClassId , AttributeId .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCPkClassIdFkClassIdAttributeId()
		{
			IPredicateExpression filter = new PredicateExpression();
			filter.Add(new FieldCompareValuePredicate(base.Fields[(int)RelationshipMetadataFieldIndex.PkClassId], null, ComparisonOperator.Equal));
			filter.Add(new FieldCompareValuePredicate(base.Fields[(int)RelationshipMetadataFieldIndex.FkClassId], null, ComparisonOperator.Equal));
			filter.Add(new FieldCompareValuePredicate(base.Fields[(int)RelationshipMetadataFieldIndex.AttributeId], null, ComparisonOperator.Equal)); 
			return filter;
		}

		/// <summary>Returns true if the original value for the field with the fieldIndex passed in, read from the persistent storage was NULL, false otherwise.
		/// Should not be used for testing if the current value is NULL, use <see cref="TestCurrentFieldValueForNull"/> for that.</summary>
		/// <param name="fieldIndex">Index of the field to test if that field was NULL in the persistent storage</param>
		/// <returns>true if the field with the passed in index was NULL in the persistent storage, false otherwise</returns>
		public bool TestOriginalFieldValueForNull(RelationshipMetadataFieldIndex fieldIndex)
		{
			return base.Fields[(int)fieldIndex].IsNull;
		}
		
		/// <summary>Returns true if the current value for the field with the fieldIndex passed in represents null/not defined, false otherwise.
		/// Should not be used for testing if the original value (read from the db) is NULL</summary>
		/// <param name="fieldIndex">Index of the field to test if its currentvalue is null/undefined</param>
		/// <returns>true if the field's value isn't defined yet, false otherwise</returns>
		public bool TestCurrentFieldValueForNull(RelationshipMetadataFieldIndex fieldIndex)
		{
			return base.CheckIfCurrentFieldValueIsNull((int)fieldIndex);
		}

				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			return new RelationshipMetadataRelations().GetAllRelations();
		}
		

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'RelationAttributeValue' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRelationAttributeValue()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RelationAttributeValueFields.RelationshipMetadataId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}


		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entity of type 'Attribute' to this entity. Use DataAccessAdapter.FetchNewEntity() to fetch this related entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAttribute()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(AttributeFields.Id, null, ComparisonOperator.Equal, this.AttributeId));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entity of type 'Class' to this entity. Use DataAccessAdapter.FetchNewEntity() to fetch this related entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPkClass()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ClassFields.Id, null, ComparisonOperator.Equal, this.PkClassId));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entity of type 'Class' to this entity. Use DataAccessAdapter.FetchNewEntity() to fetch this related entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoFkClass()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ClassFields.Id, null, ComparisonOperator.Equal, this.FkClassId));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entity of type 'Object' to this entity. Use DataAccessAdapter.FetchNewEntity() to fetch this related entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ObjectFields.Id, null, ComparisonOperator.Equal, this.CascadeDefaultValue));
			return bucket;
		}

	
		
		/// <summary>Creates entity fields object for this entity. Used in constructor to setup this entity in a polymorphic scenario.</summary>
		protected virtual IEntityFields2 CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(Dogwood.Data.EntityType.RelationshipMetadataEntity);
		}

		/// <summary>
		/// Creates the ITypeDefaultValue instance used to provide default values for value types which aren't of type nullable(of T)
		/// </summary>
		/// <returns></returns>
		protected override ITypeDefaultValue CreateTypeDefaultValueProvider()
		{
			return new TypeDefaultValue();
		}

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(RelationshipMetadataEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._relationAttributeValue);

		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._relationAttributeValue = (EntityCollection<RelationAttributeValueEntity>) collectionsQueue.Dequeue();

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			if (this._relationAttributeValue != null)
			{
				return true;
			}

			return base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RelationAttributeValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationAttributeValueEntityFactory))) : null);

		}
#endif
		/// <summary>
		/// Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element. 
		/// </summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		public override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Attribute", _attribute);
			toReturn.Add("PkClass", _pkClass);
			toReturn.Add("FkClass", _fkClass);
			toReturn.Add("Object", _object);
			toReturn.Add("RelationAttributeValue", _relationAttributeValue);


			return toReturn;
		}
		
		/// <summary> Adds the internals to the active context. </summary>
		protected override void AddInternalsToContext()
		{
			if(_relationAttributeValue!=null)
			{
				_relationAttributeValue.ActiveContext = base.ActiveContext;
			}

			if(_attribute!=null)
			{
				_attribute.ActiveContext = base.ActiveContext;
			}
			if(_pkClass!=null)
			{
				_pkClass.ActiveContext = base.ActiveContext;
			}
			if(_fkClass!=null)
			{
				_fkClass.ActiveContext = base.ActiveContext;
			}
			if(_object!=null)
			{
				_object.ActiveContext = base.ActiveContext;
			}

		}

		/// <summary> Initializes the class members</summary>
		protected virtual void InitClassMembers()
		{

			_relationAttributeValue = null;

			_attribute = null;
			_pkClass = null;
			_fkClass = null;
			_object = null;

			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();

			Dictionary<string, string> fieldHashtable = null;
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("PkClassId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("FkClassId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("AttributeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("IsNullable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("OneToManyValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("CascadeBehavior", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("CascadeDefaultValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("IsImmutable", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _attribute</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttribute(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _attribute, new PropertyChangedEventHandler( OnAttributePropertyChanged ), "Attribute", RelationshipMetadataEntity.Relations.AttributeEntityUsingAttributeId, true, signalRelatedEntity, "RelationshipMetadata", resetFKFields, new int[] { (int)RelationshipMetadataFieldIndex.AttributeId } );		
			_attribute = null;
		}

		/// <summary> setups the sync logic for member _attribute</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttribute(IEntity2 relatedEntity)
		{
			if(_attribute!=relatedEntity)
			{
				DesetupSyncAttribute(true, true);
				_attribute = (AttributeEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _attribute, new PropertyChangedEventHandler( OnAttributePropertyChanged ), "Attribute", RelationshipMetadataEntity.Relations.AttributeEntityUsingAttributeId, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pkClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPkClass(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _pkClass, new PropertyChangedEventHandler( OnPkClassPropertyChanged ), "PkClass", RelationshipMetadataEntity.Relations.ClassEntityUsingPkClassId, true, signalRelatedEntity, "RelationshipMetadataAsPk", resetFKFields, new int[] { (int)RelationshipMetadataFieldIndex.PkClassId } );		
			_pkClass = null;
		}

		/// <summary> setups the sync logic for member _pkClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPkClass(IEntity2 relatedEntity)
		{
			if(_pkClass!=relatedEntity)
			{
				DesetupSyncPkClass(true, true);
				_pkClass = (ClassEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _pkClass, new PropertyChangedEventHandler( OnPkClassPropertyChanged ), "PkClass", RelationshipMetadataEntity.Relations.ClassEntityUsingPkClassId, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPkClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _fkClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFkClass(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _fkClass, new PropertyChangedEventHandler( OnFkClassPropertyChanged ), "FkClass", RelationshipMetadataEntity.Relations.ClassEntityUsingFkClassId, true, signalRelatedEntity, "RelationshipMetadataAsFk", resetFKFields, new int[] { (int)RelationshipMetadataFieldIndex.FkClassId } );		
			_fkClass = null;
		}

		/// <summary> setups the sync logic for member _fkClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFkClass(IEntity2 relatedEntity)
		{
			if(_fkClass!=relatedEntity)
			{
				DesetupSyncFkClass(true, true);
				_fkClass = (ClassEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _fkClass, new PropertyChangedEventHandler( OnFkClassPropertyChanged ), "FkClass", RelationshipMetadataEntity.Relations.ClassEntityUsingFkClassId, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFkClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _object</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncObject(bool signalRelatedEntity, bool resetFKFields)
		{
			base.PerformDesetupSyncRelatedEntity( _object, new PropertyChangedEventHandler( OnObjectPropertyChanged ), "Object", RelationshipMetadataEntity.Relations.ObjectEntityUsingCascadeDefaultValue, true, signalRelatedEntity, "RelationshipMetadata", resetFKFields, new int[] { (int)RelationshipMetadataFieldIndex.CascadeDefaultValue } );		
			_object = null;
		}

		/// <summary> setups the sync logic for member _object</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncObject(IEntity2 relatedEntity)
		{
			if(_object!=relatedEntity)
			{
				DesetupSyncObject(true, true);
				_object = (ObjectEntity)relatedEntity;
				base.PerformSetupSyncRelatedEntity( _object, new PropertyChangedEventHandler( OnObjectPropertyChanged ), "Object", RelationshipMetadataEntity.Relations.ObjectEntityUsingCascadeDefaultValue, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnObjectPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}


		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this RelationshipMetadataEntity</param>
		/// <param name="fields">Fields of this entity</param>
		protected virtual void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			base.Fields = fields;
			base.IsNew=true;
			base.Validator = validator;
			InitClassMembers();

			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RelationshipMetadataRelations Relations
		{
			get	{ return new RelationshipMetadataRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'RelationAttributeValue' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRelationAttributeValue
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<RelationAttributeValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationAttributeValueEntityFactory))),
					(IEntityRelation)GetRelationsForField("RelationAttributeValue")[0], (int)Dogwood.Data.EntityType.RelationshipMetadataEntity, (int)Dogwood.Data.EntityType.RelationAttributeValueEntity, 0, null, null, null, null, "RelationAttributeValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}


		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Attribute' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAttribute
		{
			get
			{
				return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(AttributeEntityFactory))),
					(IEntityRelation)GetRelationsForField("Attribute")[0], (int)Dogwood.Data.EntityType.RelationshipMetadataEntity, (int)Dogwood.Data.EntityType.AttributeEntity, 0, null, null, null, null, "Attribute", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Class' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPkClass
		{
			get
			{
				return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory))),
					(IEntityRelation)GetRelationsForField("PkClass")[0], (int)Dogwood.Data.EntityType.RelationshipMetadataEntity, (int)Dogwood.Data.EntityType.ClassEntity, 0, null, null, null, null, "PkClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Class' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathFkClass
		{
			get
			{
				return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory))),
					(IEntityRelation)GetRelationsForField("FkClass")[0], (int)Dogwood.Data.EntityType.RelationshipMetadataEntity, (int)Dogwood.Data.EntityType.ClassEntity, 0, null, null, null, null, "FkClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Object' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObject
		{
			get
			{
				return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))),
					(IEntityRelation)GetRelationsForField("Object")[0], (int)Dogwood.Data.EntityType.RelationshipMetadataEntity, (int)Dogwood.Data.EntityType.ObjectEntity, 0, null, null, null, null, "Object", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return RelationshipMetadataEntity.CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value
		/// pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return RelationshipMetadataEntity.FieldsCustomProperties;}
		}

		/// <summary> The Id property of the Entity RelationshipMetadata<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationshipMetadata"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)RelationshipMetadataFieldIndex.Id, true); }
			set	{ SetValue((int)RelationshipMetadataFieldIndex.Id, value); }
		}

		/// <summary> The PkClassId property of the Entity RelationshipMetadata<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationshipMetadata"."PkClassId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PkClassId
		{
			get { return (System.Int32)GetValue((int)RelationshipMetadataFieldIndex.PkClassId, true); }
			set	{ SetValue((int)RelationshipMetadataFieldIndex.PkClassId, value); }
		}

		/// <summary> The FkClassId property of the Entity RelationshipMetadata<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationshipMetadata"."FkClassId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FkClassId
		{
			get { return (System.Int32)GetValue((int)RelationshipMetadataFieldIndex.FkClassId, true); }
			set	{ SetValue((int)RelationshipMetadataFieldIndex.FkClassId, value); }
		}

		/// <summary> The AttributeId property of the Entity RelationshipMetadata<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationshipMetadata"."AttributeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AttributeId
		{
			get { return (System.Int32)GetValue((int)RelationshipMetadataFieldIndex.AttributeId, true); }
			set	{ SetValue((int)RelationshipMetadataFieldIndex.AttributeId, value); }
		}

		/// <summary> The IsNullable property of the Entity RelationshipMetadata<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationshipMetadata"."IsNullable"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsNullable
		{
			get { return (System.Boolean)GetValue((int)RelationshipMetadataFieldIndex.IsNullable, true); }
			set	{ SetValue((int)RelationshipMetadataFieldIndex.IsNullable, value); }
		}

		/// <summary> The OneToManyValue property of the Entity RelationshipMetadata<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationshipMetadata"."OneToManyValue"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OneToManyValue
		{
			get { return (System.String)GetValue((int)RelationshipMetadataFieldIndex.OneToManyValue, true); }
			set	{ SetValue((int)RelationshipMetadataFieldIndex.OneToManyValue, value); }
		}

		/// <summary> The CascadeBehavior property of the Entity RelationshipMetadata<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationshipMetadata"."CascadeBehavior"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CascadeBehavior
		{
			get { return (System.Int32)GetValue((int)RelationshipMetadataFieldIndex.CascadeBehavior, true); }
			set	{ SetValue((int)RelationshipMetadataFieldIndex.CascadeBehavior, value); }
		}

		/// <summary> The CascadeDefaultValue property of the Entity RelationshipMetadata<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationshipMetadata"."CascadeDefaultValue"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CascadeDefaultValue
		{
			get { return (Nullable<System.Int32>)GetValue((int)RelationshipMetadataFieldIndex.CascadeDefaultValue, false); }
			set	{ SetValue((int)RelationshipMetadataFieldIndex.CascadeDefaultValue, value); }
		}

		/// <summary> The IsImmutable property of the Entity RelationshipMetadata<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRelationshipMetadata"."IsImmutable"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsImmutable
		{
			get { return (System.Boolean)GetValue((int)RelationshipMetadataFieldIndex.IsImmutable, true); }
			set	{ SetValue((int)RelationshipMetadataFieldIndex.IsImmutable, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'RelationAttributeValueEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RelationAttributeValueEntity))]
		public virtual EntityCollection<RelationAttributeValueEntity> RelationAttributeValue
		{
			get
			{
				if(_relationAttributeValue==null)
				{
					_relationAttributeValue = new EntityCollection<RelationAttributeValueEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationAttributeValueEntityFactory)));
					_relationAttributeValue.SetContainingEntityInfo(this, "RelationshipMetadata");
				}
				return _relationAttributeValue;
			}
		}


		/// <summary> Gets / sets related entity of type 'AttributeEntity' which has to be set using a fetch action earlier. If no related entity
		/// is set for this property, null is returned. This property is not visible in databound grids.</summary>
		[Browsable(false)]
		public virtual AttributeEntity Attribute
		{
			get
			{
				return _attribute;
			}
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncAttribute(value);
				}
				else
				{
					if(value==null)
					{
						if(_attribute != null)
						{
							_attribute.UnsetRelatedEntity(this, "RelationshipMetadata");
						}
					}
					else
					{
						if(_attribute!=value)
						{
							((IEntity2)value).SetRelatedEntity(this, "RelationshipMetadata");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'ClassEntity' which has to be set using a fetch action earlier. If no related entity
		/// is set for this property, null is returned. This property is not visible in databound grids.</summary>
		[Browsable(false)]
		public virtual ClassEntity PkClass
		{
			get
			{
				return _pkClass;
			}
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncPkClass(value);
				}
				else
				{
					if(value==null)
					{
						if(_pkClass != null)
						{
							_pkClass.UnsetRelatedEntity(this, "RelationshipMetadataAsPk");
						}
					}
					else
					{
						if(_pkClass!=value)
						{
							((IEntity2)value).SetRelatedEntity(this, "RelationshipMetadataAsPk");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'ClassEntity' which has to be set using a fetch action earlier. If no related entity
		/// is set for this property, null is returned. This property is not visible in databound grids.</summary>
		[Browsable(false)]
		public virtual ClassEntity FkClass
		{
			get
			{
				return _fkClass;
			}
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncFkClass(value);
				}
				else
				{
					if(value==null)
					{
						if(_fkClass != null)
						{
							_fkClass.UnsetRelatedEntity(this, "RelationshipMetadataAsFk");
						}
					}
					else
					{
						if(_fkClass!=value)
						{
							((IEntity2)value).SetRelatedEntity(this, "RelationshipMetadataAsFk");
						}
					}
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'ObjectEntity' which has to be set using a fetch action earlier. If no related entity
		/// is set for this property, null is returned. This property is not visible in databound grids.</summary>
		[Browsable(false)]
		public virtual ObjectEntity Object
		{
			get
			{
				return _object;
			}
			set
			{
				if(base.IsDeserializing)
				{
					SetupSyncObject(value);
				}
				else
				{
					if(value==null)
					{
						if(_object != null)
						{
							_object.UnsetRelatedEntity(this, "RelationshipMetadata");
						}
					}
					else
					{
						if(_object!=value)
						{
							((IEntity2)value).SetRelatedEntity(this, "RelationshipMetadata");
						}
					}
				}
			}
		}

	
		
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the Dogwood.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		public override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Dogwood.Data.EntityType.RelationshipMetadataEntity; }
		}
		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
