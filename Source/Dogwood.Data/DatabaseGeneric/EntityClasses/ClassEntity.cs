﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:04 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using Dogwood.Data;
using Dogwood.Data.HelperClasses;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>
	/// Entity class which represents the entity 'Class'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ClassEntity : CommonEntityBase, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EntityCollection<AttributeMetadataEntity> _attributeMetadata;
		private EntityCollection<BranchHeadObjectEntity> _branchHeadObject;
		private EntityCollection<ObjectEntity> _object;
		private EntityCollection<RelationshipMetadataEntity> _relationshipMetadataAsPk;
		private EntityCollection<RelationshipMetadataEntity> _relationshipMetadataAsFk;
		private EntityCollection<RevisionHeadObjectEntity> _revisionHeadObject;
		private EntityCollection<ObjectEntity> _objectCollectionViaRevisionHeadObject;
		private EntityCollection<ObjectRevisionEntity> _objectRevisionCollectionViaRevisionHeadObject;
		private EntityCollection<RevisionEntity> _revisionCollectionViaRevisionHeadObject_;
		private EntityCollection<RevisionEntity> _revisionCollectionViaRevisionHeadObject;


		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{

			/// <summary>Member name AttributeMetadata</summary>
			public static readonly string AttributeMetadata = "AttributeMetadata";
			/// <summary>Member name BranchHeadObject</summary>
			public static readonly string BranchHeadObject = "BranchHeadObject";
			/// <summary>Member name Object</summary>
			public static readonly string Object = "Object";
			/// <summary>Member name RelationshipMetadataAsPk</summary>
			public static readonly string RelationshipMetadataAsPk = "RelationshipMetadataAsPk";
			/// <summary>Member name RelationshipMetadataAsFk</summary>
			public static readonly string RelationshipMetadataAsFk = "RelationshipMetadataAsFk";
			/// <summary>Member name RevisionHeadObject</summary>
			public static readonly string RevisionHeadObject = "RevisionHeadObject";
			/// <summary>Member name ObjectCollectionViaRevisionHeadObject</summary>
			public static readonly string ObjectCollectionViaRevisionHeadObject = "ObjectCollectionViaRevisionHeadObject";
			/// <summary>Member name ObjectRevisionCollectionViaRevisionHeadObject</summary>
			public static readonly string ObjectRevisionCollectionViaRevisionHeadObject = "ObjectRevisionCollectionViaRevisionHeadObject";
			/// <summary>Member name RevisionCollectionViaRevisionHeadObject_</summary>
			public static readonly string RevisionCollectionViaRevisionHeadObject_ = "RevisionCollectionViaRevisionHeadObject_";
			/// <summary>Member name RevisionCollectionViaRevisionHeadObject</summary>
			public static readonly string RevisionCollectionViaRevisionHeadObject = "RevisionCollectionViaRevisionHeadObject";

		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ClassEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public ClassEntity():base("ClassEntity")
		{
			InitClassEmpty(null, CreateFields());
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public ClassEntity(IEntityFields2 fields):base("ClassEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this ClassEntity</param>
		public ClassEntity(IValidator validator):base("ClassEntity")
		{
			InitClassEmpty(validator, CreateFields());
		}
				

		/// <summary> CTor</summary>
		/// <param name="id">PK value for Class which data should be fetched into this Class object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ClassEntity(System.Int32 id):base("ClassEntity")
		{
			InitClassEmpty(null, CreateFields());
			this.Id = id;
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for Class which data should be fetched into this Class object</param>
		/// <param name="validator">The custom validator object for this ClassEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public ClassEntity(System.Int32 id, IValidator validator):base("ClassEntity")
		{
			InitClassEmpty(validator, CreateFields());
			this.Id = id;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected ClassEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_attributeMetadata = (EntityCollection<AttributeMetadataEntity>)info.GetValue("_attributeMetadata", typeof(EntityCollection<AttributeMetadataEntity>));
				_branchHeadObject = (EntityCollection<BranchHeadObjectEntity>)info.GetValue("_branchHeadObject", typeof(EntityCollection<BranchHeadObjectEntity>));
				_object = (EntityCollection<ObjectEntity>)info.GetValue("_object", typeof(EntityCollection<ObjectEntity>));
				_relationshipMetadataAsPk = (EntityCollection<RelationshipMetadataEntity>)info.GetValue("_relationshipMetadataAsPk", typeof(EntityCollection<RelationshipMetadataEntity>));
				_relationshipMetadataAsFk = (EntityCollection<RelationshipMetadataEntity>)info.GetValue("_relationshipMetadataAsFk", typeof(EntityCollection<RelationshipMetadataEntity>));
				_revisionHeadObject = (EntityCollection<RevisionHeadObjectEntity>)info.GetValue("_revisionHeadObject", typeof(EntityCollection<RevisionHeadObjectEntity>));
				_objectCollectionViaRevisionHeadObject = (EntityCollection<ObjectEntity>)info.GetValue("_objectCollectionViaRevisionHeadObject", typeof(EntityCollection<ObjectEntity>));
				_objectRevisionCollectionViaRevisionHeadObject = (EntityCollection<ObjectRevisionEntity>)info.GetValue("_objectRevisionCollectionViaRevisionHeadObject", typeof(EntityCollection<ObjectRevisionEntity>));
				_revisionCollectionViaRevisionHeadObject_ = (EntityCollection<RevisionEntity>)info.GetValue("_revisionCollectionViaRevisionHeadObject_", typeof(EntityCollection<RevisionEntity>));
				_revisionCollectionViaRevisionHeadObject = (EntityCollection<RevisionEntity>)info.GetValue("_revisionCollectionViaRevisionHeadObject", typeof(EntityCollection<RevisionEntity>));


				base.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ClassFieldIndex)fieldIndex)
			{
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}
				
		/// <summary>Gets the inheritance info provider instance of the project this entity instance is located in. </summary>
		/// <returns>ready to use inheritance info provider instance.</returns>
		protected override IInheritanceInfoProvider GetInheritanceInfoProvider()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntityProperty(string propertyName, IEntity2 entity)
		{
			switch(propertyName)
			{

				case "AttributeMetadata":
					this.AttributeMetadata.Add((AttributeMetadataEntity)entity);
					break;
				case "BranchHeadObject":
					this.BranchHeadObject.Add((BranchHeadObjectEntity)entity);
					break;
				case "Object":
					this.Object.Add((ObjectEntity)entity);
					break;
				case "RelationshipMetadataAsPk":
					this.RelationshipMetadataAsPk.Add((RelationshipMetadataEntity)entity);
					break;
				case "RelationshipMetadataAsFk":
					this.RelationshipMetadataAsFk.Add((RelationshipMetadataEntity)entity);
					break;
				case "RevisionHeadObject":
					this.RevisionHeadObject.Add((RevisionHeadObjectEntity)entity);
					break;
				case "ObjectCollectionViaRevisionHeadObject":
					this.ObjectCollectionViaRevisionHeadObject.IsReadOnly = false;
					this.ObjectCollectionViaRevisionHeadObject.Add((ObjectEntity)entity);
					this.ObjectCollectionViaRevisionHeadObject.IsReadOnly = true;
					break;
				case "ObjectRevisionCollectionViaRevisionHeadObject":
					this.ObjectRevisionCollectionViaRevisionHeadObject.IsReadOnly = false;
					this.ObjectRevisionCollectionViaRevisionHeadObject.Add((ObjectRevisionEntity)entity);
					this.ObjectRevisionCollectionViaRevisionHeadObject.IsReadOnly = true;
					break;
				case "RevisionCollectionViaRevisionHeadObject_":
					this.RevisionCollectionViaRevisionHeadObject_.IsReadOnly = false;
					this.RevisionCollectionViaRevisionHeadObject_.Add((RevisionEntity)entity);
					this.RevisionCollectionViaRevisionHeadObject_.IsReadOnly = true;
					break;
				case "RevisionCollectionViaRevisionHeadObject":
					this.RevisionCollectionViaRevisionHeadObject.IsReadOnly = false;
					this.RevisionCollectionViaRevisionHeadObject.Add((RevisionEntity)entity);
					this.RevisionCollectionViaRevisionHeadObject.IsReadOnly = true;
					break;

				default:
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return ClassEntity.GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{

				case "AttributeMetadata":
					toReturn.Add(ClassEntity.Relations.AttributeMetadataEntityUsingClassId);
					break;
				case "BranchHeadObject":
					toReturn.Add(ClassEntity.Relations.BranchHeadObjectEntityUsingClassId);
					break;
				case "Object":
					toReturn.Add(ClassEntity.Relations.ObjectEntityUsingClassId);
					break;
				case "RelationshipMetadataAsPk":
					toReturn.Add(ClassEntity.Relations.RelationshipMetadataEntityUsingPkClassId);
					break;
				case "RelationshipMetadataAsFk":
					toReturn.Add(ClassEntity.Relations.RelationshipMetadataEntityUsingFkClassId);
					break;
				case "RevisionHeadObject":
					toReturn.Add(ClassEntity.Relations.RevisionHeadObjectEntityUsingClassId);
					break;
				case "ObjectCollectionViaRevisionHeadObject":
					toReturn.Add(ClassEntity.Relations.RevisionHeadObjectEntityUsingClassId, "ClassEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.ObjectEntityUsingObjectId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;
				case "ObjectRevisionCollectionViaRevisionHeadObject":
					toReturn.Add(ClassEntity.Relations.RevisionHeadObjectEntityUsingClassId, "ClassEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.ObjectRevisionEntityUsingObjectRevisionId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;
				case "RevisionCollectionViaRevisionHeadObject_":
					toReturn.Add(ClassEntity.Relations.RevisionHeadObjectEntityUsingClassId, "ClassEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.RevisionEntityUsingBranchId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;
				case "RevisionCollectionViaRevisionHeadObject":
					toReturn.Add(ClassEntity.Relations.RevisionHeadObjectEntityUsingClassId, "ClassEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.RevisionEntityUsingRevisionId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;

				default:

					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it
		/// will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override bool CheckOneWayRelations(string propertyName)
		{
			// use template trick to calculate the # of single-sided / oneway relations
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));


				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntity(IEntity2 relatedEntity, string fieldName)
		{
			switch(fieldName)
			{

				case "AttributeMetadata":
					this.AttributeMetadata.Add((AttributeMetadataEntity)relatedEntity);
					break;
				case "BranchHeadObject":
					this.BranchHeadObject.Add((BranchHeadObjectEntity)relatedEntity);
					break;
				case "Object":
					this.Object.Add((ObjectEntity)relatedEntity);
					break;
				case "RelationshipMetadataAsPk":
					this.RelationshipMetadataAsPk.Add((RelationshipMetadataEntity)relatedEntity);
					break;
				case "RelationshipMetadataAsFk":
					this.RelationshipMetadataAsFk.Add((RelationshipMetadataEntity)relatedEntity);
					break;
				case "RevisionHeadObject":
					this.RevisionHeadObject.Add((RevisionHeadObjectEntity)relatedEntity);
					break;

				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void UnsetRelatedEntity(IEntity2 relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{

				case "AttributeMetadata":
					base.PerformRelatedEntityRemoval(this.AttributeMetadata, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BranchHeadObject":
					base.PerformRelatedEntityRemoval(this.BranchHeadObject, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Object":
					base.PerformRelatedEntityRemoval(this.Object, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RelationshipMetadataAsPk":
					base.PerformRelatedEntityRemoval(this.RelationshipMetadataAsPk, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RelationshipMetadataAsFk":
					base.PerformRelatedEntityRemoval(this.RelationshipMetadataAsFk, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RevisionHeadObject":
					base.PerformRelatedEntityRemoval(this.RevisionHeadObject, relatedEntity, signalRelatedEntityManyToOne);
					break;

				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		public override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();

			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		public override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();


			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. The contents of the ArrayList is used by the DataAccessAdapter to perform recursive saves. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		public override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.AttributeMetadata);
			toReturn.Add(this.BranchHeadObject);
			toReturn.Add(this.Object);
			toReturn.Add(this.RelationshipMetadataAsPk);
			toReturn.Add(this.RelationshipMetadataAsFk);
			toReturn.Add(this.RevisionHeadObject);

			return toReturn;
		}
		


		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_attributeMetadata", ((_attributeMetadata!=null) && (_attributeMetadata.Count>0) && !this.MarkedForDeletion)?_attributeMetadata:null);
				info.AddValue("_branchHeadObject", ((_branchHeadObject!=null) && (_branchHeadObject.Count>0) && !this.MarkedForDeletion)?_branchHeadObject:null);
				info.AddValue("_object", ((_object!=null) && (_object.Count>0) && !this.MarkedForDeletion)?_object:null);
				info.AddValue("_relationshipMetadataAsPk", ((_relationshipMetadataAsPk!=null) && (_relationshipMetadataAsPk.Count>0) && !this.MarkedForDeletion)?_relationshipMetadataAsPk:null);
				info.AddValue("_relationshipMetadataAsFk", ((_relationshipMetadataAsFk!=null) && (_relationshipMetadataAsFk.Count>0) && !this.MarkedForDeletion)?_relationshipMetadataAsFk:null);
				info.AddValue("_revisionHeadObject", ((_revisionHeadObject!=null) && (_revisionHeadObject.Count>0) && !this.MarkedForDeletion)?_revisionHeadObject:null);
				info.AddValue("_objectCollectionViaRevisionHeadObject", ((_objectCollectionViaRevisionHeadObject!=null) && (_objectCollectionViaRevisionHeadObject.Count>0) && !this.MarkedForDeletion)?_objectCollectionViaRevisionHeadObject:null);
				info.AddValue("_objectRevisionCollectionViaRevisionHeadObject", ((_objectRevisionCollectionViaRevisionHeadObject!=null) && (_objectRevisionCollectionViaRevisionHeadObject.Count>0) && !this.MarkedForDeletion)?_objectRevisionCollectionViaRevisionHeadObject:null);
				info.AddValue("_revisionCollectionViaRevisionHeadObject_", ((_revisionCollectionViaRevisionHeadObject_!=null) && (_revisionCollectionViaRevisionHeadObject_.Count>0) && !this.MarkedForDeletion)?_revisionCollectionViaRevisionHeadObject_:null);
				info.AddValue("_revisionCollectionViaRevisionHeadObject", ((_revisionCollectionViaRevisionHeadObject!=null) && (_revisionCollectionViaRevisionHeadObject.Count>0) && !this.MarkedForDeletion)?_revisionCollectionViaRevisionHeadObject:null);


			}
			
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}

		/// <summary> Method which will construct a filter (predicate expression) for the unique constraint defined on the fields:
		/// Name .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCName()
		{
			IPredicateExpression filter = new PredicateExpression();
			filter.Add(new FieldCompareValuePredicate(base.Fields[(int)ClassFieldIndex.Name], null, ComparisonOperator.Equal)); 
			return filter;
		}

		/// <summary>Returns true if the original value for the field with the fieldIndex passed in, read from the persistent storage was NULL, false otherwise.
		/// Should not be used for testing if the current value is NULL, use <see cref="TestCurrentFieldValueForNull"/> for that.</summary>
		/// <param name="fieldIndex">Index of the field to test if that field was NULL in the persistent storage</param>
		/// <returns>true if the field with the passed in index was NULL in the persistent storage, false otherwise</returns>
		public bool TestOriginalFieldValueForNull(ClassFieldIndex fieldIndex)
		{
			return base.Fields[(int)fieldIndex].IsNull;
		}
		
		/// <summary>Returns true if the current value for the field with the fieldIndex passed in represents null/not defined, false otherwise.
		/// Should not be used for testing if the original value (read from the db) is NULL</summary>
		/// <param name="fieldIndex">Index of the field to test if its currentvalue is null/undefined</param>
		/// <returns>true if the field's value isn't defined yet, false otherwise</returns>
		public bool TestCurrentFieldValueForNull(ClassFieldIndex fieldIndex)
		{
			return base.CheckIfCurrentFieldValueIsNull((int)fieldIndex);
		}

				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			return new ClassRelations().GetAllRelations();
		}
		

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'AttributeMetadata' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoAttributeMetadata()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(AttributeMetadataFields.ClassId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'BranchHeadObject' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoBranchHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(BranchHeadObjectFields.ClassId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Object' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ObjectFields.ClassId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'RelationshipMetadata' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRelationshipMetadataAsPk()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RelationshipMetadataFields.PkClassId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'RelationshipMetadata' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRelationshipMetadataAsFk()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RelationshipMetadataFields.FkClassId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'RevisionHeadObject' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RevisionHeadObjectFields.ClassId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Object' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObjectCollectionViaRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("ObjectCollectionViaRevisionHeadObject"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ClassFields.Id, null, ComparisonOperator.Equal, this.Id, "ClassEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'ObjectRevision' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObjectRevisionCollectionViaRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("ObjectRevisionCollectionViaRevisionHeadObject"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ClassFields.Id, null, ComparisonOperator.Equal, this.Id, "ClassEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Revision' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRevisionCollectionViaRevisionHeadObject_()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("RevisionCollectionViaRevisionHeadObject_"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ClassFields.Id, null, ComparisonOperator.Equal, this.Id, "ClassEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Revision' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRevisionCollectionViaRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("RevisionCollectionViaRevisionHeadObject"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ClassFields.Id, null, ComparisonOperator.Equal, this.Id, "ClassEntity__"));
			return bucket;
		}


	
		
		/// <summary>Creates entity fields object for this entity. Used in constructor to setup this entity in a polymorphic scenario.</summary>
		protected virtual IEntityFields2 CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(Dogwood.Data.EntityType.ClassEntity);
		}

		/// <summary>
		/// Creates the ITypeDefaultValue instance used to provide default values for value types which aren't of type nullable(of T)
		/// </summary>
		/// <returns></returns>
		protected override ITypeDefaultValue CreateTypeDefaultValueProvider()
		{
			return new TypeDefaultValue();
		}

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._attributeMetadata);
			collectionsQueue.Enqueue(this._branchHeadObject);
			collectionsQueue.Enqueue(this._object);
			collectionsQueue.Enqueue(this._relationshipMetadataAsPk);
			collectionsQueue.Enqueue(this._relationshipMetadataAsFk);
			collectionsQueue.Enqueue(this._revisionHeadObject);
			collectionsQueue.Enqueue(this._objectCollectionViaRevisionHeadObject);
			collectionsQueue.Enqueue(this._objectRevisionCollectionViaRevisionHeadObject);
			collectionsQueue.Enqueue(this._revisionCollectionViaRevisionHeadObject_);
			collectionsQueue.Enqueue(this._revisionCollectionViaRevisionHeadObject);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._attributeMetadata = (EntityCollection<AttributeMetadataEntity>) collectionsQueue.Dequeue();
			this._branchHeadObject = (EntityCollection<BranchHeadObjectEntity>) collectionsQueue.Dequeue();
			this._object = (EntityCollection<ObjectEntity>) collectionsQueue.Dequeue();
			this._relationshipMetadataAsPk = (EntityCollection<RelationshipMetadataEntity>) collectionsQueue.Dequeue();
			this._relationshipMetadataAsFk = (EntityCollection<RelationshipMetadataEntity>) collectionsQueue.Dequeue();
			this._revisionHeadObject = (EntityCollection<RevisionHeadObjectEntity>) collectionsQueue.Dequeue();
			this._objectCollectionViaRevisionHeadObject = (EntityCollection<ObjectEntity>) collectionsQueue.Dequeue();
			this._objectRevisionCollectionViaRevisionHeadObject = (EntityCollection<ObjectRevisionEntity>) collectionsQueue.Dequeue();
			this._revisionCollectionViaRevisionHeadObject_ = (EntityCollection<RevisionEntity>) collectionsQueue.Dequeue();
			this._revisionCollectionViaRevisionHeadObject = (EntityCollection<RevisionEntity>) collectionsQueue.Dequeue();
		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			if (this._attributeMetadata != null)
			{
				return true;
			}
			if (this._branchHeadObject != null)
			{
				return true;
			}
			if (this._object != null)
			{
				return true;
			}
			if (this._relationshipMetadataAsPk != null)
			{
				return true;
			}
			if (this._relationshipMetadataAsFk != null)
			{
				return true;
			}
			if (this._revisionHeadObject != null)
			{
				return true;
			}
			if (this._objectCollectionViaRevisionHeadObject != null)
			{
				return true;
			}
			if (this._objectRevisionCollectionViaRevisionHeadObject != null)
			{
				return true;
			}
			if (this._revisionCollectionViaRevisionHeadObject_ != null)
			{
				return true;
			}
			if (this._revisionCollectionViaRevisionHeadObject != null)
			{
				return true;
			}
			return base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<AttributeMetadataEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AttributeMetadataEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<BranchHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(BranchHeadObjectEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RelationshipMetadataEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationshipMetadataEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RelationshipMetadataEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationshipMetadataEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory))) : null);
		}
#endif
		/// <summary>
		/// Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element. 
		/// </summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		public override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();

			toReturn.Add("AttributeMetadata", _attributeMetadata);
			toReturn.Add("BranchHeadObject", _branchHeadObject);
			toReturn.Add("Object", _object);
			toReturn.Add("RelationshipMetadataAsPk", _relationshipMetadataAsPk);
			toReturn.Add("RelationshipMetadataAsFk", _relationshipMetadataAsFk);
			toReturn.Add("RevisionHeadObject", _revisionHeadObject);
			toReturn.Add("ObjectCollectionViaRevisionHeadObject", _objectCollectionViaRevisionHeadObject);
			toReturn.Add("ObjectRevisionCollectionViaRevisionHeadObject", _objectRevisionCollectionViaRevisionHeadObject);
			toReturn.Add("RevisionCollectionViaRevisionHeadObject_", _revisionCollectionViaRevisionHeadObject_);
			toReturn.Add("RevisionCollectionViaRevisionHeadObject", _revisionCollectionViaRevisionHeadObject);

			return toReturn;
		}
		
		/// <summary> Adds the internals to the active context. </summary>
		protected override void AddInternalsToContext()
		{
			if(_attributeMetadata!=null)
			{
				_attributeMetadata.ActiveContext = base.ActiveContext;
			}
			if(_branchHeadObject!=null)
			{
				_branchHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_object!=null)
			{
				_object.ActiveContext = base.ActiveContext;
			}
			if(_relationshipMetadataAsPk!=null)
			{
				_relationshipMetadataAsPk.ActiveContext = base.ActiveContext;
			}
			if(_relationshipMetadataAsFk!=null)
			{
				_relationshipMetadataAsFk.ActiveContext = base.ActiveContext;
			}
			if(_revisionHeadObject!=null)
			{
				_revisionHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_objectCollectionViaRevisionHeadObject!=null)
			{
				_objectCollectionViaRevisionHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_objectRevisionCollectionViaRevisionHeadObject!=null)
			{
				_objectRevisionCollectionViaRevisionHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_revisionCollectionViaRevisionHeadObject_!=null)
			{
				_revisionCollectionViaRevisionHeadObject_.ActiveContext = base.ActiveContext;
			}
			if(_revisionCollectionViaRevisionHeadObject!=null)
			{
				_revisionCollectionViaRevisionHeadObject.ActiveContext = base.ActiveContext;
			}


		}

		/// <summary> Initializes the class members</summary>
		protected virtual void InitClassMembers()
		{

			_attributeMetadata = null;
			_branchHeadObject = null;
			_object = null;
			_relationshipMetadataAsPk = null;
			_relationshipMetadataAsFk = null;
			_revisionHeadObject = null;
			_objectCollectionViaRevisionHeadObject = null;
			_objectRevisionCollectionViaRevisionHeadObject = null;
			_revisionCollectionViaRevisionHeadObject_ = null;
			_revisionCollectionViaRevisionHeadObject = null;


			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();

			Dictionary<string, string> fieldHashtable = null;
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Name", fieldHashtable);
		}
		#endregion



		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this ClassEntity</param>
		/// <param name="fields">Fields of this entity</param>
		protected virtual void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			base.Fields = fields;
			base.IsNew=true;
			base.Validator = validator;
			InitClassMembers();

			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ClassRelations Relations
		{
			get	{ return new ClassRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'AttributeMetadata' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathAttributeMetadata
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<AttributeMetadataEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AttributeMetadataEntityFactory))),
					(IEntityRelation)GetRelationsForField("AttributeMetadata")[0], (int)Dogwood.Data.EntityType.ClassEntity, (int)Dogwood.Data.EntityType.AttributeMetadataEntity, 0, null, null, null, null, "AttributeMetadata", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}
		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'BranchHeadObject' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathBranchHeadObject
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<BranchHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(BranchHeadObjectEntityFactory))),
					(IEntityRelation)GetRelationsForField("BranchHeadObject")[0], (int)Dogwood.Data.EntityType.ClassEntity, (int)Dogwood.Data.EntityType.BranchHeadObjectEntity, 0, null, null, null, null, "BranchHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}
		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Object' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObject
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))),
					(IEntityRelation)GetRelationsForField("Object")[0], (int)Dogwood.Data.EntityType.ClassEntity, (int)Dogwood.Data.EntityType.ObjectEntity, 0, null, null, null, null, "Object", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}
		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'RelationshipMetadata' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRelationshipMetadataAsPk
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<RelationshipMetadataEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationshipMetadataEntityFactory))),
					(IEntityRelation)GetRelationsForField("RelationshipMetadataAsPk")[0], (int)Dogwood.Data.EntityType.ClassEntity, (int)Dogwood.Data.EntityType.RelationshipMetadataEntity, 0, null, null, null, null, "RelationshipMetadataAsPk", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}
		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'RelationshipMetadata' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRelationshipMetadataAsFk
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<RelationshipMetadataEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationshipMetadataEntityFactory))),
					(IEntityRelation)GetRelationsForField("RelationshipMetadataAsFk")[0], (int)Dogwood.Data.EntityType.ClassEntity, (int)Dogwood.Data.EntityType.RelationshipMetadataEntity, 0, null, null, null, null, "RelationshipMetadataAsFk", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}
		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'RevisionHeadObject' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRevisionHeadObject
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory))),
					(IEntityRelation)GetRelationsForField("RevisionHeadObject")[0], (int)Dogwood.Data.EntityType.ClassEntity, (int)Dogwood.Data.EntityType.RevisionHeadObjectEntity, 0, null, null, null, null, "RevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Object' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObjectCollectionViaRevisionHeadObject
		{
			get
			{
				IEntityRelation intermediateRelation = ClassEntity.Relations.RevisionHeadObjectEntityUsingClassId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.ClassEntity, (int)Dogwood.Data.EntityType.ObjectEntity, 0, null, null, GetRelationsForField("ObjectCollectionViaRevisionHeadObject"), null, "ObjectCollectionViaRevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ObjectRevision' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObjectRevisionCollectionViaRevisionHeadObject
		{
			get
			{
				IEntityRelation intermediateRelation = ClassEntity.Relations.RevisionHeadObjectEntityUsingClassId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.ClassEntity, (int)Dogwood.Data.EntityType.ObjectRevisionEntity, 0, null, null, GetRelationsForField("ObjectRevisionCollectionViaRevisionHeadObject"), null, "ObjectRevisionCollectionViaRevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Revision' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRevisionCollectionViaRevisionHeadObject_
		{
			get
			{
				IEntityRelation intermediateRelation = ClassEntity.Relations.RevisionHeadObjectEntityUsingClassId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.ClassEntity, (int)Dogwood.Data.EntityType.RevisionEntity, 0, null, null, GetRelationsForField("RevisionCollectionViaRevisionHeadObject_"), null, "RevisionCollectionViaRevisionHeadObject_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Revision' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRevisionCollectionViaRevisionHeadObject
		{
			get
			{
				IEntityRelation intermediateRelation = ClassEntity.Relations.RevisionHeadObjectEntityUsingClassId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.ClassEntity, (int)Dogwood.Data.EntityType.RevisionEntity, 0, null, null, GetRelationsForField("RevisionCollectionViaRevisionHeadObject"), null, "RevisionCollectionViaRevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}



		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return ClassEntity.CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value
		/// pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return ClassEntity.FieldsCustomProperties;}
		}

		/// <summary> The Id property of the Entity Class<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblClass"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)ClassFieldIndex.Id, true); }
			set	{ SetValue((int)ClassFieldIndex.Id, value); }
		}

		/// <summary> The Name property of the Entity Class<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblClass"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ClassFieldIndex.Name, true); }
			set	{ SetValue((int)ClassFieldIndex.Name, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'AttributeMetadataEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(AttributeMetadataEntity))]
		public virtual EntityCollection<AttributeMetadataEntity> AttributeMetadata
		{
			get
			{
				if(_attributeMetadata==null)
				{
					_attributeMetadata = new EntityCollection<AttributeMetadataEntity>(EntityFactoryCache2.GetEntityFactory(typeof(AttributeMetadataEntityFactory)));
					_attributeMetadata.SetContainingEntityInfo(this, "Class");
				}
				return _attributeMetadata;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'BranchHeadObjectEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(BranchHeadObjectEntity))]
		public virtual EntityCollection<BranchHeadObjectEntity> BranchHeadObject
		{
			get
			{
				if(_branchHeadObject==null)
				{
					_branchHeadObject = new EntityCollection<BranchHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(BranchHeadObjectEntityFactory)));
					_branchHeadObject.SetContainingEntityInfo(this, "Class");
				}
				return _branchHeadObject;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ObjectEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ObjectEntity))]
		public virtual EntityCollection<ObjectEntity> Object
		{
			get
			{
				if(_object==null)
				{
					_object = new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory)));
					_object.SetContainingEntityInfo(this, "Class");
				}
				return _object;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'RelationshipMetadataEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RelationshipMetadataEntity))]
		public virtual EntityCollection<RelationshipMetadataEntity> RelationshipMetadataAsPk
		{
			get
			{
				if(_relationshipMetadataAsPk==null)
				{
					_relationshipMetadataAsPk = new EntityCollection<RelationshipMetadataEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationshipMetadataEntityFactory)));
					_relationshipMetadataAsPk.SetContainingEntityInfo(this, "PkClass");
				}
				return _relationshipMetadataAsPk;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'RelationshipMetadataEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RelationshipMetadataEntity))]
		public virtual EntityCollection<RelationshipMetadataEntity> RelationshipMetadataAsFk
		{
			get
			{
				if(_relationshipMetadataAsFk==null)
				{
					_relationshipMetadataAsFk = new EntityCollection<RelationshipMetadataEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RelationshipMetadataEntityFactory)));
					_relationshipMetadataAsFk.SetContainingEntityInfo(this, "FkClass");
				}
				return _relationshipMetadataAsFk;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'RevisionHeadObjectEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RevisionHeadObjectEntity))]
		public virtual EntityCollection<RevisionHeadObjectEntity> RevisionHeadObject
		{
			get
			{
				if(_revisionHeadObject==null)
				{
					_revisionHeadObject = new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory)));
					_revisionHeadObject.SetContainingEntityInfo(this, "Class");
				}
				return _revisionHeadObject;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ObjectEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ObjectEntity))]
		public virtual EntityCollection<ObjectEntity> ObjectCollectionViaRevisionHeadObject
		{
			get
			{
				if(_objectCollectionViaRevisionHeadObject==null)
				{
					_objectCollectionViaRevisionHeadObject = new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory)));
					_objectCollectionViaRevisionHeadObject.IsReadOnly=true;
				}
				return _objectCollectionViaRevisionHeadObject;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ObjectRevisionEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ObjectRevisionEntity))]
		public virtual EntityCollection<ObjectRevisionEntity> ObjectRevisionCollectionViaRevisionHeadObject
		{
			get
			{
				if(_objectRevisionCollectionViaRevisionHeadObject==null)
				{
					_objectRevisionCollectionViaRevisionHeadObject = new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory)));
					_objectRevisionCollectionViaRevisionHeadObject.IsReadOnly=true;
				}
				return _objectRevisionCollectionViaRevisionHeadObject;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'RevisionEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RevisionEntity))]
		public virtual EntityCollection<RevisionEntity> RevisionCollectionViaRevisionHeadObject_
		{
			get
			{
				if(_revisionCollectionViaRevisionHeadObject_==null)
				{
					_revisionCollectionViaRevisionHeadObject_ = new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory)));
					_revisionCollectionViaRevisionHeadObject_.IsReadOnly=true;
				}
				return _revisionCollectionViaRevisionHeadObject_;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'RevisionEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RevisionEntity))]
		public virtual EntityCollection<RevisionEntity> RevisionCollectionViaRevisionHeadObject
		{
			get
			{
				if(_revisionCollectionViaRevisionHeadObject==null)
				{
					_revisionCollectionViaRevisionHeadObject = new EntityCollection<RevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory)));
					_revisionCollectionViaRevisionHeadObject.IsReadOnly=true;
				}
				return _revisionCollectionViaRevisionHeadObject;
			}
		}


	
		
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the Dogwood.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		public override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Dogwood.Data.EntityType.ClassEntity; }
		}
		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
