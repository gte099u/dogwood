﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:03 PM
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using Dogwood.Data;
using Dogwood.Data.HelperClasses;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>
	/// Entity class which represents the entity 'Revision'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class RevisionEntity : CommonEntityBase, ISerializable
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations


		private EntityCollection<ObjectRevisionEntity> _objectRevision;

		private EntityCollection<RevisionHeadObjectEntity> _revisionHeadObject_;
		private EntityCollection<RevisionHeadObjectEntity> _revisionHeadObject;
		private EntityCollection<ClassEntity> _classCollectionViaRevisionHeadObject_;
		private EntityCollection<ClassEntity> _classCollectionViaRevisionHeadObject;
		private EntityCollection<ObjectEntity> _objectCollectionViaRevisionHeadObject_;
		private EntityCollection<ObjectEntity> _objectCollectionViaRevisionHeadObject;
		private EntityCollection<ObjectRevisionEntity> _objectRevisionCollectionViaRevisionHeadObject_;
		private EntityCollection<ObjectRevisionEntity> _objectRevisionCollectionViaRevisionHeadObject;


		
		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{



			/// <summary>Member name ObjectRevision</summary>
			public static readonly string ObjectRevision = "ObjectRevision";

			/// <summary>Member name RevisionHeadObject_</summary>
			public static readonly string RevisionHeadObject_ = "RevisionHeadObject_";
			/// <summary>Member name RevisionHeadObject</summary>
			public static readonly string RevisionHeadObject = "RevisionHeadObject";
			/// <summary>Member name ClassCollectionViaRevisionHeadObject_</summary>
			public static readonly string ClassCollectionViaRevisionHeadObject_ = "ClassCollectionViaRevisionHeadObject_";
			/// <summary>Member name ClassCollectionViaRevisionHeadObject</summary>
			public static readonly string ClassCollectionViaRevisionHeadObject = "ClassCollectionViaRevisionHeadObject";
			/// <summary>Member name ObjectCollectionViaRevisionHeadObject_</summary>
			public static readonly string ObjectCollectionViaRevisionHeadObject_ = "ObjectCollectionViaRevisionHeadObject_";
			/// <summary>Member name ObjectCollectionViaRevisionHeadObject</summary>
			public static readonly string ObjectCollectionViaRevisionHeadObject = "ObjectCollectionViaRevisionHeadObject";
			/// <summary>Member name ObjectRevisionCollectionViaRevisionHeadObject_</summary>
			public static readonly string ObjectRevisionCollectionViaRevisionHeadObject_ = "ObjectRevisionCollectionViaRevisionHeadObject_";
			/// <summary>Member name ObjectRevisionCollectionViaRevisionHeadObject</summary>
			public static readonly string ObjectRevisionCollectionViaRevisionHeadObject = "ObjectRevisionCollectionViaRevisionHeadObject";

		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static RevisionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary> CTor</summary>
		public RevisionEntity():base("RevisionEntity")
		{
			InitClassEmpty(null, CreateFields());
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public RevisionEntity(IEntityFields2 fields):base("RevisionEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this RevisionEntity</param>
		public RevisionEntity(IValidator validator):base("RevisionEntity")
		{
			InitClassEmpty(validator, CreateFields());
		}
				

		/// <summary> CTor</summary>
		/// <param name="id">PK value for Revision which data should be fetched into this Revision object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public RevisionEntity(System.Int32 id):base("RevisionEntity")
		{
			InitClassEmpty(null, CreateFields());
			this.Id = id;
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for Revision which data should be fetched into this Revision object</param>
		/// <param name="validator">The custom validator object for this RevisionEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public RevisionEntity(System.Int32 id, IValidator validator):base("RevisionEntity")
		{
			InitClassEmpty(validator, CreateFields());
			this.Id = id;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected RevisionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{


				_objectRevision = (EntityCollection<ObjectRevisionEntity>)info.GetValue("_objectRevision", typeof(EntityCollection<ObjectRevisionEntity>));

				_revisionHeadObject_ = (EntityCollection<RevisionHeadObjectEntity>)info.GetValue("_revisionHeadObject_", typeof(EntityCollection<RevisionHeadObjectEntity>));
				_revisionHeadObject = (EntityCollection<RevisionHeadObjectEntity>)info.GetValue("_revisionHeadObject", typeof(EntityCollection<RevisionHeadObjectEntity>));
				_classCollectionViaRevisionHeadObject_ = (EntityCollection<ClassEntity>)info.GetValue("_classCollectionViaRevisionHeadObject_", typeof(EntityCollection<ClassEntity>));
				_classCollectionViaRevisionHeadObject = (EntityCollection<ClassEntity>)info.GetValue("_classCollectionViaRevisionHeadObject", typeof(EntityCollection<ClassEntity>));
				_objectCollectionViaRevisionHeadObject_ = (EntityCollection<ObjectEntity>)info.GetValue("_objectCollectionViaRevisionHeadObject_", typeof(EntityCollection<ObjectEntity>));
				_objectCollectionViaRevisionHeadObject = (EntityCollection<ObjectEntity>)info.GetValue("_objectCollectionViaRevisionHeadObject", typeof(EntityCollection<ObjectEntity>));
				_objectRevisionCollectionViaRevisionHeadObject_ = (EntityCollection<ObjectRevisionEntity>)info.GetValue("_objectRevisionCollectionViaRevisionHeadObject_", typeof(EntityCollection<ObjectRevisionEntity>));
				_objectRevisionCollectionViaRevisionHeadObject = (EntityCollection<ObjectRevisionEntity>)info.GetValue("_objectRevisionCollectionViaRevisionHeadObject", typeof(EntityCollection<ObjectRevisionEntity>));


				base.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((RevisionFieldIndex)fieldIndex)
			{
				case RevisionFieldIndex.BranchId:

					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}
				
		/// <summary>Gets the inheritance info provider instance of the project this entity instance is located in. </summary>
		/// <returns>ready to use inheritance info provider instance.</returns>
		protected override IInheritanceInfoProvider GetInheritanceInfoProvider()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntityProperty(string propertyName, IEntity2 entity)
		{
			switch(propertyName)
			{



				case "ObjectRevision":
					this.ObjectRevision.Add((ObjectRevisionEntity)entity);
					break;

				case "RevisionHeadObject_":
					this.RevisionHeadObject_.Add((RevisionHeadObjectEntity)entity);
					break;
				case "RevisionHeadObject":
					this.RevisionHeadObject.Add((RevisionHeadObjectEntity)entity);
					break;
				case "ClassCollectionViaRevisionHeadObject_":
					this.ClassCollectionViaRevisionHeadObject_.IsReadOnly = false;
					this.ClassCollectionViaRevisionHeadObject_.Add((ClassEntity)entity);
					this.ClassCollectionViaRevisionHeadObject_.IsReadOnly = true;
					break;
				case "ClassCollectionViaRevisionHeadObject":
					this.ClassCollectionViaRevisionHeadObject.IsReadOnly = false;
					this.ClassCollectionViaRevisionHeadObject.Add((ClassEntity)entity);
					this.ClassCollectionViaRevisionHeadObject.IsReadOnly = true;
					break;
				case "ObjectCollectionViaRevisionHeadObject_":
					this.ObjectCollectionViaRevisionHeadObject_.IsReadOnly = false;
					this.ObjectCollectionViaRevisionHeadObject_.Add((ObjectEntity)entity);
					this.ObjectCollectionViaRevisionHeadObject_.IsReadOnly = true;
					break;
				case "ObjectCollectionViaRevisionHeadObject":
					this.ObjectCollectionViaRevisionHeadObject.IsReadOnly = false;
					this.ObjectCollectionViaRevisionHeadObject.Add((ObjectEntity)entity);
					this.ObjectCollectionViaRevisionHeadObject.IsReadOnly = true;
					break;
				case "ObjectRevisionCollectionViaRevisionHeadObject_":
					this.ObjectRevisionCollectionViaRevisionHeadObject_.IsReadOnly = false;
					this.ObjectRevisionCollectionViaRevisionHeadObject_.Add((ObjectRevisionEntity)entity);
					this.ObjectRevisionCollectionViaRevisionHeadObject_.IsReadOnly = true;
					break;
				case "ObjectRevisionCollectionViaRevisionHeadObject":
					this.ObjectRevisionCollectionViaRevisionHeadObject.IsReadOnly = false;
					this.ObjectRevisionCollectionViaRevisionHeadObject.Add((ObjectRevisionEntity)entity);
					this.ObjectRevisionCollectionViaRevisionHeadObject.IsReadOnly = true;
					break;

				default:
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return RevisionEntity.GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		public static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{



				case "ObjectRevision":
					toReturn.Add(RevisionEntity.Relations.ObjectRevisionEntityUsingRevisionId);
					break;

				case "RevisionHeadObject_":
					toReturn.Add(RevisionEntity.Relations.RevisionHeadObjectEntityUsingBranchId);
					break;
				case "RevisionHeadObject":
					toReturn.Add(RevisionEntity.Relations.RevisionHeadObjectEntityUsingRevisionId);
					break;
				case "ClassCollectionViaRevisionHeadObject_":
					toReturn.Add(RevisionEntity.Relations.RevisionHeadObjectEntityUsingBranchId, "RevisionEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.ClassEntityUsingClassId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;
				case "ClassCollectionViaRevisionHeadObject":
					toReturn.Add(RevisionEntity.Relations.RevisionHeadObjectEntityUsingRevisionId, "RevisionEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.ClassEntityUsingClassId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;
				case "ObjectCollectionViaRevisionHeadObject_":
					toReturn.Add(RevisionEntity.Relations.RevisionHeadObjectEntityUsingBranchId, "RevisionEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.ObjectEntityUsingObjectId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;
				case "ObjectCollectionViaRevisionHeadObject":
					toReturn.Add(RevisionEntity.Relations.RevisionHeadObjectEntityUsingRevisionId, "RevisionEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.ObjectEntityUsingObjectId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;
				case "ObjectRevisionCollectionViaRevisionHeadObject_":
					toReturn.Add(RevisionEntity.Relations.RevisionHeadObjectEntityUsingBranchId, "RevisionEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.ObjectRevisionEntityUsingObjectRevisionId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;
				case "ObjectRevisionCollectionViaRevisionHeadObject":
					toReturn.Add(RevisionEntity.Relations.RevisionHeadObjectEntityUsingRevisionId, "RevisionEntity__", "RevisionHeadObject_", JoinHint.None);
					toReturn.Add(RevisionHeadObjectEntity.Relations.ObjectRevisionEntityUsingObjectRevisionId, "RevisionHeadObject_", string.Empty, JoinHint.None);
					break;

				default:

					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it
		/// will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override bool CheckOneWayRelations(string propertyName)
		{
			// use template trick to calculate the # of single-sided / oneway relations
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));


				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void SetRelatedEntity(IEntity2 relatedEntity, string fieldName)
		{
			switch(fieldName)
			{



				case "ObjectRevision":
					this.ObjectRevision.Add((ObjectRevisionEntity)relatedEntity);
					break;

				case "RevisionHeadObject_":
					this.RevisionHeadObject_.Add((RevisionHeadObjectEntity)relatedEntity);
					break;
				case "RevisionHeadObject":
					this.RevisionHeadObject.Add((RevisionHeadObjectEntity)relatedEntity);
					break;

				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void UnsetRelatedEntity(IEntity2 relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{



				case "ObjectRevision":
					base.PerformRelatedEntityRemoval(this.ObjectRevision, relatedEntity, signalRelatedEntityManyToOne);
					break;

				case "RevisionHeadObject_":
					base.PerformRelatedEntityRemoval(this.RevisionHeadObject_, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RevisionHeadObject":
					base.PerformRelatedEntityRemoval(this.RevisionHeadObject, relatedEntity, signalRelatedEntityManyToOne);
					break;

				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		public override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();

			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		public override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();


			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. The contents of the ArrayList is used by the DataAccessAdapter to perform recursive saves. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		public override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();


			toReturn.Add(this.ObjectRevision);

			toReturn.Add(this.RevisionHeadObject_);
			toReturn.Add(this.RevisionHeadObject);

			return toReturn;
		}
		


		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{


				info.AddValue("_objectRevision", ((_objectRevision!=null) && (_objectRevision.Count>0) && !this.MarkedForDeletion)?_objectRevision:null);

				info.AddValue("_revisionHeadObject_", ((_revisionHeadObject_!=null) && (_revisionHeadObject_.Count>0) && !this.MarkedForDeletion)?_revisionHeadObject_:null);
				info.AddValue("_revisionHeadObject", ((_revisionHeadObject!=null) && (_revisionHeadObject.Count>0) && !this.MarkedForDeletion)?_revisionHeadObject:null);
				info.AddValue("_classCollectionViaRevisionHeadObject_", ((_classCollectionViaRevisionHeadObject_!=null) && (_classCollectionViaRevisionHeadObject_.Count>0) && !this.MarkedForDeletion)?_classCollectionViaRevisionHeadObject_:null);
				info.AddValue("_classCollectionViaRevisionHeadObject", ((_classCollectionViaRevisionHeadObject!=null) && (_classCollectionViaRevisionHeadObject.Count>0) && !this.MarkedForDeletion)?_classCollectionViaRevisionHeadObject:null);
				info.AddValue("_objectCollectionViaRevisionHeadObject_", ((_objectCollectionViaRevisionHeadObject_!=null) && (_objectCollectionViaRevisionHeadObject_.Count>0) && !this.MarkedForDeletion)?_objectCollectionViaRevisionHeadObject_:null);
				info.AddValue("_objectCollectionViaRevisionHeadObject", ((_objectCollectionViaRevisionHeadObject!=null) && (_objectCollectionViaRevisionHeadObject.Count>0) && !this.MarkedForDeletion)?_objectCollectionViaRevisionHeadObject:null);
				info.AddValue("_objectRevisionCollectionViaRevisionHeadObject_", ((_objectRevisionCollectionViaRevisionHeadObject_!=null) && (_objectRevisionCollectionViaRevisionHeadObject_.Count>0) && !this.MarkedForDeletion)?_objectRevisionCollectionViaRevisionHeadObject_:null);
				info.AddValue("_objectRevisionCollectionViaRevisionHeadObject", ((_objectRevisionCollectionViaRevisionHeadObject!=null) && (_objectRevisionCollectionViaRevisionHeadObject.Count>0) && !this.MarkedForDeletion)?_objectRevisionCollectionViaRevisionHeadObject:null);


			}
			
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}

		/// <summary>Returns true if the original value for the field with the fieldIndex passed in, read from the persistent storage was NULL, false otherwise.
		/// Should not be used for testing if the current value is NULL, use <see cref="TestCurrentFieldValueForNull"/> for that.</summary>
		/// <param name="fieldIndex">Index of the field to test if that field was NULL in the persistent storage</param>
		/// <returns>true if the field with the passed in index was NULL in the persistent storage, false otherwise</returns>
		public bool TestOriginalFieldValueForNull(RevisionFieldIndex fieldIndex)
		{
			return base.Fields[(int)fieldIndex].IsNull;
		}
		
		/// <summary>Returns true if the current value for the field with the fieldIndex passed in represents null/not defined, false otherwise.
		/// Should not be used for testing if the original value (read from the db) is NULL</summary>
		/// <param name="fieldIndex">Index of the field to test if its currentvalue is null/undefined</param>
		/// <returns>true if the field's value isn't defined yet, false otherwise</returns>
		public bool TestCurrentFieldValueForNull(RevisionFieldIndex fieldIndex)
		{
			return base.CheckIfCurrentFieldValueIsNull((int)fieldIndex);
		}

				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			return new RevisionRelations().GetAllRelations();
		}
		



		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'ObjectRevision' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObjectRevision()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(ObjectRevisionFields.RevisionId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}


		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'RevisionHeadObject' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRevisionHeadObject_()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RevisionHeadObjectFields.BranchId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'RevisionHeadObject' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RevisionHeadObjectFields.RevisionId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Class' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClassCollectionViaRevisionHeadObject_()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("ClassCollectionViaRevisionHeadObject_"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RevisionFields.Id, null, ComparisonOperator.Equal, this.Id, "RevisionEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Class' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoClassCollectionViaRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("ClassCollectionViaRevisionHeadObject"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RevisionFields.Id, null, ComparisonOperator.Equal, this.Id, "RevisionEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Object' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObjectCollectionViaRevisionHeadObject_()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("ObjectCollectionViaRevisionHeadObject_"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RevisionFields.Id, null, ComparisonOperator.Equal, this.Id, "RevisionEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'Object' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObjectCollectionViaRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("ObjectCollectionViaRevisionHeadObject"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RevisionFields.Id, null, ComparisonOperator.Equal, this.Id, "RevisionEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'ObjectRevision' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObjectRevisionCollectionViaRevisionHeadObject_()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("ObjectRevisionCollectionViaRevisionHeadObject_"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RevisionFields.Id, null, ComparisonOperator.Equal, this.Id, "RevisionEntity__"));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch
		/// the related entities of type 'ObjectRevision' to this entity. Use DataAccessAdapter.FetchEntityCollection() to fetch these related entities.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoObjectRevisionCollectionViaRevisionHeadObject()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.Relations.AddRange(GetRelationsForFieldOfType("ObjectRevisionCollectionViaRevisionHeadObject"));
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(RevisionFields.Id, null, ComparisonOperator.Equal, this.Id, "RevisionEntity__"));
			return bucket;
		}


	
		
		/// <summary>Creates entity fields object for this entity. Used in constructor to setup this entity in a polymorphic scenario.</summary>
		protected virtual IEntityFields2 CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(Dogwood.Data.EntityType.RevisionEntity);
		}

		/// <summary>
		/// Creates the ITypeDefaultValue instance used to provide default values for value types which aren't of type nullable(of T)
		/// </summary>
		/// <returns></returns>
		protected override ITypeDefaultValue CreateTypeDefaultValueProvider()
		{
			return new TypeDefaultValue();
		}

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(RevisionEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);


			collectionsQueue.Enqueue(this._objectRevision);

			collectionsQueue.Enqueue(this._revisionHeadObject_);
			collectionsQueue.Enqueue(this._revisionHeadObject);
			collectionsQueue.Enqueue(this._classCollectionViaRevisionHeadObject_);
			collectionsQueue.Enqueue(this._classCollectionViaRevisionHeadObject);
			collectionsQueue.Enqueue(this._objectCollectionViaRevisionHeadObject_);
			collectionsQueue.Enqueue(this._objectCollectionViaRevisionHeadObject);
			collectionsQueue.Enqueue(this._objectRevisionCollectionViaRevisionHeadObject_);
			collectionsQueue.Enqueue(this._objectRevisionCollectionViaRevisionHeadObject);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);


			this._objectRevision = (EntityCollection<ObjectRevisionEntity>) collectionsQueue.Dequeue();

			this._revisionHeadObject_ = (EntityCollection<RevisionHeadObjectEntity>) collectionsQueue.Dequeue();
			this._revisionHeadObject = (EntityCollection<RevisionHeadObjectEntity>) collectionsQueue.Dequeue();
			this._classCollectionViaRevisionHeadObject_ = (EntityCollection<ClassEntity>) collectionsQueue.Dequeue();
			this._classCollectionViaRevisionHeadObject = (EntityCollection<ClassEntity>) collectionsQueue.Dequeue();
			this._objectCollectionViaRevisionHeadObject_ = (EntityCollection<ObjectEntity>) collectionsQueue.Dequeue();
			this._objectCollectionViaRevisionHeadObject = (EntityCollection<ObjectEntity>) collectionsQueue.Dequeue();
			this._objectRevisionCollectionViaRevisionHeadObject_ = (EntityCollection<ObjectRevisionEntity>) collectionsQueue.Dequeue();
			this._objectRevisionCollectionViaRevisionHeadObject = (EntityCollection<ObjectRevisionEntity>) collectionsQueue.Dequeue();
		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{


			if (this._objectRevision != null)
			{
				return true;
			}

			if (this._revisionHeadObject_ != null)
			{
				return true;
			}
			if (this._revisionHeadObject != null)
			{
				return true;
			}
			if (this._classCollectionViaRevisionHeadObject_ != null)
			{
				return true;
			}
			if (this._classCollectionViaRevisionHeadObject != null)
			{
				return true;
			}
			if (this._objectCollectionViaRevisionHeadObject_ != null)
			{
				return true;
			}
			if (this._objectCollectionViaRevisionHeadObject != null)
			{
				return true;
			}
			if (this._objectRevisionCollectionViaRevisionHeadObject_ != null)
			{
				return true;
			}
			if (this._objectRevisionCollectionViaRevisionHeadObject != null)
			{
				return true;
			}
			return base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);


			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory))) : null);

			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ClassEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ClassEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory))) : null);
		}
#endif
		/// <summary>
		/// Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element. 
		/// </summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		public override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();



			toReturn.Add("ObjectRevision", _objectRevision);

			toReturn.Add("RevisionHeadObject_", _revisionHeadObject_);
			toReturn.Add("RevisionHeadObject", _revisionHeadObject);
			toReturn.Add("ClassCollectionViaRevisionHeadObject_", _classCollectionViaRevisionHeadObject_);
			toReturn.Add("ClassCollectionViaRevisionHeadObject", _classCollectionViaRevisionHeadObject);
			toReturn.Add("ObjectCollectionViaRevisionHeadObject_", _objectCollectionViaRevisionHeadObject_);
			toReturn.Add("ObjectCollectionViaRevisionHeadObject", _objectCollectionViaRevisionHeadObject);
			toReturn.Add("ObjectRevisionCollectionViaRevisionHeadObject_", _objectRevisionCollectionViaRevisionHeadObject_);
			toReturn.Add("ObjectRevisionCollectionViaRevisionHeadObject", _objectRevisionCollectionViaRevisionHeadObject);

			return toReturn;
		}
		
		/// <summary> Adds the internals to the active context. </summary>
		protected override void AddInternalsToContext()
		{


			if(_objectRevision!=null)
			{
				_objectRevision.ActiveContext = base.ActiveContext;
			}

			if(_revisionHeadObject_!=null)
			{
				_revisionHeadObject_.ActiveContext = base.ActiveContext;
			}
			if(_revisionHeadObject!=null)
			{
				_revisionHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_classCollectionViaRevisionHeadObject_!=null)
			{
				_classCollectionViaRevisionHeadObject_.ActiveContext = base.ActiveContext;
			}
			if(_classCollectionViaRevisionHeadObject!=null)
			{
				_classCollectionViaRevisionHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_objectCollectionViaRevisionHeadObject_!=null)
			{
				_objectCollectionViaRevisionHeadObject_.ActiveContext = base.ActiveContext;
			}
			if(_objectCollectionViaRevisionHeadObject!=null)
			{
				_objectCollectionViaRevisionHeadObject.ActiveContext = base.ActiveContext;
			}
			if(_objectRevisionCollectionViaRevisionHeadObject_!=null)
			{
				_objectRevisionCollectionViaRevisionHeadObject_.ActiveContext = base.ActiveContext;
			}
			if(_objectRevisionCollectionViaRevisionHeadObject!=null)
			{
				_objectRevisionCollectionViaRevisionHeadObject.ActiveContext = base.ActiveContext;
			}


		}

		/// <summary> Initializes the class members</summary>
		protected virtual void InitClassMembers()
		{



			_objectRevision = null;

			_revisionHeadObject_ = null;
			_revisionHeadObject = null;
			_classCollectionViaRevisionHeadObject_ = null;
			_classCollectionViaRevisionHeadObject = null;
			_objectCollectionViaRevisionHeadObject_ = null;
			_objectCollectionViaRevisionHeadObject = null;
			_objectRevisionCollectionViaRevisionHeadObject_ = null;
			_objectRevisionCollectionViaRevisionHeadObject = null;


			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();

			Dictionary<string, string> fieldHashtable = null;
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("BranchId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Tag", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("TimeStamp", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();

			_fieldsCustomProperties.Add("Comments", fieldHashtable);
		}
		#endregion



		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this RevisionEntity</param>
		/// <param name="fields">Fields of this entity</param>
		protected virtual void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			base.Fields = fields;
			base.IsNew=true;
			base.Validator = validator;
			InitClassMembers();

			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static RevisionRelations Relations
		{
			get	{ return new RevisionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ObjectRevision' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObjectRevision
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory))),
					(IEntityRelation)GetRelationsForField("ObjectRevision")[0], (int)Dogwood.Data.EntityType.RevisionEntity, (int)Dogwood.Data.EntityType.ObjectRevisionEntity, 0, null, null, null, null, "ObjectRevision", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}
		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'RevisionHeadObject' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRevisionHeadObject_
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory))),
					(IEntityRelation)GetRelationsForField("RevisionHeadObject_")[0], (int)Dogwood.Data.EntityType.RevisionEntity, (int)Dogwood.Data.EntityType.RevisionHeadObjectEntity, 0, null, null, null, null, "RevisionHeadObject_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}
		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'RevisionHeadObject' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathRevisionHeadObject
		{
			get
			{
				return new PrefetchPathElement2( new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory))),
					(IEntityRelation)GetRelationsForField("RevisionHeadObject")[0], (int)Dogwood.Data.EntityType.RevisionEntity, (int)Dogwood.Data.EntityType.RevisionHeadObjectEntity, 0, null, null, null, null, "RevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Class' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClassCollectionViaRevisionHeadObject_
		{
			get
			{
				IEntityRelation intermediateRelation = RevisionEntity.Relations.RevisionHeadObjectEntityUsingBranchId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<ClassEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.RevisionEntity, (int)Dogwood.Data.EntityType.ClassEntity, 0, null, null, GetRelationsForField("ClassCollectionViaRevisionHeadObject_"), null, "ClassCollectionViaRevisionHeadObject_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Class' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathClassCollectionViaRevisionHeadObject
		{
			get
			{
				IEntityRelation intermediateRelation = RevisionEntity.Relations.RevisionHeadObjectEntityUsingRevisionId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<ClassEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.RevisionEntity, (int)Dogwood.Data.EntityType.ClassEntity, 0, null, null, GetRelationsForField("ClassCollectionViaRevisionHeadObject"), null, "ClassCollectionViaRevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Object' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObjectCollectionViaRevisionHeadObject_
		{
			get
			{
				IEntityRelation intermediateRelation = RevisionEntity.Relations.RevisionHeadObjectEntityUsingBranchId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.RevisionEntity, (int)Dogwood.Data.EntityType.ObjectEntity, 0, null, null, GetRelationsForField("ObjectCollectionViaRevisionHeadObject_"), null, "ObjectCollectionViaRevisionHeadObject_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Object' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObjectCollectionViaRevisionHeadObject
		{
			get
			{
				IEntityRelation intermediateRelation = RevisionEntity.Relations.RevisionHeadObjectEntityUsingRevisionId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.RevisionEntity, (int)Dogwood.Data.EntityType.ObjectEntity, 0, null, null, GetRelationsForField("ObjectCollectionViaRevisionHeadObject"), null, "ObjectCollectionViaRevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ObjectRevision' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObjectRevisionCollectionViaRevisionHeadObject_
		{
			get
			{
				IEntityRelation intermediateRelation = RevisionEntity.Relations.RevisionHeadObjectEntityUsingBranchId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.RevisionEntity, (int)Dogwood.Data.EntityType.ObjectRevisionEntity, 0, null, null, GetRelationsForField("ObjectRevisionCollectionViaRevisionHeadObject_"), null, "ObjectRevisionCollectionViaRevisionHeadObject_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'ObjectRevision' 
		/// for this entity. Add the object returned by this property to an existing PrefetchPath2 instance.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathObjectRevisionCollectionViaRevisionHeadObject
		{
			get
			{
				IEntityRelation intermediateRelation = RevisionEntity.Relations.RevisionHeadObjectEntityUsingRevisionId;
				intermediateRelation.SetAliases(string.Empty, "RevisionHeadObject_");
				return new PrefetchPathElement2(new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory))), intermediateRelation,
					(int)Dogwood.Data.EntityType.RevisionEntity, (int)Dogwood.Data.EntityType.ObjectRevisionEntity, 0, null, null, GetRelationsForField("ObjectRevisionCollectionViaRevisionHeadObject"), null, "ObjectRevisionCollectionViaRevisionHeadObject", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}



		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return RevisionEntity.CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value
		/// pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		public override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return RevisionEntity.FieldsCustomProperties;}
		}

		/// <summary> The Id property of the Entity Revision<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRevision"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)RevisionFieldIndex.Id, true); }
			set	{ SetValue((int)RevisionFieldIndex.Id, value); }
		}

		/// <summary> The BranchId property of the Entity Revision<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRevision"."BranchId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BranchId
		{
			get { return (System.Int32)GetValue((int)RevisionFieldIndex.BranchId, true); }
			set	{ SetValue((int)RevisionFieldIndex.BranchId, value); }
		}

		/// <summary> The Tag property of the Entity Revision<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRevision"."Tag"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Tag
		{
			get { return (System.String)GetValue((int)RevisionFieldIndex.Tag, true); }
			set	{ SetValue((int)RevisionFieldIndex.Tag, value); }
		}

		/// <summary> The TimeStamp property of the Entity Revision<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRevision"."TimeStamp"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TimeStamp
		{
			get { return (System.DateTime)GetValue((int)RevisionFieldIndex.TimeStamp, true); }
			set	{ SetValue((int)RevisionFieldIndex.TimeStamp, value); }
		}

		/// <summary> The CreatedBy property of the Entity Revision<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRevision"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)RevisionFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)RevisionFieldIndex.CreatedBy, value); }
		}

		/// <summary> The Comments property of the Entity Revision<br/><br/>
		/// </summary>
		/// <remarks>Mapped on  table field: "tblRevision"."Comments"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Comments
		{
			get { return (System.String)GetValue((int)RevisionFieldIndex.Comments, true); }
			set	{ SetValue((int)RevisionFieldIndex.Comments, value); }
		}



		/// <summary> Gets the EntityCollection with the related entities of type 'ObjectRevisionEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ObjectRevisionEntity))]
		public virtual EntityCollection<ObjectRevisionEntity> ObjectRevision
		{
			get
			{
				if(_objectRevision==null)
				{
					_objectRevision = new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory)));
					_objectRevision.SetContainingEntityInfo(this, "Revision");
				}
				return _objectRevision;
			}
		}


		/// <summary> Gets the EntityCollection with the related entities of type 'RevisionHeadObjectEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RevisionHeadObjectEntity))]
		public virtual EntityCollection<RevisionHeadObjectEntity> RevisionHeadObject_
		{
			get
			{
				if(_revisionHeadObject_==null)
				{
					_revisionHeadObject_ = new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory)));
					_revisionHeadObject_.SetContainingEntityInfo(this, "Revision_");
				}
				return _revisionHeadObject_;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'RevisionHeadObjectEntity' which are related to this entity via a relation of type '1:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(RevisionHeadObjectEntity))]
		public virtual EntityCollection<RevisionHeadObjectEntity> RevisionHeadObject
		{
			get
			{
				if(_revisionHeadObject==null)
				{
					_revisionHeadObject = new EntityCollection<RevisionHeadObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(RevisionHeadObjectEntityFactory)));
					_revisionHeadObject.SetContainingEntityInfo(this, "Revision");
				}
				return _revisionHeadObject;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ClassEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ClassEntity))]
		public virtual EntityCollection<ClassEntity> ClassCollectionViaRevisionHeadObject_
		{
			get
			{
				if(_classCollectionViaRevisionHeadObject_==null)
				{
					_classCollectionViaRevisionHeadObject_ = new EntityCollection<ClassEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory)));
					_classCollectionViaRevisionHeadObject_.IsReadOnly=true;
				}
				return _classCollectionViaRevisionHeadObject_;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ClassEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ClassEntity))]
		public virtual EntityCollection<ClassEntity> ClassCollectionViaRevisionHeadObject
		{
			get
			{
				if(_classCollectionViaRevisionHeadObject==null)
				{
					_classCollectionViaRevisionHeadObject = new EntityCollection<ClassEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ClassEntityFactory)));
					_classCollectionViaRevisionHeadObject.IsReadOnly=true;
				}
				return _classCollectionViaRevisionHeadObject;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ObjectEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ObjectEntity))]
		public virtual EntityCollection<ObjectEntity> ObjectCollectionViaRevisionHeadObject_
		{
			get
			{
				if(_objectCollectionViaRevisionHeadObject_==null)
				{
					_objectCollectionViaRevisionHeadObject_ = new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory)));
					_objectCollectionViaRevisionHeadObject_.IsReadOnly=true;
				}
				return _objectCollectionViaRevisionHeadObject_;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ObjectEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ObjectEntity))]
		public virtual EntityCollection<ObjectEntity> ObjectCollectionViaRevisionHeadObject
		{
			get
			{
				if(_objectCollectionViaRevisionHeadObject==null)
				{
					_objectCollectionViaRevisionHeadObject = new EntityCollection<ObjectEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectEntityFactory)));
					_objectCollectionViaRevisionHeadObject.IsReadOnly=true;
				}
				return _objectCollectionViaRevisionHeadObject;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ObjectRevisionEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ObjectRevisionEntity))]
		public virtual EntityCollection<ObjectRevisionEntity> ObjectRevisionCollectionViaRevisionHeadObject_
		{
			get
			{
				if(_objectRevisionCollectionViaRevisionHeadObject_==null)
				{
					_objectRevisionCollectionViaRevisionHeadObject_ = new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory)));
					_objectRevisionCollectionViaRevisionHeadObject_.IsReadOnly=true;
				}
				return _objectRevisionCollectionViaRevisionHeadObject_;
			}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'ObjectRevisionEntity' which are related to this entity via a relation of type 'm:n'.
		/// If the EntityCollection hasn't been fetched yet, the collection returned will be empty.</summary>
		[TypeContainedAttribute(typeof(ObjectRevisionEntity))]
		public virtual EntityCollection<ObjectRevisionEntity> ObjectRevisionCollectionViaRevisionHeadObject
		{
			get
			{
				if(_objectRevisionCollectionViaRevisionHeadObject==null)
				{
					_objectRevisionCollectionViaRevisionHeadObject = new EntityCollection<ObjectRevisionEntity>(EntityFactoryCache2.GetEntityFactory(typeof(ObjectRevisionEntityFactory)));
					_objectRevisionCollectionViaRevisionHeadObject.IsReadOnly=true;
				}
				return _objectRevisionCollectionViaRevisionHeadObject;
			}
		}


	
		
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the Dogwood.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		public override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Dogwood.Data.EntityType.RevisionEntity; }
		}
		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
