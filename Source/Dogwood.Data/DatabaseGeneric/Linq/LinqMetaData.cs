﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:02 PM
// Code is generated using templates: SD.TemplateBindings.Linq
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

using Dogwood.Data;
using Dogwood.Data.EntityClasses;
using Dogwood.Data.FactoryClasses;
using Dogwood.Data.HelperClasses;
using Dogwood.Data.RelationClasses;

namespace Dogwood.Data.Linq
{
	/// <summary>Meta-data class for the construction of Linq queries which are to be executed using LLBLGen Pro code.</summary>
	public partial class LinqMetaData: ILinqMetaData
	{
		#region Class Member Declarations
		private IDataAccessAdapter _adapterToUse;
		private FunctionMappingStore _customFunctionMappings;
		private Context _contextToUse;
		#endregion
		
		/// <summary>CTor. Using this ctor will leave the IDataAccessAdapter object to use empty. To be able to execute the query, an IDataAccessAdapter instance
		/// is required, and has to be set on the LLBLGenProProvider2 object in the query to execute. </summary>
		public LinqMetaData() : this(null, null)
		{
		}
		
		/// <summary>CTor which accepts an IDataAccessAdapter implementing object, which will be used to execute queries created with this metadata class.</summary>
		/// <param name="adapterToUse">the IDataAccessAdapter to use in queries created with this meta data</param>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(IDataAccessAdapter adapterToUse) : this (adapterToUse, null)
		{
		}

		/// <summary>CTor which accepts an IDataAccessAdapter implementing object, which will be used to execute queries created with this metadata class.</summary>
		/// <param name="adapterToUse">the IDataAccessAdapter to use in queries created with this meta data</param>
		/// <param name="customFunctionMappings">The custom function mappings to use. These take higher precedence than the ones in the DQE to use.</param>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(IDataAccessAdapter adapterToUse, FunctionMappingStore customFunctionMappings)
		{
			_adapterToUse = adapterToUse;
			_customFunctionMappings = customFunctionMappings;
		}
	
		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <param name="typeOfEntity">the type of the entity to get the datasource for</param>
		/// <returns>the requested datasource</returns>
		public IDataSource GetQueryableForEntity(int typeOfEntity)
		{
			IDataSource toReturn = null;
			switch((Dogwood.Data.EntityType)typeOfEntity)
			{
				case Dogwood.Data.EntityType.AttributeEntity:
					toReturn = this.Attribute;
					break;
				case Dogwood.Data.EntityType.AttributeMetadataEntity:
					toReturn = this.AttributeMetadata;
					break;
				case Dogwood.Data.EntityType.AttributeValueEntity:
					toReturn = this.AttributeValue;
					break;
				case Dogwood.Data.EntityType.BranchHeadObjectEntity:
					toReturn = this.BranchHeadObject;
					break;
				case Dogwood.Data.EntityType.ClassEntity:
					toReturn = this.Class;
					break;
				case Dogwood.Data.EntityType.DataTypeEntity:
					toReturn = this.DataType;
					break;
				case Dogwood.Data.EntityType.ObjectEntity:
					toReturn = this.Object;
					break;
				case Dogwood.Data.EntityType.ObjectRevisionEntity:
					toReturn = this.ObjectRevision;
					break;
				case Dogwood.Data.EntityType.RelationAttributeValueEntity:
					toReturn = this.RelationAttributeValue;
					break;
				case Dogwood.Data.EntityType.RelationshipMetadataEntity:
					toReturn = this.RelationshipMetadata;
					break;
				case Dogwood.Data.EntityType.RevisionEntity:
					toReturn = this.Revision;
					break;
				case Dogwood.Data.EntityType.RevisionHeadObjectEntity:
					toReturn = this.RevisionHeadObject;
					break;
				default:
					toReturn = null;
					break;
			}
			return toReturn;
		}

		/// <summary>returns the datasource to use in a Linq query when targeting AttributeEntity instances in the database.</summary>
		public DataSource2<AttributeEntity> Attribute
		{
			get { return new DataSource2<AttributeEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting AttributeMetadataEntity instances in the database.</summary>
		public DataSource2<AttributeMetadataEntity> AttributeMetadata
		{
			get { return new DataSource2<AttributeMetadataEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting AttributeValueEntity instances in the database.</summary>
		public DataSource2<AttributeValueEntity> AttributeValue
		{
			get { return new DataSource2<AttributeValueEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting BranchHeadObjectEntity instances in the database.</summary>
		public DataSource2<BranchHeadObjectEntity> BranchHeadObject
		{
			get { return new DataSource2<BranchHeadObjectEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting ClassEntity instances in the database.</summary>
		public DataSource2<ClassEntity> Class
		{
			get { return new DataSource2<ClassEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting DataTypeEntity instances in the database.</summary>
		public DataSource2<DataTypeEntity> DataType
		{
			get { return new DataSource2<DataTypeEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting ObjectEntity instances in the database.</summary>
		public DataSource2<ObjectEntity> Object
		{
			get { return new DataSource2<ObjectEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting ObjectRevisionEntity instances in the database.</summary>
		public DataSource2<ObjectRevisionEntity> ObjectRevision
		{
			get { return new DataSource2<ObjectRevisionEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting RelationAttributeValueEntity instances in the database.</summary>
		public DataSource2<RelationAttributeValueEntity> RelationAttributeValue
		{
			get { return new DataSource2<RelationAttributeValueEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting RelationshipMetadataEntity instances in the database.</summary>
		public DataSource2<RelationshipMetadataEntity> RelationshipMetadata
		{
			get { return new DataSource2<RelationshipMetadataEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting RevisionEntity instances in the database.</summary>
		public DataSource2<RevisionEntity> Revision
		{
			get { return new DataSource2<RevisionEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		/// <summary>returns the datasource to use in a Linq query when targeting RevisionHeadObjectEntity instances in the database.</summary>
		public DataSource2<RevisionHeadObjectEntity> RevisionHeadObject
		{
			get { return new DataSource2<RevisionHeadObjectEntity>(_adapterToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		
		
		#region Class Property Declarations
		/// <summary> Gets / sets the IDataAccessAdapter to use for the queries created with this meta data object.</summary>
		/// <remarks> Be aware that the IDataAccessAdapter object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public IDataAccessAdapter AdapterToUse
		{
			get { return _adapterToUse;}
			set { _adapterToUse = value;}
		}

		/// <summary>Gets or sets the custom function mappings to use. These take higher precedence than the ones in the DQE to use</summary>
		public FunctionMappingStore CustomFunctionMappings
		{
			get { return _customFunctionMappings; }
			set { _customFunctionMappings = value; }
		}
		
		/// <summary>Gets or sets the Context instance to use for entity fetches.</summary>
		public Context ContextToUse
		{
			get { return _contextToUse;}
			set { _contextToUse = value;}
		}
		#endregion
	}
}