﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:07 PM
// Code is generated using templates: SD.TemplateBindings.SqlServerSpecific.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.SqlClient;

namespace Dogwood.Data.DatabaseSpecific
{
	/// <summary>
	/// Class which contains the static logic to execute action stored procedures in the database.
	/// </summary>
	public partial class ActionProcedures
	{
		/// <summary>
		/// private CTor so no instance can be created.
		/// </summary>
		private ActionProcedures()
		{
		}

	
		/// <summary>
		/// Delegate definition for stored procedure 'spAddRevisionToHeadObjects' to be used in combination of a UnitOfWork2 object. 
		/// </summary>
		public delegate int AddRevisionToHeadObjectsCallBack(System.Int32 branchId, System.Int32 revisionId, DataAccessAdapter adapter);

		/// <summary>
		/// Calls stored procedure 'spAddRevisionToHeadObjects'.<br/><br/>
		/// 
		/// </summary>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="revisionId">Input parameter of stored procedure</param>
		/// <returns>Amount of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int AddRevisionToHeadObjects(System.Int32 branchId, System.Int32 revisionId)
		{
			using(DataAccessAdapter adapter = new DataAccessAdapter())
			{
				return AddRevisionToHeadObjects(branchId, revisionId,  adapter);
			}
		}


		/// <summary>
		/// Calls stored procedure 'spAddRevisionToHeadObjects'.<br/><br/>
		/// 
		/// </summary>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="revisionId">Input parameter of stored procedure</param>
		/// <param name="adapter">The DataAccessAdapter object to use for the call</param>
		/// <returns>Amount of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int AddRevisionToHeadObjects(System.Int32 branchId, System.Int32 revisionId, DataAccessAdapter adapter)
		{
			SqlParameter[] parameters = new SqlParameter[2];
			parameters[0] = new SqlParameter("@branchId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, branchId);
			parameters[1] = new SqlParameter("@revisionId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, revisionId);

			int toReturn = adapter.CallActionStoredProcedure("[dbDogwood].[dbo].[spAddRevisionToHeadObjects]", parameters);

			return toReturn;
		}
		

		/// <summary>
		/// Calls stored procedure 'spAddRevisionToHeadObjects'.<br/><br/>
		/// 
		/// </summary>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="revisionId">Input parameter of stored procedure</param>
		/// <param name="returnValue">Return value of the stored procedure</param>
		/// <returns>Amount of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int AddRevisionToHeadObjects(System.Int32 branchId, System.Int32 revisionId, ref System.Int32 returnValue)
		{
			using(DataAccessAdapter adapter = new DataAccessAdapter())
			{
				return AddRevisionToHeadObjects(branchId, revisionId, ref returnValue, adapter);
			}
		}

		
		/// <summary>
		/// Calls stored procedure 'spAddRevisionToHeadObjects'.<br/><br/>
		/// 
		/// </summary>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="revisionId">Input parameter of stored procedure</param>
		/// <param name="returnValue">Return value of the stored procedure</param>
		/// <param name="adapter">The DataAccessAdapter object to use for the call</param>
		/// <returns>Amount of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int AddRevisionToHeadObjects(System.Int32 branchId, System.Int32 revisionId, ref System.Int32 returnValue, DataAccessAdapter adapter)
		{
			// create parameters. Add 1 to make room for the return value parameter.
			SqlParameter[] parameters = new SqlParameter[2 + 1];
			parameters[0] = new SqlParameter("@branchId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, branchId);
			parameters[1] = new SqlParameter("@revisionId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, revisionId);

			parameters[2] = new SqlParameter("RETURNVALUE", SqlDbType.Int, 0, ParameterDirection.ReturnValue, true, 10, 0, "",  DataRowVersion.Current, returnValue);
			int toReturn = adapter.CallActionStoredProcedure("[dbDogwood].[dbo].[spAddRevisionToHeadObjects]", parameters);

			
			returnValue = (int)parameters[2].Value;
			return toReturn;
		}
	

		/// <summary>
		/// Delegate definition for stored procedure 'spFillHeadObjects' to be used in combination of a UnitOfWork2 object. 
		/// </summary>
		public delegate int FillHeadObjectsCallBack(DataAccessAdapter adapter);

		/// <summary>
		/// Calls stored procedure 'spFillHeadObjects'.<br/><br/>
		/// 
		/// </summary>
		/// <returns>Amount of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int FillHeadObjects()
		{
			using(DataAccessAdapter adapter = new DataAccessAdapter())
			{
				return FillHeadObjects( adapter);
			}
		}


		/// <summary>
		/// Calls stored procedure 'spFillHeadObjects'.<br/><br/>
		/// 
		/// </summary>
		/// <param name="adapter">The DataAccessAdapter object to use for the call</param>
		/// <returns>Amount of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int FillHeadObjects(DataAccessAdapter adapter)
		{
			SqlParameter[] parameters = new SqlParameter[0];


			int toReturn = adapter.CallActionStoredProcedure("[dbDogwood].[dbo].[spFillHeadObjects]", parameters);

			return toReturn;
		}
		

		/// <summary>
		/// Calls stored procedure 'spFillHeadObjects'.<br/><br/>
		/// 
		/// </summary>
		/// <param name="returnValue">Return value of the stored procedure</param>
		/// <returns>Amount of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int FillHeadObjects(ref System.Int32 returnValue)
		{
			using(DataAccessAdapter adapter = new DataAccessAdapter())
			{
				return FillHeadObjects(ref returnValue, adapter);
			}
		}

		
		/// <summary>
		/// Calls stored procedure 'spFillHeadObjects'.<br/><br/>
		/// 
		/// </summary>
		/// <param name="returnValue">Return value of the stored procedure</param>
		/// <param name="adapter">The DataAccessAdapter object to use for the call</param>
		/// <returns>Amount of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int FillHeadObjects(ref System.Int32 returnValue, DataAccessAdapter adapter)
		{
			// create parameters. Add 1 to make room for the return value parameter.
			SqlParameter[] parameters = new SqlParameter[0 + 1];


			parameters[0] = new SqlParameter("RETURNVALUE", SqlDbType.Int, 0, ParameterDirection.ReturnValue, true, 10, 0, "",  DataRowVersion.Current, returnValue);
			int toReturn = adapter.CallActionStoredProcedure("[dbDogwood].[dbo].[spFillHeadObjects]", parameters);

			
			returnValue = (int)parameters[0].Value;
			return toReturn;
		}
	

		#region Included Code

		#endregion
	}
}
