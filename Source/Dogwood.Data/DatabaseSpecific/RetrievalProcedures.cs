﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, June 06, 2012 4:11:43 PM
// Code is generated using templates: SD.TemplateBindings.SqlServerSpecific.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.SqlClient;
using SD.LLBLGen.Pro.ORMSupportClasses;
namespace Dogwood.Data.DatabaseSpecific
{
	/// <summary>
	/// Class which contains the static logic to execute retrieval stored procedures in the database.
	/// </summary>
	public partial class RetrievalProcedures
	{
		/// <summary>
		/// private CTor so no instance can be created.
		/// </summary>
		private RetrievalProcedures()
		{
		}

	
		/// <summary>
		/// Calls stored procedure 'spGetAllRelatedToObject'.<br/><br/>
		/// 
		/// </summary>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="objectId">Input parameter of stored procedure</param>
		/// <returns>Filled DataTable with resultset(s) of stored procedure</returns>
		public static DataTable GetAllRelatedToObject(System.Int32 branchId, System.Int32 objectId)
		{
			using(DataAccessAdapter adapter = new DataAccessAdapter()) 
			{
				return GetAllRelatedToObject(branchId, objectId,  adapter);
			}
		}


		/// <summary>
		/// Calls stored procedure 'spGetAllRelatedToObject'.<br/><br/>
		/// 
		/// </summary>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="objectId">Input parameter of stored procedure</param>
		/// <param name="adapter">The DataAccessAdapter object to use for the call</param>
		/// <returns>Filled DataTable with resultset(s) of stored procedure</returns>
		public static DataTable GetAllRelatedToObject(System.Int32 branchId, System.Int32 objectId, DataAccessAdapter adapter)
		{
			SqlParameter[] parameters = new SqlParameter[2];
			parameters[0] = new SqlParameter("@branchId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, branchId);
			parameters[1] = new SqlParameter("@objectId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, objectId);

			DataTable toReturn = new DataTable("GetAllRelatedToObject");
			bool hasSucceeded = adapter.CallRetrievalStoredProcedure("[dbDogwood].[dbo].[spGetAllRelatedToObject]", parameters, toReturn);

			return toReturn;
		}


		/// <summary>
		/// Calls stored procedure 'spGetAllRelatedToObject'. This version also returns the return value of the stored procedure.<br/><br/>
		/// 
		/// </summary>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="objectId">Input parameter of stored procedure</param>
		/// <param name="returnValue">Return value of the stored procedure</param>
		/// <returns>Filled DataTable with resultset(s) of stored procedure</returns>
		public static DataTable GetAllRelatedToObject(System.Int32 branchId, System.Int32 objectId, ref System.Int32 returnValue)
		{
			using(DataAccessAdapter adapter = new DataAccessAdapter()) 
			{
				return GetAllRelatedToObject(branchId, objectId, ref returnValue, adapter);
			}
		}
	
	
		/// <summary>
		/// Calls stored procedure 'spGetAllRelatedToObject'. This version also returns the return value of the stored procedure.<br/><br/>
		/// 
		/// </summary>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="objectId">Input parameter of stored procedure</param>
		/// <param name="returnValue">Return value of the stored procedure</param>
		/// <param name="adapter">The DataAccessAdapter object to use for the call</param>
		/// <returns>Filled DataTable with resultset(s) of stored procedure</returns>
		public static DataTable GetAllRelatedToObject(System.Int32 branchId, System.Int32 objectId, ref System.Int32 returnValue, DataAccessAdapter adapter)
		{
			// create parameters. Add 1 to make room for the return value parameter.
			SqlParameter[] parameters = new SqlParameter[2 + 1];
			parameters[0] = new SqlParameter("@branchId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, branchId);
			parameters[1] = new SqlParameter("@objectId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, objectId);

			parameters[2] = new SqlParameter("RETURNVALUE", SqlDbType.Int, 0, ParameterDirection.ReturnValue, true, 10, 0, "",  DataRowVersion.Current, returnValue);
			DataTable toReturn = new DataTable("GetAllRelatedToObject");
			bool hasSucceeded = adapter.CallRetrievalStoredProcedure("[dbDogwood].[dbo].[spGetAllRelatedToObject]", parameters, toReturn);


			returnValue = (int)parameters[2].Value;
			return toReturn;
		}

		/// <summary>Creates an IRetrievalQuery object for a call to the procedure 'spGetAllRelatedToObject'.
		/// 
		/// </summary>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="objectId">Input parameter of stored procedure</param>
		/// <returns>IRetrievalQuery object which is ready to use for datafetching</returns>
		public static IRetrievalQuery GetGetAllRelatedToObjectCallAsQuery( System.Int32 branchId, System.Int32 objectId)
		{
			RetrievalQuery toReturn = new RetrievalQuery( new SqlCommand("[dbDogwood].[dbo].[spGetAllRelatedToObject]" ) );
			toReturn.Parameters.Add(new SqlParameter("@branchId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, branchId));
			toReturn.Parameters.Add(new SqlParameter("@objectId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, objectId));

			toReturn.Command.CommandType = CommandType.StoredProcedure;
			return toReturn;
		}
	

		/// <summary>
		/// Calls stored procedure 'spGetObjectHistory'.<br/><br/>
		/// 
		/// </summary>
		/// <param name="objectId">Input parameter of stored procedure</param>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <returns>Filled DataTable with resultset(s) of stored procedure</returns>
		public static DataTable GetObjectHistory(System.Int32 objectId, System.Int32 branchId)
		{
			using(DataAccessAdapter adapter = new DataAccessAdapter()) 
			{
				return GetObjectHistory(objectId, branchId,  adapter);
			}
		}


		/// <summary>
		/// Calls stored procedure 'spGetObjectHistory'.<br/><br/>
		/// 
		/// </summary>
		/// <param name="objectId">Input parameter of stored procedure</param>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="adapter">The DataAccessAdapter object to use for the call</param>
		/// <returns>Filled DataTable with resultset(s) of stored procedure</returns>
		public static DataTable GetObjectHistory(System.Int32 objectId, System.Int32 branchId, DataAccessAdapter adapter)
		{
			SqlParameter[] parameters = new SqlParameter[2];
			parameters[0] = new SqlParameter("@objectId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, objectId);
			parameters[1] = new SqlParameter("@branchId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, branchId);

			DataTable toReturn = new DataTable("GetObjectHistory");
			bool hasSucceeded = adapter.CallRetrievalStoredProcedure("[dbDogwood].[dbo].[spGetObjectHistory]", parameters, toReturn);

			return toReturn;
		}


		/// <summary>
		/// Calls stored procedure 'spGetObjectHistory'. This version also returns the return value of the stored procedure.<br/><br/>
		/// 
		/// </summary>
		/// <param name="objectId">Input parameter of stored procedure</param>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="returnValue">Return value of the stored procedure</param>
		/// <returns>Filled DataTable with resultset(s) of stored procedure</returns>
		public static DataTable GetObjectHistory(System.Int32 objectId, System.Int32 branchId, ref System.Int32 returnValue)
		{
			using(DataAccessAdapter adapter = new DataAccessAdapter()) 
			{
				return GetObjectHistory(objectId, branchId, ref returnValue, adapter);
			}
		}
	
	
		/// <summary>
		/// Calls stored procedure 'spGetObjectHistory'. This version also returns the return value of the stored procedure.<br/><br/>
		/// 
		/// </summary>
		/// <param name="objectId">Input parameter of stored procedure</param>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <param name="returnValue">Return value of the stored procedure</param>
		/// <param name="adapter">The DataAccessAdapter object to use for the call</param>
		/// <returns>Filled DataTable with resultset(s) of stored procedure</returns>
		public static DataTable GetObjectHistory(System.Int32 objectId, System.Int32 branchId, ref System.Int32 returnValue, DataAccessAdapter adapter)
		{
			// create parameters. Add 1 to make room for the return value parameter.
			SqlParameter[] parameters = new SqlParameter[2 + 1];
			parameters[0] = new SqlParameter("@objectId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, objectId);
			parameters[1] = new SqlParameter("@branchId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, branchId);

			parameters[2] = new SqlParameter("RETURNVALUE", SqlDbType.Int, 0, ParameterDirection.ReturnValue, true, 10, 0, "",  DataRowVersion.Current, returnValue);
			DataTable toReturn = new DataTable("GetObjectHistory");
			bool hasSucceeded = adapter.CallRetrievalStoredProcedure("[dbDogwood].[dbo].[spGetObjectHistory]", parameters, toReturn);


			returnValue = (int)parameters[2].Value;
			return toReturn;
		}

		/// <summary>Creates an IRetrievalQuery object for a call to the procedure 'spGetObjectHistory'.
		/// 
		/// </summary>
		/// <param name="objectId">Input parameter of stored procedure</param>
		/// <param name="branchId">Input parameter of stored procedure</param>
		/// <returns>IRetrievalQuery object which is ready to use for datafetching</returns>
		public static IRetrievalQuery GetGetObjectHistoryCallAsQuery( System.Int32 objectId, System.Int32 branchId)
		{
			RetrievalQuery toReturn = new RetrievalQuery( new SqlCommand("[dbDogwood].[dbo].[spGetObjectHistory]" ) );
			toReturn.Parameters.Add(new SqlParameter("@objectId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, objectId));
			toReturn.Parameters.Add(new SqlParameter("@branchId", SqlDbType.Int, 0, ParameterDirection.Input, true, 10, 0, "",  DataRowVersion.Current, branchId));

			toReturn.Command.CommandType = CommandType.StoredProcedure;
			return toReturn;
		}
	

		#region Included Code

		#endregion
	}
}
