﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: Wednesday, April 25, 2012 12:26:07 PM
// Code is generated using templates: SD.TemplateBindings.SqlServerSpecific.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Dogwood.Data.DatabaseSpecific
{
	/// <summary>
	/// Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.
	/// </summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal sealed class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion
		
		/// <summary>private ctor to prevent instances of this class.</summary>
		private PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			base.InitClass((12 + 2));
			InitAttributeEntityMappings();
			InitAttributeMetadataEntityMappings();
			InitAttributeValueEntityMappings();
			InitBranchHeadObjectEntityMappings();
			InitClassEntityMappings();
			InitDataTypeEntityMappings();
			InitObjectEntityMappings();
			InitObjectRevisionEntityMappings();
			InitRelationAttributeValueEntityMappings();
			InitRelationshipMetadataEntityMappings();
			InitRevisionEntityMappings();
			InitRevisionHeadObjectEntityMappings();
			InitBranchesTypedViewMappings();
			InitIndexedRevisionsTypedViewMappings();
		}


		/// <summary>Inits AttributeEntity's mappings</summary>
		private void InitAttributeEntityMappings()
		{
			base.AddElementMapping( "AttributeEntity", "dbDogwood", @"dbo", "tblAttribute", 2 );
			base.AddElementFieldMapping( "AttributeEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "AttributeEntity", "Value", "Value", false, (int)SqlDbType.VarChar, 50, 0, 0, false, "", null, typeof(System.String), 1 );
		}
		/// <summary>Inits AttributeMetadataEntity's mappings</summary>
		private void InitAttributeMetadataEntityMappings()
		{
			base.AddElementMapping( "AttributeMetadataEntity", "dbDogwood", @"dbo", "tblAttributeMetadata", 6 );
			base.AddElementFieldMapping( "AttributeMetadataEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "AttributeMetadataEntity", "ClassId", "ClassId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 1 );
			base.AddElementFieldMapping( "AttributeMetadataEntity", "DataTypeId", "DataTypeId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 2 );
			base.AddElementFieldMapping( "AttributeMetadataEntity", "AttributeId", "AttributeId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 3 );
			base.AddElementFieldMapping( "AttributeMetadataEntity", "IsNullable", "IsNullable", false, (int)SqlDbType.Bit, 0, 0, 0, false, "", null, typeof(System.Boolean), 4 );
			base.AddElementFieldMapping( "AttributeMetadataEntity", "IsImmutable", "IsImmutable", false, (int)SqlDbType.Bit, 0, 0, 0, false, "", null, typeof(System.Boolean), 5 );
		}
		/// <summary>Inits AttributeValueEntity's mappings</summary>
		private void InitAttributeValueEntityMappings()
		{
			base.AddElementMapping( "AttributeValueEntity", "dbDogwood", @"dbo", "tblAttributeValue", 4 );
			base.AddElementFieldMapping( "AttributeValueEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "AttributeValueEntity", "ObjectRevisionId", "ObjectRevisionId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 1 );
			base.AddElementFieldMapping( "AttributeValueEntity", "AttributeMetadataId", "AttributeMetadataId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 2 );
			base.AddElementFieldMapping( "AttributeValueEntity", "Value", "Value", false, (int)SqlDbType.VarBinary, 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 3 );
		}
		/// <summary>Inits BranchHeadObjectEntity's mappings</summary>
		private void InitBranchHeadObjectEntityMappings()
		{
			base.AddElementMapping( "BranchHeadObjectEntity", "dbDogwood", @"dbo", "tblBranchHeadObject", 6 );
			base.AddElementFieldMapping( "BranchHeadObjectEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "BranchHeadObjectEntity", "BranchId", "BranchId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 1 );
			base.AddElementFieldMapping( "BranchHeadObjectEntity", "ClassId", "ClassId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 2 );
			base.AddElementFieldMapping( "BranchHeadObjectEntity", "ObjectId", "ObjectId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 3 );
			base.AddElementFieldMapping( "BranchHeadObjectEntity", "ObjectRevisionId", "ObjectRevisionId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 4 );
			base.AddElementFieldMapping( "BranchHeadObjectEntity", "RevisionId", "RevisionId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 5 );
		}
		/// <summary>Inits ClassEntity's mappings</summary>
		private void InitClassEntityMappings()
		{
			base.AddElementMapping( "ClassEntity", "dbDogwood", @"dbo", "tblClass", 2 );
			base.AddElementFieldMapping( "ClassEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "ClassEntity", "Name", "Name", false, (int)SqlDbType.VarChar, 50, 0, 0, false, "", null, typeof(System.String), 1 );
		}
		/// <summary>Inits DataTypeEntity's mappings</summary>
		private void InitDataTypeEntityMappings()
		{
			base.AddElementMapping( "DataTypeEntity", "dbDogwood", @"dbo", "tblDataType", 2 );
			base.AddElementFieldMapping( "DataTypeEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "DataTypeEntity", "Value", "Value", false, (int)SqlDbType.VarChar, 50, 0, 0, false, "", null, typeof(System.String), 1 );
		}
		/// <summary>Inits ObjectEntity's mappings</summary>
		private void InitObjectEntityMappings()
		{
			base.AddElementMapping( "ObjectEntity", "dbDogwood", @"dbo", "tblObject", 2 );
			base.AddElementFieldMapping( "ObjectEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "ObjectEntity", "ClassId", "ClassId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 1 );
		}
		/// <summary>Inits ObjectRevisionEntity's mappings</summary>
		private void InitObjectRevisionEntityMappings()
		{
			base.AddElementMapping( "ObjectRevisionEntity", "dbDogwood", @"dbo", "tblObjectRevision", 4 );
			base.AddElementFieldMapping( "ObjectRevisionEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "ObjectRevisionEntity", "ObjectId", "ObjectId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 1 );
			base.AddElementFieldMapping( "ObjectRevisionEntity", "RevisionId", "RevisionId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 2 );
			base.AddElementFieldMapping( "ObjectRevisionEntity", "IsDeleted", "IsDeleted", false, (int)SqlDbType.Bit, 0, 0, 0, false, "", null, typeof(System.Boolean), 3 );
		}
		/// <summary>Inits RelationAttributeValueEntity's mappings</summary>
		private void InitRelationAttributeValueEntityMappings()
		{
			base.AddElementMapping( "RelationAttributeValueEntity", "dbDogwood", @"dbo", "tblRelationAttributeValue", 4 );
			base.AddElementFieldMapping( "RelationAttributeValueEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "RelationAttributeValueEntity", "ObjectRevisionId", "ObjectRevisionId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 1 );
			base.AddElementFieldMapping( "RelationAttributeValueEntity", "RelationshipMetadataId", "RelationshipMetadataId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 2 );
			base.AddElementFieldMapping( "RelationAttributeValueEntity", "Value", "Value", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 3 );
		}
		/// <summary>Inits RelationshipMetadataEntity's mappings</summary>
		private void InitRelationshipMetadataEntityMappings()
		{
			base.AddElementMapping( "RelationshipMetadataEntity", "dbDogwood", @"dbo", "tblRelationshipMetadata", 9 );
			base.AddElementFieldMapping( "RelationshipMetadataEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "RelationshipMetadataEntity", "PkClassId", "PkClassId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 1 );
			base.AddElementFieldMapping( "RelationshipMetadataEntity", "FkClassId", "FkClassId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 2 );
			base.AddElementFieldMapping( "RelationshipMetadataEntity", "AttributeId", "AttributeId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 3 );
			base.AddElementFieldMapping( "RelationshipMetadataEntity", "IsNullable", "IsNullable", false, (int)SqlDbType.Bit, 0, 0, 0, false, "", null, typeof(System.Boolean), 4 );
			base.AddElementFieldMapping( "RelationshipMetadataEntity", "OneToManyValue", "OneToManyValue", true, (int)SqlDbType.VarChar, 50, 0, 0, false, "", null, typeof(System.String), 5 );
			base.AddElementFieldMapping( "RelationshipMetadataEntity", "CascadeBehavior", "CascadeBehavior", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 6 );
			base.AddElementFieldMapping( "RelationshipMetadataEntity", "CascadeDefaultValue", "CascadeDefaultValue", true, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 7 );
			base.AddElementFieldMapping( "RelationshipMetadataEntity", "IsImmutable", "IsImmutable", false, (int)SqlDbType.Bit, 0, 0, 0, false, "", null, typeof(System.Boolean), 8 );
		}
		/// <summary>Inits RevisionEntity's mappings</summary>
		private void InitRevisionEntityMappings()
		{
			base.AddElementMapping( "RevisionEntity", "dbDogwood", @"dbo", "tblRevision", 6 );
			base.AddElementFieldMapping( "RevisionEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "RevisionEntity", "BranchId", "BranchId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 1 );
			base.AddElementFieldMapping( "RevisionEntity", "Tag", "Tag", true, (int)SqlDbType.VarChar, 50, 0, 0, false, "", null, typeof(System.String), 2 );
			base.AddElementFieldMapping( "RevisionEntity", "TimeStamp", "TimeStamp", false, (int)SqlDbType.DateTime, 0, 0, 0, false, "", null, typeof(System.DateTime), 3 );
			base.AddElementFieldMapping( "RevisionEntity", "CreatedBy", "CreatedBy", true, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 4 );
			base.AddElementFieldMapping( "RevisionEntity", "Comments", "Comments", true, (int)SqlDbType.VarChar, 255, 0, 0, false, "", null, typeof(System.String), 5 );
		}
		/// <summary>Inits RevisionHeadObjectEntity's mappings</summary>
		private void InitRevisionHeadObjectEntityMappings()
		{
			base.AddElementMapping( "RevisionHeadObjectEntity", "dbDogwood", @"dbo", "tblRevisionHeadObject", 6 );
			base.AddElementFieldMapping( "RevisionHeadObjectEntity", "Id", "Id", false, (int)SqlDbType.Int, 0, 0, 10, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "RevisionHeadObjectEntity", "BranchId", "BranchId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 1 );
			base.AddElementFieldMapping( "RevisionHeadObjectEntity", "ClassId", "ClassId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 2 );
			base.AddElementFieldMapping( "RevisionHeadObjectEntity", "ObjectId", "ObjectId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 3 );
			base.AddElementFieldMapping( "RevisionHeadObjectEntity", "ObjectRevisionId", "ObjectRevisionId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 4 );
			base.AddElementFieldMapping( "RevisionHeadObjectEntity", "RevisionId", "RevisionId", false, (int)SqlDbType.Int, 0, 0, 10, false, "", null, typeof(System.Int32), 5 );
		}

		/// <summary>Inits View's mappings</summary>
		private void InitBranchesTypedViewMappings()
		{
			base.AddElementMapping( "BranchesTypedView", "dbDogwood", @"dbo", "vwBranches", 1 );
			base.AddElementFieldMapping( "BranchesTypedView", "BranchId", "BranchId", false, (int)SqlDbType.Int, 0, 0, 10,false, string.Empty, null, typeof(System.Int32), 0 );
		}
		/// <summary>Inits View's mappings</summary>
		private void InitIndexedRevisionsTypedViewMappings()
		{
			base.AddElementMapping( "IndexedRevisionsTypedView", "dbDogwood", @"dbo", "vwIndexedRevisions", 2 );
			base.AddElementFieldMapping( "IndexedRevisionsTypedView", "RevisionId", "RevisionId", false, (int)SqlDbType.Int, 0, 0, 10,false, string.Empty, null, typeof(System.Int32), 0 );
			base.AddElementFieldMapping( "IndexedRevisionsTypedView", "BranchId", "BranchId", false, (int)SqlDbType.Int, 0, 0, 10,false, string.Empty, null, typeof(System.Int32), 1 );
		}
	}
}













